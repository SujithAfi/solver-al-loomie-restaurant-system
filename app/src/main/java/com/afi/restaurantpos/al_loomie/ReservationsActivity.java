package com.afi.restaurantpos.al_loomie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Fragments.ReservationDetailsFragment;
import com.afi.restaurantpos.al_loomie.Models.CancelKotOrBill;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by afi on 10/21/2015.
 * Activity to display the pending Reservation
 */
public class ReservationsActivity extends AppCompatActivity implements ReservationDetailsFragment.OnHeadlineSelectedListener{

    private RecyclerView pendingBillView;
    private Toolbar toolbar;
    private TextView txtNoPendingBills;

    private boolean clickLock = false;
    private ReservationsAdapter resvAdapter;
    List<Reservation> reservations;
    private Calendar calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.initKitkatStatusbarTransparancy(this);
        setContentView(R.layout.activity_reservations);
        Utilities.initURL(this);
        pendingBillView = (RecyclerView) findViewById(R.id.rvpendingBill);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtNoPendingBills = (TextView) findViewById(R.id.txtNoPendingBills);
        toolbar.setTitle("Reservations");
        pendingBillView.setHasFixedSize(true);
        pendingBillView.setLayoutManager(new GridLayoutManager(this, 2));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        calendar = Calendar.getInstance();
        if(ApplicationSingleton.getInstance().getCurrentDate() != null)
            calendar.setTime(ApplicationSingleton.getInstance().getCurrentDate());


    }



    /**
     * Load the pending Reservation
     */
    private void loadPendingBills() {

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Loading Pending Reservation's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        dialog.show();


        String reservationDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + " 00:00:00.000";
//        String reservationDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) + " 00:00:00.000";

        Utilities.getRetrofitWebService(getApplicationContext()).getReservation(reservationDate).enqueue(new Callback<List<Reservation>>() {
            @Override
            public void onResponse(Response<List<Reservation>> response, Retrofit retrofit) {
                try {
                    reservations = response.body();
                    dialog.cancel();

                    resvAdapter = new ReservationsAdapter(reservations);
                    pendingBillView.setAdapter(resvAdapter);
                    if ( reservations != null)
                        showResult(reservations.isEmpty());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.cancel();
                showResult(true);
            }
        });


    }

    @Override
    public void onArticleSelected() {

        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else
            loadPendingBills();
    }

    private class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {


        private List<Reservation> reservationsTable = new ArrayList<>();

        public ReservationsAdapter(List<Reservation> reservations) {

            for (Reservation reservation : reservations) {

                    reservationsTable.add(reservation);

            }

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_list_content, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            if(reservationsTable.get(position).getReservationTimeFrom() != null) {

                List<String> items = Arrays.asList(reservationsTable.get(position).getReservationTimeFrom().split("\\s+"));

                if(items.size() > 0) {

                    holder.tvTime.setText(items.get(1) + " " + items.get(2));

                }

            }

            if(reservationsTable.get(position).getResDate() != null) {
                List<String> items = Arrays.asList(reservationsTable.get(position).getResDate().split("\\s+"));

                if(items.size() > 0){

                    holder.tvDate.setText(items.get(0));

                }
            }
            if(reservationsTable.get(position).getDocNumber() != null)
                holder.tvResNo.setText(reservationsTable.get(position).getDocNumber().replace("##" , ""));
            if(reservationsTable.get(position).getCustomer() != null){

                if(reservationsTable.get(position).getCustomer().getName() != null)
                    holder.tvCustName.setText(reservationsTable.get(position).getCustomer().getName());
            }

            if(reservationsTable.get(position).getGuestCount() != null)
                holder.tvGuest.setText(reservationsTable.get(position).getGuestCount());
            if(reservationsTable.get(position).getTableName() != null){
                holder.trTableNo.setVisibility(View.VISIBLE);
                holder.tvTable.setText(reservationsTable.get(position).getTableName());
            }
            else
                holder.trTableNo.setVisibility(View.GONE);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ReservationDetailsFragment reservationFragment = new ReservationDetailsFragment();
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("reservation", reservationsTable.get(position));
                    arguments.putBoolean("ResvAct", true);
                    reservationFragment.setArguments(arguments);
                    Utilities.replaceFragmentWithAnimation1(reservationFragment, "Reservation Details", getSupportFragmentManager() , R.id.reservation_container);

                }
            });


            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new AlertDialog.Builder(ReservationsActivity.this)
                            .setTitle("Info")
                            .setMessage("Cancel Reservation")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    try {
                                        onCancel(reservationsTable.get(position).getDocNumber() , reservationsTable.get(position).getResDate() , position);
                                        reservationsTable.remove(position);
                                        notifyDataSetChanged();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            })
                            .setNegativeButton("cancel" , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create().show();




                }
            });
        }

        @Override
        public int getItemCount() {
            return reservationsTable.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView tvResNo;
            private final TextView tvDate;
            private final TextView tvGuest;
            private final ImageView ivDelete;
            private final TextView tvCustName;
            private final TableRow trTableNo;
            private final TextView tvTable;
            private TextView tvTime;
            public ViewHolder(View itemView) {
                super(itemView);

                tvResNo = (TextView) itemView.findViewById(R.id.tv_reservation_no);
                tvTime = (TextView)  itemView.findViewById(R.id.tv_time);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvGuest = (TextView) itemView.findViewById(R.id.tv_guest);
                ivDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
                tvCustName = (TextView) itemView.findViewById(R.id.tv_cutname);
                trTableNo = (TableRow) itemView.findViewById(R.id.tr_tableno);
                tvTable = (TextView) itemView.findViewById(R.id.tv_table);
            }
        }

    }

    public void onCancel(String docNo , String resDate , final int pos){

        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();
        cancelKotOrBill.id = docNo ;
        cancelKotOrBill.date = resDate;
        cancelKotOrBill.reson = "";

        Utilities.getRetrofitWebService(ReservationsActivity.this).cancelReservation(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {

                if (response != null) {
                    if ((Boolean) response.body().get("response_code")) {
                        new AlertDialog.Builder(ReservationsActivity.this)
                                .setTitle(R.string.success)
                                .setMessage("Reservation Cancelled")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        reservations.remove(pos);
                                        ReservationsActivity.this.sendBroadcast(new Intent("reloadBillAndKot"));

                                    }
                                })
                                .create().show();
                    } else {
                        new AlertDialog.Builder(ReservationsActivity.this)
                                .setTitle(R.string.error)
                                .setMessage("Canot delete")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .create().show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();


        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else
            loadPendingBills();

        this.registerReceiver(refreshActionReciever, new IntentFilter("refreshTableOptionsLists"));
        if(reservations != null)
            txtNoPendingBills.setVisibility(reservations.size() == 0 ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onPause() {
        super.onPause();
        this.unregisterReceiver(refreshActionReciever);
    }

    private BroadcastReceiver refreshActionReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ReservationsAdapter tableKotOptionsListAdapter = (ReservationsAdapter) pendingBillView.getAdapter();
            tableKotOptionsListAdapter.notifyDataSetChanged();
            txtNoPendingBills.setVisibility(reservations.size() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private void showResult( boolean isListEmpty ) {
        if(isListEmpty) {
            pendingBillView.setVisibility(View.INVISIBLE);
            txtNoPendingBills.setVisibility(View.VISIBLE);

        }
        else {
            pendingBillView.setVisibility(View.VISIBLE);
            txtNoPendingBills.setVisibility(View.INVISIBLE);
            pendingBillView.setBackgroundColor(getResources().getColor(R.color.cardview_outer));
        }
    }

    private Handler clickHandler = new Handler();

    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };


}
