package com.afi.restaurantpos.al_loomie.Dialogs;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.TableDetailsFragmentPagerAdapter;
import com.afi.restaurantpos.al_loomie.LoginActivity;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TableActionDetailsFragment extends DialogFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Table table;
    private List<Reservation> reservations;
    private TextView tableNo;
    private String selectedDate;
    private static final String ARG_PARAM_TABLE_ID = "param1";
    private String kotnumber;
    public static DialogFragment dialog;
    private Button btnNewReservation;
    private Button btnNewKot;
    private List<Table> tables;
    private SharedPreferences sharedPreferences;
    private List<Reservation> reservationsTable = new ArrayList<>();


    public TableActionDetailsFragment() {
        // Required empty public constructor
    }


    public static TableActionDetailsFragment newInstance(Table table, List<Reservation> reservations , List<Table> tables) {
        TableActionDetailsFragment fragment = new TableActionDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TABLE_ID, table.Tab_Cd);
        fragment.setArguments(args);
        fragment.table = table;
        fragment.reservations = reservations;
        fragment.tables = tables;
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = this;

        if (getArguments() != null) {
            kotnumber = getArguments().getString(ARG_PARAM_TABLE_ID);
        }

        if(reservations != null && table != null){
            for (Reservation reservation : reservations) {
                if (reservation.getTable_Cd().equals(table.getTab_Cd())) {
                    reservationsTable.add(reservation);
                }
            }

        }



//        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(calendar.getTime());


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.table_action_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewPager = (ViewPager) getView().findViewById(R.id.ViewPager);
        tabLayout = (TabLayout) getView().findViewById(R.id.tabs);
        tableNo = (TextView) getView().findViewById(R.id.tv_table);
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbarContainer);
        btnNewReservation = (Button) getView().findViewById(R.id.btnNewReservation);
        btnNewKot = (Button) getView().findViewById(R.id.btnNewkot);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TableActionDetailsFragment.this.dismiss();
            }
        });

        if(table != null){

            if(table.getName() != null)
                tableNo.setText("Table: " + table.getCaption());
        }


        if(ApplicationSingleton.getInstance().getCurrentDate() != null)
             selectedDate =  new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate());
        final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar1 = Calendar.getInstance();

        String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

        if(shiftDateString != null && !shiftDateString.matches("")){

//            Log.e("Inside Shift date Shing =====>" , "Not Null");
//            Log.e("Date Inside Not Null====>>" , shiftDateString);

            try {
                calendar1.setTime(dateFormat1.parse(shiftDateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            Log.e("Inside Shift date Shing =====>" , " Null");
        }

        final String shiftDate = new SimpleDateFormat("dd-MM-yyyy").format(calendar1.getTime());


        getView().findViewById(R.id.btnNewkot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                    if(selectedDate.equalsIgnoreCase(shiftDate)) {
    //                    ((OrderDetails) getActivity()).onTableSelected(table);
                        getParamDate();
                    }
                    else {
                        dismiss();
                        /*Log.e("Current Date===>>" , selectedDate);
                        Log.e("Shift Date===>>" , shiftDate);*/

                        Toast.makeText(getActivity(), "Select current date for create KOT", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if(!Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_ADDORSAVE, false)){

            try {
                btnNewKot.setVisibility(View.GONE);
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
                btnNewReservation.setLayoutParams(param);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        getView().findViewById(R.id.btnNewReservation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                ((OrderDetails) getActivity()).onReservationSelected(table);

            }
        });



        if(!Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_ADDORSAVE, false)){

//            Log.e("Inside====>>" , "Reservation FAlse");
            try {
                btnNewReservation.setVisibility(View.GONE);
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
                btnNewKot.setLayoutParams(param);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        loadKotAndBills();
    }

    public void getParamDate(){

        sharedPreferences = Utilities.getSharedPreferences(getActivity().getApplicationContext());


        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {


            try {

                Utilities.getRetrofitWebService(getActivity()).getParamDate(sharedPreferences.getString(Constants.COUNTER_NAME , "")).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Response<String> response, Retrofit retrofit) {


                        if (response.body() != null){

                            Log.e("Param Date====>>" , response.body());

                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                            if(response.body() != null && !response.body().matches("")){

                                try {
                                    calendar.setTime(dateFormat.parse(response.body()));
//                                    calendar.add(Calendar.DATE , 1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            String paramDate =  new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime());
//                            final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                            Calendar calendar1 = Calendar.getInstance();

                            String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

                            if(shiftDateString != null && !shiftDateString.matches("")){

                                try {
                                    calendar1.setTime(dateFormat1.parse(shiftDateString));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                            final String shiftDate = new SimpleDateFormat("dd-MM-yyyy").format(calendar1.getTime());


                            if(paramDate.equalsIgnoreCase(shiftDate)) {

                                if(getActivity() != null) {

                                    if(reservationsTable != null && reservationsTable.size() > 0){

                                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                                .setTitle("Reservation exists, do you want to make direct KOT.")
                                                .setCancelable(false)
                                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        ((OrderDetails) getActivity()).onTableSelected(table);
                                                        dismiss();

                                                    }
                                                })
                                                .setNegativeButton("No" , null)
                                                .create();
                                        alertDialog.show();


                                    }
                                    else {
                                        ((OrderDetails) getActivity()).onTableSelected(table);
                                        dismiss();
                                    }


                                }
                            }
                            else{

                                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                        .setTitle("Can't make KOT, Day closed.")
                                        .setCancelable(false)
                                        .setMessage("The application will be closed.")
                                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                getActivity().finish();
                                                System.exit(0);

                                            }
                                        })
                                        .create();
                                alertDialog.show();
                            }



                        }

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    public void loadKotAndBills() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ApplicationSingleton.getInstance().getCurrentDate());

        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        Log.e("Date===>>" , date);

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            Utilities.getRetrofitWebService(getContext()).getKotAndBillsInTable(kotnumber , date).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
                @Override
                public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                    try {
                        if (response.body() != null && TableActionDetailsFragment.this.isVisible()) {
                            Map<String, List<PendingKotItem>> data = response.body();

                            TableDetailsFragmentPagerAdapter tableOptionsAdapter = (TableDetailsFragmentPagerAdapter) viewPager.getAdapter();
                            if (tableOptionsAdapter == null) {
                                viewPager.setAdapter(new TableDetailsFragmentPagerAdapter(getChildFragmentManager() , table , reservations , data , tables));
                                tabLayout.setupWithViewPager(viewPager);
                            } else {
                                tableOptionsAdapter.refreshData(data);
                            }


                            try {
                                if (data.get("kot").size() == 0 && data.get("bill").size() == 0)
                                    viewPager.setCurrentItem(0, false);
                                else if(reservationsTable != null && reservationsTable.size() > 0)
                                    viewPager.setCurrentItem(0, false);
                                else if (data.get("kot").size() > 0 && data.get("bill").size() == 0)
                                    viewPager.setCurrentItem(1, false);
                                else if (data.get("kot").size() == 0 && data.get("bill").size() > 0)
                                    viewPager.setCurrentItem(2, false);
                                else if (data.get("kot").size() > 0)
                                    viewPager.setCurrentItem(1, false);
                                else if (data.get("bill").size() > 0)
                                    viewPager.setCurrentItem(2, false);
                                else
                                    viewPager.setCurrentItem(0, false);
                            } catch (Exception e) {

                            }

                            getActivity().sendBroadcast(new Intent("refreshTableOptionsLists"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent("refreshTable");
        getActivity().sendBroadcast(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(reloadKotAndBillReceiver, new IntentFilter("reloadBillAndKot"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(reloadKotAndBillReceiver);
    }

    private BroadcastReceiver reloadKotAndBillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadKotAndBills();
        }
    };


}
