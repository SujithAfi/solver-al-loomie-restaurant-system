package com.afi.restaurantpos.al_loomie.Fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

public class UserDetailsFragment extends Fragment {

    private TextView txtCounter;
    private TextView txtUser;
    private TextView txtShiftId;
    private TextView txtKotNumber;
    private TextView txtShiftdate;
    private TextView txtServedBy;
    private TextView txtCustomerName;
    private TextView txtCustomerPhoneNumber;

    public static UserDetailsFragment newInstance() {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    public UserDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_details, container, false);
        txtCounter = (TextView)view.findViewById(R.id.txtCOunter);
        txtUser = (TextView)view.findViewById(R.id.txtUser);
        txtShiftId = (TextView)view.findViewById(R.id.txtShiftId);
        txtKotNumber = (TextView)view.findViewById(R.id.txtKotNumber);
        txtShiftdate = (TextView)view.findViewById(R.id.txtShiftDate);
        txtCustomerName = (TextView)view.findViewById(R.id.txtCustomerName);
        txtCustomerPhoneNumber = (TextView)view.findViewById(R.id.txtPhoneNumber);
        txtServedBy = (TextView)view.findViewById(R.id.txtServedBy);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SharedPreferences preferences = Utilities.getSharedPreferences(getContext());
        txtKotNumber.setText(preferences.getBoolean(Constants.SHARED_PREF_KEY_NEW_KOT_LOADED , false) ? preferences.getString(Constants.SHARED_PREF_KEY_NEW_KOT , "") : "");

        txtUser.setText(preferences.getString(Constants.SHARED_PREF_KEY_USERNAME , ""));
        txtCounter.setText(preferences.getString(Constants.COUNTER_NAME , ""));
        txtShiftdate.setText(preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE , ""));
        txtShiftId.setText(preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_ID , 0) + "");

        try {
            txtCustomerName.setText(ApplicationSingleton.getInstance().getCustomerDetails().getName());
            txtCustomerPhoneNumber.setText(ApplicationSingleton.getInstance().getCustomerDetails().getId());
            txtServedBy.setText(ApplicationSingleton.getInstance().getSalesman(getContext()).Name);
        }
        catch (Exception e){
            txtServedBy.setText(Utilities.getSharedPreferences(getContext()).getString(Constants.SHARED_PREF_KEY_USERNAME , ""));
        }
    }
}
