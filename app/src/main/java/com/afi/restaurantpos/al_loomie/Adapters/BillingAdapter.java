package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.List;


/**
 * Created by afi on 10/21/2015.
 */
public class BillingAdapter extends RecyclerView.Adapter<BillingAdapter.BillAdapterVH>{


    private List<BillItem> billItems;

    private Context context = null;
    private onItemChange itemChange;

    public BillingAdapter(List<BillItem> selectedItemDetailses) {
        this.billItems = selectedItemDetailses;
    }

    public interface onItemChange{
        void onItemInvalidate(int pos , boolean ischecked);
    }

    @Override
    public BillAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_billing_fragment_list, viewGroup, false);
        if ( context == null)
            context = viewGroup.getContext();
        BillAdapterVH billAdapterVH = new BillAdapterVH(v);
        return billAdapterVH;
    }

    @Override
    public void onBindViewHolder(BillAdapterVH holder, final int position) {
        holder.txtNo.setText(String.valueOf(position+1));
        holder.txtItemName.setText(billItems.get(position).Cmd_ItmName);
        holder.txtItemQuantity.setText(Utilities.getItemQuantityFormat(billItems.get(position).Cmd_Qty));
        holder.txtItemPrice.setText(Utilities.getDefaultCurrencyFormat(billItems.get(position).Cmd_Ucost + "", holder.txtItemPrice.getContext()));
        holder.txtItemDiscount.setText(Utilities.getDefaultCurrencyFormat(billItems.get(position).Cmd_DiscAmt + "",holder.txtItemDiscount.getContext()));
        holder.txtItemTotal.setText(Utilities.getDefaultCurrencyFormat(billItems.get(position).Cmd_Amt + "",holder.txtItemTotal.getContext()));
        holder.cbIsFOC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (itemChange != null) {
                    itemChange.onItemInvalidate(position , isChecked);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        if(billItems == null)
            return  0;
        return billItems.size();
    }

    public static class BillAdapterVH extends RecyclerView.ViewHolder{
        private CheckBox cbIsFOC;
        private TextView txtNo;
        private TextView txtItemName;
        private TextView txtItemQuantity;
        private TextView txtItemPrice;
        private TextView txtItemDiscount;
        private TextView txtItemTotal;

        public BillAdapterVH(View itemView) {
            super(itemView);
            txtNo=(TextView) itemView.findViewById(R.id.txtItemNo);
            txtItemName = (TextView) itemView.findViewById(R.id.txtItemName);
            txtItemQuantity = (TextView) itemView.findViewById(R.id.txtItemQuantity);
            txtItemPrice = (TextView) itemView.findViewById(R.id.txtItemRate);
            txtItemDiscount = (TextView) itemView.findViewById(R.id.txtItemDiscount);
            txtItemTotal = (TextView) itemView.findViewById(R.id.txtItemAmount);
            cbIsFOC = (CheckBox) itemView.findViewById(R.id.cb_foc);
        }
    }

    public void setItemChange(onItemChange itemChange) {
        this.itemChange = itemChange;
    }


    public double getTotalAmount(){
        double totAmt = 0;
        for(BillItem billItem : billItems){
            totAmt += Double.parseDouble(billItem.Cmd_Amt);
        }
        return totAmt;
    }
}
