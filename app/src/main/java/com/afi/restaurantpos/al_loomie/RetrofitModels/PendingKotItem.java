package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by AFI on 10/16/2015.
 */
public class PendingKotItem {
    public String Itm_Cd;
    public String KotD_Qty;
    public String KotD_Rate;
    public String KotD_Amt;
    public String KotD_UCost;
    public String KotD_Ctr;
    public String KotD_Counter;
    public String Itm_Name;
    public String Unit_Fraction;
    public String Kot_MuUnit;
    public String KotD_Remarks;
    public String Barcode;
    public boolean isChecked;
    public String Kot_No;
    public String menu_SideDish;

    public String Cus_Name;
    public String Cus_Cd;
    public String Cm_Type;
    public String Cm_Covers;
    public String Sman_Cd;
    public String Sman_Name;
    public String Tab_Cd;
    public String CmD_Amt;
    public String Disc_Per;

    public String Tab_cd;
    public String Tab_Name;
    public String Kot_TotAmt;
    public String Kot_Covers;
    public String Cm_Counter;

    public boolean is_Subitem;
    public boolean isSelected;

    public boolean Thru_Res;
    public String Res_No;
    public boolean isFoc;



}
