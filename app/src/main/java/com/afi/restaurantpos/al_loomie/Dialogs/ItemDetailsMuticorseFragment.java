package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.McItemModifiersAdapter;
import com.afi.restaurantpos.al_loomie.Adapters.McItemsAdater;
import com.afi.restaurantpos.al_loomie.Fragments.SearchFragment;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;


public class ItemDetailsMuticorseFragment extends DialogFragment {

    private static final String ITEM_ID_KEY = "key_item_id";
    private static final String BARCODE_KEY = "key_bar_code";
    private static final String RATE_KEY = "rate_code";
    private static final String NAME_KEY = "item_name";
    public boolean clickLock = false;
    int lastPOsition = -1;
    List<ItemCosts> itemCostses = new ArrayList<>();
    List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
    boolean firstrun = true;
    private boolean isItemDetailsLoading = false;
    private String itemId;
    private String barCode;
    private ViewGroup mContainer;
    private View mLayout;
    private Toolbar toolbar;
    private RecyclerView rVModifiers;
    private RecyclerView rVModifiersList;
    private TextView txtRate;
    private TextView txtExraRate;
    private TextView txtTotalCost;
    private EditText edtTxtQuantity;
    private Button btnPlus;
    private Button btnMinus;
    private SearchFragment searchFragment;
    private ItemDdetailsListCallBack callBack;
    private RecyclerView recyclerViewMuticorseItems;
    private Handler uiHandler = new Handler();
    private TextWatcher textWatcher = new TextWatcher() {
        private CharSequence prevSequence;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            this.prevSequence = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            calculateCostForItem1(Long.parseLong(edtTxtQuantity.getText().toString()));

        }
    };
    private int selectedMainItemPos = 0;
    private List<Item> mainItemList;
    private List<Item> subitems;
    private boolean loadMapFlag = false;
    private AlertDialog d;
    private String rate = "0";
    private TextView TxtGtotal;
    private String itemName;
    private TextView tvAllergicInfo;
    private LinearLayout llAllergicInfo;
    private FloatingActionButton fabCustomerDeatailsDown;

    public ItemDetailsMuticorseFragment() {
    }

    public static ItemDetailsMuticorseFragment newInstance(String itemId, String Barcode, String rate , String name , SearchFragment searchFragment) {
        ItemDetailsMuticorseFragment fragment = new ItemDetailsMuticorseFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_ID_KEY, itemId);
        args.putString(BARCODE_KEY, Barcode);
        args.putString(RATE_KEY, rate);
        args.putString(NAME_KEY, name);
        fragment.setArguments(args);
        fragment.searchFragment = searchFragment;
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
         d = (AlertDialog) getDialog();
        if (d != null) {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (edtTxtQuantity.getText().toString().length() == 0) {
                        Utilities.showToast("Quantity canot empty", getContext());
                        edtTxtQuantity.setError("Enter quantity");
                    } else {
                        if(mainItemList != null){
                        if (callBack != null) {


                            if (mainItemList.size() > 0) {


                                for (Item item1 : mainItemList) {

                                    SelectedItemDetails details = new SelectedItemDetails();
                                    details.itemName = item1.Item_Name;
                                    details.barcode = item1.ItemCd;
                                    details.itemCode = item1.ItemCd;
                                    details.Unit_Fraction = "1";
                                    details.Itm_UCost = item1.Rate;
                                    details.itemUnitCost = Double.parseDouble(item1.Rate);
                                    details.itemExtraRate = 0;
                                    details.itemTotalCost = 0;

                                    try {
                                        details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                    } catch (Exception e) {
                                        details.itemQuantity = 1;
                                    }
                                    details.modify = new ArrayList<>();
                                    details.Cm_Covers = "1";
                                    details.isSubitem = true;
                                    details.isMultiC = true;
                                    selectedItemDetailses.add(details);
                                }

                                SelectedItemDetails details = new SelectedItemDetails();
                                details.itemName = itemName;
                                details.barcode = barCode;
                                details.itemCode = itemId;
                                details.Unit_Fraction = "1";
                                details.Itm_UCost = rate;
                                details.itemUnitCost = Double.parseDouble(rate);
                                details.itemExtraRate = 0;
                                details.itemTotalCost = Double.parseDouble(TxtGtotal.getText().toString());

                                try {
                                    details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                } catch (Exception e) {
                                    details.itemQuantity = 1;
                                }
                                details.modify = new ArrayList<>();
                                details.Cm_Covers = "1";
                                details.isSubitem = false;
                                details.isMultiC = true;

                                selectedItemDetailses.add(0, details);


                            }


                            callBack.onItemSelected(selectedItemDetailses, false);
                            Snackbar
                                    .make(mLayout, getResources().getString(R.string.item_saved), Snackbar.LENGTH_LONG)
                                    .show();

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    d.dismiss();
                                }
                            }, 500);


                        }
                    }
                        else{
                            Utilities.showToast("Items Should not be empty.", getContext());

                        }
                    }
                }
            });
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadMapFlag = true;

        if (getArguments() != null) {
            itemId = getArguments().getString(ITEM_ID_KEY);
            barCode = getArguments().getString(BARCODE_KEY);
            rate = getArguments().getString(RATE_KEY);
            itemName = getArguments().getString(NAME_KEY);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_item_details_multicorse, container, false);

        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        rVModifiers = (RecyclerView) mLayout.findViewById(R.id.rv_modifiers);
        rVModifiersList = (RecyclerView) mLayout.findViewById(R.id.rv_modifiersList);
        txtRate = (TextView) mLayout.findViewById(R.id.txtRate);
        txtExraRate = (TextView) mLayout.findViewById(R.id.txtExtraRate);
        txtTotalCost = (TextView) mLayout.findViewById(R.id.txtTotalCost);
        edtTxtQuantity = (EditText) mLayout.findViewById(R.id.edtTxtQuantity);
        TxtGtotal = (TextView) mLayout.findViewById(R.id.txtGrandTotal);
        btnPlus = (Button) mLayout.findViewById(R.id.btnPlus);
        btnMinus = (Button) mLayout.findViewById(R.id.btnMinus);
        tvAllergicInfo = (TextView) mLayout.findViewById(R.id.tv_remark);
        llAllergicInfo = (LinearLayout) mLayout.findViewById(R.id.ll_all_info);
        recyclerViewMuticorseItems = (RecyclerView) mLayout.findViewById(R.id.recyclerViewMuticorseItems);
        fabCustomerDeatailsDown = (FloatingActionButton) mLayout.findViewById(R.id.fabCustomerDeatailsDown);
        fabCustomerDeatailsDown.setVisibility(View.INVISIBLE);
        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        rVModifiers.setHasFixedSize(true);
        rVModifiers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rVModifiersList.setHasFixedSize(true);
        rVModifiersList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewMuticorseItems.setLayoutManager(new GridLayoutManager(getContext(), 3));
        if(itemName != null)
            toolbar.setTitle(itemName);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ItemDetailsMuticorseFragment.this.dismiss();
            }
        });


        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            isItemDetailsLoading = true;

        }
        edtTxtQuantity.addTextChangedListener(textWatcher);
        initButtons();
        loadMuticorseMenu();
        if(Long.parseLong(rate) > 0)
            txtRate.setText(rate);

        ViewCompat.animate(fabCustomerDeatailsDown)
                .scaleX(0)
                .scaleY(0)
                .setDuration(100)
                .start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabCustomerDeatailsDown.setVisibility(View.VISIBLE);
                fabCustomerDeatailsDown.clearAnimation();
                ViewCompat.animate(fabCustomerDeatailsDown)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator())
                        .start();

            }
        }, 1000);

        fabCustomerDeatailsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((SelectItemsActivity) getActivity()).setUpperFabAnimation(true, 1);
                ((SelectItemsActivity) getActivity()).animateBillFragment();
            }
        });
    }

    private void loadMuticorseMenu() {


        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    final Response<List<Item>> response = Utilities.getRetrofitWebService(getContext()).getMuticourseItems(itemId).execute();
                    if (response != null && response.body() != null) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                mainItemList = response.body();
                                if(mainItemList.size() > 0) {
                                    selectedMainItemPos = 0;
                                    onMulticorseMenuloaded(mainItemList);
                                }
                            }
                        });
                    }
                } catch (Exception e) {

                }

            }
        }).start();
    }

    private void onMulticorseMenuloaded(final List<Item> itemList) {


        McItemsAdater adapter = new McItemsAdater(itemList, new McItemsAdater.ItemsClickListner() {
            @Override
            public void onItemClick(String id, String barcode, boolean isMutiCorse) {
            }
        }, null);

        adapter.setOnItemPositionClickListner(new McItemsAdater.ItemPositionClickListner() {
            @Override
            public void onItemClick(int position) {
                try {
                    selectedMainItemPos = position;
                    onMulticorseItemSelected(itemList.get(position).ItemCd);
                    if(itemList.get(position).getRemarks() != null && !itemList.get(position).getRemarks().matches("")) {
                        llAllergicInfo.setVisibility(View.VISIBLE);
                        tvAllergicInfo.setText(itemList.get(position).getRemarks());
                    }
                    else
                        llAllergicInfo.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        recyclerViewMuticorseItems.setAdapter(adapter);


        try {
            if(loadMapFlag) {
                if(rate != null)
                    calculateCostForItem1(1);
                loadMapFlag = false;
                if (itemList.size() > 0)
                    if (itemList.get(0) != null)
                        if (itemList.get(0).ItemCd != null) {
                            onMulticorseItemSelected(itemList.get(0).ItemCd);
                            if(itemList.get(0).getRemarks() != null && !itemList.get(0).getRemarks().matches("")) {
                                llAllergicInfo.setVisibility(View.VISIBLE);
                                tvAllergicInfo.setText(itemList.get(0).getRemarks());
                            }
                            else
                                llAllergicInfo.setVisibility(View.GONE);

                        }
            }

            if (itemList.size() > 0)
                if (itemList.get(0) != null)
                        if(itemList.get(0).getRemarks() != null && !itemList.get(0).getRemarks().matches("")) {
                            llAllergicInfo.setVisibility(View.VISIBLE);
                            tvAllergicInfo.setText(itemList.get(0).getRemarks());
                        }
                        else
                            llAllergicInfo.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void onMulticorseItemSelected(final String itemCode) {

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {

                            final Response<List<Item>> response = Utilities.getRetrofitWebService(getContext()).getMcMappedItems(itemCode).execute();
                            if (response != null && response.body() != null) {
                                uiHandler.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (response != null) {

                                            subitems = response.body();
                                           /* rVModifiersList.setAdapter(new McItemModifiersAdapter(new McItemModifiersAdapter.onItemClickCallBack() {
                                                @Override
                                                public void onItemSelected(int position) {


                                                    if(mainItemList.size() > 0) {

                                                        mainItemList.remove(selectedMainItemPos);
                                                        mainItemList.add(selectedMainItemPos , subitems.get(position));
                                                        onMulticorseMenuloaded(mainItemList);
                                                    }


                                                }
                                            }, subitems));*/


                                        } else {

                                            ((McItemModifiersAdapter) rVModifiersList.getAdapter()).removeAllList();


                                        }
                                    }
                                });
                            } else {
                                ((McItemModifiersAdapter) rVModifiersList.getAdapter()).removeAllList();

                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                    }
                }).start();
    }

    }

    private void initButtons() {

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTxtQuantity.setText(
                        edtTxtQuantity.getText().toString().length() > 0 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) + 1) : ""
                );
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTxtQuantity.setText(
                        edtTxtQuantity.getText().toString().length() > 0 ?
                                (Integer.parseInt(edtTxtQuantity.getText().toString()) > 1 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) - 1)
                                        : edtTxtQuantity.getText().toString()) :
                                edtTxtQuantity.getText().toString()
                );
            }
        });

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", null)
                .create();
    }


    private void calculateCostForItem1(long qty) {

        try {
            long total_cost = 0;

            if (rate != null) {
                total_cost = qty * Long.parseLong(rate);
            }


            if (total_cost > 0) {
                TxtGtotal.setText(total_cost + "");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }


    public void setCallBack(ItemDdetailsListCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onStop() {
        super.onStop();
        ApplicationSingleton.getInstance().setLockFlag(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (searchFragment != null)
            searchFragment.clickLock = false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface ItemDdetailsListCallBack {
        void onItemSelected(List<SelectedItemDetails> selectedItemDetailses, boolean isSuccess);

        void onCanceled();
    }


    private class ItemCosts {
        public double itemPrice = 0;
        public double totalCost = 0;
        public double extraCost = 0;
    }
}
