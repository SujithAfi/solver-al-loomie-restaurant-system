package com.afi.restaurantpos.al_loomie.Utills;

/**
 * Created by AFI on 9/29/2015.
 * Class to store all application constants
 */
public class Constants {

    public static final String API_BASE_URL_1 =
            //"http://192.168.2.3:86/restrunt/api/" ;
            // "http://117.247.190.137:86/restrunt-new/api/" ;
//            "http://192.168.2.29:81/api/"
    "http://afigeor.cloudapp.net:92/SolverKOT/api/";

    public static final String SHARED_PREF_NAME = "RESTAURENT_PREFRENCES";
    public static final String SHARED_PREF_KEY_USERNAME = "USERNAME";
    public static final String SHARED_PREF_KEY_PASSWORD = "PASSWORD";
    public static final String SHARED_PREF_KEY_ID = "ID";
    public static final String SHARED_PREF_KEY_SESSION_ID = "SESION_ID";
    public static final String SHARED_PREF_KEY_AUTHENTICATED = "KEY_AUTHENTICATED";
    public static final String SHARED_PREF_KEY_CURRENCY_FORMAT = "KEY_CURRENCY_FORMAT";
    public static final String SHARED_PREF_KEY_ = "KEY_TAX_1";
    public static final String SHARED_PREF_KEY_TAX_1 = "KEY_TAX_1";
    public static final String SHARED_PREF_KEY_TAX_1_CAPTION = "KEY_TAX_1_CAPTION";
    public static final String SHARED_PREF_KEY_TAX_2 = "KEY_TAX_2";
    public static final String SHARED_PREF_KEY_TAX_1_ENABLED = "KEY_TAX_1_ENABLED";
    public static final String SHARED_PREF_KEY_TAX_2_ENABLED = "KEY_TAX_2_ENABLED";
    public static final String SHARED_PREF_KEY_TAX_2_CAPTION = "KEY_TAX_2_CAPTION";
    public static final String SHARED_PREF_KEY_SERVICE_CHARGE_ENABLED = "KEY_SERVICE_CHARGE_ENABLED";
    public static final String SHARED_PREF_KEY_SERVICE_CHARGE_RATE = "KEY_SERVICE_CHARGE_RATE";
    public static final String SHARED_PREF_KEY_NEW_KOT = "KEY_NEW_KOT";
    public static final String SHARED_PREF_KEY_NEW_KOT_LOADED = "KEY_NEW_KOT_LOADED";
    public static final String SHARED_PREF_KEY_SHIFT_ID = "KEY_NEW_SHIFT_ID";
    public static final String SHARED_PREF_KEY_SHIFT_NO = "KEY_SHIFT_NO";
    public static final String SHARED_PREF_KEY_SHIFT_DATE = "KEY_SHIFT_DATE";
    public static final String SHARED_PREF_KEY_SHIFT_TIME = "KEY_SHIFT_TIME";
    public static final String SHARED_PREF_WEB_API_URL = "WEB_API_URL";
    public static final String SHARED_PREF_WEB_API_URL_CONFIGURED = "WEB_API_URL_CONFIGURED";
    public static final String SHARED_PREF_COUNTER = "KEY_COUNTER";
    public static final String SHARED_PREF_ISADMIN = "is_admin";

    public static final int ORDER_TYPE_DINE_IN = 1;
    public static final int ORDER_TYPE_TAKE_AWAY = 2;
    public static final int ORDER_TYPE_HOME_DELIVERY = 3;



    public static final String DATABASE_NAME = "AFI_RESTUARENT_DB";
    public static final int CURRENT_DB_VERSION = 1;


    public static final int ITEM_ID_INDEX  = 0;

    public static final int ITEM_BARCODE_INEX = 1;
    public static final String COUNTER_NAME = "KEY_COUNTER_NAME";
    public static final String SOFTENG_USERNAME = "SOFTENG";

    //User Rights
    // main
    public static final String USER_RIGHTS_TABLELAYOUT = "Table Layout";
    public static final String USER_RIGHTS_RESERVATION = "Table Reservation";
    public static final String USER_RIGHTS_BILLING = "Billing";
    public static final String USER_RIGHTS_PAYMENT = "Payment";
    public static final String USER_RIGHTS_CUSTOMER = "New Customer";

    //User rights Table Layout
    public static final String SHARED_PRE_UR_TABLELAYOUT_ADDORSAVE = "tl_add_or_save";
    public static final String SHARED_PRE_UR_TABLELAYOUT_EDIT = "tl_edit";
    public static final String SHARED_PRE_UR_TABLELAYOUT_DELETE = "tl_delete";
    public static final String SHARED_PRE_UR_TABLELAYOUT_VIEW = "tl_view";

    //User rights Reservation
    public static final String SHARED_PRE_UR_RESERVATION_ADDORSAVE = "resv_add_or_save";
    public static final String SHARED_PRE_UR_RESERVATION_EDIT = "resv_edit";
    public static final String SHARED_PRE_UR_RESERVATION_DELETE = "resv_delete";
    public static final String SHARED_PRE_UR_RESERVATION_VIEW = "resv_view";

    //User rights Billing
    public static final String SHARED_PRE_UR_BILLING_ADDORSAVE = "billing_add_or_save";
    public static final String SHARED_PRE_UR_BILLING_EDIT = "billing_edit";
    public static final String SHARED_PRE_UR_BILLING_DELETE = "billing_delete";
    public static final String SHARED_PRE_UR_BILLING_VIEW = "billing_view";

    //User rights Payment
    public static final String SHARED_PRE_UR_PAYMENT_ADDORSAVE = "payment_add_or_save";
    public static final String SHARED_PRE_UR_PAYMENT_EDIT = "payment_edit";
    public static final String SHARED_PRE_UR_PAYMENT_DELETE = "payment_delete";
    public static final String SHARED_PRE_UR_PAYMENT_VIEW = "payment_view";

    //User rights Customer
    public static final String UR_CUSTOMER_ADDORSAVE = "cust_add_or_save";
    public static final String UR_CUSTOMER_EDIT = "cust_edit";
    public static final String UR_CUSTOMER_DELETE = "cust_delete";
    public static final String UR_CUSTOMER_VIEW = "cust_view";

    public static final String PDF_FOLDER_NAME = "Al Loomie Restaurant";








}
