package com.afi.restaurantpos.al_loomie.Dialogs;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.afi.restaurantpos.al_loomie.Adapters.CustomersDialogAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCustomerDialogFragment extends DialogFragment implements SearchView.OnQueryTextListener, View.OnFocusChangeListener {


    public static final String CREATE_CUSTOMER_FRAGMENT_ACTION = "pos.restaurent.afi.restuarentpos.Dialogs.SendMessage";

    private ViewPager viewpagerCustomers;

    private TabLayout tabLayout;
    private SearchView serchView;
    private SelectCustomerListner selectCustomerListner;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Customer customer = (Customer) intent.getSerializableExtra("customer");
            if (customer != null) {
                if (selectCustomerListner != null) {
                    selectCustomerListner.onCustomerSelected(customer);
                }
                dismiss();
            }
        }
    };

    public SelectCustomerDialogFragment() {
        // Required empty public constructor
    }

    public void setSelectCustomerListner(SelectCustomerListner selectCustomerListner) {
        this.selectCustomerListner = selectCustomerListner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return getCustomView();
    }

    private View getCustomView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_select_customer_dialog, null);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbarContainer);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewpagerCustomers = (ViewPager) view.findViewById(R.id.viewpagerCustomers);
        serchView = (SearchView) view.findViewById(R.id.serchView);
        toolbar.setTitle("Customers");
        toolbar.setLogo(R.drawable.ic_person_white_24dp);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCustomerDialogFragment.this.dismiss();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter("customerSelected"));
            viewpagerCustomers.setAdapter(new CustomersDialogAdapter(getChildFragmentManager() , getActivity()));
            tabLayout.setupWithViewPager(viewpagerCustomers);
            serchView.setOnQueryTextListener(this);
            serchView.setOnQueryTextFocusChangeListener(this);

            if( ApplicationSingleton.getInstance().getReservation())
                getView().findViewById(R.id.btnSkip).setVisibility(View.VISIBLE);
            else
                getView().findViewById(R.id.btnSkip).setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().findViewById(R.id.btnSkip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectCustomerListner != null) {
                    selectCustomerListner.onSkipAction();
                }
                dismiss();
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        sendBroadcast(0, query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        sendBroadcast(0, newText);
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        sendBroadcast(hasFocus ? 1 : 2, "");
    }

    private void sendBroadcast(int flag, String query) {

        try {
            Intent intent = new Intent(CREATE_CUSTOMER_FRAGMENT_ACTION);
            intent.putExtra("action", flag);
            intent.putExtra("query", query);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface SelectCustomerListner {

        void onCustomerSelected(Customer customer);

        void onSkipAction();

    }

}
