package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;
import com.afi.restaurantpos.al_loomie.Models.Ingredients;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;

import java.util.ArrayList;

public class McItemIngredientsAdapter extends RecyclerView.Adapter<McItemIngredientsAdapter.ItemModifiersAdapterViewHolder>{
    private  int mainQty ;
    Context context;

    public ArrayList<Ingredients>  modifiers;


    private onItemClickCallBack itemClickCallback;



    public interface onItemClickCallBack{
        void onItemSelected(int pos);
    }

    public McItemIngredientsAdapter(onItemClickCallBack itemClickCallback) {
        this.modifiers = new ArrayList<>();
        this.itemClickCallback = itemClickCallback;
    }

    public McItemIngredientsAdapter(onItemClickCallBack itemClickCallback, ArrayList<Ingredients>  modifiers , int mainQty) {
        this.modifiers = modifiers;
        this.mainQty = mainQty;
        this.itemClickCallback = itemClickCallback;
    }

    @Override
    public ItemModifiersAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context=viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ingredients_view, viewGroup, false);
        ItemModifiersAdapterViewHolder viewHolder = new ItemModifiersAdapterViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemModifiersAdapterViewHolder holder, int position) {
        holder.btnRemove.setOnClickListener(new Listner(position));
       /* if(!modifiers.get(position).Rate.trim().equals("0"))
            holder.itemName.setText(modifiers.get(position).Item_Name+" ("+ Utilities.getDefaultCurrencyFormat(Double.parseDouble(modifiers.get(position).Rate),context)+")");
        else
            holder.itemName.setText(modifiers.get(position).Item_Name);*/

        if(modifiers.get(position).getName() != null)
            holder.tvIngredient.setText(modifiers.get(position).getName());
        holder.tvCount.setText(modifiers.get(position).getQuantity() + "");

    }

    @Override
    public int getItemCount() {
        if(modifiers == null)
            return 0;
        else
            return modifiers.size();
    }




    public static class ItemModifiersAdapterViewHolder extends RecyclerView.ViewHolder{


        private final TextView tvIngredient;
        private final TextView tvCount;
        private final Button btnRemove;

        /*private TextView itemName;
                private LinearLayout closeBtn;*/
        public ItemModifiersAdapterViewHolder(View itemView) {
            super(itemView);
            /*itemName = (TextView) itemView.findViewById(R.id.txtModifierName);
            closeBtn = (LinearLayout) itemView.findViewById(R.id.ll_root);*/

            tvIngredient = (TextView) itemView.findViewById(R.id.tv_ingredient_name);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
            btnRemove = (Button) itemView.findViewById(R.id.btn_remove);
        }
    }

    private class Listner extends RecyclerViewCallBack{

        public Listner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {

            if(itemClickCallback != null)
                itemClickCallback.onItemSelected(position);
//            removeFromList(position);


        }
    }

    public void removeFromList(int position) {
        modifiers.remove(position);
        this.notifyDataSetChanged();
    }

    public void removeAllList() {

        int size = this.modifiers.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.modifiers.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }

    }


}
