package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.ItemModifiersAdapter;
import com.afi.restaurantpos.al_loomie.Adapters.ItemModifiersAdaptersList;
import com.afi.restaurantpos.al_loomie.Fragments.SearchFragment;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ItemDetailsFragment extends Fragment {

    private static final String ITEM_ID_KEY = "key_item_id";
    private static final String BARCODE_KEY = "key_bar_code";
    public boolean clickLock = false;
    IntentFilter intentFilter = new IntentFilter("okButtonAction");
    private boolean isItemDetailsLoading = false;
    private String itemId;
    private String barCode;
    private ImageView iVItemImage;
    //private Toolbar toolbar;
    private ViewGroup mContainer;
    private View mLayout;
    private RecyclerView rVModifiers;
    private RecyclerView rVModifiersList;
    private TextView txtRate;
    private TextView txtExraRate;
    private TextView txtTotalCost;
    private EditText edtTxtQuantity;
    private Button btnPlus;
    private Button btnMinus;
    private double itemPrice = 0;
    private double totalCost = 0;
    private double extraCost = 0;

    //private SearchFragment searchFragment;
    private ItemDetails itemDetails;
    private ItemDdetailsCallBack callBack;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onOkButtonClicked();
        }
    };
    private TextWatcher textWatcher = new TextWatcher() {
        private CharSequence prevSequence;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            this.prevSequence = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {

            calculateCost();

        }
    };

    public ItemDetailsFragment() {
    }

    public static ItemDetailsFragment newInstance(String itemId, String Barcode, SearchFragment searchFragment) {
        ItemDetailsFragment fragment = new ItemDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_ID_KEY, itemId);
        args.putString(BARCODE_KEY, Barcode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog d = null;
        if (d != null) {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if ( isItemDetailsLoading) {
                        Utilities.showSnackBar("Item details not loaded or incorrect data", getActivity());
                        return;
                    }


                    if (edtTxtQuantity.getText().toString() == null || edtTxtQuantity.getText().toString().matches("")) {
                        Utilities.showToast("Quantity can not be empty.", getContext());
                        edtTxtQuantity.setError("Enter quantity");
                    }
                    else if(Integer.parseInt(edtTxtQuantity.getText().toString()) == 0){

                        Utilities.showToast("Item quantity not valid.", getContext());
                        edtTxtQuantity.setError("Enter quantity.");
                    }
                    else {

                        try {
                            if (itemDetails != null) {

                                SelectedItemDetails details = new SelectedItemDetails();
                                details.itemName = itemDetails.Item_Name;
                                details.barcode = itemDetails.ItemCd;
                                details.itemCode = itemDetails.ItemCd;
                                details.Unit_Fraction = itemDetails.Unit_Fraction;
                                details.Itm_UCost = itemDetails.Itm_UCost;
                                details.Unit_cd = itemDetails.Unit_cd;
                                details.Cmd_IsBuffet = itemDetails.Cmd_IsBuffet;
                                details.isSubitem = false;
                                details.isMultiC = false;

                                try {
                                    details.itemUnitCost = Double.parseDouble(itemDetails.price);
                                } catch (Exception e) {
                                    details.itemUnitCost = 99;
                                }
                                details.itemExtraRate = extraCost;
                                details.itemTotalCost = totalCost;

                                try {
                                    details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                } catch (Exception e) {
                                    details.itemQuantity = 9;
                                }
                                if (((ItemModifiersAdapter) rVModifiers.getAdapter()) != null){


                                    if (((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers() != null) {


//                                    details.modify = ((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers();
                                        List<ItemModifiers> modify = ((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers();
                                        if (modify.size() > 0) {

                                            String remarks = "";
                                            for (ItemModifiers modiItems : modify) {

                                                if (remarks.matches(""))
                                                    remarks = "( " + modiItems.name;
                                                else
                                                    remarks = remarks + ", " + modiItems.name;

                                            }

                                            details.modificationRemarks = remarks + " )";
                                        }


                                    }


                            }




                            if (callBack != null)
                                callBack.onItemSelected(details, true);
                            else
                                callBack.onItemSelected(details, false);
                        }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void onOkButtonClicked() {

        if (isItemDetailsLoading) {
            Utilities.showSnackBar("Item details not loaded or incorrect data", getActivity());
            return;
        }


        if (edtTxtQuantity.getText().toString() == null || edtTxtQuantity.getText().toString().matches("")) {
            Utilities.showToast("Quantity can not be empty.", getContext());
            edtTxtQuantity.setError("Enter quantity");
        }
        else if(Integer.parseInt(edtTxtQuantity.getText().toString()) == 0){

            Utilities.showToast("Item quantity not valid.", getContext());
            edtTxtQuantity.setError("Enter quantity.");
        }
        else {

            try {
                if (itemDetails != null){

                SelectedItemDetails details = new SelectedItemDetails();
                details.itemName = itemDetails.Item_Name;
                details.barcode = itemDetails.ItemCd;
                details.itemCode = itemDetails.ItemCd;
                details.Unit_Fraction = itemDetails.Unit_Fraction;
                details.Itm_UCost = itemDetails.Itm_UCost;
                details.Unit_cd = itemDetails.Unit_cd;
                details.Cmd_IsBuffet = itemDetails.Cmd_IsBuffet;
                details.isSubitem = false;
                details.isMultiC = false;


                try {
                    details.itemUnitCost = Double.parseDouble(itemDetails.price);
                } catch (Exception e) {
                    details.itemUnitCost = 99;
                }
                details.itemExtraRate = extraCost;
                details.itemTotalCost = totalCost;

                try {
                    details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                } catch (Exception e) {
                    details.itemQuantity = 9;
                }

        /*        if(((ItemModifiersAdapter) rVModifiers.getAdapter()) != null)
                    if(((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers() != null)
                        details.modify = ((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers();*/

                    if (((ItemModifiersAdapter) rVModifiers.getAdapter()) != null){


                        if (((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers() != null) {


//                                    details.modify = ((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers();
                            List<ItemModifiers> modify = ((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers();
                            if (modify.size() > 0) {

                                String remarks = "";
                                for (ItemModifiers modiItems : modify) {

                                    if (remarks.matches(""))
                                        remarks = "( " + modiItems.name;
                                    else
                                        remarks = remarks + ", " + modiItems.name;

                                }

                                details.modificationRemarks = remarks + " )";
                            }


                        }


                    }




                if (callBack != null)
                    callBack.onItemSelected(details, true);
                else
                    callBack.onItemSelected(details, false);
            } else{

                    Utilities.showSnackBar("Check your internet connection" , getActivity());

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemId = getArguments().getString(ITEM_ID_KEY);
            barCode = getArguments().getString(BARCODE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;

        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_item_details, container, false);
        iVItemImage = (ImageView) mLayout.findViewById(R.id.iVItemImage);
        iVItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!clickLock) {
                    clickLock = true;
                }
            }
        });
        //toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        rVModifiers = (RecyclerView) mLayout.findViewById(R.id.rv_modifiers);
        rVModifiersList = (RecyclerView) mLayout.findViewById(R.id.rv_modifiersList);
        txtRate = (TextView) mLayout.findViewById(R.id.txtRate);
        txtExraRate = (TextView) mLayout.findViewById(R.id.txtExtraRate);
        txtTotalCost = (TextView) mLayout.findViewById(R.id.txtTotalCost);
        edtTxtQuantity = (EditText) mLayout.findViewById(R.id.edtTxtQuantity);
        btnPlus = (Button) mLayout.findViewById(R.id.btnPlus);
        btnMinus = (Button) mLayout.findViewById(R.id.btnMinus);

        Utilities.hideKeybord(getActivity());

        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rVModifiers.setHasFixedSize(true);
        rVModifiers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rVModifiersList.setHasFixedSize(true);
        rVModifiersList.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        Picasso.with(getContext())
                .load(
                        Utilities.getImageUrl(itemId, barCode)
                )
                .placeholder(R.drawable.logo)
                .fit()
                .centerCrop()
                .into(iVItemImage);

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            isItemDetailsLoading = true;
            Utilities.getRetrofitWebService(getContext()).getItemDetails(itemId, barCode).enqueue(new Callback<ItemDetails>() {
                @Override
                public void onResponse(Response<ItemDetails> response, Retrofit retrofit) {
                    try {
                        isItemDetailsLoading = false;
                        if ( response.body() != null && ItemDetailsFragment.this.isVisible()) {
                            processtemDetails(response.body());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    isItemDetailsLoading = false;
                }
            });

        }
        edtTxtQuantity.addTextChangedListener(textWatcher);
        initButtons();


    }



    private void initButtons() {

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTxtQuantity.setText(
                        edtTxtQuantity.getText().toString().length() > 0 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) + 1) : ""
                );
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTxtQuantity.setText(
                        edtTxtQuantity.getText().toString().length() > 0 ?
                                (Integer.parseInt(edtTxtQuantity.getText().toString()) > 1 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) - 1)
                                        : edtTxtQuantity.getText().toString()) :
                                edtTxtQuantity.getText().toString()
                );
            }
        });

    }

    /**
     * Process the item details
     *
     * @param itemDetailses
     */
    private void processtemDetails(@NonNull ItemDetails itemDetailses) {

        this.itemDetails = itemDetailses;
        rVModifiers.setAdapter(new ItemModifiersAdapter(new ItemModifiersAdapter.onItemClickCallBack() {
            @Override
            public void onItemSelected(ItemModifiers itemModifiers) {
                ((ItemModifiersAdaptersList) rVModifiersList.getAdapter()).addToList(itemModifiers);
                calculateCost();
            }
        }));
        rVModifiersList.setAdapter(new ItemModifiersAdaptersList(itemDetailses.modify, new ItemModifiersAdaptersList.ItemModifiersAdaptersListListner() {
            @Override
            public void onItemSelected(ItemModifiers modifiers) {
                ((ItemModifiersAdapter) rVModifiers.getAdapter()).addToItemModifiers(modifiers);
                rVModifiers.scrollToPosition(((ItemModifiersAdapter) rVModifiers.getAdapter()).getModifiers().size() - 1);
                calculateCost();
            }
        }));
        try {
            itemPrice = Double.parseDouble(itemDetailses.price);
        } catch (Exception e) {

        }
        calculateCost();
    }

    /**
     * Calculate the total cost
     */
    private void calculateCost() {
        try {
            ItemModifiersAdapter itemModifiersAdapter = (ItemModifiersAdapter) rVModifiers.getAdapter();
            if (itemModifiersAdapter.getModifiers() == null)
                return;
            if (itemModifiersAdapter.getModifiers().size() != 0) {
                List<ItemModifiers> list = itemModifiersAdapter.getModifiers();
                double totalExtraCost = 0;
                for (ItemModifiers modifiers : list)
                    totalExtraCost += Double.parseDouble(modifiers.rate);
                this.extraCost = totalExtraCost;
                totalCost = itemPrice + totalExtraCost;
                totalCost = edtTxtQuantity.getText().toString().length() > 0 ? totalCost * Double.parseDouble(edtTxtQuantity.getText().toString()) : totalCost;
                txtRate.setText(Utilities.getDefaultCurrencyFormat(itemPrice, getContext()));
                txtExraRate.setText(Utilities.getDefaultCurrencyFormat(totalExtraCost, getContext()));
                txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(totalCost, getContext()));
            } else {
                extraCost = 0;
                totalCost = edtTxtQuantity.getText().toString().length() > 0 ? itemPrice * Double.parseDouble(edtTxtQuantity.getText().toString()) : itemPrice;
                txtRate.setText(Utilities.getDefaultCurrencyFormat(itemPrice, getContext()));
                txtExraRate.setText(Utilities.getDefaultCurrencyFormat("0.000", getContext()));
                txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(totalCost, getContext()));
            }
        } catch (Exception e) {

        }
    }

    public ItemDetailsFragment setCallBack(ItemDdetailsCallBack callBack) {
        this.callBack = callBack;
        return this;
    }

    @Override
    public void onStop() {
        super.onStop();
        ApplicationSingleton.getInstance().setLockFlag(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface ItemDdetailsCallBack {
        void onItemSelected(SelectedItemDetails selecctedItemDetails, boolean isSuccess);

        void onCanceled();
    }
}
