package com.afi.restaurantpos.al_loomie.Models;

import java.util.List;

import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;

/**
 * Created by AFI on 10/6/2015.
 */
public class SelectedItemDetails {
    public String itemName;
    public List<ItemModifiers> modify;
    public double itemUnitCost;
    public double itemExtraRate;
    public double itemTotalCost;
    public double itemQuantity;

    public String barcode;
    public String itemCode;
    public String Unit_Fraction;
    public String Itm_UCost;
    public String Unit_cd;
    public String Kot_No;
    public String Menu_SideDish;
    public String Cmd_IsBuffet;
    public String Remarks;

    public String Cus_Name;
    public String Cus_Cd;
    public String Cm_Type;
    public String Cm_Covers;
    public String Sman_Cd;
    public String Tab_Cd;
    public String CmD_Amt;
    public String Disc_Per;
    public Boolean isSubitem;
    public Boolean isMultiC;
    public boolean Is_TempItem;
    public boolean Thru_Res;
    public String Res_No;

    public String modificationRemarks;

    public List<ItemModifiers> selectedModifiers;
    public List<ItemModifiers> availableModifiers;

    public boolean isSelected;

    public boolean isFoc;
}
