package com.afi.restaurantpos.al_loomie;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by afi-mac-001 on 13/07/16.
 */
@ReportsCrashes(formKey = "", // will not be used
        mailTo = "sujith@afiinfotech.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast)
public class RestaurantApplication extends Application {

    public ReservationBillListener listener;

    public interface ReservationBillListener {

        public void onComplete(Boolean success);

    }


    public void setCustomObjectListener(ReservationBillListener listener) {
        this.listener = listener;
    }

    public  ReservationBillListener getCustomObjectListener() {
        return this.listener;
    }

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        ACRA.init(this);
    }

}
