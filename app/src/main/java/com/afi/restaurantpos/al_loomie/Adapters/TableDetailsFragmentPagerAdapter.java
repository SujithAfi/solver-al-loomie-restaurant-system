package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.afi.restaurantpos.al_loomie.Dialogs.TableReservationDetailsDialogFragment;
import com.afi.restaurantpos.al_loomie.Fragments.TableBillOptions;
import com.afi.restaurantpos.al_loomie.Fragments.TableKotOptions;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;

import java.util.List;
import java.util.Map;


/**
 * Created by afi-mac-001 on 13/06/16.
 */
public class TableDetailsFragmentPagerAdapter extends FragmentPagerAdapter {

    private final List<Table> tables;
    private Table table;
    private List<Reservation> reservations;
    private Map<String, List<PendingKotItem>> data;

    private TableKotOptions tableKotOptions;
    private TableBillOptions tableBillOptions;


    public TableDetailsFragmentPagerAdapter(FragmentManager fm , Table table, List<Reservation> reservations , Map<String, List<PendingKotItem>> tableId , List<Table> tables) {
        super(fm);
        this.table = table;
        this.reservations = reservations;
        this.data = tableId;
        this.tables = tables;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TableReservationDetailsDialogFragment.create(table , reservations , tables);
            case 1: tableKotOptions =  TableKotOptions.newInstance(data.get("kot") , tables);
                return tableKotOptions;
            case 2: tableBillOptions =  TableBillOptions.newInstance(data.get("bill"));
                return tableBillOptions;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Reservation";
            case 1:
                return "Kot";
            case 2:
                return "Bill";
            default:
                return super.getPageTitle(position);
        }
    }

    public void refreshData(Map<String, List<PendingKotItem>> dat) {
        if ( tableBillOptions != null )
            tableBillOptions.bills = dat.get("bill");
        if ( tableKotOptions != null )
            tableKotOptions.kotumbers = dat.get("kot");
    }
}
