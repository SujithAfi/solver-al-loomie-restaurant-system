package com.afi.restaurantpos.al_loomie.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.CustomerSelectionListAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.SelectCustomerDialogFragment;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.lb.recyclerview_fast_scroller.RecyclerViewFastScroller;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomersFragment extends Fragment implements CustomerSelectionListAdapter.CustomerSelectionListner {


    private RecyclerView rvCustomers;
    private ProgressBar progresCustomerLoading;

    private Handler uiHandler = new Handler();

    private String oldQuery = "";

    RecyclerViewFastScroller fastScroller;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            try {
                int action = intent.getIntExtra("action", -1);

                if (action == 0) {//Query changed
                    String query = intent.getStringExtra("query");
                    onQueryChanged(query);
                } else if (action == 1) {//has focus
                    Log.e("has focus", "has focus");
                } else if (action == 2) {//Lost focus
                    Log.e("Lost focus", "Lost focus");
                } else if (action == 3) {//refresh list
                    loadCustomers();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };


    public CustomersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_customers, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvCustomers = (RecyclerView) getView().findViewById(R.id.rvCustomers);
        progresCustomerLoading = (ProgressBar) getView().findViewById(R.id.progresCustomerLoading);
        rvCustomers.setHasFixedSize(true);
        fastScroller = (RecyclerViewFastScroller) getView().findViewById(R.id.fastscroller);
        loadCustomers();

    }



    private void loadCustomers() {
        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {
            progresCustomerLoading.setVisibility(View.VISIBLE);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        final Response<List<Customer>> response = Utilities.getRetrofitWebService(getContext()).getAllCustomersN().execute();

//                        Log.e("getAllCustomers===>>", response.message());

                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                progresCustomerLoading.setVisibility(View.GONE);
                                customerListLoaded(response.body());
                            }
                        });
                    } catch (Exception e) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                progresCustomerLoading.setVisibility(View.GONE);
                            }
                        });
                    }


                }
            }).start();
        }

    }


    private void customerListLoaded(List<Customer> customers) {

        if (customers != null) {

            final CustomerSelectionListAdapter adapter = new CustomerSelectionListAdapter(customers, this, getActivity());
            rvCustomers.setAdapter(adapter);

            rvCustomers.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) {
                @Override
                public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
                    super.onLayoutChildren(recycler, state);
                    //TODO if the items are filtered, considered hiding the fast scroller here
                    final int firstVisibleItemPosition = findFirstVisibleItemPosition();
                    if (firstVisibleItemPosition != 0) {
                        // this avoids trying to handle un-needed calls
                        if (firstVisibleItemPosition == -1)
                            //not initialized, or no items shown, so hide fast-scroller
                            fastScroller.setVisibility(View.GONE);
                        return;
                    }
                    final int lastVisibleItemPosition = findLastVisibleItemPosition();
                    int itemsShown = lastVisibleItemPosition - firstVisibleItemPosition + 1;
                    //if all items are shown, hide the fast-scroller
                    fastScroller.setVisibility(adapter.getItemCount() > itemsShown ? View.VISIBLE : View.GONE);
                }
            });
            fastScroller.setRecyclerView(rvCustomers);
            fastScroller.setViewsToUse(R.layout.recycler_view_fast_scroller__fast_scroller, R.id.fastscroller_bubble, R.id.fastscroller_handle);
        }
    }

    @Override
    public void onItemSelected(Customer customer) {

        try {
            Intent intent = new Intent("customerSelected");
            intent.putExtra("customer", customer);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter(SelectCustomerDialogFragment.CREATE_CUSTOMER_FRAGMENT_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onQueryChanged(String query) {

        try {
            if (query.equals(oldQuery)) {
                return;
            } else {
                oldQuery = query;
            }

            if (rvCustomers.getAdapter() != null) {
                ((CustomerSelectionListAdapter) rvCustomers.getAdapter()).filerResults(query);
            }
            Log.e("Query changed", query);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
