package com.afi.restaurantpos.al_loomie.Fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.PendingBillAdapter;
import com.afi.restaurantpos.al_loomie.Adapters.PendingKotReservationAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.BillFragmentDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.CancelBillAndKotDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.PayBillDialog;
import com.afi.restaurantpos.al_loomie.Models.CancelKotOrBill;
import com.afi.restaurantpos.al_loomie.Models.FinalAmount;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Models.ReservationContainer;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.ReservationMakeBillActivity;
import com.afi.restaurantpos.al_loomie.RestaurantApplication;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationDetailsFragment extends Fragment implements View.OnClickListener, RestaurantApplication.ReservationBillListener {


    private TextView txtreservationNumber;
    private TextView txtdate;
    private TextView txtTime;

    private EditText edtTxtGuestNumber;
//    private TextView txtTableName;
    private TextView txtCustomerCode;
    private TextView txtCustomerName;
    private Reservation reservation;
    private Handler uiHandler = new Handler();

    private Date selectedReservationDate = null;
    private Date selectedReservationTime = null;
    private Button btnCancel;
    private Button btnEdit;
    private ProgressDialog progressDialog;
    private Customer customer;
    private OnHeadlineSelectedListener mCallback;
    private Button btnItems;
    private boolean fromResvAct = false;
    private Button btnMakeKot;
    private TextView tvHabits;
    private CardView cvHabits;
    private TextView tvDate3Desc;
    private TextView tvdate3;
    private LinearLayout lldate3;
    private TextView tvDate2Desc;
    private TextView tvdate2;
    private LinearLayout lldate2;
    private TextView tvDate1Desc;
    private TextView tvdate1;
    private LinearLayout lldate1;
    private CardView cvImptDates;
    private TextView tvFav3;
    private LinearLayout llFav3;
    private TextView tvFav2;
    private LinearLayout llFav2;
    private TextView tvFav1;
    private LinearLayout llFav1;
    private CardView cvFavorites;
    private TextView tvAllergy;
    private CardView cvAllergy;
    private Spinner spTableNumber;
    private String tableCode;
    private List<Table> tables;
    private RecyclerView recyclerViewKot;
    private List<PendingKot> pendingKots;
    List<SelectedItemDetails> selectedItemDetailses;
    String newBillNumber;
    private Button btnMakeBill;
    private RecyclerView recyclerViewBill;
    private TextView tvKotHead;
    private TextView tvBillHead;
    private boolean clickLock = false;
    List<PendingBill> pendingBills;
    private ReservationMakeBillActivity object;
    private TextView tvStatus;
    private double service_value;
    private CardView cvAccountDetails;
    private TextView tvCheckinDate;
    private TextView tvRNumber;
    private TextView tvAccName;

    @Override
    public void onComplete(Boolean success) {

        Log.e("Refresh====>>" , "Working");
        loadPendingKots();
    }


    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected();
    }

    public ReservationDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            fromResvAct  = getArguments().getBoolean("ResvAct" , false);
            reservation = (Reservation) getArguments().getSerializable("reservation");
            if(reservation != null){
                if(reservation.getCustomer() != null)
                    customer = reservation.getCustomer();

            }

        }

        RestaurantApplication application = (RestaurantApplication)getActivity().getApplication();

        application.setCustomObjectListener(ReservationDetailsFragment.this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.reservation_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txtreservationNumber = (TextView) getView().findViewById(R.id.txtreservationNumber);
        txtdate = (TextView) getView().findViewById(R.id.txtdate);
        txtTime = (TextView) getView().findViewById(R.id.txtTime);

//        txtTableName = (TextView) getView().findViewById(R.id.txtTableName);
        txtCustomerCode = (TextView) getView().findViewById(R.id.txtCustomerCode);
        txtCustomerName = (TextView) getView().findViewById(R.id.txtCustomerName);

        edtTxtGuestNumber = (EditText) getView().findViewById(R.id.edtTxtGuestNumber);

        btnCancel = (Button) getView().findViewById(R.id.btn_cancel);
        btnEdit = (Button) getView().findViewById(R.id.btn_edit);
        btnItems = (Button) getView().findViewById(R.id.btn_items);
        btnMakeKot = (Button) getView().findViewById(R.id.btn_makekot);

        tvHabits = (TextView) getView().findViewById(R.id.tv_habit);
        cvHabits = (CardView) getView().findViewById(R.id.cv_habits);

        tvAllergy = (TextView) getView().findViewById(R.id.tv_allergy);
        cvAllergy = (CardView) getView().findViewById(R.id.cv_allergy);

        tvDate3Desc = (TextView) getView().findViewById(R.id.tv_date3desc);
        tvdate3 = (TextView) getView().findViewById(R.id.tv_date3);
        lldate3 = (LinearLayout) getView().findViewById(R.id.ll_date3);
        tvDate2Desc = (TextView) getView().findViewById(R.id.tv_date2desc);
        tvdate2 = (TextView) getView().findViewById(R.id.tv_date2);
        lldate2 = (LinearLayout) getView().findViewById(R.id.ll_date2);
        tvDate1Desc = (TextView) getView().findViewById(R.id.tv_date1disc);
        tvdate1 = (TextView) getView().findViewById(R.id.tv_date1);
        lldate1 = (LinearLayout) getView().findViewById(R.id.ll_date1);
        cvImptDates = (CardView) getView().findViewById(R.id.cv_impdates);


        tvFav3 = (TextView) getView().findViewById(R.id.tv_favorite3);
        llFav3 = (LinearLayout) getView().findViewById(R.id.ll_fav3);
        tvFav2 = (TextView) getView().findViewById(R.id.tv_favorite2);
        llFav2 = (LinearLayout) getView().findViewById(R.id.ll_fav2);
        tvFav1 = (TextView) getView().findViewById(R.id.tv_favorite1);
        llFav1 = (LinearLayout) getView().findViewById(R.id.ll_fav1);
        cvFavorites = (CardView) getView().findViewById(R.id.cv_favourites);

        spTableNumber = (Spinner) getView().findViewById(R.id.spinner_table_number);

        cvAccountDetails = (CardView) getView().findViewById(R.id.cv_credit_acc_details);
        tvAccName = (TextView) getView().findViewById(R.id.tv_acc_name);
        tvRNumber = (TextView) getView().findViewById(R.id.tv_room_number);
        tvCheckinDate = (TextView) getView().findViewById(R.id.tv_checkin_dt);

        recyclerViewKot = (RecyclerView) getView().findViewById(R.id.recyclerView_kots);
        recyclerViewKot.setHasFixedSize(true);
        recyclerViewKot.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        recyclerViewBill = (RecyclerView) getView().findViewById(R.id.recyclerView_bills);
        recyclerViewBill.setHasFixedSize(true);
        recyclerViewBill.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        btnMakeBill = (Button) getView().findViewById(R.id.btn_bill);
        tvKotHead = (TextView) getView().findViewById(R.id.tv_kothead);
        tvBillHead = (TextView) getView().findViewById(R.id.tv_billhead);

        tvStatus = (TextView) getView().findViewById(R.id.tv_status);



        btnMakeKot.setVisibility(View.INVISIBLE);

        // Check Permission to Edit details
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_EDIT, false))
            btnEdit.setVisibility(View.VISIBLE);

        // Check Permission to delete reservation
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_DELETE, false))
            btnCancel.setVisibility(View.VISIBLE);

        btnCancel.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnItems.setOnClickListener(this);
        btnMakeKot.setOnClickListener(this);
        btnMakeBill.setOnClickListener(this);

        getView().findViewById(R.id.btnAdd).setOnClickListener(this);
        getView().findViewById(R.id.btnSubstract).setOnClickListener(this);

        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                final Calendar calendar = Calendar.getInstance();

                String date = txtdate.getText().toString();
                if (!date.isEmpty()) {
                    try {
                        String[] dateParts = date.split("-");
                        calendar.set(Calendar.YEAR, Integer.parseInt(dateParts[2]));
                        calendar.set(Calendar.MONTH, Integer.parseInt(dateParts[1]) - 1);
                        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateParts[0]));
                    } catch (Exception e) {

                    }
                }


                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);
                        txtdate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));

                        selectedReservationDate = calendar.getTime();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show();
            }
        });

        txtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeBundle timeBundle = null;
                final Calendar calendar = Calendar.getInstance();
                if (view.getTag() == null) {
                    timeBundle = new TimeBundle();

                    timeBundle.hour = calendar.get(Calendar.HOUR_OF_DAY);
                    timeBundle.minute = calendar.get(Calendar.MINUTE);
                } else {
                    timeBundle = (TimeBundle) view.getTag();
                }

                final TimeBundle finalTimeBundle = timeBundle;
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        finalTimeBundle.hour = i;
                        finalTimeBundle.minute = i1;
                        txtTime.setTag(finalTimeBundle);
                        calendar.set(Calendar.HOUR_OF_DAY, i);
                        calendar.set(Calendar.MINUTE, i1);
                        txtTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                        selectedReservationTime = calendar.getTime();
                    }
                }, timeBundle.hour, timeBundle.minute, false).show();
            }
        });



        if(reservation != null){

            Calendar calDate = Calendar.getInstance();
            Calendar cal1Time = Calendar.getInstance();
            SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm:ss a");

            if(reservation.getDocNumber() != null)
                txtreservationNumber.setText(reservation.getDocNumber().replace("##" , ""));
            if(reservation.getResDate() != null);
            {
                List<String> items = Arrays.asList(reservation.getResDate().split("\\s+"));

                if(items.size() > 0) {
                    try {

                        calDate.setTime(sdfDate.parse(items.get(0)));
                        txtdate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                        selectedReservationDate = calDate.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()).equals(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()))) {
                        ViewCompat.animate(btnMakeKot)
                                .scaleX(0)
                                .scaleY(0)
                                .setDuration(100)
                                .start();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btnMakeKot.setVisibility(View.VISIBLE);
                                btnMakeKot.clearAnimation();
                                ViewCompat.animate(btnMakeKot)
                                        .scaleX(1f)
                                        .scaleY(1f)
                                        .setDuration(300)
                                        .setInterpolator(new DecelerateInterpolator())
                                        .start();

                            }
                        }, 1000);
                    }

                }

            }
            if(reservation.getReservationTimeFrom() != null) {


                List<String> items = Arrays.asList(reservation.getReservationTimeFrom().split("\\s+"));

                if(items.size() > 0) {

                    try {
                        cal1Time.setTime(sdfTime.parse(items.get(1) + " " + items.get(2)));
                        txtTime.setText(new SimpleDateFormat("hh:mm a").format(cal1Time.getTime()));
                        selectedReservationTime = cal1Time.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

            }

            List<String> datas = new ArrayList<>();

            if(TableDetailsFragment.tables != null)
                tables = TableDetailsFragment.tables;

            if(tables != null) {
                for (Table table : tables) {

                    datas.add(table.getCaption());

                }
            }

            int indx = 0;

            if(reservation.getTableName() != null) {

                indx = datas.indexOf(reservation.getTableName());
//                txtTableName.setText(reservation.getTableName());
            }
            if(reservation.getTable_Cd() != null)
                tableCode = reservation.getTable_Cd();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);
            spTableNumber.setAdapter(adapter);

            spTableNumber.setSelection(indx , true);

            spTableNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    try {
                        tableCode = tables.get(position).getTab_Cd();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            if(reservation.getGuestCount() != null)
                edtTxtGuestNumber.setText(reservation.getGuestCount());
            /*if(reservation.getTableName() != null)
                txtTableName.setText(reservation.getTableName());*/
            if(reservation.getCustomer() != null){

                if(reservation.getCustomer().getName() != null)
                    txtCustomerName.setText(reservation.getCustomer().getName());
                if(reservation.getCustomer().getId() != null)
                    txtCustomerCode.setText(reservation.getCustomer().getId());
                /*if(reservation.getCustomer().getCus_Phone() != null)
                    txtCustomerCode.setText(reservation.getCustomer().getCus_Phone());*/
                if(reservation.getCustomer().getCusFavorite1() != null && !reservation.getCustomer().getCusFavorite1().matches("")){

                    cvFavorites.setVisibility(View.VISIBLE);
                    tvFav1.setText(reservation.getCustomer().getCusFavorite1());

                    if(reservation.getCustomer().getCusFavorite2() != null && !reservation.getCustomer().getCusFavorite2().matches("")){

                        llFav2.setVisibility(View.VISIBLE);
                        tvFav2.setText(reservation.getCustomer().getCusFavorite2());
                    }

                    if(reservation.getCustomer().getCusFavorite3() != null && !reservation.getCustomer().getCusFavorite3().matches("")){

                        llFav3.setVisibility(View.VISIBLE);
                        tvFav3.setText(reservation.getCustomer().getCusFavorite3());
                    }



                }

                if(reservation.getCustomer().getImpDate1() != null && !reservation.getCustomer().getImpDate1().matches("") && !reservation.getCustomer().getImpDate1().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                    Calendar calDateImp = Calendar.getInstance();
                    SimpleDateFormat sdfDateImp = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());

                    cvImptDates.setVisibility(View.VISIBLE);

                    List<String> items = Arrays.asList(reservation.getCustomer().getImpDate1().split("\\s+"));

                    if(items.size() > 0) {
                        try {

                            calDateImp.setTime(sdfDateImp.parse(items.get(0)));
                            tvdate1.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    tvDate1Desc.setText(reservation.getCustomer().getImpDate1Desc());

                    if(reservation.getCustomer().getImpDate2() != null && !reservation.getCustomer().getImpDate2().matches("") && !reservation.getCustomer().getImpDate2().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                        lldate2.setVisibility(View.VISIBLE);
                        List<String> items1 = Arrays.asList(reservation.getCustomer().getImpDate2().split("\\s+"));

                        if(items1.size() > 0) {
                            try {

                                calDateImp.setTime(sdfDateImp.parse(items1.get(0)));
                                tvdate2.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        tvDate2Desc.setText(reservation.getCustomer().getImpDate2Desc());
                    }

                    if(reservation.getCustomer().getImpDate3() != null && !reservation.getCustomer().getImpDate3().matches("") && !reservation.getCustomer().getImpDate3().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                        lldate3.setVisibility(View.VISIBLE);

                        List<String> items2 = Arrays.asList(reservation.getCustomer().getImpDate3().split("\\s+"));

                        if(items2.size() > 0) {
                            try {

                                calDateImp.setTime(sdfDateImp.parse(items2.get(0)));
                                tvdate3.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }

                        tvDate3Desc.setText(reservation.getCustomer().getImpDate3Desc());
                    }



                }

                if(reservation.getCustomer().getCusHabits() != null && !reservation.getCustomer().getCusHabits().matches("")){

                    cvHabits.setVisibility(View.VISIBLE);
                    tvHabits.setText(reservation.getCustomer().getCusHabits());
                }

                if(reservation.getCustomer().getAllergicInfo() != null && !reservation.getCustomer().getAllergicInfo().matches("")){

                    cvAllergy.setVisibility(View.VISIBLE);
                    tvAllergy.setText(reservation.getCustomer().getAllergicInfo());
                }

                if(customer.getIsCredit()){

                    cvAccountDetails.setVisibility(View.VISIBLE);

                    if(customer.getAccName() != null)
                        tvAccName.setText(customer.getAccName());
                    if(customer.getRoomNo() != null)
                        tvRNumber.setText(customer.getRoomNo());
                    if(customer.getCheckinDate() != null)
                        tvCheckinDate.setText(customer.getCheckinDate());
                    tvAllergy.setText(customer.getAllergicInfo());
                }
            }




        }

        txtdate.setEnabled(false);
        txtTime.setEnabled(false);
        spTableNumber.setEnabled(false);

        loadPendingKots();

//        animateStatus();


    }

    private void animateStatus() {


        ViewCompat.animate(tvStatus)
                .scaleX(0)
                .scaleY(0)
                .setDuration(100)
                .start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tvStatus.setVisibility(View.VISIBLE);
                tvStatus.clearAnimation();
                ViewCompat.animate(tvStatus)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator())
                        .start();

            }
        }, 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                animateStatus();

            }
        }, 10);
    }

    private void updateReservation() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Updating Reservation");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    final ReservationContainer reservationContainer = new ReservationContainer();
                    reservationContainer.setDocumentId(reservation.getDocNumber());
                    reservationContainer.setDocumentDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " 00:00:00.000");
                    reservationContainer.setDocumentYear(new SimpleDateFormat("yyyy").format(new Date()));
                    reservationContainer.setCounter(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_COUNTER, ""));
                    reservationContainer.setCashierId(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_ID, ""));
                    if(reservation.getCustomer() != null){

                        if(reservation.getCustomer().getName() != null)
                            reservationContainer.setCustomerName(reservation.getCustomer().getName());
                        if(reservation.getCustomer().getId() != null)
                            reservationContainer.setCustomerCode(reservation.getCustomer().getId());

                        reservationContainer.setRoom_No(customer == null ? "" : customer.getRoomNo());

                        try {
                            if(customer != null){

                                if(customer.getCheckinDate() != null && !customer.getCheckinDate().matches("")){


                                    Calendar calDate = Calendar.getInstance();
                                    SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                                    List<String> dtitems = Arrays.asList(customer.getCheckinDate().split("\\s+"));

                                    if (dtitems.size() > 0) {
                                        try {

                                            calDate.setTime(sdfDate.parse(dtitems.get(0)));
                                            String checkinDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime()) + " 00:00:00.000";
                                            reservationContainer.setCheckIn_Dt(checkinDate);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                                else
                                    reservationContainer.setCheckIn_Dt("");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    reservationContainer.setGuestCount(edtTxtGuestNumber.getText().toString());
                    reservationContainer.setReservationDate(new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate) + " 00:00:00.000");
                    reservationContainer.setReservationTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(selectedReservationTime));
                    reservationContainer.setTableCode(tableCode);
                    reservationContainer.setUserId(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_ID, ""));
                    final Response<String> response = Utilities.getRetrofitWebService(getContext()).updateReservation(reservationContainer).execute();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                            if (response != null && response.body() != null) {
                                if (!response.body().equals("error")) {
                                    Utilities.showSnackBar("Reservation Updated" , getActivity());
//                                    Toast.makeText(getActivity() , "Reservation Updated" , Toast.LENGTH_LONG).show();

                                /*    new AlertDialog.Builder(getContext())
                                            .setTitle("Reservation successfully Updated")
                                            .setMessage("Do you want to add KOT for this reservation ?")
                                            .setPositiveButton("Add KOT", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    ((OrderDetails) getActivity()).onAddItemForReservation(response.body(), customer , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                                                }
                                            })
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    mCallback.onArticleSelected();
                                                    getActivity().getSupportFragmentManager().popBackStackImmediate();

                                                }
                                            })
                                            .setCancelable(false)
                                            .create().show();*/
                                } else {
                                    Utilities.showSnackBar("Failed to Update Reservation" , getActivity());


                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());

                        }
                    });
                }
            }
        }).start();
    }

    private boolean validateForm() {

        boolean result = true;

        if( txtdate.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Date." , getActivity());
            result =  false;
        }
        else if( txtTime.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Time." , getActivity());
            result =  false;
        }
        else if(edtTxtGuestNumber.getText().toString() == null || edtTxtGuestNumber.getText().toString().matches("")){
            Utilities.showSnackBar("Guests number can not be empty." , getActivity());
            edtTxtGuestNumber.setError("Enter valid Guest no");
            edtTxtGuestNumber.requestFocus();
            result =  false;
        }
        else if(Integer.parseInt(edtTxtGuestNumber.getText().toString()) == 0){
            Utilities.showSnackBar("Guests number not valid." , getActivity());
            edtTxtGuestNumber.setError("Enter valid Guest no");
            edtTxtGuestNumber.requestFocus();
            result =  false;
        }

        /*if( txtdate.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Date" , getActivity());
            result =  false;
        }
        else if( txtTime.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Time" , getActivity());
            result =  false;
        }*/

        return result; //TODO
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_cancel) {

            new AlertDialog.Builder(getContext())
                    .setTitle("Info")
                    .setMessage("Cancel Reservation")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {
                                onCancel();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton("cancel" , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();

        }

        else if(v.getId() == R.id.btn_edit){

            if(btnEdit.getText().toString().equalsIgnoreCase("Edit")){

                btnEdit.setText("Save");
                txtdate.setEnabled(true);
                txtTime.setEnabled(true);
                spTableNumber.setEnabled(true);
                getView().findViewById(R.id.btnAdd).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.btnSubstract).setVisibility(View.VISIBLE);
                edtTxtGuestNumber.setEnabled(true);


            }
            else if(btnEdit.getText().toString().equalsIgnoreCase("Save")){

                if (validateForm()) {
                    btnEdit.setText("Edit");
                    txtdate.setEnabled(false);
                    txtTime.setEnabled(false);
                    spTableNumber.setEnabled(false);
                    getView().findViewById(R.id.btnAdd).setVisibility(View.GONE);
                    getView().findViewById(R.id.btnSubstract).setVisibility(View.GONE);
                    edtTxtGuestNumber.setEnabled(false);
//                    UpdateReservation();
                    updateReservation();
                }


            }
        }
        else if(v.getId() == R.id.btn_items){

            try {
                if(!fromResvAct)
                    ((OrderDetails) getActivity()).onViewItemForReservation(reservation, customer , false , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                else
                {
                    Intent intent = new Intent(getActivity(), SelectItemsActivity.class);
                    intent.putExtra("customer", customer);
                    intent.putExtra("isReservationKot", true);
                    intent.putExtra("isReservationItemEdit", true);
                    intent.putExtra("makeKOT", false);
                    intent.putExtra("reservation", reservation);
                    intent.putExtra("table", ApplicationSingleton.getInstance().getTable());
                    intent.putExtra("hasCustomer", true);
                    intent.putExtra("resvDate", new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                    startActivityForResult(intent, 67);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        else if(v.getId() == R.id.btn_makekot){

            try {
                if(!fromResvAct)
                    ((OrderDetails) getActivity()).onViewItemForReservation(reservation, customer , true , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                else
                {
                    Intent intent = new Intent(getActivity(), SelectItemsActivity.class);
                    intent.putExtra("customer", customer);
                    intent.putExtra("isReservationKot", true);
                    intent.putExtra("isReservationItemEdit", true);
                    intent.putExtra("makeKOT", true);
                    intent.putExtra("reservation", reservation);
                    intent.putExtra("table", ApplicationSingleton.getInstance().getTable());
                    intent.putExtra("hasCustomer", true);
                    intent.putExtra("resvDate", new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                    startActivityForResult(intent, 67);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if (v.getId() == R.id.btnAdd) {
            try {
                edtTxtGuestNumber.setText((Integer.parseInt((edtTxtGuestNumber.getText().toString())) + 1) + "");
            } catch (Exception e) {

            }
        } else if (v.getId() == R.id.btnSubstract) {
            try {
                edtTxtGuestNumber.setText(Integer.parseInt((edtTxtGuestNumber.getText().toString())) > 1 ? ((Integer.parseInt((edtTxtGuestNumber.getText().toString()))) - 1) + "" : edtTxtGuestNumber.getText().toString());
            } catch (Exception e) {

            }
        }
        else if (v.getId() == R.id.btn_bill) {
            try {
                mergeKot();
            } catch (Exception e) {

            }
        }
    }

    /*private void UpdateReservation() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Updating Reservation");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        progressDialog.show();


        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();
        cancelKotOrBill.id = reservation.getDocNumber() ;
        cancelKotOrBill.date = reservation.getResDate();
        cancelKotOrBill.reson = "";

        Utilities.getRetrofitWebService(getContext()).cancelReservation(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {

                if (response != null) {
                    if ((Boolean) response.body().get("response_code")) {

                        loadResrvationNumber();


                    } else {

                        progressDialog.dismiss();

                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

                progressDialog.dismiss();

            }
        });

    }*/

    /*private void loadResrvationNumber() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final Response<Map<String, String>> res = Utilities.getRetrofitWebService(getContext()).getNewBillNumberN(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_COUNTER, "")).execute();

                    if (res != null && res.body() != null) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                onReservationDetailsLoaded(res.body());

                            }
                        });


                    }

                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());
                        }
                    });
                }
            }
        }).start();

    }*/

    /*private void onReservationDetailsLoaded(Map<String, String> reservationDetails) {

        txtreservationNumber.setText(new DecimalFormat("##000000").format(Double.parseDouble(reservationDetails.get("bill_no"))));

        updateReservation();
    }*/

    public void onCancel(){

        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();
        cancelKotOrBill.id = reservation.getDocNumber() ;
        cancelKotOrBill.date = reservation.getResDate();
        cancelKotOrBill.reson = "";

        Utilities.getRetrofitWebService(getContext()).cancelReservation(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {

                if (response != null) {
                    if ((Boolean) response.body().get("response_code")) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.success)
                                .setMessage("Reservation Cancelled")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(getActivity() != null) {
                                            mCallback.onArticleSelected();
                                            getActivity().getSupportFragmentManager().popBackStackImmediate();
                                        }

                                    }
                                })
                                .create().show();
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.error)
                                .setMessage("Canot delete")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                })
                                .create().show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private class TimeBundle {
        int hour;
        int minute;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent("refreshTable");
        getActivity().sendBroadcast(intent);
    }

    private void loadPendingKots() {

        Log.e("Inside=====>" , "Load pending kot");

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Loading  Kot's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        dialog.show();
        Utilities.getRetrofitWebService(getActivity()).getAllPendingKotForReservation(reservation.getDocNumber().replace("##" , "")).enqueue(new Callback<List<PendingKot>>() {
            @Override
            public void onResponse(Response<List<PendingKot>> response, Retrofit retrofit) {
                loadPendingBills();
                dialog.cancel();
                pendingKots = response.body() == null ? new ArrayList<PendingKot>() : response.body();

                Log.e("Response Size=====>" , pendingKots.size() + "");

                if(pendingKots.size() > 0) {
                    tvStatus.setText("In Progress");
                    btnMakeBill.setVisibility(View.VISIBLE);
                    tvKotHead.setVisibility(View.VISIBLE);
                    btnEdit.setVisibility(View.GONE);
                }
                else {
                    btnMakeBill.setVisibility(View.GONE);
                    tvKotHead.setVisibility(View.GONE);

                }

                recyclerViewKot.setAdapter(new PendingKotReservationAdapter(pendingKots, new PendingKotReservationAdapter.PendingKotSelectListner() {
                    @Override
                    public void onKotselected(String kotNumber) {//Todo direct click on item
                        /*if (!clickLock) {
                            clickLock = true;
                            clickHandler.postDelayed(clickLockRunnable, 1000);
                            List<String> kot = new ArrayList<String>();
                            kot.add(kotNumber);
                            loadKotDetails(kot, false, true);
                        }*/


                    }

                    @Override
                    public void onKotCheckChange() {
                        /*int size = getSelectedPendingKots().size();
                        if (size == 0) {
                            btnMergeKot.setEnabled(false);
                            btnSplitKot.setEnabled(false);
                            btnCancelKot.setEnabled(false);
                            toolbar.setSubtitle("");
                        } else if (size == 1) {
                            btnMergeKot.setEnabled(false);
                            btnSplitKot.setEnabled(true);
                            btnCancelKot.setEnabled(true);
                            toolbar.setSubtitle("1 Kot selected");
                        } else if (size > 1) {
                            btnMergeKot.setEnabled(true);
                            btnSplitKot.setEnabled(false);
                            btnCancelKot.setEnabled(false);
                            toolbar.setSubtitle(String.format("%d Kot selected", size));
                        }*/
                    }
                }));


               /* initCheckListner(((PendingKotAdapter) pendingKotView.getAdapter()).getAllSelectedKots().size());

                makeLoadingResult(pendingKots.isEmpty());*/
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.cancel();
//                makeLoadingResult(true);
            }
        });


    }

    public void mergeKot() {

        try {
            List<PendingKot> kots = pendingKots;
            if (!kots.isEmpty()) {
                List<String> kotStringList = new ArrayList<>();
                for (PendingKot pendingKot : kots)
                    kotStringList.add(pendingKot.Kot_No);

                loadKotDetails(kotStringList, true, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadKotDetails(final List<String> kotStringList, final boolean isMerge, final boolean isDirectSelection) {

        Calendar calendar1 = Calendar.getInstance();
        try {
            calendar1.setTime(ApplicationSingleton.getInstance().getCurrentDate());
        } catch (Exception e) {
            e.printStackTrace();
        }

        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime());

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Loading  KOT's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {

            dialog.show();
            Utilities.getRetrofitWebService(getActivity()).getKotDetailsFromKotNumber(kotStringList , date).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
                @Override
                public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                    try {
                        dialog.dismiss();
                        if (response.body() != null) {
                            Map<String, List<PendingKotItem>> listMap = response.body();

                            if (isDirectSelection) {//todo go directly to bill
                                //prepare bill from here
                                try {
                                    ApplicationSingleton.getInstance().setSelectedKotMap(listMap);
                                    ApplicationSingleton.getInstance().setTemporaryPendingKotItems(listMap.get(kotStringList.get(0)));
                                } catch (Exception e) {
                                    String t = e.getMessage();
                                }


                                //Pocess bill data
                                selectedItemDetailses = new ArrayList<>();
                                for (PendingKotItem item : ApplicationSingleton.getInstance().getTemporaryPendingKotItems()) {
                                    SelectedItemDetails details = new SelectedItemDetails();
                                    details.itemCode = item.Itm_Cd;
                                    details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                                    details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                                    details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                                    details.Itm_UCost = item.KotD_UCost;
                                    details.itemName = item.Itm_Name;
                                    details.Unit_Fraction = item.Unit_Fraction;
                                    details.Unit_cd = item.Kot_MuUnit;
                                    details.barcode = item.Barcode;
                                    details.Kot_No = item.Kot_No;
                                    details.Kot_No = item.Kot_No;
                                    details.Menu_SideDish = item.menu_SideDish;
                                    details.Cmd_IsBuffet = "0";

                                    details.Cus_Name = item.Cus_Name;
                                    details.Cus_Cd = item.Cus_Cd;
                                    details.Disc_Per = item.Disc_Per;

                                    details.Cm_Type = item.Cm_Type;
                                    details.Cm_Covers = item.Cm_Covers;
                                    details.Sman_Cd = item.Sman_Cd;
                                    details.Tab_Cd = item.Tab_Cd;
                                    details.CmD_Amt = item.CmD_Amt;
                                    selectedItemDetailses.add(details);
                                }


                                final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                        .content("Creating new Bill")
                                        .progress(true, 1)
                                        .titleColorRes(R.color.colorAccentDark)
                                        .cancelable(false).build();
                                dialog.show();
                                Utilities.getRetrofitWebService(getActivity()).getNewBillNumber(ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<Map<String, String>>() {
                                    @Override
                                    public void onResponse(Response<Map<String, String>> response, Retrofit retrofit) {
                                        dialog.cancel();
                                        Map<String, String> stringStringMap = response.body();
                                        if (stringStringMap.get("bill_no") != null) {
                                            if (stringStringMap.get("bill_no").length() > 0) {
                                                ApplicationSingleton.getInstance().setNewBillNumber(Utilities.getKotFormat(stringStringMap.get("bill_no")));
                                            }
                                        }
                                        newBillNumber = String.format("Bill No : %s", Utilities.getKotFormat(stringStringMap.get("bill_no")));

                                        if (selectedItemDetailses != null) {
                                            if (selectedItemDetailses.size() > 0) {
                                                final BillContainer saveBillData = prepareData(selectedItemDetailses);
                                                new BillFragmentDialog()
                                                        .setBillItems(saveBillData.details)
                                                        .setNewBillNumber(newBillNumber)
                                                        .setBillListner(new BillFragmentDialog.BillListner() {
                                                            @Override
                                                            public void onPositive(List<BillItem> billItems , FinalAmount finalAmount) {

                                                                if(billItems != null){

                                                                    saveBillData.details = billItems;

                                                                    if(finalAmount != null) {
                                                                        if(finalAmount.focAmount != null)
                                                                            saveBillData.Cm_FocAmt = finalAmount.focAmount;
                                                                        if(finalAmount.serviceAmount != null)
                                                                            saveBillData.Service_Amt = finalAmount.serviceAmount;
                                                                        if(finalAmount.tax1Amount != null)
                                                                            saveBillData.Cm_Tax1_Amt = finalAmount.tax1Amount;
                                                                        if(finalAmount.tax2Amount != null)
                                                                            saveBillData.Cm_Tax2_Amt = finalAmount.tax2Amount;
                                                                        if(finalAmount.netAmount != null)
                                                                            saveBillData.Cm_NetAmt = finalAmount.netAmount;
                                                                    }


                                                                    for(BillItem items : billItems){

                                                                        Log.e("items is FOC====>>" , String.valueOf(items.isFoc));
                                                                    }

                                                                }


                                                                if (!Utilities.isNetworkConnected(getActivity()))
                                                                    Utilities.createNoNetworkDialog(getActivity());
                                                                else {

                                                                    final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                                                            .content("Saving Bill")
                                                                            .progress(true, 1)
                                                                            .titleColorRes(R.color.colorAccentDark)
                                                                            .cancelable(false).build();
                                                                    dialog.show();
                                                                    Utilities.getRetrofitWebService(getActivity()).saveBill(saveBillData).enqueue(new Callback<Map<String, Object>>() {
                                                                        @Override
                                                                        public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                                                            dialog.dismiss();
                                                                            final boolean response_code = (Boolean) response.body().get("response_code");
                                                                            if (response_code)
                                                                                new MaterialDialog.Builder(getActivity())
                                                                                        .titleColorRes(R.color.colorAccentDark)
                                                                                        .title(response_code ? "Bill Saved" : "Can't save bill")
                                                                                        .positiveText("Ok")
                                                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                                            @Override
                                                                                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                                                if (response_code) {
                                                                                                    loadPendingKots();
                                                                                                }
                                                                                            }
                                                                                        }).build().show();
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Throwable t) {
                                                                            dialog.dismiss();

                                                                        }
                                                                    });


                                                                }
                                                            }

                                                            @Override
                                                            public void onNegative() {

                                                            }
                                                        })
                                                        .show(getActivity().getSupportFragmentManager(), "Billu");


                                            }
                                        }

                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        dialog.cancel();
                                    }
                                });


                            } else {

                                if(isMerge){
                                    if (listMap != null) {

                                        List<String> cusCodes = new ArrayList<String>();
                                        for (Map.Entry<String, List<PendingKotItem>> entry : listMap.entrySet()) {

                                            List<PendingKotItem> itemList = entry.getValue();

                                            if (itemList != null) {


                                                for (PendingKotItem item : itemList) {

                                                    if (item.Cus_Cd != null && !item.Cus_Cd.matches("")) {

                                                        cusCodes.add(item.Cus_Cd);

                                                    }

                                                }
                                            }


                                        }

                                        if (cusCodes != null) {

                                            if (cusCodes.size() > 1) {

                                                for (int i = 0; i < cusCodes.size(); i++) {

                                                    for (int j = 0; j < cusCodes.size(); j++) {

                                                        if (!cusCodes.get(i).equalsIgnoreCase(cusCodes.get(j))) {

                                                            Toast.makeText(getActivity(), "Can't Merge KOT's with different customers.", Toast.LENGTH_LONG).show();
                                                            return;

                                                        }
                                                    }
                                                }

                                                gotoSplitAndMergeActivity(listMap , isMerge);

                                            } else {

                                                gotoSplitAndMergeActivity(listMap , isMerge);

                                            }

                                        } else {

                                            gotoSplitAndMergeActivity(listMap , isMerge);
                                        }


                                    }

                                }


                            }
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                }
            });


        }

    }

    private void gotoSplitAndMergeActivity(Map<String, List<PendingKotItem>> listMap, boolean isMerge) {

        try {
            SharedPreferences preferences = Utilities.getSharedPreferences(getActivity());
            if (preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED, false))
                ApplicationSingleton.getInstance().setTaxFraction1(Float.parseFloat(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0")));
            if (preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED, false))
                ApplicationSingleton.getInstance().setTaxFraction2(Float.parseFloat(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0")));

            Intent intent = new Intent(getActivity(), ReservationMakeBillActivity.class);
            ApplicationSingleton.getInstance().setSelectedKotMap(listMap);
            intent.putExtra("operation", isMerge);
            startActivityForResult(intent, 67);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private BillContainer prepareData(List<SelectedItemDetails> selectedItemDetailses) {
        String Tab_Cd = "";
        String Cm_Covers = "";

        Context context = getActivity();
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        SharedPreferences preferences = Utilities.getSharedPreferences(context);
        int index = 0;
        double Cm_TotAmt = 0;
        String Cus_Name = "";
        String Cus_Cd = "";
        String Disc_Per = "";
        BillContainer billContainer = new BillContainer();
        List<BillItem> billItems = new ArrayList<>();

        for (SelectedItemDetails details : selectedItemDetailses) {

            Cus_Name = details.Cus_Name;
            Cus_Cd = details.Cus_Cd;
            Disc_Per = details.Disc_Per;

            Tab_Cd = details.Tab_Cd;
            Cm_Covers = details.Cm_Covers;
            BillItem billItem = new BillItem();
            billItem.Itm_cd = details.itemCode;
            //put modifiers
            billItem.Menu_SideDish = details.Menu_SideDish;
            if (details.modify != null) {
                for (int i = 0; i < details.modify.size(); i++) {
                    ItemModifiers modifier = details.modify.get(i);
                    if (i + 1 == details.modify.size())
                        billItem.Menu_SideDish += modifier.name;
                    else if (i == 0)
                        billItem.Menu_SideDish += modifier.name;
                    else
                        billItem.Menu_SideDish += "," + modifier.name;
                }
            }
            billItem.Cmd_ItmName = details.itemName;
            billItem.BarCode = details.barcode;
            billItem.Unit_Fraction = details.Unit_Fraction;
            billItem.Cmd_Qty = details.itemQuantity + "";
            billItem.Unit_Cd = details.Unit_cd;
            billItem.Cmd_MuUnit = "1";//TODO
            billItem.Cmd_MuQty = details.itemQuantity + "";
            billItem.Cmd_MuFraction = "1";
            billItem.Cmd_MuConvToBase = "1";
            billItem.Cmd_MuUcost = "1";
            billItem.Cmd_Rate = details.itemUnitCost + "";//// TODO: 10/21/2015
            double Cmd_Amta = (details.itemTotalCost);//*(details.itemQuantity);
            String Cmd_DiscPers = billItem.Cmd_DiscPer;
            billItem.Cmd_DiscPer = details.Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
            if (Cmd_DiscPers != null) {
                billItem.Cmd_DiscAmt = Utilities.getPercentage(Cmd_Amta, Double.parseDouble(billItem.Cmd_DiscPer)) + "";
            } else {
                billItem.Cmd_DiscAmt = "0";
            }
            billItem.Cmd_Amt = (Cmd_Amta - Double.parseDouble(billItem.Cmd_DiscAmt)) + "";
            billItem.Cmd_Ctr = index++ + "";
            billItem.Cmd_SubCtr = "0";
            billItem.Cmd_RowType = "A";
            billItem.Cmd_NotCosted = "0";
            billItem.Cmd_Cancelled = "0";
            billItem.Cmd_IsLzQty = "1";
            billItem.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 1) + "";
            billItem.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
            billItem.Cmd_Status = "1";
            billItem.Cm_ThruDo = "0";
            billItem.IsModified = "0";
            billItem.Cm_Ref = "1";
            billItem.Cmd_DiscReason = "No";
            billItem.Kot_No = details.Kot_No == null ? "0" : details.Kot_No;
            billItem.Rcp_Cd = "1";
            billItem.Cmd_Ucost = details.itemUnitCost + "";
            billItem.Cmd_IsBuffet = details.Cmd_IsBuffet;//// TODO: 10/21/2015
            billItems.add(billItem);
            Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);
        }
        billContainer.details = billItems;
        billContainer.Usr_id = preferences.getString(Constants.SHARED_PREF_KEY_ID, "");
        billContainer.Cm_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_No = singleton.getNewBillNumber();
        billContainer.Sman_Cd = singleton.getSalesman(getActivity()) == null ? "0" : singleton.getSalesman(getActivity()).Id + "";
        billContainer.Cus_Cd = Cus_Cd;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Id + "";
        billContainer.Cus_Name = Cus_Name;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Name + "";billContainer.Cm_TotAmt =
        billContainer.Cm_DiscPer = Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
        billContainer.Cm_DiscAmt = Utilities.getPercentage(Cm_TotAmt, Double.parseDouble(billContainer.Cm_DiscPer)) + "";

//        billContainer.Cm_TotAmt = Cm_TotAmt + "";

//        Cm_TotAmt -= Double.parseDouble(billContainer.Cm_DiscAmt);

        billContainer.Cm_Tax1_Per = Utilities.getTaxFraction1(context) + "";
        billContainer.Cm_Tax1_Amt = Utilities.findTax1(context, Cm_TotAmt) + "";
        billContainer.Cm_Tax2_Per = Utilities.getTaxFraction2(context) + "";
        billContainer.Cm_Tax2_Amt = Utilities.findTax2(context, Cm_TotAmt) + "";
        Log.e("Insideeeee====>>" ,  "hgashjfgjhsadfgj");
        Log.e("Tax111====>>" ,  billContainer.Cm_Tax1_Amt);
        Log.e("Tax222====>>" ,  billContainer.Cm_Tax2_Amt);
        billContainer.Cm_NetAmt = (Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt)) + "";
        billContainer.Cm_Tips_Amt = "0";
        billContainer.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 0) + "";
        billContainer.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
        billContainer.Cm_BC_Settle_Amt = "0.000";
        billContainer.Cm_Paid_Amt = "0.000";
        billContainer.Cm_Cancel_Reason = "";
        billContainer.Cm_Cancelled = "0";
        billContainer.Cm_Cus_Phone = singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().getId() + "";
        billContainer.Cm_Type = "1";//TODO get order type
        billContainer.Tab_Cd = Tab_Cd;//singleton.getTable() == null ? "" : singleton.getTable().Tab_Cd;//TODO
        billContainer.Cm_Covers = Cm_Covers;//singleton.getGuestCount();
        billContainer.Srvr_Cd = "1";
        billContainer.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "0");

        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_ENABLED , false)) {
            billContainer.Service_Per = preferences.getString(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_RATE, "0");
        }
        service_value = Double.parseDouble(billContainer.Cm_TotAmt);
        service_value = Utilities.findServiceCharge(getActivity(), service_value);
        service_value = Double.parseDouble(Utilities.getDefaultCurrencyFormat(service_value , getActivity()));
        double service_vlu = Double.parseDouble(billContainer.Cm_NetAmt);
        service_vlu += service_value;
        billContainer.Cm_NetAmt = String.valueOf(service_vlu);
        billContainer.Service_Amt = String.valueOf(service_value);
        //(Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) - Double.parseDouble( billContainer.Cm_DiscPer)) + "";
        // billContainer.Collect_Sft_No = "2";//// TODO: 10/21/2015
        //billContainer.Collect_Sft_Dt = "10-10-2015";//// TODO: 10/21/2015
        int i = 0;
        i++;
        billContainer.Srvr_Cd = i + "";

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar1 = Calendar.getInstance();

        String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

        if(shiftDateString != null && !shiftDateString.matches("")){

            try {
                calendar1.setTime(dateFormat1.parse(shiftDateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        billContainer.date = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
        billContainer.time = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");


        return billContainer;
    }

    /**
     * Load the pending bills
     */
    private void loadPendingBills() {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Loading Pending Bill's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        dialog.show();

        Utilities.getRetrofitWebService(getActivity()).getAllPendingBillReservation(reservation.getDocNumber().replace("##" , "")).enqueue(new Callback<List<PendingBill>>() {
            @Override
            public void onResponse(Response<List<PendingBill>> response, Retrofit retrofit) {
//                 pendingBills = response.body();
                pendingBills = response.body() == null ? new ArrayList<PendingBill>() : response.body();

                if(pendingBills.size() > 0){
                    tvStatus.setText("In Progress");
                    recyclerViewBill.setVisibility(View.VISIBLE);
                    tvBillHead.setVisibility(View.VISIBLE);
                    btnEdit.setVisibility(View.GONE);
                }
                else{

                    recyclerViewBill.setVisibility(View.GONE);
                    tvBillHead.setVisibility(View.GONE);
                }
                dialog.cancel();
                recyclerViewBill.setAdapter(new PendingBillAdapter(pendingBills == null ? new ArrayList<PendingBill>() : pendingBills, getActivity() , new PendingBillAdapter.PendingBillSelectListner() {
                    @Override
                    public void onBillselected(String BillNumber, final PendingBill pendingBill) {
                        if (!clickLock) {
                            clickLock = true;
                            clickHandler.postDelayed(clickLockRunnable, 1000);
                            PayBillDialog.newInstance(CancelType.CANCEL_BILL)
                                    .setPendingBill(pendingBill)
                                    .setDialogListner(new PayBillDialog.PayBillDialogListner() {
                                        @Override
                                        public void onFinishedAction(PayBillDialog PayBillDialog) {
                                            //TODO refresh items
                                            PayBillDialog.dismiss();
                                            tvStatus.setText("Executed");
                                            getActivity().getSupportFragmentManager().popBackStackImmediate();
                                            /*if (!Utilities.isNetworkConnected(getActivity()))
                                                Utilities.createNoNetworkDialog(getActivity());
                                            else
                                                loadPendingKots();*/

                                           /* Intent i = new Intent(getActivity(), OrderDetails.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(i);*/
                                        }
                                    })
                                    .show(getActivity().getSupportFragmentManager(), "Pay bill KOT");
                        }
                    }

                    @Override
                    public void onBillCheckChange() {


                    }

                    @Override
                    public void onBilllongselected(String BillNumber, final PendingBill pendingBill) {
                        CancelBillAndKotDialog.newInstance(CancelType.CANCEL_BILL)
                                .setPendingBill(pendingBill)
                                .setDialogListner(new CancelBillAndKotDialog.CancelBillAndKotDialogListner() {
                                    @Override
                                    public void onFinishedAction(CancelBillAndKotDialog cancelBillAndKotDialog) {
                                        //TODO refresh items
                                        cancelBillAndKotDialog.dismiss();
                                        if (!Utilities.isNetworkConnected(getActivity()))
                                            Utilities.createNoNetworkDialog(getActivity());
                                        else
                                            loadPendingBills();

                                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                                    }
                                })
                                .show(getActivity().getSupportFragmentManager(), "Pending KOT");

                    }


                }));
                /*if ( pendingBills != null)
                    showResult(pendingBills.isEmpty());*/
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.cancel();
//                showResult(true);
            }
        });


    }

    private Handler clickHandler = new Handler();

    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };



}
