package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 10/7/2015.
 */
public class PayAdapter extends RecyclerView.Adapter<PayAdapter.PayAdapterVH>{


    private List<SelectedItemDetails> selectedItemDetailses;

    public PayAdapter() {
        selectedItemDetailses = new ArrayList<>();
    }

    public PayAdapter(List<SelectedItemDetails> selectedItemDetailses) {
        this.selectedItemDetailses = selectedItemDetailses;
    }

    @Override
    public PayAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_pay_fragment_list, viewGroup, false);
        PayAdapterVH billAdapterVH = new PayAdapterVH(v);
        return billAdapterVH;
    }

    @Override
    public void onBindViewHolder(PayAdapterVH holder, int position) {
        holder.txtItemName.setText(selectedItemDetailses.get(position).itemName);
        holder.txtItemQuantity.setText(Utilities.getItemQuantityFormat(selectedItemDetailses.get(position).itemQuantity ));
        holder.txtItemRate.setText(Utilities.getDefaultCurrencyFormat(selectedItemDetailses.get(position).itemUnitCost + "" ,holder.txtItemAmount.getContext() ));
        holder.txtItemAmount.setText(Utilities.getDefaultCurrencyFormat(selectedItemDetailses.get(position).itemTotalCost + "",holder.txtItemAmount.getContext()));
    }

    @Override
    public int getItemCount() {
        if(selectedItemDetailses == null)
            return  0;
        return selectedItemDetailses.size();
    }


    public void addToBillDetails(SelectedItemDetails selectedItemDetails){
        selectedItemDetailses.add(selectedItemDetails);
        this.notifyDataSetChanged();
    }

    public static class PayAdapterVH extends RecyclerView.ViewHolder{
        private TextView txtItemName;
        private TextView txtItemQuantity;
        private TextView txtItemRate;
        private TextView txtItemAmount;

        public PayAdapterVH(View itemView) {
            super(itemView);
            txtItemName = (TextView) itemView.findViewById(R.id.txtItemName);
            txtItemQuantity = (TextView) itemView.findViewById(R.id.txtItemQuantity);
            txtItemRate = (TextView) itemView.findViewById(R.id.txtItemRate);
            txtItemAmount = (TextView) itemView.findViewById(R.id.txtItemAmount);
        }
    }

    public void setSelectedItemDetailses(List<SelectedItemDetails> selectedItemDetailses) {
        this.selectedItemDetailses = selectedItemDetailses;
    }

    public List<SelectedItemDetails> getSelectedItemDetailses(){
        return this.selectedItemDetailses;
    }
}
