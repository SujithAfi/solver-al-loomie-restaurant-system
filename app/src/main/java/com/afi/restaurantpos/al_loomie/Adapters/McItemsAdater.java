package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.List;


/**
 * Created by AFI on 10/2/2015.
 */
public class McItemsAdater extends RecyclerView.Adapter<McItemsAdater.ItemsAdapterViewHolder> {
    SharedPreferences preferences;
    private List<Item> items;
    private ItemsClickListner itemsClickListner;
    private Fragment fragment;
    private ItemPositionClickListner itemPositionClickListner;
    private SparseBooleanArray selectedItems;
    private int selectedItemPosition = 0;
    private ItemsClickListner1 itemsClickListner1;
    private ItemPositionClickListner1 itemPositionClickListner1;

    public McItemsAdater(List<Item> items , ItemsClickListner itemsClickListner , Fragment fragment) {
        this.items = items;
        this.itemsClickListner = itemsClickListner;
        this.fragment = fragment;
    }

    @Override
    public ItemsAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        preferences = Utilities.getDefaultSharedPref(viewGroup.getContext());
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.multicorse_item, viewGroup, false);
        ItemsAdapterViewHolder itemsAdapterViewHolder = new ItemsAdapterViewHolder(v);
        return itemsAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemsAdapterViewHolder holder, final int position) {

        try {
            if (items.get(position).itemCount == 0) {
                holder.ivClose.setVisibility(View.VISIBLE);
                holder.tvItemName.setText(items.get(position).Item_Name);
            } else if (items.get(position).itemCount == -1)
                holder.tvItemName.setText(items.get(position).Item_Name);
            else
                holder.tvItemName.setText(items.get(position).Item_Name + "  (" + items.get(position).itemCount + ")");

            if (items != null){
                if (items.get(position).getIngredients().size() > 0) {

                    String ingredient = "";
                    for (int i = 0; i < items.get(position).getIngredients().size(); i++) {

                        if (ingredient.matches(""))
                            ingredient = items.get(position).getIngredients().get(i).getName();
                        else
                            ingredient = ingredient + ", " + items.get(position).getIngredients().get(i).getName();
                    }

                    holder.tvItemIngredients.setText(ingredient);
                }
        }
        } catch (Exception e) {
            e.printStackTrace();
        }


            holder.tvItemName.setOnClickListener(new ItemCLickListner(position));
            holder.tvItemIngredients.setOnClickListener(new ItemCLickListner1(position));

    }

    @Override
    public int getItemCount() {
        if(items == null)
            return 0;
        else
            return items.size();
    }

    public interface ItemsClickListner {
        void onItemClick(String id, String barcode, boolean isMutiCorse);
    }
    public interface ItemsClickListner1 {
        void onItemClick(String id, String barcode, boolean isMutiCorse);
    }

    public static class ItemsAdapterViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvItemName;
        private final TextView tvItemIngredients;
        private final ImageView ivClose;


        public ItemsAdapterViewHolder(View itemView) {
            super(itemView);

            tvItemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvItemIngredients = (TextView) itemView.findViewById(R.id.tv_item_ingredients);
            ivClose = (ImageView) itemView.findViewById(R.id.iv_close);
        }
    }

    public void setOnItemPositionClickListner(ItemPositionClickListner onItemPositionClickListner) {
        this.itemPositionClickListner = onItemPositionClickListner;
    }

    public void setOnItemPositionClickListner1(ItemPositionClickListner1 onItemPositionClickListner1) {
        this.itemPositionClickListner1 = onItemPositionClickListner1;
    }
    public interface ItemPositionClickListner {

        void onItemClick(int position);

    }

    public interface ItemPositionClickListner1 {

        void onItemClick(int position);

    }

    private class ItemCLickListner extends RecyclerViewCallBack{

        public ItemCLickListner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {


            try {
                if(itemsClickListner != null)
                    itemsClickListner.onItemClick(items.get(position).ItemCd, items.get(position).BarCd, items.get(position).Is_MultiCourse);

                if (itemPositionClickListner != null) {
                    itemPositionClickListner.onItemClick(position);
                }

                McItemsAdater.this.selectedItemPosition = position;
            } catch (Exception e) {
                e.printStackTrace();
            }
//            notifyDataSetChanged();
        }
    }

    private class ItemCLickListner1 extends RecyclerViewCallBack{

        public ItemCLickListner1(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {


            try {
                if(itemsClickListner1 != null)
                    itemsClickListner1.onItemClick(items.get(position).ItemCd, items.get(position).BarCd, items.get(position).Is_MultiCourse);

                if (itemPositionClickListner1 != null) {
                    itemPositionClickListner1.onItemClick(position);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

//            notifyDataSetChanged();
        }
    }

    public void updateSelected(final int position){


        try {
            int size = this.items.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    if(i != position)
                        this.items.get(position).setSelected(false);
                    else
                        this.items.get(position).setSelected(true);
                }

                this.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
