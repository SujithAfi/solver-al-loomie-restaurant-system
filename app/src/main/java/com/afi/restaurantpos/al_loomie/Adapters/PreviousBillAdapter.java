package com.afi.restaurantpos.al_loomie.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PreviousBill;

/**
 * Created by AFI on 11/23/2015.
 */
public class PreviousBillAdapter extends RecyclerView.Adapter<PreviousBillAdapter.PreviousBillAdapterVH> {

    List<PreviousBill> previousBills;

    private itemCLickListner itemCLickListner;




    public interface itemCLickListner {
        void onItemClick(int position, PreviousBill previousBill);
    }


    public PreviousBillAdapter(List<PreviousBill> previousBills, PreviousBillAdapter.itemCLickListner itemCLickListner) {
        this.previousBills = previousBills;
        this.itemCLickListner = itemCLickListner;
    }

    @Override
    public PreviousBillAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_previous_bill, viewGroup, false);
        return new PreviousBillAdapterVH(v);
    }

    @Override
    public void onBindViewHolder(PreviousBillAdapterVH holder, final int position) {

        final PreviousBill previousBill = previousBills.get(position);

        holder.txtBillNumber.setText(previousBill.Cm_No.replace("#" , ""));
        holder.txtCounter.setText(previousBill.Cm_Counter);
        holder.txtNetAmount.setText(previousBill.Cm_NetAmt);

        if ( previousBill.Tab_Cd.length() == 0 ) {
            holder.cusOrTableKey.setText("Customer");
            holder.cusOrTableValue.setText(previousBill.Cus_Name);
        }
        else  {
            holder.cusOrTableKey.setText("Table");
            holder.cusOrTableValue.setText(previousBill.Tab_Cd);
        }

        if ( previousBill.Cm_Status.equals("P")) {
            holder.txtPayStatus.setText("Pending");
            holder.txtPayStatus.setBackgroundResource(R.drawable.highlighted_text_red);
            holder.txtPayStatus.setTextColor(Color.parseColor("#f2585c"));
        }

        else {
            holder.txtPayStatus.setText("Paid");
            holder.txtPayStatus.setBackgroundResource(R.drawable.highlighted_text_green);
            holder.txtPayStatus.setTextColor(Color.parseColor("#33c48f"));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( itemCLickListner != null)
                    itemCLickListner.onItemClick(position , previousBill);

            }
        });

    }

    @Override
    public int getItemCount() {
        return previousBills.size();
    }

    public static class PreviousBillAdapterVH extends RecyclerView.ViewHolder {

        public TextView txtBillNumber;
        public TextView txtCounter;
        public TextView cusOrTableKey;
        public TextView cusOrTableValue;
        public TextView txtNetAmount;
        public TextView txtPayStatus;

        public PreviousBillAdapterVH(View itemView) {
            super(itemView);

            txtBillNumber = (TextView ) itemView.findViewById(R.id.txtBillNumber);
            txtCounter = (TextView ) itemView.findViewById(R.id.txtCounter);
            cusOrTableKey = (TextView ) itemView.findViewById(R.id.cusOrTableKey);
            cusOrTableValue = (TextView ) itemView.findViewById(R.id.cusOrTableValue);
            txtNetAmount = (TextView ) itemView.findViewById(R.id.txtNetAmount);
            txtPayStatus = (TextView ) itemView.findViewById(R.id.txtPayStatus);

        }
    }

}
