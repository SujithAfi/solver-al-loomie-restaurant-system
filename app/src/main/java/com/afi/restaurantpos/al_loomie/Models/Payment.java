package com.afi.restaurantpos.al_loomie.Models;

/**
 * Created by AFI on 10/22/2015.
 */
public class Payment {

    public String Cm_Collected_Counter;
    public String Cm_Bc_Amt;
    public String Cm_Counter;
    public String Cm_No;
    public String Cm_Collected_User;
    public String Collect_Sft_No;
    public String Collect_Sft_Dt;
    public String Cus_Cd;
    public String payment_Type;
    public String credit_Card_No;
    public Boolean isCredit;
    public String acc_Code;
    public String acc_Name;
    public String room_No;
    public String checkin_Date;

    public String tax1_Rate;
    public String tax2_Rate;
    public String service_charge_Rate;

    public String tax1_value;
    public String tax2_value;
    public String service_value;

    public String Cm_DiscPer;
    public String Cm_DiscAmt;
    public String Cm_Tips_Amt;
    public String Cm_RoundAmt;

    public String Cm_NetAmt;
    public String Cm_Bc_Settle_Amt;
    public String Cm_Paid_Amt;

    public String date;
    public String time;

}
