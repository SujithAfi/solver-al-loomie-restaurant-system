package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.afi.restaurantpos.al_loomie.Models.History;
import com.afi.restaurantpos.al_loomie.R;

/**
 * Created by afi-mac-001 on 14/06/16.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHoulder> {

    /*
    * <Kot_Dt>3/24/2015 12:00:00 AM</Kot_Dt><Kot_No>##1052682</Kot_No><Kot_Time>12/30/1899 5:32:21 PM</Kot_Time>*/
    private SimpleDateFormat inputDateFormat = new SimpleDateFormat("M/d/yyyy k:mm:ss a");
    private SimpleDateFormat outputDateFormat = new SimpleDateFormat("d-MMM-yy");

    private List<History> histories;

    public HistoryAdapter(List<History> histories) {
        this.histories = histories;
    }

    @Override
    public HistoryViewHoulder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryViewHoulder(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_customer_history_list, parent, false));
    }

    @Override
    public void onBindViewHolder(HistoryViewHoulder holder, int position) {
        History history = histories.get(position);

        if(history.getKotNumber() != null)
            holder.txtKotNumber.setText(history.getKotNumber().replace("##" , ""));
        if(history.getTotalAmount() != null)
            holder.txtTotalAmount.setText(history.getTotalAmount());
        if(history.getTable() != null)
            holder.txtTableValue.setText(history.getTable());
        if(history.getCounter() != null)
            holder.txtCounterValue.setText(history.getCounter());
        try {
            Date inDate = inputDateFormat.parse(history.getKotDate());
            holder.txtDate.setText(outputDateFormat.format(inDate));
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return this.histories.size();
    }

    public static class HistoryViewHoulder extends RecyclerView.ViewHolder {

        private TextView txtKotNumber;
        private TextView txtCounterValue;
        private TextView txtTableValue;
        private TextView txtTotalAmount;
        private TextView txtDate;

        public HistoryViewHoulder(View itemView) {
            super(itemView);

            txtKotNumber = (TextView) itemView.findViewById(R.id.txtKotNumber);
            txtCounterValue = (TextView) itemView.findViewById(R.id.txtCounterValue);
            txtTableValue = (TextView) itemView.findViewById(R.id.txtTableValue);
            txtTotalAmount = (TextView) itemView.findViewById(R.id.txtTotalAmount);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
        }
    }

}
