package com.afi.restaurantpos.al_loomie.Dialogs;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;

import com.afi.restaurantpos.al_loomie.Adapters.ItemSelectionDialogFragmentAdapter;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemSelectionDialogFragment extends DialogFragment implements ItemDetailsFragment.ItemDdetailsCallBack {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ItemDetailsFragment.ItemDdetailsCallBack callBack;
    private String itemId;
    private String Barcode;
    private Toolbar toolbar;
    private FloatingActionButton fabCustomerDeatailsDown;

    public ItemSelectionDialogFragment() {
        // Required empty public constructor
    }


    public static ItemSelectionDialogFragment newInstance(String itemId, String Barcode) {
        ItemSelectionDialogFragment itemSelectionDialogFragment = new ItemSelectionDialogFragment();
        itemSelectionDialogFragment.Barcode = Barcode;
        itemSelectionDialogFragment.itemId = itemId;
        return itemSelectionDialogFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setCancelable(false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return getCustomView();
    }

    public ItemSelectionDialogFragment setCallBack(ItemDetailsFragment.ItemDdetailsCallBack callBack) {
        this.callBack = callBack;
        return this;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ViewCompat.animate(fabCustomerDeatailsDown)
                .scaleX(0)
                .scaleY(0)
                .setDuration(100)
                .start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabCustomerDeatailsDown.setVisibility(View.VISIBLE);
                fabCustomerDeatailsDown.clearAnimation();
                ViewCompat.animate(fabCustomerDeatailsDown)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator())
                        .start();

            }
        }, 1000);


    }

    @Override
    public void onResume() {
        super.onResume();

        viewPager.setAdapter(new ItemSelectionDialogFragmentAdapter(getChildFragmentManager(), itemId, Barcode).setCallBack(this));
        tabLayout.setupWithViewPager(viewPager);

    }

    private View getCustomView() {

        final View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_item_selection_dialog, null);
        viewPager = (ViewPager) view.findViewById(R.id.ViewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        fabCustomerDeatailsDown = (FloatingActionButton) view.findViewById(R.id.fabCustomerDeatailsDown);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        fabCustomerDeatailsDown.setVisibility(View.INVISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ItemSelectionDialogFragment.this.dismiss();
            }
        });
        toolbar.inflateMenu(R.menu.menu_select_item_details);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.action_select_Items) {
                    try {

                        Snackbar
                                .make(view, getResources().getString(R.string.item_saved), Snackbar.LENGTH_LONG)
                                .show();

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent("okButtonAction"));
                            }
                        }, 500);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });

        fabCustomerDeatailsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((SelectItemsActivity) getActivity()).setUpperFabAnimation(true, 1);
                ((SelectItemsActivity) getActivity()).animateBillFragment();
            }
        });




        return view;

    }

    @Override
    public void onItemSelected(SelectedItemDetails selecctedItemDetails, boolean isSuccess) {
        if (this.callBack != null) {
            this.callBack.onItemSelected(selecctedItemDetails, isSuccess);

            ItemSelectionDialogFragment.this.dismiss();
        }
    }

    @Override
    public void onCanceled() {

    }
}
