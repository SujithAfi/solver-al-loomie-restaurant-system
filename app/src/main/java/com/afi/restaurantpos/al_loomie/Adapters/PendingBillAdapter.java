package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by afi on 10/21/2015.
 */
public class PendingBillAdapter extends RecyclerView.Adapter<PendingBillAdapter.PendingBillAdapterVH> {

    private final Context context;
    private PendingBillSelectListner pendingBillSelectListner;

private List<PendingBill> pendingBillList;
public interface PendingBillSelectListner{
    void onBillselected(String BillNumber , PendingBill pendingBill);
    void onBilllongselected(String BillNumber , PendingBill pendingBill);
    void onBillCheckChange();
}

    public PendingBillAdapter(List<PendingBill> pendingBillList , Context context , PendingBillSelectListner pendingBillSelectListner) {
        this.pendingBillList = pendingBillList;
        this.pendingBillSelectListner = pendingBillSelectListner;
        this.context = context;
    }

    @Override
    public PendingBillAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_pending_bill, viewGroup, false);
        PendingBillAdapterVH vh = new PendingBillAdapterVH(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(PendingBillAdapterVH holder, final int position) {
        final PendingBill pendingBill = pendingBillList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pendingBillSelectListner != null)
                    pendingBillSelectListner.onBillselected(pendingBill.Cm_No , pendingBill);
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (pendingBillSelectListner != null)
                    pendingBillSelectListner.onBilllongselected(pendingBill.Cm_No, pendingBill);
                return  false;
                        }
        });
        holder.txtBillNumber.setText(pendingBill.Cm_No.replace("#" , ""));
        holder.txtCounter.setText(pendingBill.Cm_Counter);

        if(pendingBill.Cus_name != null && !pendingBill.Cus_name.matches("")){
            holder.trCust_Name.setVisibility(View.VISIBLE);
            holder.tvCust_Name.setText(pendingBill.Cus_name);
        }
        holder.tvTable.setText(pendingBill.Cus_Cd);
        holder.txtTotalAmount.setText(Utilities.getDefaultCurrencyFormat(pendingBill.Cm_TotAmt  , context));
        holder.txtDiscountAmount.setText(Utilities.getDefaultCurrencyFormat(pendingBill.Cm_DiscAmt  , context));
        holder.txtNetAmount.setText(Utilities.getDefaultCurrencyFormat(pendingBill.Cm_NetAmt  , context));
        holder.tvSalesMan.setText(pendingBill.Sman_Name);
        holder.tvTable.setText(pendingBill.Tab_Name);

       /* if (!(pendingBill.Cus_name == null)) {
            holder.txtCustomerCodeOrTableName.setText("Customer Code");
            holder.tvTable.setText(pendingBill.Cus_name);
        }
        else  {
            holder.txtCustomerCodeOrTableName.setText("Table Code");
            holder.tvTable.setText(pendingBill.Tab_Cd);
        }*/


    }

    @Override
    public int getItemCount() {
        return pendingBillList.size();
    }

public static class PendingBillAdapterVH extends RecyclerView.ViewHolder{

    private final TextView tvSalesMan;
    private final TextView tvCust_Name;
    private final TableRow trCust_Name;
    private TextView txtBillNumber;
    private TextView txtCounter;
    private TextView tvTable;
    private TextView txtTotalAmount;
    private TextView txtDiscountAmount;
    private TextView txtNetAmount;
    private TextView txtCustomerCodeOrTableName;

    public PendingBillAdapterVH(View itemView) {
        super(itemView);
        txtCounter = (TextView) itemView.findViewById(R.id.txtCounter);
        tvTable = (TextView) itemView.findViewById(R.id.tv_table_name);
        txtTotalAmount = (TextView) itemView.findViewById(R.id.txtTotalAmount);
        txtDiscountAmount = (TextView) itemView.findViewById(R.id.txtDiscountAmount);
        txtNetAmount = (TextView) itemView.findViewById(R.id.txtNetAmount);
        txtBillNumber = (TextView) itemView.findViewById(R.id.txtBillNumber);
//        txtCustomerCodeOrTableName = (TextView) itemView.findViewById(R.id.txtCustomerCodeOrTableName);
        tvSalesMan = (TextView) itemView.findViewById(R.id.txtSalesman);
        tvCust_Name = (TextView) itemView.findViewById(R.id.tv_customer_name);
        trCust_Name = (TableRow) itemView.findViewById(R.id.tr_customer);
    }
}

    public List<PendingBill> getAllSelectedBills(){
        List<PendingBill> selectedPendingBills = new ArrayList<>();
        for(PendingBill pendingBill : pendingBillList)
                selectedPendingBills.add(pendingBill);
        return selectedPendingBills;
    }

    public void setPendingBillSelectListner(PendingBillSelectListner pendingBillSelectListner) {
        this.pendingBillSelectListner = pendingBillSelectListner;
    }
}
