package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Recipe;

/**
 * Created by AFI on 10/12/2015.
 */
public class IngrediantsAdapter extends RecyclerView.Adapter<IngrediantsAdapter.IngrediantsAdapterVH> {

    private List<Recipe> recipes;

    public IngrediantsAdapter(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    @Override
    public IngrediantsAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_igrediants, viewGroup, false);
        return new IngrediantsAdapterVH(v);
    }

    @Override
    public void onBindViewHolder(IngrediantsAdapterVH holder, int position) {
        holder.name.setText(recipes.get(position).Name);
        holder.quantity.setText(recipes.get(position).Qty);
        holder.unit.setText(recipes.get(position).Unit);
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public static class IngrediantsAdapterVH extends RecyclerView.ViewHolder{

        public TextView name;
        public TextView quantity;
        public TextView unit;

        public IngrediantsAdapterVH(View itemView) {
            super(itemView);
            name  = (TextView) itemView.findViewById(R.id.txtName);
            quantity  = (TextView) itemView.findViewById(R.id.txtQuantity);
            unit  = (TextView) itemView.findViewById(R.id.txtUnit);
        }
    }

}
