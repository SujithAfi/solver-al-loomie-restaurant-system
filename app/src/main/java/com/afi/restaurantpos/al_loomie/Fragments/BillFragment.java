package com.afi.restaurantpos.al_loomie.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.BillAdapter;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Fragment to display the bill
 */
public class BillFragment extends Fragment {

    private RecyclerView rVBills;
    private BillAdapter billAdapter;
    Context context;
    private TextView txtTotalCost;
    private TextView txtTax1;
    private TextView txtTax2;
    private TextView txtGrandTotal;

    private boolean isTemporaryMode;

    private boolean isNewInvoiceMode = false;

    /**
     * Static method to create instance of the fragment
     * @param isNewInvoiceMode
     * @return
     */
    public static BillFragment newInstance( boolean isNewInvoiceMode ) {
        BillFragment fragment = new BillFragment();
        fragment.isNewInvoiceMode = isNewInvoiceMode;
        fragment.setIsNewInvoiceMode1(isNewInvoiceMode);
        return fragment;
    }

    public BillFragment() {
        billAdapter = new BillAdapter(getContext());
        billAdapter.setSelectedItemDetailses(ApplicationSingleton.getInstance().getSelectedItemDetails());
        billAdapter.setItemChange(new BillAdapter.onItemChange() {
            @Override
            public void onItemInvalidate() {
                caluculateCosts();
            }
        });

    }


    /**
     * Set the bill data
     * @param data
     */
    public void setData(List<SelectedItemDetails> data){
        billAdapter.setSelectedItemDetailses(data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context=getContext();
        View view = inflater.inflate(R.layout.fragment_bill, container, false);
        rVBills = (RecyclerView)view.findViewById(R.id.rVBill);
        txtTotalCost = (TextView) view.findViewById(R.id.txtBillTotal);
        txtGrandTotal = (TextView) view.findViewById(R.id.txtBillGrandTotal);
        txtTax1 = (TextView) view.findViewById(R.id.txtBillTax);
        txtTax2 = (TextView) view.findViewById(R.id.txtBillTax2);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rVBills.setHasFixedSize(true);
        rVBills.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rVBills.setAdapter(billAdapter);

        caluculateCosts();

        try {
            if(isNewInvoiceMode) {

                ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = ((BillAdapter.BillAdapterVH) viewHolder).position;
                        billAdapter.removeItemAt(position);
                    }
                };
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
                itemTouchHelper.attachToRecyclerView(rVBills);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Calclulate the total cost of the bills in this fragments
     */
    private void caluculateCosts(){
        try {
            ApplicationSingleton singleton = ApplicationSingleton.getInstance();
            if(isTemporaryMode) {
                // txtTotalCost.setText(String.valueOf(billAdapter.total()));
                txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(singleton.getTotalOrderCostTemp(), getContext()));
                try {
                    txtTax1.setText(Utilities.getDefaultCurrencyFormat(singleton.getTax1Temp(), getContext()));
                    txtTax2.setText(Utilities.getDefaultCurrencyFormat(singleton.getTax2Temp(), getContext()));
                    txtGrandTotal.setText(Utilities.getDefaultCurrencyFormat(singleton.getGrandTotalTemp(), getContext()));
                } catch (Exception e) {

                }
            }
            else{


                Double total= singleton.getTotalOrderCost();

                if(total == 0)//TODO
                    total = billAdapter.getTotalCost();
                Double  tax1 = Utilities.getPercentage(total, singleton.getTaxFraction1());
                Double tax2 = Utilities.getPercentage(total , singleton.getTaxFraction2());

                Double grandTotal = total + tax1 + tax2;
                //    txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(total, context));
                txtTotalCost.setText(Utilities.getDefaultCurrencyFormat((String.valueOf(total)) , getContext()));
                try {

                    txtTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, context));
                    txtTax2.setText(Utilities.getDefaultCurrencyFormat(tax2, context));
                    txtGrandTotal.setText(Utilities.getDefaultCurrencyFormat(grandTotal, context));
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setIsTemporaryMode(boolean isTemporaryMode) {
        this.isTemporaryMode = isTemporaryMode;
    }

    /**
     * Get all selected item details from the bill fragment
     * @return
     */
    public List<SelectedItemDetails> getAllSelectedItemDetailses(){
        return this.billAdapter.getSelectedItemDetailses();
    }

    public void setIsNewInvoiceMode1(boolean isNewInvoiceMode) {
        this.isNewInvoiceMode = isNewInvoiceMode;
        if(billAdapter != null)
            billAdapter.setIsNewOrderBill1(isNewInvoiceMode);
    }
}
