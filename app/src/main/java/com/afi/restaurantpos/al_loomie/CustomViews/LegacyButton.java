package com.afi.restaurantpos.al_loomie.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.afi.restaurantpos.al_loomie.R;

/**
 * Created by AFI on 11/12/2015.
 * <p>
 *     Button which having custom states
 * </p>
 * @author JITHIN
 *
 */
public class LegacyButton extends Button {

    // (Combination of) States are usually specified as an array.
    // Our custom attribute will be generated as R.attr.state_private_mode.
    // Note: This is in our app's scope.
    private static final int[] STATE_RED_MODE = {R.attr.state_red_mode};
    private static final int[] STATE_ORANGE_MODE = {R.attr.state_orange_mode};
    private static final int[] STATE_PINK_MODE = {R.attr.state_pink_mode};
    private static final int[] STATE_GREEN_MODE = {R.attr.state_green_mode};

    // The view needs a way to know if it's in private mode or not.
    private boolean isRedMode = false;
    private boolean isOrangeMode = false;
    private boolean isPinkMOde = false;
    private boolean isGreenMode = false;

    public LegacyButton(Context context) {
        super(context);
    }

    public LegacyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LegacyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        // Ask the parent to add its default states.
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);

        // If we are private, add the state to array of states.
        // If not added, the value will be treated as false.
        // mergeDrawableStates() takes care of resolving the duplicates.
        if (isRedMode)
            mergeDrawableStates(drawableState, STATE_RED_MODE);
        else if ( isOrangeMode)
            mergeDrawableStates(drawableState, STATE_ORANGE_MODE);
        else if ( isPinkMOde )
            mergeDrawableStates(drawableState, STATE_PINK_MODE);
        else
            mergeDrawableStates(drawableState , STATE_GREEN_MODE);

        // Return the new drawable state.
        return drawableState;
    }


    public void setIsRedMode(boolean isRedMode) {
        if( isRedMode != this.isRedMode) {
            this.isRedMode = isRedMode;
            this.isPinkMOde = false;
            this.isOrangeMode = false;
            this.isGreenMode = false;
            refreshDrawableState();
        }

    }

    public void setIsOrangeMode(boolean isOrangeMode) {
        if( isOrangeMode != this.isOrangeMode) {
            this.isOrangeMode = isOrangeMode;
            this.isRedMode = false;
            this.isPinkMOde = false;
            this.isGreenMode = false;
            refreshDrawableState();
        }
    }

    public void setIsPinkMOde(boolean isPinkMOde) {
        if ( isPinkMOde != this.isPinkMOde) {
            this.isPinkMOde = isPinkMOde;
            this.isRedMode = false;
            this.isOrangeMode = false;
            this.isGreenMode = false;
            refreshDrawableState();
        }
    }
    public void setIsGreenMode(boolean isGreenMode) {
        if ( isGreenMode != this.isGreenMode) {
            this.isGreenMode = isGreenMode;
            this.isRedMode = false;
            this.isOrangeMode = false;
            this.isPinkMOde = false;
            this.isGreenMode = true;
            refreshDrawableState();
        }
    }
}
