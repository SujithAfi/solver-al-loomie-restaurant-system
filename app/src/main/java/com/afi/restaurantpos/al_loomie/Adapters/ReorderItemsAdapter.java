package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.CallBacks.ItemClickCallback;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AFI on 10/7/2015.
 */
public class ReorderItemsAdapter extends RecyclerView.Adapter<ReorderItemsAdapter.BillAdapterVH>{

    private List<PendingKotItem> selectedItemDetailses;
    private ItemClickCallback itemClickCallback;
    private onItemChange itemChange;
    private Context context;
    private boolean isNewOrderBill = false;

    public ReorderItemsAdapter(Context context) {
        this.context = context;
        selectedItemDetailses = new ArrayList<>();
    }

    public interface onItemChange{
        void onItemInvalidate(int pos , boolean ischecked);
    }
    public ReorderItemsAdapter(List<PendingKotItem> selectedItemDetailses , ItemClickCallback callback) {
        this.selectedItemDetailses = selectedItemDetailses;
        this.itemClickCallback = callback;
    }

    @Override
    public BillAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_reorder, viewGroup, false);
        BillAdapterVH billAdapterVH = new BillAdapterVH(v);
        return billAdapterVH;
    }

    @Override
    public void onBindViewHolder(BillAdapterVH holder, final int position) {
        /*holder.itemView.setOnClickListener(new ItemClickListner(position, selectedItemDetailses.get(position).Itm_Name));
        holder.cbSelected.setOnClickListener(new ItemClickListner(position, selectedItemDetailses.get(position).Itm_Name));*/
        holder.txtItemName.setText(selectedItemDetailses.get(position).Itm_Name);
        holder.txtItemQuantity.setText(Utilities.getItemQuantityFormat(selectedItemDetailses.get(position).KotD_Qty));
        if(selectedItemDetailses.get(position).KotD_Rate != null && !selectedItemDetailses.get(position).KotD_Rate.matches(""))
            holder.txtItemRate.setText(Utilities.getDefaultCurrencyFormat(selectedItemDetailses.get(position).KotD_Rate + "", holder.txtItemAmount.getContext()));
        if(selectedItemDetailses.get(position).KotD_Amt != null && !selectedItemDetailses.get(position).KotD_Amt.matches(""))
            holder.txtItemAmount.setText(Utilities.getDefaultCurrencyFormat(selectedItemDetailses.get(position).KotD_Amt + "", holder.txtItemAmount.getContext()));
        if(selectedItemDetailses.get(position).isSelected)
            holder.cbSelected.setChecked(true);
        else
            holder.cbSelected.setChecked(false);

        holder.cbSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (itemChange != null) {
                    itemChange.onItemInvalidate(position , isChecked);
                }
            }
        });

        holder.position = position;

    }

    @Override
    public int getItemCount() {
        if(selectedItemDetailses == null)
            return  0;
        return selectedItemDetailses.size();
    }


    public void addToBillDetails(PendingKotItem selectedItemDetails){
        selectedItemDetailses.add(selectedItemDetails);
        this.notifyDataSetChanged();
    }

    public static class BillAdapterVH extends RecyclerView.ViewHolder{
        private final CheckBox cbSelected;
        public int position;
        private TextView txtItemName;
        private TextView txtItemQuantity;
        private TextView txtItemRate;
        private TextView txtItemAmount;
        private TableRow layoutt;

        public BillAdapterVH(View itemView) {
            super(itemView);
            txtItemName = (TextView) itemView.findViewById(R.id.txtItemName);
            txtItemQuantity = (TextView) itemView.findViewById(R.id.txtItemQuantity);
            txtItemRate = (TextView) itemView.findViewById(R.id.txtItemRate);
            txtItemAmount = (TextView) itemView.findViewById(R.id.txtItemAmount);
            layoutt= (TableRow) itemView.findViewById(R.id.row);
            cbSelected = (CheckBox) itemView.findViewById(R.id.cb_item);
        }
    }

    public void setSelectedItemDetailses(List<PendingKotItem> selectedItemDetailses) {
        this.selectedItemDetailses = selectedItemDetailses;
    }

    public List<PendingKotItem> getSelectedItemDetailses(){
        return this.selectedItemDetailses;
    }

    private class ItemClickListner implements View.OnClickListener{
        private int position ;

        private String Id;

        public ItemClickListner(int position, String id) {
            this.position = position;
            Id = id;

        }

        @Override
        public void onClick(View v) {

            if (itemChange != null) {
                itemChange.onItemInvalidate(position , false);
            }

        }
    }

    public void removeItemAt(int position){
        if (isNewOrderBill) {


            selectedItemDetailses.remove(position);
            ApplicationSingleton.getInstance().calculateTotalCost();
            notifyDataSetChanged();
            if (itemChange != null) {
                itemChange.onItemInvalidate(position , false);
            }

        }
    }
    public void setItemChange(onItemChange itemChange) {
        this.itemChange = itemChange;
    }

    public void setIsNewOrderBill1(boolean isNewOrderBill) {
        this.isNewOrderBill = isNewOrderBill;
    }

    public double getTotalCost(){
        double total = 0;
        for( PendingKotItem details : selectedItemDetailses){
            total += Double.parseDouble(details.KotD_Amt);
        }
        return total;
    }
}
