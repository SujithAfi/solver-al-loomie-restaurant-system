package com.afi.restaurantpos.al_loomie.Fragments;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.BillAdapter;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Fragment to display the bill
 */
public class BillDialogFragment extends DialogFragment {

    Context context;
    private RecyclerView rVBills;
    private BillAdapter billAdapter;
    private TextView txtTotalCost;
    private TextView txtTax1;
    private TextView txtTax2;
    private TextView txtGrandTotal;

    private boolean isTemporaryMode;

    private boolean isNewInvoiceMode = false;
    private TextView tvServiceCharge;
    private TextView tvTax1Head;
    private TextView tvTax2Head;
    private TextView tvServiceChargeHead;

    public BillDialogFragment() {
        try {
            billAdapter = new BillAdapter(getContext());
            billAdapter.setSelectedItemDetailses(ApplicationSingleton.getInstance().getSelectedItemDetails());
            billAdapter.setItemChange(new BillAdapter.onItemChange() {
                @Override
                public void onItemInvalidate() {
                    caluculateCosts();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Static method to create instance of the fragment
     *
     * @param isNewInvoiceMode
     * @return
     */
    public static BillDialogFragment newInstance(boolean isNewInvoiceMode) {
        BillDialogFragment fragment = new BillDialogFragment();
        fragment.isNewInvoiceMode = isNewInvoiceMode;
        fragment.setIsNewInvoiceMode1(isNewInvoiceMode);
        return fragment;
    }

    /**
     * Set the bill data
     *
     * @param data
     */
    public void setData(List<SelectedItemDetails> data) {
        billAdapter.setSelectedItemDetailses(data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setView(getCView(LayoutInflater.from(getContext())))
                .create();
    }

    private View getCView(LayoutInflater layoutInflater) {

        View view = layoutInflater.inflate(R.layout.fragment_bill, null, false);
        rVBills = (RecyclerView) view.findViewById(R.id.rVBill);
        txtTotalCost = (TextView) view.findViewById(R.id.txtBillTotal);
        txtGrandTotal = (TextView) view.findViewById(R.id.txtBillGrandTotal);
        txtTax1 = (TextView) view.findViewById(R.id.txtBillTax);
        txtTax2 = (TextView) view.findViewById(R.id.txtBillTax2);

        tvServiceCharge = (TextView) view.findViewById(R.id.tv_service_charge);

        tvTax1Head = (TextView) view.findViewById(R.id.tv_tax1_head);
        tvTax2Head = (TextView) view.findViewById(R.id.tv_tax2_head);
        tvServiceChargeHead = (TextView) view.findViewById(R.id.tv_service_charge_head);


        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillDialogFragment.this.dismiss();
            }
        });
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        rVBills.setHasFixedSize(true);
        rVBills.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rVBills.setAdapter(billAdapter);

        caluculateCosts();

        if (isNewInvoiceMode) {

            ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int position = ((BillAdapter.BillAdapterVH) viewHolder).position;
                    billAdapter.removeItemAt(position);
                }
            };
            try {
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
                itemTouchHelper.attachToRecyclerView(rVBills);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * Calclulate the total cost of the bills in this fragments
     */
    private void caluculateCosts() {
        try {
            ApplicationSingleton singleton = ApplicationSingleton.getInstance();
            if (isTemporaryMode) {
                txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(singleton.getTotalOrderCostTemp(), getContext()));
                try {
                    txtTax1.setText(Utilities.getDefaultCurrencyFormat(singleton.getTax1Temp(), getContext()));
                    txtTax2.setText(Utilities.getDefaultCurrencyFormat(singleton.getTax2Temp(), getContext()));
                    txtGrandTotal.setText(Utilities.getDefaultCurrencyFormat(singleton.getGrandTotalTemp(), getContext()));
                } catch (Exception e) {

                }
            } else {


                if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null).matches(""))
                    tvTax1Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null));
                if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null).matches(""))
                    tvTax2Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null));
                Double total = singleton.getTotalOrderCost();

                if (total == 0)//TODO
                    total = billAdapter.getTotalCost();
                Double tax1 = Utilities.getPercentage(total, singleton.getTaxFraction1());
                Double tax2 = Utilities.getPercentage(total, singleton.getTaxFraction2());
                Double serviceCharge = Utilities.findServiceCharge(getContext(), total);

                Double grandTotal = total + tax1 + tax2 + serviceCharge;
                txtTotalCost.setText(Utilities.getDefaultCurrencyFormat((String.valueOf(total)), getContext()));
                try {
                    txtTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));
                } catch (Exception e) {

                }
                try {
                    txtTax2.setText(Utilities.getDefaultCurrencyFormat(tax2, getContext()));

                } catch (Exception e) {

                }
                try {
                    tvServiceCharge.setText(Utilities.getDefaultCurrencyFormat(serviceCharge, getContext()));

                } catch (Exception e) {

                }
                try {
                    txtGrandTotal.setText(Utilities.getDefaultCurrencyFormat(grandTotal, getContext()));
                } catch (Exception e) {
                    Log.e("sdfds", "ssdff");
                }



            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setIsTemporaryMode(boolean isTemporaryMode) {
        this.isTemporaryMode = isTemporaryMode;
    }

    /**
     * Get all selected item details from the bill fragment
     *
     * @return
     */
    public List<SelectedItemDetails> getAllSelectedItemDetailses() {
        return this.billAdapter.getSelectedItemDetailses();
    }

    public void setIsNewInvoiceMode1(boolean isNewInvoiceMode) {
        try {
            this.isNewInvoiceMode = isNewInvoiceMode;
            if (billAdapter != null)
                billAdapter.setIsNewOrderBill1(isNewInvoiceMode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
