package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.afi.restaurantpos.al_loomie.Fragments.TableBillOptions;
import com.afi.restaurantpos.al_loomie.Fragments.TableKotOptions;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;

import java.util.List;
import java.util.Map;


/**
 * Created by AFI on 11/25/2015.
 */
public class TableOptionsAdapter extends FragmentStatePagerAdapter {

    private Map<String, List<PendingKotItem>> data;

    private TableKotOptions tableKotOptions;
    private TableBillOptions tableBillOptions;

    public TableOptionsAdapter(FragmentManager fm, Map<String, List<PendingKotItem>> tableId) {
        super(fm);
        this.data = tableId;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0 : tableKotOptions =  TableKotOptions.newInstance(data.get("kot") , null);
                return tableKotOptions;
            case 1 : tableBillOptions =  TableBillOptions.newInstance(data.get("bill"));
                return tableBillOptions;
            default:return null;

        }
    }

    public void refreshData(Map<String, List<PendingKotItem>> dat) {
        if ( tableBillOptions != null )
            tableBillOptions.bills = dat.get("bill");
        if ( tableKotOptions != null )
            tableKotOptions.kotumbers = dat.get("kot");
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0 : return "Kot's on Table";
            case 1 : return "Bills on Table";
            default: return "";
        }
    }


}
