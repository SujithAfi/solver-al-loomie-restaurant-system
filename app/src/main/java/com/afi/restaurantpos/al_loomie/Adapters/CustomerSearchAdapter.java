package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;

/**
 * Created by AFI on 1/22/2016.
 */
public class CustomerSearchAdapter extends ArrayAdapter<Customer> {


    String filterString = "";

    private List<Customer> allData = new ArrayList<>();
    private List<Customer> suggestions = new ArrayList<>();

    public CustomerSearchAdapter(Context context, List<Customer> objects) {
        super(context, R.layout.child_customer_list, objects);
        allData.addAll(objects);
        suggestions.clear();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Viewholder viewholder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.child_customer_list, parent, false);
            viewholder = new Viewholder();
            viewholder.txtMobile = (TextView) convertView.findViewById(R.id.txtMobile);
            viewholder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            convertView.setTag(viewholder);
        } else
            viewholder = (Viewholder) convertView.getTag();
        String itemValue = getItem(position).getId();
        int startPos = itemValue.toLowerCase(Locale.US).indexOf(filterString.toLowerCase(Locale.US));
        int endPos = startPos + filterString.length();

        if (startPos != -1) // This should always be true, just a sanity check
        {
            Spannable spannable = new SpannableString(itemValue);
            ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{Color.BLUE});
            TextAppearanceSpan highlightSpan = new TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null);

            spannable.setSpan(highlightSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            viewholder.txtMobile.setText(spannable);
        } else
            viewholder.txtMobile.setText(itemValue);

        viewholder.txtName.setText(getItem(position).getName());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class Viewholder {
        private TextView txtMobile;
        private TextView txtName;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint == null ? "" : constraint;
            filterString = constraint.toString();
            if (constraint != null)
                suggestions.clear();
            for (Customer customer : allData) {
                if (customer.getId().startsWith(constraint.toString())) {
                    suggestions.add(customer);
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = suggestions;
            filterResults.count = suggestions.size();
            return filterResults;
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Customer) resultValue).getId();
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null) {
                clear();
                for (Customer customer : (ArrayList<Customer>) results.values) {
                    add(customer);
                }
                notifyDataSetChanged();
            }
        }
    };
}
