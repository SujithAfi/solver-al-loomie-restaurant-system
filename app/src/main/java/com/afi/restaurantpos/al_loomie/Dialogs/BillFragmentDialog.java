package com.afi.restaurantpos.al_loomie.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.BillingAdapter;
import com.afi.restaurantpos.al_loomie.Adapters.ReorderItemsAdapter;
import com.afi.restaurantpos.al_loomie.Models.FinalAmount;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.List;


/**
 * Created by afi on 10/21/2015.
 */
public class BillFragmentDialog extends DialogFragment {
    private ViewGroup mContainer;
    private View mLayout;
    private Toolbar toolbar;
    private RecyclerView rVBilling;
    private BillingAdapter billAdapter;
    private BillListner billListner;
    private String newBillNumber = "";

    private TextView txtTotal;
    private TextView txtTax1;
    private TextView txtTax2;
    private TextView txtNetAmout;
    private TextView tvServiceCharge;
    private TextView tvTax1Head;
    private TextView tvTax2Head;
    private TextView tvServiceChargeHead;
    private List<BillItem> mbillItems;
    private TextView tvFocAmount;


    public interface BillListner{
        void onPositive(List<BillItem> mbillItems , FinalAmount finalAmount);
        void onNegative();
    }

    public BillFragmentDialog() {
    }

    public BillFragmentDialog setBillListner(BillListner billListner) {
        this.billListner = billListner;
        return this;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(billListner != null) {
                        String focAmount = tvFocAmount.getText().toString();
                        String serviceAmount = tvServiceCharge.getText().toString();
                        String tax1Amount = txtTax1.getText().toString();
                        String tax2Amount = txtTax2.getText().toString();
                        String netAmount = txtNetAmout.getText().toString();
                        FinalAmount finalAmount = new FinalAmount();
                            finalAmount.focAmount = focAmount;
                            finalAmount.serviceAmount = serviceAmount;
                            finalAmount.tax1Amount = tax1Amount;
                            finalAmount.tax2Amount = tax2Amount;
                            finalAmount.netAmount = netAmount;
                        billListner.onPositive(mbillItems , finalAmount);
                        BillFragmentDialog.this.dismiss();
                    }
                }
            });

            Button negativeButton = d.getButton(Dialog.BUTTON_NEGATIVE);
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(billListner != null) {
                        billListner.onNegative();
                        BillFragmentDialog.this.dismiss();
                    }
                }
            });

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }


    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_billling_dialog, container, false);
        toolbar = (Toolbar)mLayout.findViewById(R.id.bill_toolbar);
        toolbar.setTitle("Bill");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BillFragmentDialog.this.dismiss();
            }
        });
        rVBilling = (RecyclerView)mLayout.findViewById(R.id.rVBilling);
        rVBilling.setHasFixedSize(true);
        rVBilling.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        txtTotal = (TextView) mLayout.findViewById(R.id.txtBillTotal);
        txtTax1 = (TextView) mLayout.findViewById(R.id.txtBillTax);
        txtTax2 = (TextView) mLayout.findViewById(R.id.txtBillTax2);

        tvTax1Head = (TextView) mLayout.findViewById(R.id.tv_tax1);
        tvTax2Head = (TextView) mLayout.findViewById(R.id.tv_tax2);

        tvServiceCharge = (TextView) mLayout.findViewById(R.id.tv_service_charge_rate);
        tvServiceChargeHead = (TextView) mLayout.findViewById(R.id.tv_service_charge);

        txtNetAmout = (TextView) mLayout.findViewById(R.id.txtBillGrandTotal);

        tvFocAmount = (TextView) mLayout.findViewById(R.id.tv_foc);


        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setTitle(newBillNumber);
        billAdapter.setItemChange(new BillingAdapter.onItemChange() {
            @Override
            public void onItemInvalidate(int pos , boolean ischecked) {

                mbillItems.get(pos).isFoc = ischecked;
                setAmountDetails();
//                            reorderKotAdapter.notifyDataSetChanged();
            }
        });
        rVBilling.setAdapter(billAdapter);
        setAmountDetails();
        this.setCancelable(true);
    }

    /**
     * Calculate and display total amunt in the bill
     */
    private void setAmountDetails() {
        try {
            ApplicationSingleton singleton = ApplicationSingleton.getInstance();
            double focAmount = 0;
            if(mbillItems != null){

                if(mbillItems.size() > 0){

                    for (BillItem calItems : mbillItems){

                        if(calItems.isFoc)
                            focAmount += Double.parseDouble(calItems.Cmd_Amt);
                    }

                }
            }
            tvFocAmount.setText(Utilities.getDefaultCurrencyFormat(focAmount , getContext()));
            double cost = (billAdapter.getTotalAmount() - focAmount);
            //double cost =Double.parseDouble(Utilities.getDefaultCurrencyFormat(singleton.getTotalOrderCostTemp(), getContext()));
            double tax1 = Utilities.findTax1(getContext(), cost);
            double tax2 = Utilities.findTax2(getContext(), cost);
            double serviceCharge = Utilities.findServiceCharge(getContext(), cost);
            double totCost = cost + tax1 + tax2 + serviceCharge;

        /*if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null).matches(""))
            tvTax1Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null));
        if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null).matches(""))
            tvTax2Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null));*/

            if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null).matches(""))
                tvTax1Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null) + " (" + Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1, "0") + "%)");
            if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null).matches(""))
                tvTax2Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null) + " (" + Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2, "0") + "%)");
            tvServiceChargeHead.setText("Service Charge (" +  Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_RATE, "0") + "%)");

            txtTotal.setText(Utilities.getDefaultCurrencyFormat(billAdapter.getTotalAmount()  , getContext()));
            txtTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));
            txtTax2.setText(Utilities.getDefaultCurrencyFormat(tax2 , getContext()));
            tvServiceCharge.setText(Utilities.getDefaultCurrencyFormat(serviceCharge , getContext()));
            txtNetAmout.setText(Utilities.getDefaultCurrencyFormat(totCost , getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Set items to be addded on the bills
     * @param billItems
     * @return
     */
    public BillFragmentDialog setBillItems(List<BillItem> billItems) {
        mbillItems = billItems;
        billAdapter = new BillingAdapter(billItems);

        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_BILLING_ADDORSAVE, false))
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                    .setPositiveButton("Save and Print Bill" , null)
                    .setNegativeButton("Cancel", null)
                    .create();
        else
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
//                    .setPositiveButton("Save and Print Bill" , null)
                    .setNegativeButton("Cancel", null)
                    .create();
    }

    /**
     * Set a new Bill Number
     * @param newBillNumber
     * @return
     */
    public BillFragmentDialog setNewBillNumber(String newBillNumber) {
        this.newBillNumber = newBillNumber;
        return this;
    }
}

