package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.afi.restaurantpos.al_loomie.Fragments.CreateCustomerFragment;
import com.afi.restaurantpos.al_loomie.Fragments.CustomersFragment;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by afi-mac-001 on 13/06/16.
 */
public class CustomersDialogAdapter extends FragmentPagerAdapter {

    private final Context context;

    public CustomersDialogAdapter(FragmentManager fm , Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CustomersFragment();
            case 1:
                return new CreateCustomerFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {

        if(!Utilities.getSharedPreferences(context).getBoolean(Constants.UR_CUSTOMER_ADDORSAVE, false))
            return 1;
        else
            return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Customers";
            case 1:
                return "New customer";
            default:
                return super.getPageTitle(position);
        }

    }
}
