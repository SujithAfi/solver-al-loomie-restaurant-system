package com.afi.restaurantpos.al_loomie;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.LinearInterpolator;

import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.afi.restaurantpos.al_loomie.Dialogs.CustomerDetailsFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.ItemDetailsFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.ItemSelectionDialogFragment;
import com.afi.restaurantpos.al_loomie.Fragments.BillDialogFragment;
import com.afi.restaurantpos.al_loomie.Fragments.FoodItemsFragment;
import com.afi.restaurantpos.al_loomie.Fragments.SearchFragment;
import com.afi.restaurantpos.al_loomie.Fragments.UserDetailsFragment;
import com.afi.restaurantpos.al_loomie.Models.KotContainer;
import com.afi.restaurantpos.al_loomie.Models.KotItem;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.RetrofitModels.KotSaveResponse;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class SelectItemsActivity extends AppCompatActivity implements ItemDetailsFragment.ItemDdetailsCallBack {

    private static final int BILL_FRAGMENT = 0;
    private static final int CUSTOMER_FRAGMENT = 1;
    private static final int MAIN_ACTIVITY = 2;
    private static final int SEARCH_FRAGMENT = 3;
    private static final int ITEM_FRAGMENT = 3;
    public boolean clickLock = false;
    com.afi.restaurantpos.al_loomie.Fragments.ItemFragment itemFragmentDialog;
    SearchView searchView;
    private KotContainer kotContainer;
    private SharedPreferences sharedPreferences;
    private String searchQuery = "";
    private boolean isSearchViewFocused;
    private Toolbar mToolbar;
    private FoodItemsFragment foodItemsFragment;
    private FloatingActionButton fabCustomerDeatailsDown;
    private int currentFragmentstate = MAIN_ACTIVITY;
    private SearchFragment searchFragment;
    private MenuItem menuItemSaveBill;
    private MenuItem menuCancelKot;
    private boolean isSearchBarShowing = false;
    private Customer customer;
    private Table table;
    private String resvCode;
    private boolean isReservationKot = false;
    private Reservation reservation;
    private boolean makeKOT = false;
    private String kotDate;
//    private boolean completedFlag = true;
    private boolean savingItems = false;
    private String resvGuestCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_items);
        try {

            if (getIntent().getBooleanExtra("hasCustomer", false))
                this.customer = (Customer) getIntent().getSerializableExtra("customer");
                this.table = (Table) getIntent().getSerializableExtra("table");
                this.isReservationKot = getIntent().getBooleanExtra("isReservationKot" , false);
                this.kotDate = getIntent().getStringExtra("kotDate");

            if(getIntent().getBooleanExtra("isReservationItemEdit" , false)) {
                this.kotDate = getIntent().getStringExtra("resvDate");
                makeKOT = getIntent().getBooleanExtra("makeKOT", false);
                this.reservation = (Reservation) getIntent().getSerializableExtra("reservation");
                if(reservation != null) {
                    this.resvCode = reservation.getDocNumber();
                    this.resvGuestCount = reservation.getGuestCount();


                    if(reservation.getItemdetails().size() > 0) {
                        ArrayList<SelectedItemDetails> selectedItems = new ArrayList<SelectedItemDetails>();
                        for (int i = 0; i < reservation.getItemdetails().size(); i++) {
                            SelectedItemDetails selItemDetails = new SelectedItemDetails();
                            selItemDetails.itemName = reservation.getItemdetails().get(i).getItm_Name();
                            selItemDetails.barcode = reservation.getItemdetails().get(i).getBarcode();
                            selItemDetails.itemCode = reservation.getItemdetails().get(i).getItm_Cd();
                            selItemDetails.Unit_Fraction = reservation.getItemdetails().get(i).getUnit_Fraction();
                            if(reservation.getItemdetails().get(i).getKotD_Rate() != null) {
                                selItemDetails.itemUnitCost = Double.parseDouble(reservation.getItemdetails().get(i).getKotD_Rate());
                                selItemDetails.Itm_UCost = reservation.getItemdetails().get(i).getKotD_Rate();
                            }
                            if(reservation.getItemdetails().get(i).getKotD_Qty() != null)
                                selItemDetails.itemQuantity = Double.parseDouble(reservation.getItemdetails().get(i).getKotD_Qty());
                            if(reservation.getItemdetails().get(i).getKotD_Amt() != null)
                                selItemDetails.itemTotalCost = Double.parseDouble(reservation.getItemdetails().get(i).getKotD_Amt());
                            selItemDetails.isMultiC = reservation.getItemdetails().get(i).getIsSubitem();
                            selItemDetails.isSubitem = reservation.getItemdetails().get(i).getIsSubitem();
                            if(reservation.getItemdetails().get(i).getKotD_Remarks() != null || !reservation.getItemdetails().get(i).getKotD_Remarks().matches(""))
                                selItemDetails.modificationRemarks = reservation.getItemdetails().get(i).getKotD_Remarks();
                            else
                                selItemDetails.modificationRemarks = "";

                            if(selItemDetails != null) {
                                selectedItems.add(selItemDetails);
                            }

                        }

                        if(selectedItems.size() > 0) {
                            ApplicationSingleton.getInstance().setSelectedItemDetailses(selectedItems);

                        }


                    }
                    else
                        ApplicationSingleton.getInstance().setSelectedItemDetailses(new ArrayList<SelectedItemDetails>());

                }
            }
            else if(getIntent().getBooleanExtra("isReservationKot" , false)) {
                this.resvCode = getIntent().getStringExtra("resvId");
                this.kotDate = getIntent().getStringExtra("resvDate");
            }

        } catch (Exception e) {

        }
        Utilities.initURL(this);
        Utilities.initKitkatStatusbarTransparancy(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        fabCustomerDeatailsDown = (FloatingActionButton) findViewById(R.id.fabCustomerDeatailsDown);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Menu");
        foodItemsFragment = FoodItemsFragment.newInstance(kotDate);
        addFragment(foodItemsFragment, "sdffsfd");
        fabCustomerDeatailsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (customer != null)
                    CustomerDetailsFragment.newInstance(customer , false).show(getSupportFragmentManager(), "Thsjkd");
            }
        });

        savingItems = false;


    }


    public void showItemDetailsFragment(String itemId , String barcode , SearchFragment searchFragment){
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();

            ItemSelectionDialogFragment.newInstance(itemId, barcode).setCallBack(this).show(fragmentManager, "item");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public void showItemFragment(String itemId , String name){
        currentFragmentstate = ITEM_FRAGMENT;
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        itemFragmentDialog = com.afi.restaurantpos.al_loomie.Fragments.ItemFragment.newInstance(itemId , name , kotDate);
        itemFragmentDialog.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyMaterialTheme_AppBarOverlay);
        itemFragmentDialog.show(fragmentManager, "Items");

    }

    public void ShowCategoryNotFoundDialog(){

        new MaterialDialog.Builder(SelectItemsActivity.this)
                .title("No category found.")
                .content("You can go back to the previous Page.")
                .titleColorRes(R.color.black)
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {

                        finish();
                    }
                })
                .cancelable(false).build().show();
    }




    @Override
    public void onItemSelected(SelectedItemDetails selecctedItemDetails, boolean isSuccess) {

        ApplicationSingleton.getInstance().addToSelectedItems(selecctedItemDetails);
        if(foodItemsFragment != null)
            foodItemsFragment.addToSubBillData(selecctedItemDetails);

    }

    @Override
    public void onCanceled() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_select_items, menu);

        menuItemSaveBill = menu.findItem(R.id.menu_option_save);
        menuCancelKot = menu.findItem(R.id.menu_option_cancel);
        menuItemSaveBill.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(!isReservationKot)
                    saveKot();
                else
                    saveReservationItems();
                return true;
            }
        });
        menuCancelKot.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                cancelKot();
                return false;
            }
        });



        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("onQueryTextSubmit", query + " ; ");
                searchQuery = query + "";
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.length() > 0)
                    searchQuery = newText + "";

                if (searchFragment != null)
                    searchFragment.searchWithQuery(newText);
                return false;
            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.e("onFocusChange", hasFocus + " ; ");
                if (hasFocus)
                    switchSearchFragment1(hasFocus);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.e("onClose", "onClose");
                switchSearchFragment1(false);
                return false;
            }
        });

        return true;
    }

    private void cancelKot() {

        List<SelectedItemDetails> list = ApplicationSingleton.getInstance().getSelectedItemDetails();

        if (list.isEmpty()){
            SelectItemsActivity.this.setResult(RESULT_CANCELED);
            finish();
            Intent i = new Intent(SelectItemsActivity.this, OrderDetails.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        else {
            try {
                new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog))
                        .setMessage("Calcel current order ?")
                        .setMessage("OK to cancel the order")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SelectItemsActivity.this.setResult(RESULT_CANCELED);
                                finish();
                                Intent i = new Intent(SelectItemsActivity.this, OrderDetails.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("Cancel" , null).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void saveKot() {
        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else {
        if (!savingItems) {
            savingItems = true;
            try {
                List<SelectedItemDetails> list = ApplicationSingleton.getInstance().getSelectedItemDetails();
                if (list.isEmpty()) {
                    Utilities.showSnackBar("No Items", this);
                    savingItems = false;
                    return;
                }
                switch (ApplicationSingleton.getInstance().getKotOrderType()) {
                    case NEW_KOT:
                        final MaterialDialog dialogSave = new MaterialDialog.Builder(this)
                                .content(getResources().getString(R.string.saving_kot))
                                .progress(true, 0)
                                .cancelable(false)
                                .build();


                        sharedPreferences = Utilities.getSharedPreferences(getApplicationContext());
                        kotContainer = new KotContainer();
                        processItemDetaiils();

                        dialogSave.show();
                        Utilities.getRetrofitWebService(getBaseContext()).saveKot(kotContainer).enqueue(new Callback<KotSaveResponse>() {
                            @Override
                            public void onResponse(Response<KotSaveResponse> response, Retrofit retrofit) {
                                dialogSave.dismiss();
                                KotSaveResponse kotSaveResponse = response.body();
                                if (kotSaveResponse.response_code) {
                                    //  Utilities.showToast(getResources().getString(R.string.kot_saved), SelectItemsActivity.this);
                                    new MaterialDialog.Builder(SelectItemsActivity.this)
                                            .content(getResources().getString(R.string.kot_saved))
                                            .cancelable(false)
                                            .positiveText("Ok")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                    finish();
                                                    if (!(ApplicationSingleton.getInstance().getKotType() == Constants.ORDER_TYPE_DINE_IN)) {
//                                                        completedFlag = true;
                                                        savingItems = false;
                                                        Intent i = new Intent(SelectItemsActivity.this, OrderDetails.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(i);
                                                    }
                                                }
                                            })
                                            .build().show();
                                } else {
                                    savingItems = false;
                                    Utilities.showToast(getResources().getString(R.string.kot_not_saved), SelectItemsActivity.this);
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                savingItems = false;
                                dialogSave.dismiss();
                            }
                        });

//                        }

                        break;
                    case MODIFY_KOT:// TODO: 10/19/2015
                        final MaterialDialog dialogModify = new MaterialDialog.Builder(this)
                                .content(getResources().getString(R.string.modifying_kot))
                                .progress(true, 0)
                                .cancelable(false)
                                .build();


                        if (!Utilities.isNetworkConnected(this))
                            Utilities.createNoNetworkDialog(this);
                        else {

                            dialogModify.show();
                            Utilities.getRetrofitWebService(getApplicationContext()).updateKot(null).enqueue(new Callback<KotSaveResponse>() {
                                @Override
                                public void onResponse(Response<KotSaveResponse> response, Retrofit retrofit) {
                                    dialogModify.cancel();

                                    resetOrderDetails();

                                    KotSaveResponse kotSaveResponse = response.body();
                                    if (kotSaveResponse.response_code)
                                        Utilities.showToast(getResources().getString(R.string.kot_saved), SelectItemsActivity.this);
                                    else
                                        Utilities.showToast(getResources().getString(R.string.kot_not_saved), SelectItemsActivity.this);

                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    dialogModify.cancel();
                                }
                            });


                        }
                        break;
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    }

    public void saveReservationItems() {
        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else {
        if (!savingItems) {
            savingItems = true;
            if (!makeKOT) {

                try {
                    List<SelectedItemDetails> list = ApplicationSingleton.getInstance().getSelectedItemDetails();
                    if (list.isEmpty()) {
                        Utilities.showSnackBar("No Items", this);
                        savingItems = false;
                        return;
                    }
                    switch (ApplicationSingleton.getInstance().getKotOrderType()) {
                        case NEW_KOT:
                            final MaterialDialog dialogSave = new MaterialDialog.Builder(this)
                                    .content(getResources().getString(R.string.saving_resv_items))
                                    .progress(true, 0)
                                    .cancelable(false)
                                    .build();

                            if (!Utilities.isNetworkConnected(this))
                                Utilities.createNoNetworkDialog(this);
                            else {
                            sharedPreferences = Utilities.getSharedPreferences(getApplicationContext());
                            kotContainer = new KotContainer();
                            processItemDetailsResv();

                            dialogSave.show();
                            Utilities.getRetrofitWebService(getBaseContext()).saveResvItems(kotContainer).enqueue(new Callback<KotSaveResponse>() {
                                @Override
                                public void onResponse(Response<KotSaveResponse> response, Retrofit retrofit) {
                                    dialogSave.dismiss();
                                    KotSaveResponse kotSaveResponse = response.body();
                                    if (kotSaveResponse.response_code) {
                                        new MaterialDialog.Builder(SelectItemsActivity.this)
                                                .content(getResources().getString(R.string.resv_items_saved))
                                                .cancelable(false)
                                                .positiveText("Ok")
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                        savingItems = false;
                                                        finish();
                                                        Intent i = new Intent(SelectItemsActivity.this, OrderDetails.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(i);

                                                    }
                                                })
                                                .build().show();
                                    } else {
                                        savingItems = false;
                                        kotSaveResponse.insertResponse.toString();
                                        Utilities.showToast(getResources().getString(R.string.resv_items_not_saved), SelectItemsActivity.this);
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {

                                    savingItems = false;
                                    dialogSave.dismiss();
                                }
                            });

                        }

                            break;

                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    List<SelectedItemDetails> list = ApplicationSingleton.getInstance().getSelectedItemDetails();
                    if (list.isEmpty()) {
                        Utilities.showSnackBar("No Items", this);
                        savingItems = false;
                        return;
                    }
                    switch (ApplicationSingleton.getInstance().getKotOrderType()) {
                        case NEW_KOT:
                            final MaterialDialog dialogSave = new MaterialDialog.Builder(this)
                                    .content(getResources().getString(R.string.saving_resv_items))
                                    .progress(true, 0)
                                    .cancelable(false)
                                    .build();


                            if (!Utilities.isNetworkConnected(this))
                                Utilities.createNoNetworkDialog(this);
                            else {
                                sharedPreferences = Utilities.getSharedPreferences(getApplicationContext());
                                kotContainer = new KotContainer();
                                SaveItemDetailsResv();

                                dialogSave.show();
                                Utilities.getRetrofitWebService(getBaseContext()).saveKot(kotContainer).enqueue(new Callback<KotSaveResponse>() {
                                    @Override
                                    public void onResponse(Response<KotSaveResponse> response, Retrofit retrofit) {
                                        dialogSave.dismiss();
                                        KotSaveResponse kotSaveResponse = response.body();
                                        if (kotSaveResponse.response_code) {
                                            new MaterialDialog.Builder(SelectItemsActivity.this)
                                                    .content(getResources().getString(R.string.resv_items_saved))
                                                    .cancelable(false)
                                                    .positiveText("Ok")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                            savingItems = false;
                                                            finish();
                                                            Intent i = new Intent(SelectItemsActivity.this, OrderDetails.class);
                                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                            startActivity(i);

                                                        }
                                                    })
                                                    .build().show();
                                        } else {
//                                            Log.e("Error Message===>", response.message());
                                            savingItems = false;
                                            try {
                                                if(response.message() != null)
                                                    Utilities.showToast(response.message().toString(), SelectItemsActivity.this);
                                                else
                                                    Utilities.showToast(getResources().getString(R.string.resv_items_not_saved), SelectItemsActivity.this);
                                            } catch (Resources.NotFoundException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        dialogSave.dismiss();
                                    }
                                });

                            }

                            break;

                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    }

    private void resetOrderDetails() {

        ApplicationSingleton.getInstance().clearSelectedItemDetails();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /*if (id == R.id.action_settings) {
            return true;
        }*/
         if ( id == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (!searchView.isIconified()) {
            searchView.setIconified(true);

            if (searchQuery.length() > 0  ) {
                super.onBackPressed();
                searchView.clearFocus();
                searchView.setIconified(true);
                switchSearchFragment1(false);
            }
            //}
        }
        else {
            cancelKot();
        }

    }

    public void animateBillFragment(){
        currentFragmentstate = BILL_FRAGMENT;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.pop_exit, R.anim.pop_enter, R.anim.exit);
        BillDialogFragment newCustomFragment = BillDialogFragment.newInstance(true);
        newCustomFragment.show(getSupportFragmentManager(), "some");
    }

    public void animateCustomerDetailsFragment(){
        currentFragmentstate = CUSTOMER_FRAGMENT;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_customer, R.anim.pop_exit_customer, R.anim.pop_enter_customer, R.anim.exit_customer);
        UserDetailsFragment newCustomFragment = UserDetailsFragment.newInstance();
        transaction.replace(R.id.main_container, newCustomFragment);
        transaction.addToBackStack("CUSTOMER_DETAILS");
        transaction.commit();
    }

    public void switchSearchFragment1(boolean searchViewHasFocus){

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(searchViewHasFocus && !isSearchViewFocused) {
            isSearchViewFocused = true;
            searchFragment = SearchFragment.newInstance();
            transaction.replace(R.id.main_container, searchFragment);
            transaction.addToBackStack("SEARCH_FRAGMENT");
            transaction.commit();
            fabCustomerDeatailsDown.clearAnimation();
            ViewCompat.
                    animate(fabCustomerDeatailsDown)
                    .scaleX(0f)
                    .setDuration(300)
                    .scaleY(0f)
                    .setInterpolator(new AnticipateInterpolator())
                    .setListener(new ViewPropertyAnimatorListener() {
                        @Override
                        public void onAnimationStart(View view) {

                        }

                        @Override
                        public void onAnimationEnd(View view) {
                            view.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationCancel(View view) {

                        }
                    })
                    .start();
            menuItemSaveBill.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            menuCancelKot.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        else if( !searchViewHasFocus && isSearchViewFocused) {
            isSearchViewFocused = false;
            fragmentManager.popBackStack();
            fabCustomerDeatailsDown.setVisibility(View.VISIBLE);
            fabCustomerDeatailsDown.clearAnimation();
            ViewCompat.
                    animate(fabCustomerDeatailsDown)
                    .scaleX(1f)
                    .setDuration(300)
                    .scaleY(1f)
                    .setInterpolator(new AnticipateInterpolator())
                    .setListener(new ViewPropertyAnimatorListener() {
                        @Override
                        public void onAnimationStart(View view) {

                        }

                        @Override
                        public void onAnimationEnd(View view) {
                        }

                        @Override
                        public void onAnimationCancel(View view) {

                        }
                    })
                    .start();
            menuItemSaveBill.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menuCancelKot.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }


    }


    private void addFragment(Fragment fragment , String TAG){

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.main_container, fragment).commit();
    }

    public void setUpperFabAnimation(boolean isShrink , int i){
        fabCustomerDeatailsDown.clearAnimation();
        if(isShrink)
            ViewCompat
                    .animate(fabCustomerDeatailsDown)
                    .scaleX(0f)
                    .scaleY(0f)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .setListener(new ViewPropertyAnimatorListener() {
                        @Override
                        public void onAnimationStart(View view) {

                        }

                        @Override
                        public void onAnimationEnd(View view) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    setUpperFabAnimation(false , 1);
                                }
                            } , getResources().getInteger(R.integer.animation_speed_1) + 300);
                        }

                        @Override
                        public void onAnimationCancel(View view) {

                        }
                    })
                    .start();
        else
            ViewCompat
                    .animate(fabCustomerDeatailsDown)
                    .scaleX(1f)
                    .scaleY(1f)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .start();
    }

    public void showDetailsActivity(Item item){

        throw new UnsupportedOperationException("Not implemented");

    }

    private void processItemDetaiils(){


//        Log.e("Inside Data=====>>" , "Populating");


        try {
            List<SelectedItemDetails> items = ApplicationSingleton.getInstance().getSelectedItemDetails();
            if(items != null){
                int i = 0;
                List<KotItem> kotItems = new ArrayList<>();
                for(SelectedItemDetails selectedItemDetails : items){
                    KotItem kotItem = new KotItem();
                    kotItem.setItm_Cd(selectedItemDetails.itemCode);
                    kotItem.setKotD_Qty(selectedItemDetails.itemQuantity + "");
                    kotItem.setKotD_Rate(selectedItemDetails.itemUnitCost + "");
                    kotItem.setKotD_Amt(selectedItemDetails.itemTotalCost + "");
                    kotItem.setKotD_UCost(selectedItemDetails.Itm_UCost);
                    kotItem.setKotD_Ctr(i++ + "");
                    kotItem.setKotD_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                    kotItem.setItm_Name(selectedItemDetails.itemName);
                    kotItem.setUnit_Fraction(selectedItemDetails.Unit_Fraction);
                    kotItem.setKot_MuUnit(selectedItemDetails.Unit_cd + "");
                    kotItem.setKotD_Remarks(selectedItemDetails.modificationRemarks == null ? "" : selectedItemDetails.modificationRemarks);
                    kotItem.setBarcode(selectedItemDetails.barcode);
                    kotItem.setMenu_SideDish("");

                    /*if(selectedItemDetails.modificationRemarks != null)
                        Log.e("Modification Remark in Save1 === > " , selectedItemDetails.modificationRemarks);

                    if(kotItem.getKotD_Remarks() != null)
                        Log.e("Modification Remark in Save2 === > " , kotItem.getKotD_Remarks());*/

                    if(selectedItemDetails.modify != null){
                        for(int j = 0 ; j < selectedItemDetails.modify.size() ; j++){
                            String modifiers = selectedItemDetails.modify.get(j).name;
                            if( j == 0)
                                kotItem.setMenu_SideDish(modifiers);
                            else
                                kotItem.setMenu_SideDish(kotItem.getMenu_SideDish() + " , " + modifiers);
                        }
                    }
//                    kotItem.setKotD_Remarks(kotItem.getMenu_SideDish());
                    kotItem.setCmd_IsBuffet(selectedItemDetails.Cmd_IsBuffet);
                    kotItem.setMultic(selectedItemDetails.isMultiC);
                    kotItem.setIsSubitem(selectedItemDetails.isSubitem);
                    kotItems.add(kotItem);


                }
                kotContainer.setDetails(kotItems);
                kotContainer.setUsr_Id(sharedPreferences.getString(Constants.SHARED_PREF_KEY_USERNAME, ""));
                kotContainer.setKot_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                kotContainer.setKot_No("");
                kotContainer.setKot_Type(ApplicationSingleton.getInstance().getKotType() + "");
                try {
                    if(ApplicationSingleton.getInstance().getTable() != null)
                        kotContainer.setTab_cd(ApplicationSingleton.getInstance().getTable().getTab_Cd());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                kotContainer.setKot_TotAmt(ApplicationSingleton.getInstance().getGrandTotalWithoutTax() + "");
                kotContainer.setCus_Cd(this.customer == null ? "" : this.customer.getId());
                kotContainer.setCus_Name(this.customer == null ? "" : this.customer.getName());
                kotContainer.setRoom_No(this.customer == null ? "" : this.customer.getRoomNo());

                try {
                    if(customer != null){

                        if(customer.getCheckinDate() != null && !customer.getCheckinDate().matches("")){


                            Calendar calDate = Calendar.getInstance();
                            SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                            List<String> dtitems = Arrays.asList(customer.getCheckinDate().split("\\s+"));

                            if (dtitems.size() > 0) {
                                try {

                                    calDate.setTime(sdfDate.parse(dtitems.get(0)));
                                    String checkinDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime()) + " 00:00:00.000";
                                    kotContainer.setCheckIn_Dt(checkinDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                        else
                            kotContainer.setCheckIn_Dt("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                kotContainer.setCheckIn_Dt(this.customer == null ? "" : this.customer.getCheckinDate());
                kotContainer.setSman_Cd(ApplicationSingleton.getInstance().getSalesman(getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getApplicationContext()).Id);
                kotContainer.setSrvr_Cd(ApplicationSingleton.getInstance().getSalesman(getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getApplicationContext()).Id);
                kotContainer.setKot_Covers(ApplicationSingleton.getInstance().getGuestCount());
                kotContainer.setReservation(false);
                kotContainer.setResv_no("");

                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                Calendar calendar1 = Calendar.getInstance();

                String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

                if(shiftDateString != null && !shiftDateString.matches("")){

                    try {
                        calendar1.setTime(dateFormat1.parse(shiftDateString));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                kotContainer.setDate(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
                kotContainer.setTime(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processItemDetailsResv(){

        try {
            List<SelectedItemDetails> items = ApplicationSingleton.getInstance().getSelectedItemDetails();
            if(items != null){
                int i = 0;
                List<KotItem> kotItems = new ArrayList<>();
                for(SelectedItemDetails selectedItemDetails : items){
                    KotItem kotItem = new KotItem();
                    kotItem.setItm_Cd(selectedItemDetails.itemCode);
                    kotItem.setKotD_Qty(selectedItemDetails.itemQuantity + "");
                    kotItem.setKotD_Rate(selectedItemDetails.itemUnitCost + "");
                    kotItem.setKotD_Amt(selectedItemDetails.itemTotalCost + "");
                    kotItem.setKotD_UCost(selectedItemDetails.Itm_UCost);
                    kotItem.setKotD_Ctr(i++ + "");
                    kotItem.setKotD_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                    kotItem.setItm_Name(selectedItemDetails.itemName);
                    kotItem.setUnit_Fraction(selectedItemDetails.Unit_Fraction);
                    kotItem.setKot_MuUnit(selectedItemDetails.Unit_cd + "");
                    kotItem.setKotD_Remarks(selectedItemDetails.modificationRemarks == null ? "" : selectedItemDetails.modificationRemarks);
                    kotItem.setBarcode(selectedItemDetails.barcode);
                    kotItem.setMenu_SideDish("");
                    if(selectedItemDetails.modify != null){
                        for(int j = 0 ; j < selectedItemDetails.modify.size() ; j++){
                            String modifiers = selectedItemDetails.modify.get(j).name;
                            if( j == 0)
                                kotItem.setMenu_SideDish(modifiers);
                            else
                                kotItem.setMenu_SideDish(kotItem.getMenu_SideDish() + " , " + modifiers);
                        }
                    }
//                    kotItem.setKotD_Remarks(kotItem.getMenu_SideDish());
                    kotItem.setMultic(selectedItemDetails.isMultiC);
                    kotItem.setCmd_IsBuffet(selectedItemDetails.Cmd_IsBuffet);
                    kotItem.setIsSubitem(selectedItemDetails.isSubitem);
                    kotItems.add(kotItem);


                }
                kotContainer.setDetails(kotItems);
                kotContainer.setUsr_Id(sharedPreferences.getString(Constants.SHARED_PREF_KEY_USERNAME, ""));
                kotContainer.setKot_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                kotContainer.setKot_No("");
                kotContainer.setKot_Type(ApplicationSingleton.getInstance().getKotType() + "");
                try {
                    if(ApplicationSingleton.getInstance().getTable() != null)
                        kotContainer.setTab_cd(ApplicationSingleton.getInstance().getTable().getTab_Cd());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                kotContainer.setKot_TotAmt(ApplicationSingleton.getInstance().getGrandTotalWithoutTax() + "");
                kotContainer.setCus_Cd(this.customer == null ? "" : this.customer.getId());
                kotContainer.setCus_Name(this.customer == null ? "" : this.customer.getName());
                kotContainer.setRoom_No(this.customer == null ? "" : this.customer.getRoomNo());

                try {
                    if(customer != null){

                        if(customer.getCheckinDate() != null && !customer.getCheckinDate().matches("")){


                            Calendar calDate = Calendar.getInstance();
                            SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                            List<String> dtitems = Arrays.asList(customer.getCheckinDate().split("\\s+"));

                            if (dtitems.size() > 0) {
                                try {

                                    calDate.setTime(sdfDate.parse(dtitems.get(0)));
                                    String checkinDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime()) + " 00:00:00.000";
                                    kotContainer.setCheckIn_Dt(checkinDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                        else
                            kotContainer.setCheckIn_Dt("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                kotContainer.setCheckIn_Dt(this.customer == null ? "" : this.customer.getCheckinDate());
                kotContainer.setSman_Cd(ApplicationSingleton.getInstance().getSalesman(getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getApplicationContext()).Id);
                kotContainer.setSrvr_Cd(ApplicationSingleton.getInstance().getSalesman(getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getApplicationContext()).Id);
                kotContainer.setKot_Covers(resvGuestCount != null ? resvGuestCount : "1");
                kotContainer.setReservation(true);
                kotContainer.setResv_no(resvCode);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                Calendar calendar1 = Calendar.getInstance();

                String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

                if(shiftDateString != null && !shiftDateString.matches("")){

                    try {
                        calendar1.setTime(dateFormat1.parse(shiftDateString));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                kotContainer.setDate(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
                kotContainer.setTime(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SaveItemDetailsResv(){

        try {
            List<SelectedItemDetails> items = ApplicationSingleton.getInstance().getSelectedItemDetails();
            if(items != null){
                int i = 0;
                List<KotItem> kotItems = new ArrayList<>();
                for(SelectedItemDetails selectedItemDetails : items){
                    KotItem kotItem = new KotItem();
                    kotItem.setItm_Cd(selectedItemDetails.itemCode);
                    kotItem.setKotD_Qty(selectedItemDetails.itemQuantity + "");
                    kotItem.setKotD_Rate(selectedItemDetails.itemUnitCost + "");
                    kotItem.setKotD_Amt(selectedItemDetails.itemTotalCost + "");
                    kotItem.setKotD_UCost("0");
                    kotItem.setKotD_Ctr(i++ + "");
                    kotItem.setKotD_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                    kotItem.setItm_Name(selectedItemDetails.itemName);
                    kotItem.setUnit_Fraction("1");
                    kotItem.setKot_MuUnit("");
                    kotItem.setKotD_Remarks(selectedItemDetails.modificationRemarks == null ? "" : selectedItemDetails.modificationRemarks);
                    kotItem.setBarcode(selectedItemDetails.barcode);
                    kotItem.setMenu_SideDish("");
                    if(selectedItemDetails.modify != null){
                        for(int j = 0 ; j < selectedItemDetails.modify.size() ; j++){
                            String modifiers = selectedItemDetails.modify.get(j).name;
                            if( j == 0)
                                kotItem.setMenu_SideDish(modifiers);
                            else
                                kotItem.setMenu_SideDish(kotItem.getMenu_SideDish() + " , " + modifiers);
                        }
                    }
//                    kotItem.setKotD_Remarks(kotItem.getMenu_SideDish());
                    kotItem.setMultic(selectedItemDetails.isMultiC);
                    kotItem.setCmd_IsBuffet(selectedItemDetails.Cmd_IsBuffet);
                    kotItem.setIsSubitem(selectedItemDetails.isSubitem);
                    kotItems.add(kotItem);

                    /*Log.e("Inside For loop Item Name ===>>" , selectedItemDetails.itemName);
                    Log.e("Inside For loop Item code ===>>" , selectedItemDetails.itemCode);
                    Log.e("Inside For loop Item Qty ===>>" , String.valueOf(selectedItemDetails.itemQuantity));
                    Log.e("Inside For loop Item Unit Cost ===>>" , String.valueOf(selectedItemDetails.itemUnitCost));
                    Log.e("Inside For loop Item TCost ===>>" , String.valueOf(selectedItemDetails.itemTotalCost));
                    Log.e("Inside For loop Item Item Ucost ===>>" , selectedItemDetails.Itm_UCost);
                    Log.e("Inside For loop Item Unit Fraction ===>>" , selectedItemDetails.Unit_Fraction);
                    Log.e("Inside For loop Item Barcode ===>>" , selectedItemDetails.barcode);
                    Log.e("Inside For loop Item Is Subitem ===>>" , String.valueOf(selectedItemDetails.isSubitem));
                    Log.e("Inside For loop Item unitcode ===>>" , String.valueOf(selectedItemDetails.Unit_cd) );
                    Log.e("Inside For loop Item remark ===>>" , String.valueOf(selectedItemDetails.Remarks) );
                    Log.e("Inside For loop Item isbuffet ===>>" , String.valueOf(selectedItemDetails.Cmd_IsBuffet) );
                    Log.e("Inside For loop Item ismulti ===>>" , String.valueOf(selectedItemDetails.isMultiC) );*/

                }
                kotContainer.setDetails(kotItems);
                kotContainer.setUsr_Id(sharedPreferences.getString(Constants.SHARED_PREF_KEY_USERNAME, ""));
//                reservation.getDocCounter()
                kotContainer.setKot_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                kotContainer.setKot_No("");
                kotContainer.setKot_Type( 1 + "");
                try {
                    if(ApplicationSingleton.getInstance().getTable() != null) {
                        kotContainer.setTab_cd(reservation.getTable_Cd());
//                        Log.e("Inside====>>" ,"Table");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                kotContainer.setKot_TotAmt(ApplicationSingleton.getInstance().getGrandTotalWithoutTax() + "");
                kotContainer.setCus_Cd(this.customer == null ? "" : this.customer.getId());
                kotContainer.setCus_Name(this.customer == null ? "" : this.customer.getName());
                kotContainer.setRoom_No(this.customer == null ? "" : this.customer.getRoomNo());

                try {
                    if(customer != null){

                        if(customer.getCheckinDate() != null && !customer.getCheckinDate().matches("")){


                            Calendar calDate = Calendar.getInstance();
                            SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                            List<String> dtitems = Arrays.asList(customer.getCheckinDate().split("\\s+"));

                            if (dtitems.size() > 0) {
                                try {

                                    calDate.setTime(sdfDate.parse(dtitems.get(0)));
                                    String checkinDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime()) + " 00:00:00.000";
                                    kotContainer.setCheckIn_Dt(checkinDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                        else
                            kotContainer.setCheckIn_Dt("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                kotContainer.setCheckIn_Dt(this.customer == null ? "" : this.customer.getCheckinDate());
                kotContainer.setSman_Cd(ApplicationSingleton.getInstance().getSalesman(getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getApplicationContext()).Id);
                kotContainer.setSrvr_Cd(ApplicationSingleton.getInstance().getSalesman(getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getApplicationContext()).Id);
                kotContainer.setKot_Covers(resvGuestCount != null ? resvGuestCount : "1");
                kotContainer.setReservation(true);
                kotContainer.setResv_no(resvCode);

                SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                Calendar calendar1 = Calendar.getInstance();

                String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

                if(shiftDateString != null && !shiftDateString.matches("")){

                    try {
                        calendar1.setTime(dateFormat1.parse(shiftDateString));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                kotContainer.setDate(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
                kotContainer.setTime(new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    public void showMulticorseMenu(Item item) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        com.afi.restaurantpos.al_loomie.Fragments.ItemDetailsMuticorseFragment itemDetailsMuticorseFragment = com.afi.restaurantpos.al_loomie.Fragments.ItemDetailsMuticorseFragment.newInstance(item);
        itemDetailsMuticorseFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyMaterialTheme_AppBarOverlay);
        itemDetailsMuticorseFragment.setCallBack(new com.afi.restaurantpos.al_loomie.Fragments.ItemDetailsMuticorseFragment.ItemDdetailsListCallBack() {
            @Override
            public void onItemSelected(List<SelectedItemDetails> selectedItemDetailses, boolean isSuccess , boolean makeKot) {
                for (SelectedItemDetails selectedItemDetails : selectedItemDetailses) {
                    ApplicationSingleton.getInstance().addToSelectedItems(selectedItemDetails);
                    if (foodItemsFragment != null)
                        foodItemsFragment.addToSubBillData(selectedItemDetails);

                }

                if(makeKot) {

                    if(!isReservationKot)
                        saveKot();
                    else
                        saveReservationItems();
                }
            }

            @Override
            public void onCanceled() {

            }
        });
        itemDetailsMuticorseFragment.show(fragmentManager, "Item");

    }


    /*public void showMulticorseItemDetailes(ArrayList<Item> subitems , String itemName , String itemQty , ArrayList<Item> mainItems , int pos) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        MuticorseItemChangeFragment itemDetailsMuticorseFragment = MuticorseItemChangeFragment.newInstance(subitems , itemName , itemQty , mainItems , pos);
        itemDetailsMuticorseFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyMaterialTheme_AppBarOverlay);
       *//* itemDetailsMuticorseFragment.setCallBack(new com.afi.restaurantpos.al_loomie.Fragments.ItemDetailsMuticorseFragment.ItemDdetailsListCallBack() {
            @Override
            public void onItemSelected(List<SelectedItemDetails> selectedItemDetailses, boolean isSuccess) {
                for (SelectedItemDetails selectedItemDetails : selectedItemDetailses) {
                    ApplicationSingleton.getInstance().addToSelectedItems(selectedItemDetails);
                    if (foodItemsFragment != null)
                        foodItemsFragment.addToSubBillData(selectedItemDetails);
                }
            }

            @Override
            public void onCanceled() {

            }
        });*//*
        itemDetailsMuticorseFragment.show(fragmentManager, "ChangeItem");

    }*/

    /*public void showMulticorseItemIngredients(ArrayList<Item> items , String itemName) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        MuticorseItemIngredientsFragment itemDetailsMuticorseFragment = MuticorseItemIngredientsFragment.newInstance(items , itemName);
        itemDetailsMuticorseFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyMaterialTheme_AppBarOverlay);
       *//* itemDetailsMuticorseFragment.setCallBack(new com.afi.restaurantpos.al_loomie.Fragments.ItemDetailsMuticorseFragment.ItemDdetailsListCallBack() {
            @Override
            public void onItemSelected(List<SelectedItemDetails> selectedItemDetailses, boolean isSuccess) {
                for (SelectedItemDetails selectedItemDetails : selectedItemDetailses) {
                    ApplicationSingleton.getInstance().addToSelectedItems(selectedItemDetails);
                    if (foodItemsFragment != null)
                        foodItemsFragment.addToSubBillData(selectedItemDetails);
                }
            }

            @Override
            public void onCanceled() {

            }
        });*//*
        itemDetailsMuticorseFragment.show(fragmentManager, "ingredients");

    }*/
}
