package com.afi.restaurantpos.al_loomie.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.afi.restaurantpos.al_loomie.Adapters.CustomerDetailsFragmentAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;


public class CustomerDetailsFragment extends DialogFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;


    private Customer customer;
    private Boolean isEditable = false;

    public static CustomerDetailsFragment custFrag ;

    public CustomerDetailsFragment() {
        // Required empty public constructor
    }


    public static CustomerDetailsFragment newInstance(Customer customer , Boolean isEditable) {
        CustomerDetailsFragment fragment = new CustomerDetailsFragment();
        fragment.customer = customer;
        fragment.isEditable = isEditable;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        custFrag = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.fragment_customer_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewPager = (ViewPager) getView().findViewById(R.id.ViewPager);
        tabLayout = (TabLayout) getView().findViewById(R.id.tabs);
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbarContainer);
        toolbar.setTitle(customer.getName());
        toolbar.setSubtitle(customer.getId());
//        toolbar.setSubtitle(customer.getCus_Phone());
        toolbar.setLogo(R.drawable.ic_person_white_24dp);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerDetailsFragment.this.dismiss();
            }
        });
        viewPager.setAdapter(new CustomerDetailsFragmentAdapter(getChildFragmentManager(), customer , isEditable));
        tabLayout.setupWithViewPager(viewPager);
    }
}
