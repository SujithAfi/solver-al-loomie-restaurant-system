package com.afi.restaurantpos.al_loomie.Fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.io.IOException;
import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.HistoryAdapter;
import com.afi.restaurantpos.al_loomie.Models.History;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerHistoryFragment extends Fragment {

    private Customer customer;

    private RecyclerView rvCustomerHistory;
    private ProgressBar progressbarHistory;

    private Handler uiHandler = new Handler();

    public CustomerHistoryFragment() {
        // Required empty public constructor
    }

    public CustomerHistoryFragment setCustomer(Customer customer) {
        this.customer = customer;
        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_customer_history, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvCustomerHistory = (RecyclerView) getView().findViewById(R.id.rvCustomerHistory);
        progressbarHistory = (ProgressBar) getView().findViewById(R.id.progressbarHistory);
        rvCustomerHistory.setHasFixedSize(true);
        rvCustomerHistory.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    @Override
    public void onResume() {
        super.onResume();

        loadHistory();
    }

    private void loadHistory() {

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {

            try {
                progressbarHistory.setVisibility(View.VISIBLE);
                rvCustomerHistory.setVisibility(View.INVISIBLE);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final Response<List<History>> hostories = Utilities.getRetrofitWebService(getContext()).getUserHistory(customer.getId()).execute();
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressbarHistory.setVisibility(View.GONE);
                                    rvCustomerHistory.setVisibility(View.VISIBLE);
                                    onHistoryLoaded(hostories.body());
                                }
                            });
                        } catch (IOException e) {
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressbarHistory.setVisibility(View.GONE);
                                    rvCustomerHistory.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onHistoryLoaded(List<History> histories) {
        if(histories != null)
            rvCustomerHistory.setAdapter(new HistoryAdapter(histories));
    }
}
