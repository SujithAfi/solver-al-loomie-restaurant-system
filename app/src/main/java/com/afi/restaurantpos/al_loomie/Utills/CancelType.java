package com.afi.restaurantpos.al_loomie.Utills;

/**
 * Created by AFI on 11/7/2015.
 * enum with type of canceling bill or kot
 */
public enum CancelType {
    CANCEL_KOT,
    CANCEL_BILL,
    ALERT,
    REORDER_KOT
}
