package com.afi.restaurantpos.al_loomie.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afi.restaurantpos.al_loomie.Adapters.CustomersDialogAdapter;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;

/**
 * Created by afi-mac-001 on 15/06/16.
 */
public class CustomersNewFragment extends Fragment implements SearchView.OnQueryTextListener, View.OnFocusChangeListener {


    public static final String CREATE_CUSTOMER_FRAGMENT_ACTION = "pos.restaurent.afi.restuarentpos.Dialogs.SendMessage";
    IntentFilter intentFilter = new IntentFilter("backActionBroadcastReceiver");
    private ViewPager viewpagerCustomers;
    private SelectCustomerListner selectCustomerListner;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Customer customer = (Customer) intent.getSerializableExtra("customer");
            if (customer != null) {
                if (selectCustomerListner != null) {
                    selectCustomerListner.onCustomerSelected(customer);
                }
            }
        }
    };
    private BroadcastReceiver backActionBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            viewpagerCustomers.setCurrentItem(0);
        }
    };

    public CustomersNewFragment() {
        // Required empty public constructor
    }

    public CustomersNewFragment setSelectCustomerListner(SelectCustomerListner selectCustomerListner) {
        this.selectCustomerListner = selectCustomerListner;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return getCustomView();
    }

    private View getCustomView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_select_customer_fragment, null);
        viewpagerCustomers = (ViewPager) view.findViewById(R.id.viewpagerCustomers);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter("customerSelected"));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(backActionBroadcastReceiver, intentFilter);
            viewpagerCustomers.setAdapter(new CustomersDialogAdapter(getChildFragmentManager() , getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(backActionBroadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().findViewById(R.id.btnSkip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectCustomerListner != null) {
                    selectCustomerListner.onSkipAction();
                }
            }
        });

        getView().findViewById(R.id.fabAddCustomer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpagerCustomers.setCurrentItem(1, true);
            }
        });

        viewpagerCustomers.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    if (position == 1) {
                        if (getActivity() instanceof OrderDetails) {
                            ((OrderDetails) getActivity()).isCreateCustomerFragmentActive = true;
                        }
                    } else {
                        if (getActivity() instanceof OrderDetails) {
                            ((OrderDetails) getActivity()).isCreateCustomerFragmentActive = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        sendBroadcast(0, query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        sendBroadcast(0, newText);
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        sendBroadcast(hasFocus ? 1 : 2, "");
    }

    private void sendBroadcast(int flag, String query) {

        try {
            Intent intent = new Intent(CREATE_CUSTOMER_FRAGMENT_ACTION);
            intent.putExtra("action", flag);
            intent.putExtra("query", query);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface SelectCustomerListner {

        void onCustomerSelected(Customer customer);

        void onSkipAction();

    }

}