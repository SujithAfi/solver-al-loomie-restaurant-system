package com.afi.restaurantpos.al_loomie.Dialogs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.IngrediantsAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemRecipeDetails;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Fragment to display the recipes of a item
 */
public class RecipeFragment extends Fragment {
    private static final String ARG_PARAM_ITEMID = "param1";
    private static final String ARG_PARAM_NAME = "param2";

    private String mItemID;
    private String itemName = "";
    private ViewGroup mContainer;
    private View mLayout;
    private TextView name;
    private TextView quantity;
    private TextView cost;
    private TextView itemDetails;
    private RecyclerView rVIngrediants;

    public RecipeFragment() {
    }

    public static RecipeFragment newInstance(String itemId, String name) {
        RecipeFragment fragment = new RecipeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_ITEMID, itemId);
        args.putString(ARG_PARAM_NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemID = getArguments().getString(ARG_PARAM_ITEMID);
            itemName = getArguments().getString(ARG_PARAM_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContainer = container;

        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_recipe, container, false);

        rVIngrediants = (RecyclerView) mLayout.findViewById(R.id.rvIcgrediants);
        rVIngrediants.setHasFixedSize(true);
        rVIngrediants.setLayoutManager(new LinearLayoutManager(getContext()));
        itemDetails = (TextView) mLayout.findViewById(R.id.txtDescription);
        name = (TextView) mLayout.findViewById(R.id.txtName);
        quantity = (TextView) mLayout.findViewById(R.id.txtQuantity);
        cost = (TextView) mLayout.findViewById(R.id.txtCost);
        itemDetails.setMovementMethod(new ScrollingMovementMethod());
        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            try {
                Utilities.getRetrofitWebService(getContext()).getItemRecipeDetails(mItemID).enqueue(new Callback<ItemRecipeDetails>() {
                    @Override
                    public void onResponse(Response<ItemRecipeDetails> response, Retrofit retrofit) {
                        try {
                            if (response.body() != null && RecipeFragment.this.isVisible()) {
                                processItemRecipe(response.body());
                                getView().findViewById(R.id.recipeCOntainer).setVisibility(View.VISIBLE);
                                getView().findViewById(R.id.txtNoRecepie).setVisibility(View.GONE);
                            }
                            else {

                                getView().findViewById(R.id.recipeCOntainer).setVisibility(View.GONE);
                                getView().findViewById(R.id.txtNoRecepie).setVisibility(View.VISIBLE);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void processItemRecipe(ItemRecipeDetails itemRecipeDetails) {
        try {
            if (itemRecipeDetails.Name != null)
                name.setText(itemRecipeDetails.Name);
            if (itemRecipeDetails.Cost != null)
                cost.setText(itemRecipeDetails.Cost);
            if (itemRecipeDetails.Quantity != null)
                quantity.setText(itemRecipeDetails.Quantity);
            if (itemRecipeDetails.Recipe != null)
                rVIngrediants.setAdapter(new IngrediantsAdapter(itemRecipeDetails.Recipe));

            if (itemRecipeDetails.Details != null)
                itemDetails.setText(Html.fromHtml(itemRecipeDetails.Details));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
