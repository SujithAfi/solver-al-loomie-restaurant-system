package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.CallBacks.ItemClickCallback;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.SubCategory;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 10/2/2015.
 */
public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryViewHolder> {

    private List<SubCategory> subCategories;

    private ItemClickCallback itemClickCallback;

    private int selectedItemPosition = 0;
    private Context context;
    public SubCategoryAdapter(List<SubCategory> subCategories , ItemClickCallback callback) {
        this.subCategories = subCategories;
        this.itemClickCallback = callback;
    }

    @Override
    public SubCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_sub_category, viewGroup, false);
        context = v.getContext();
        SubCategoryViewHolder subCategoryViewHolder = new SubCategoryViewHolder(v);
        return subCategoryViewHolder;
    }

    @Override
    public void onBindViewHolder(SubCategoryViewHolder holder, int position) {
        holder.textView.setText(subCategories.get(position).SubGrp_Name);
        holder.textView.setOnClickListener(new ItemClickListner(position, subCategories.get(position).SubGrpCd));
        holder.textView.setSelected(position == selectedItemPosition);
    }

    @Override
    public int getItemCount() {
        if(subCategories == null)
            return 0;
        return subCategories.size();
    }

    public static class SubCategoryViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public SubCategoryViewHolder(View itemView) {
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.txtName);
        }
    }

    private class ItemClickListner implements View.OnClickListener{
        private int position ;

        private String Id;

        public ItemClickListner(int position, String id) {
            this.position = position;
            Id = id;
        }

        @Override
        public void onClick(View v) {

            if (!Utilities.isNetworkConnected(context))
                Utilities.createNoNetworkDialog(context);
            else {
                if (itemClickCallback != null)
                    itemClickCallback.onItemSelected(position, Id);
                SubCategoryAdapter.this.selectedItemPosition = position;
                notifyDataSetChanged();
            }
        }
    }

}
