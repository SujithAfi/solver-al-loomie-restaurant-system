package com.afi.restaurantpos.al_loomie;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.afi.restaurantpos.al_loomie.Adapters.BillingAdapter;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Adapters.MergeAdapter;
import com.afi.restaurantpos.al_loomie.Adapters.SplitAdapter;
import com.afi.restaurantpos.al_loomie.CustomViews.MyCheckBox;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class SplitAndMergeActivity extends AppCompatActivity {

    private boolean isMerging;
    private Toolbar toolbar;
    private List<KotAdaterData> kotAdapterDatas;
    private RecyclerView rvKotDetailsList;
    MyCheckBox myCheckBox;
    FloatingActionButton fabAddtoBill;
    double value;

    private String newBillNumber;

    Map<String, List<PendingKotItem>> listMap;
    private boolean savingFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_and_merge);
        Utilities.initURL(this);
        fabAddtoBill = (FloatingActionButton) findViewById(R.id.fabAddtoBill);
        Utilities.initKitkatStatusbarTransparancy(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        myCheckBox = (MyCheckBox) findViewById(R.id.checkboxSpaceAdjust);
        setSupportActionBar(toolbar);
        isMerging = getIntent().getBooleanExtra("operation" , false);
        setTitle(isMerging ? "Merge" : "Split");
        myCheckBox.setVisibility(isMerging ? View.GONE : View.INVISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        kotAdapterDatas = new ArrayList<>();
        rvKotDetailsList = (RecyclerView)findViewById(R.id.rvKotDetailsList);
        rvKotDetailsList.setHasFixedSize(true);
        rvKotDetailsList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        try {
            Map<String, List<PendingKotItem>> map = ApplicationSingleton.getInstance().getSelectedKotMap();

            processData(map , isMerging);
        }
        catch (Exception e){

        }
        fabAddtoBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveToBill();



            }
        });


        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else
            loadNewBillNumber();

        if(Utilities.getSharedPreferences(SplitAndMergeActivity.this).getBoolean(Constants.SHARED_PRE_UR_BILLING_ADDORSAVE, false))
            fabAddtoBill.setVisibility(View.VISIBLE);


    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void saveToBill() {

        if(!savingFlag) {
            savingFlag = true;

            List<PendingKotItem> selectedItems = getSelectedItems();

            if (selectedItems.isEmpty()) {
                savingFlag = false;
                new AlertDialog.Builder(this)
                        .setTitle("Error")
                        .setMessage("Empty Bill")
                        .setPositiveButton("OK", null)
                        .setCancelable(false)
                        .create()
                        .show();
                return;
            }

            List<SelectedItemDetails> selectedItemDetailses = PendingKotItemsToSelectedItemDetailses(selectedItems);

            if (selectedItemDetailses != null) {
                try {
                    if (selectedItemDetailses.size() > 0) {
                        final BillContainer saveBillData = prepareData(selectedItemDetailses);

                        final MaterialDialog dialog = new MaterialDialog.Builder(SplitAndMergeActivity.this)
                                .content("Creating new Bill")
                                .progress(true, 1)
                                .titleColorRes(R.color.colorAccentDark)
                                .cancelable(false).build();

                        if (!Utilities.isNetworkConnected(this)) {
                            savingFlag = false;
                            Utilities.createNoNetworkDialog(this);
                        }
                        else {
                            dialog.show();
                            Utilities.getRetrofitWebService(getApplicationContext()).saveBill(saveBillData).enqueue(new Callback<Map<String, Object>>() {
                                @Override
                                public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                    savingFlag = false;
                                    dialog.dismiss();
                                    final boolean response_code = (Boolean) response.body().get("response_code");
                                    if (response_code) {
                                        new MaterialDialog.Builder(SplitAndMergeActivity.this)
                                                .titleColorRes(R.color.colorAccentDark)
                                                .title(response_code ? "Bill Saved" : "Can't save bill")
                                                .positiveText("Ok")
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                        if (response_code) {

                                                            setResult(RESULT_OK);
                                                            finish();
                                                        }
                                                    }
                                                }).build().show();
                                    }

                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    savingFlag = false;
                                    dialog.dismiss();
                                }
                            });


                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{

                savingFlag = false;
            }

        }


    }


    private void loadNewBillNumber() {

        try {
            Utilities.getRetrofitWebService(getApplicationContext()).getNewBillNumber(ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<Map<String, String>>() {
                @Override
                public void onResponse(Response<Map<String, String>> response, Retrofit retrofit) {
                    try {
                        newBillNumber = Utilities.getKotFormat(response.body().get("bill_no"));
                        Map<String, String> stringStringMap = response.body();
                        if (stringStringMap.get("bill_no") != null) {
                            if (stringStringMap.get("bill_no").length() > 0) {
                                ApplicationSingleton.getInstance().setNewBillNumber(Utilities.getKotFormat(stringStringMap.get("bill_no")));
                            }
                        }

                        getSupportActionBar().setSubtitle(newBillNumber);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            onBackPressed();
            return super.onOptionsItemSelected(item);
    }

    private void processData(Map<String, List<PendingKotItem>> map , boolean isMerging) {
        this.listMap = map;
        Iterator entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            String key = thisEntry.getKey().toString();
            List<PendingKotItem> value = (List<PendingKotItem>) thisEntry.getValue();
            for(PendingKotItem item : value){
                kotAdapterDatas.add(new KotAdaterData().setIsHeader(false).setKotNumber(key).setItem(item));
            }
        }
        if(!isMerging) {
            SplitAdapter splitAdapter = new SplitAdapter(kotAdapterDatas);
            rvKotDetailsList.setAdapter(splitAdapter);
            splitAdapter.setItemChange(new SplitAdapter.onItemChange() {
                @Override
                public void onItemInvalidate(int pos , boolean ischecked) {

                    kotAdapterDatas.get(pos).item.isFoc = ischecked;
                }
            });
        }
        else {
            MergeAdapter mergeAdpter =  new MergeAdapter(kotAdapterDatas);
            rvKotDetailsList.setAdapter(mergeAdpter);
            mergeAdpter.setItemChange(new MergeAdapter.onItemChange() {
                @Override
                public void onItemInvalidate(int pos , boolean ischecked) {

                    kotAdapterDatas.get(pos).item.isFoc = ischecked;
                }
            });
        }
    }

    public List<PendingKotItem> getSelectedItems() {
        if(isMerging)
            return ((MergeAdapter)rvKotDetailsList.getAdapter()).getSelectedItemDetails();
        else
            return ((SplitAdapter)rvKotDetailsList.getAdapter()).getSelectedItemDetails();
    }

    public class KotAdaterData {
        public  boolean isHeader;
        public  String kotNumber;
        public  PendingKotItem item;

        public KotAdaterData setIsHeader(boolean isHeader) {
            this.isHeader = isHeader;
            return this;
        }

        public KotAdaterData setKotNumber(String kotNumber) {
            this.kotNumber = kotNumber;
            return this;
        }

        public KotAdaterData setItem(PendingKotItem item) {
            this.item = item;
            return this;
        }
    }


    private BillContainer prepareData(List<SelectedItemDetails> selectedItemDetailses) {
        String Tab_Cd = "";
        String Cm_Covers = "";

        Context context = this;
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        SharedPreferences preferences = Utilities.getSharedPreferences(context);
        int index = 0;
        double Cm_TotAmt = 0;
        double focAmount = 0;
        String Cus_Name = "";
        String Cus_Cd = "";
        String Disc_Per = "";
        BillContainer billContainer = new BillContainer();
        List<BillItem> billItems = new ArrayList<>();
        for(SelectedItemDetails details : selectedItemDetailses){

            Cus_Name = details.Cus_Name;
            Cus_Cd = details.Cus_Cd;
            Disc_Per = details.Disc_Per;

            Tab_Cd = details.Tab_Cd;
            Cm_Covers = details.Cm_Covers;
            BillItem billItem = new BillItem();
            billItem.Itm_cd = details.itemCode;
            //put modifiers
            billItem.Menu_SideDish = "";
            if(details.modify != null){
                for (int i= 0 ; i < details.modify.size() ; i++ ) {
                    ItemModifiers modifier = details.modify.get(i);
                    if(i + 1 == details.modify.size() )
                        billItem.Menu_SideDish += modifier.name;
                    else if(i == 0)
                        billItem.Menu_SideDish += modifier.name;
                    else
                        billItem.Menu_SideDish += "," + modifier.name;
                }
            }
            billItem.Cmd_ItmName = details.itemName;
            billItem.BarCode = details.barcode;
            billItem.Unit_Fraction = details.Unit_Fraction;
            billItem.Cmd_Qty = details.itemQuantity + "";
            billItem.Unit_Cd = details.Unit_cd;
            billItem.Cmd_MuUnit = "1";//TODO
            billItem.Cmd_MuQty = details.itemQuantity + "";
            billItem.Cmd_MuFraction = "1";
            billItem.Cmd_MuConvToBase = "1";
            billItem.Cmd_MuUcost = "1";
            billItem.Cmd_Rate = details.itemUnitCost + "";//// TODO: 10/21/2015
            double Cmd_Amta =(details.itemUnitCost)*(details.itemQuantity);
            String Cmd_DiscPers =billItem.Cmd_DiscPer;
            billItem.Cmd_DiscPer = details.Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
            if(Cmd_DiscPers!=null) {

                billItem.Cmd_DiscAmt = Utilities.getPercentage(Cmd_Amta , Double.parseDouble(billItem.Cmd_DiscPer)) + "";
            }
            else{
                billItem.Cmd_DiscAmt ="0";
            }
            billItem.Cmd_Amt = (Cmd_Amta   -  Double.parseDouble( billItem.Cmd_DiscAmt )) + "";
            billItem.Cmd_Ctr = index++ + "";
            billItem.Cmd_SubCtr = "0";
            billItem.Cmd_RowType = "A";
            billItem.Cmd_NotCosted = "0";
            billItem.Cmd_Cancelled = "0";
            billItem.Cmd_IsLzQty = "1";
            billItem.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO , 1) + "";
            billItem.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE , "");
            billItem.Cmd_Status = "1";
            billItem.Cm_ThruDo = "0";
            billItem.IsModified = "0";
            billItem.Cm_Ref = "1";
            billItem.Cmd_DiscReason = "No";
            billItem.Kot_No = details.Kot_No == null ? "0" : details.Kot_No;
            billItem.Rcp_Cd = "1";
            billItem.Cmd_Ucost = details.itemUnitCost + "";
            billItem.Cmd_IsBuffet = details.Cmd_IsBuffet;//// TODO: 10/21/2015
            billItem.isFoc = details.isFoc;
            billItems.add(billItem);
            Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);
            if(billItem.isFoc)
                focAmount += Double.parseDouble(billItem.Cmd_Amt);

        }
        billContainer.details = billItems;
        billContainer.Usr_id = preferences.getString(Constants.SHARED_PREF_KEY_ID, "");
        billContainer.Cm_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_No = newBillNumber;
        billContainer.Sman_Cd = singleton.getSalesman(this) == null ? "0" : singleton.getSalesman(this).Id + "";
        billContainer.Cus_Cd = Cus_Cd;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Id + "";
        billContainer.Cus_Name = Cus_Name;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Name + "";billContainer.Cm_TotAmt =
        billContainer.Cm_DiscPer = Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
        billContainer.Cm_DiscAmt = Utilities.getPercentage(Cm_TotAmt , Double.parseDouble(billContainer.Cm_DiscPer)) + "";

        billContainer.Cm_TotAmt = Cm_TotAmt + "";

        Cm_TotAmt -= Double.parseDouble(billContainer.Cm_DiscAmt);

        billContainer.Cm_Tax1_Per = Utilities.getTaxFraction1(context) + "";
        billContainer.Cm_Tax1_Amt = Utilities.findTax1(context, (Cm_TotAmt - focAmount)) + "";
        billContainer.Cm_Tax2_Per = Utilities.getTaxFraction2(context) + "";
        billContainer.Cm_Tax2_Amt = Utilities.findTax2(context, (Cm_TotAmt - focAmount)) + "";
        billContainer.Cm_FocAmt = focAmount + "";
        billContainer.Cm_NetAmt = ((Cm_TotAmt - focAmount) + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) ) + "";
        billContainer.Cm_Tips_Amt = "0";
        billContainer.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO , 0) + "";
        billContainer.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
        billContainer.Cm_BC_Settle_Amt = "0.000";
        billContainer.Cm_Paid_Amt = "0.000";
        billContainer.Cm_Cancel_Reason = "";
        billContainer.Cm_Cancelled = "0";
        billContainer.Cm_Cus_Phone = singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().getId() + "";
        billContainer.Cm_Type = "1";//TODO get order type
        billContainer.Tab_Cd = Tab_Cd;//singleton.getTable() == null ? "" : singleton.getTable().Tab_Cd;//TODO
        billContainer.Cm_Covers = Cm_Covers;//singleton.getGuestCount();
        billContainer.Srvr_Cd = "1";
        billContainer.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME , "");
        billContainer.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME , "0");


        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_ENABLED , false)) {
            billContainer.Service_Per = preferences.getString(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_RATE, "0");
        }
        value = Double.parseDouble(billContainer.Cm_TotAmt);
        value = Utilities.findServiceCharge(this, (value - focAmount));
        value = Double.parseDouble(Utilities.getDefaultCurrencyFormat(value , this));
        double v = Double.parseDouble(billContainer.Cm_NetAmt);
        v += value;
        billContainer.Cm_NetAmt = String.valueOf(v);
        billContainer.Service_Amt = String.valueOf(value);


        //(Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) - Double.parseDouble( billContainer.Cm_DiscPer)) + "";
        // billContainer.Collect_Sft_No = "2";//// TODO: 10/21/2015
        //billContainer.Collect_Sft_Dt = "10-10-2015";//// TODO: 10/21/2015
        int i = 0;
        i++;
        billContainer.Srvr_Cd = i + "";

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar1 = Calendar.getInstance();

        String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

        if(shiftDateString != null && !shiftDateString.matches("")){

            try {
                calendar1.setTime(dateFormat1.parse(shiftDateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        billContainer.date = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
        billContainer.time = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");


        return billContainer;
    }

    private List<SelectedItemDetails> PendingKotItemsToSelectedItemDetailses ( List<PendingKotItem> pendingKotItems) {

        ArrayList<SelectedItemDetails> selectedItemDetailse = new ArrayList<>();
        for (PendingKotItem item :pendingKotItems) {
            try {
                SelectedItemDetails details = new SelectedItemDetails();
                details.itemCode = item.Itm_Cd;
                details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                details.Itm_UCost = item.KotD_UCost;
                details.itemName = item.Itm_Name;
                details.Unit_Fraction = item.Unit_Fraction;
                details.Unit_cd = item.Kot_MuUnit;
                details.barcode = item.Barcode;
                details.Kot_No = item.Kot_No;
                details.Kot_No = item.Kot_No;
                details.Menu_SideDish = item.menu_SideDish;
                details.Cmd_IsBuffet = "0";

                details.Cus_Name = item.Cus_Name;
                details.Cus_Cd = item.Cus_Cd;
                details.Disc_Per = item.Disc_Per;

                details.Cm_Type = item.Cm_Type;
                details.Cm_Covers = item.Cm_Covers;
                details.Sman_Cd = item.Sman_Cd;
                details.Tab_Cd = item.Tab_Cd;
                details.CmD_Amt = item.CmD_Amt;
                details.isFoc = item.isFoc;
                selectedItemDetailse.add(details);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return selectedItemDetailse;
    }



}
