package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.ArrayList;
import java.util.List;

public class McItemModifiersAdapter extends RecyclerView.Adapter<McItemModifiersAdapter.ItemModifiersAdapterViewHolder>{
Context context;

    public ArrayList<Item>  modifiers;


    private onItemClickCallBack itemClickCallback;



    public interface onItemClickCallBack{
        void onItemSelected(int pos);
    }

    public McItemModifiersAdapter(onItemClickCallBack itemClickCallback) {
        this.modifiers = new ArrayList<>();
        this.itemClickCallback = itemClickCallback;
    }

    public McItemModifiersAdapter(onItemClickCallBack itemClickCallback, ArrayList<Item>  modifiers) {
        this.modifiers = modifiers;
        this.itemClickCallback = itemClickCallback;
    }

    @Override
    public ItemModifiersAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context=viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.multicorse_item, viewGroup, false);
        ItemModifiersAdapterViewHolder viewHolder = new ItemModifiersAdapterViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemModifiersAdapterViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new Listner(position));
       /* if(!modifiers.get(position).Rate.trim().equals("0"))
            holder.itemName.setText(modifiers.get(position).Item_Name+" ("+ Utilities.getDefaultCurrencyFormat(Double.parseDouble(modifiers.get(position).Rate),context)+")");
        else
            holder.itemName.setText(modifiers.get(position).Item_Name);*/

        if(modifiers.get(position).Item_Name != null)
            holder.tvItemName.setText(modifiers.get(position).Item_Name);
        if(modifiers.get(position).getIngredients().size() > 0){

            String ingredient = "";
            for(int i = 0 ; i < modifiers.get(position).getIngredients().size() ; i++){

                if(ingredient.matches(""))
                    ingredient = modifiers.get(position).getIngredients().get(i).getName();
                else
                    ingredient = ingredient + ", " + modifiers.get(position).getIngredients().get(i).getName();
            }

            holder.tvItemIngredients.setText(ingredient);
        }

    }

    @Override
    public int getItemCount() {
        if(modifiers == null)
            return 0;
        else
            return modifiers.size();
    }




    public static class ItemModifiersAdapterViewHolder extends RecyclerView.ViewHolder{


        private final TextView tvItemName;
        private final TextView tvItemIngredients;

        /*private TextView itemName;
                private LinearLayout closeBtn;*/
        public ItemModifiersAdapterViewHolder(View itemView) {
            super(itemView);
            /*itemName = (TextView) itemView.findViewById(R.id.txtModifierName);
            closeBtn = (LinearLayout) itemView.findViewById(R.id.ll_root);*/

            tvItemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvItemIngredients = (TextView) itemView.findViewById(R.id.tv_item_ingredients);
        }
    }

    private class Listner extends RecyclerViewCallBack{

        public Listner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {

            if(itemClickCallback != null)
                itemClickCallback.onItemSelected(position);
            removeFromList(position);


        }
    }

    public void removeFromList(int position) {
//        modifiers.remove(position);
        this.notifyDataSetChanged();
    }

    public void removeAllList() {

        int size = this.modifiers.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.modifiers.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }

    }


}
