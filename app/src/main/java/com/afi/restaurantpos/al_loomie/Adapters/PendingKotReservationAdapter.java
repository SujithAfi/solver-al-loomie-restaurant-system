package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.CustomViews.MyCheckBox;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AFI on 10/20/2015.
 */
public class PendingKotReservationAdapter extends RecyclerView.Adapter<PendingKotReservationAdapter.PendingKotAdapterVH> {

    private PendingKotSelectListner pendingKotSelectListner;

    private Context context;

    private List<PendingKot> pendingKotList;
    public interface PendingKotSelectListner{
        void onKotselected(String kotNumber);
        void onKotCheckChange();
    }

    public PendingKotReservationAdapter(List<PendingKot> pendingKotList , PendingKotSelectListner pendingKotSelectListner) {
        this.pendingKotList = pendingKotList;
        this.pendingKotSelectListner = pendingKotSelectListner;
    }

    @Override
    public PendingKotAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_pending_kot_reservation, viewGroup, false);
        PendingKotAdapterVH vh = new PendingKotAdapterVH(v);
        context = v.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(PendingKotAdapterVH holder, final int position) {
        final PendingKot pendingKot = pendingKotList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pendingKotSelectListner != null)
                    pendingKotSelectListner.onKotselected(pendingKot.Kot_No);
            }
        });
        if(pendingKot.Cus_Name != null && !pendingKot.Cus_Name.matches("")){
            holder.trCust_Name.setVisibility(View.VISIBLE);
            holder.tvCust_Name.setText(pendingKot.Cus_Name);
        }
        holder.txtKotNumber.setText(pendingKot.Kot_No.replace("#" , ""));
        holder.txtKotType.setText(pendingKot.Kot_Type.equals("1") ? "Dine-in" : pendingKot.Kot_Type.equals("2") ? "TAKE AWAY" : "HOME DELIVERY" );
        holder.txtGuestCount.setText(pendingKot.Kot_Covers);
        holder.txtSalesman.setText(pendingKot.Sman_Name);
//        holder.checkBox.setMyCheckboxChecked(pendingKot.isSelected);
        if(pendingKot.Tab_Name != null && pendingKot.Tab_Name.length() != 0)
            holder.tvTable.setText(pendingKot.Tab_Name);
        holder.tvCounter.setText(pendingKot.Cm_Counter);

       /* try {
            if ( pendingKot.Tab_cd.length() == 0 ) {
                holder.txtTableCodeOrCusNameKey.setText("Customer Name");
                holder.tvTable.setText(pendingKot.Cus_Name);
            }
            else {
                holder.txtTableCodeOrCusNameKey.setText("Table Code");
                holder.tvTable.setText(pendingKot.Tab_cd);
            }
        }
        catch (Exception e) {

        }*/

       /* holder.checkBox.setMyCheckedChangedListner(new MyCheckBox.MyCheckedChangedListner() {
            @Override
            public void onCheckedChanged(boolean state, boolean isFromUser) {
                if(isFromUser) {
                    pendingKotList.get(position).isSelected = state;
                    pendingKotSelectListner.onKotCheckChange();
                }
            }
        });*/
        holder.txtTotalAmount.setText(Utilities.getDefaultCurrencyFormat(pendingKot.Kot_TotAmt , context));
    }

    @Override
    public int getItemCount() {
        return pendingKotList.size();
    }

    public static class PendingKotAdapterVH extends RecyclerView.ViewHolder{
        private final TextView tvCust_Name;
        private final TableRow trCust_Name;
        private final TextView tvCounter;
        public TextView txtKotType;
        public TextView tvTable;
        public TextView txtGuestCount;
        public TextView txtSalesman;
        public TextView txtTotalAmount;
//        public MyCheckBox checkBox;
        public TextView txtKotNumber;
//        public TextView txtTableCodeOrCusNameKey;
        public PendingKotAdapterVH(View itemView) {
            super(itemView);
            txtKotType = (TextView) itemView.findViewById(R.id.txtKotType);
            tvTable = (TextView) itemView.findViewById(R.id.txtTableCodeOrCusNameKeyVal);
//            txtTableCodeOrCusNameKey = (TextView) itemView.findViewById(R.id.txtTableCodeOrCusNameKey);
            txtGuestCount = (TextView) itemView.findViewById(R.id.txtGuestCount);
            txtSalesman = (TextView) itemView.findViewById(R.id.txtSalesman);
            txtTotalAmount = (TextView) itemView.findViewById(R.id.txtTotalAmount);
//            checkBox = (MyCheckBox) itemView.findViewById(R.id.checkBoxSelected);
            txtKotNumber = (TextView) itemView.findViewById(R.id.txtKotNumber);
            tvCust_Name = (TextView) itemView.findViewById(R.id.tv_customer_name);
            trCust_Name = (TableRow) itemView.findViewById(R.id.tr_customer);
            tvCounter = (TextView) itemView.findViewById(R.id.tv_counter);
        }
    }

    public List<PendingKot> getAllSelectedKots(){
        List<PendingKot> selectedPendingKots = new ArrayList<>();
        if ( pendingKotList == null)
            return selectedPendingKots;
        for(PendingKot pendingKot : pendingKotList)
            if(pendingKot.isSelected)
                selectedPendingKots.add(pendingKot);
        return selectedPendingKots;
    }

    public void setPendingKotSelectListner(PendingKotSelectListner pendingKotSelectListner) {
        this.pendingKotSelectListner = pendingKotSelectListner;
    }
}
