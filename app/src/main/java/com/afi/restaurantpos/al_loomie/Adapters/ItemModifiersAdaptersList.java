package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 10/5/2015.
 */
public class ItemModifiersAdaptersList extends RecyclerView.Adapter<ItemModifiersAdaptersList.ItemModifiersAdaptersListVH>{
Context context;

    private List<ItemModifiers> modifiers;

    //private List<Integer> filterList = null;

    private Map<Integer , Boolean> filterMap = null;

    private ItemModifiersAdaptersListListner itemClickCallback;


    public interface ItemModifiersAdaptersListListner{
        void onItemSelected(ItemModifiers modifiers);
    }

    public ItemModifiersAdaptersList(List<ItemModifiers> modifiers , ItemModifiersAdaptersListListner callback) {
        this.modifiers = modifiers;
        this.itemClickCallback = callback;
        filterMap = new HashMap<>();
    }

    @Override
    public ItemModifiersAdaptersListVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context=viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_ingrediants_list, viewGroup, false);
        ItemModifiersAdaptersListVH listVH = new ItemModifiersAdaptersListVH(v);
        return listVH;
    }

    @Override
    public void onBindViewHolder(ItemModifiersAdaptersListVH holder, int position) {
        holder.itemname.setOnClickListener(new Listner(position));
        if(!modifiers.get(position).rate.trim().equals("0"))
        holder.itemname.setText(modifiers.get(position).name+" ("+ Utilities.getDefaultCurrencyFormat(Double.parseDouble(modifiers.get(position).rate),context )+ ")");
        else
            holder.itemname.setText(modifiers.get(position).name);
    }

    @Override
    public int getItemCount() {
        if(modifiers == null)
            return 0;
        else {
            return modifiers.size() ;
        }
    }

    public static final class ItemModifiersAdaptersListVH extends RecyclerView.ViewHolder{

        private TextView itemname;

        public ItemModifiersAdaptersListVH(View itemView) {
            super(itemView);
            itemname = (TextView) itemView.findViewById(R.id.txtIngredientName);
        }
    }

    private class Listner extends RecyclerViewCallBack{

        public Listner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position ) {
            ItemModifiers data = modifiers.get(position);
            removeFromList(position);
            if(itemClickCallback != null)
                itemClickCallback.onItemSelected(data);

        }
    }

    private void removeFromList(int position) {
        modifiers.remove(position);
        this.notifyDataSetChanged();
    }

    public void addToList(ItemModifiers itemModifiers){
        modifiers.add(itemModifiers);
        this.notifyDataSetChanged();
    }

    public List<ItemModifiers> getModifiers() {
        return modifiers;
    }
}
