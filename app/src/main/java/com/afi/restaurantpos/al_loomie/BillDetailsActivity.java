package com.afi.restaurantpos.al_loomie;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.afi.restaurantpos.al_loomie.Dialogs.BillFragmentDialog;
import com.afi.restaurantpos.al_loomie.Models.FinalAmount;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Fragments.BillFragment;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotOnTable;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class BillDetailsActivity extends AppCompatActivity {

    BillFragment billFragment;

    private String newBillNumber = "";

    private boolean modifyBillMode = false;
    private double value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_details);
        Utilities.initURL(this);
        Utilities.initKitkatStatusbarTransparancy(this);
        try {
            modifyBillMode = getIntent().getBooleanExtra("modifyBillMode" , false);
        }
        catch (Exception e){

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        billFragment = new BillFragment();
        billFragment.setIsNewInvoiceMode1(!modifyBillMode);
        processBilldata();
        getSupportFragmentManager().beginTransaction().replace(R.id.bill_container, billFragment, "bill").commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if(!modifyBillMode)
            getSupportActionBar().setSubtitle("Kot No. " + getIntent().getStringExtra("kot"));

        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else
            loadNewBillNumber();
    }

    private void loadNewBillNumber() {
        try {
            final MaterialDialog dialog = new MaterialDialog.Builder(this)
                    .content("Loading Selected Kot's")
                    .progress(true, 1)
                    .titleColorRes(R.color.colorAccentDark)
                    .cancelable(false).build();
            dialog.show();
            Utilities.getRetrofitWebService(getApplicationContext()).getNewBillNumber(ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<Map<String, String>>() {
                @Override
                public void onResponse(Response<Map<String, String>> response, Retrofit retrofit) {
                    dialog.cancel();
                    Map<String, String> stringStringMap = response.body();
                    if (stringStringMap.get("bill_no") != null) {
                        if (stringStringMap.get("bill_no").length() > 0) {
                            ApplicationSingleton.getInstance().setNewBillNumber(Utilities.getKotFormat(stringStringMap.get("bill_no")));
                        }
                    }

                    newBillNumber = String.format("Bill No : %s", Utilities.getKotFormat(stringStringMap.get("bill_no")));
                    getSupportActionBar().setSubtitle(newBillNumber);
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    private void processBilldata() {


        try {
            if(!modifyBillMode) {
                PendingKotOnTable pendingKot = ApplicationSingleton.getInstance().getPendingKot();
                List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
                for (PendingKotItem item : pendingKot.details) {
                    try {
                        SelectedItemDetails details = new SelectedItemDetails();
                        details.itemCode = item.Itm_Cd;
                        details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                        details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                        details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                        details.Itm_UCost = item.KotD_UCost;
                        details.itemName = item.Itm_Name;
                        details.Unit_Fraction = item.Unit_Fraction;
                        details.Unit_cd = item.Kot_MuUnit;
                        details.barcode = item.Barcode;
                        details.Menu_SideDish = item.menu_SideDish;
                        //// TODO: 10/22/2015 add all here
                        details.Kot_No = item.Kot_No;
                        details.Menu_SideDish = item.menu_SideDish;
                        details.Cmd_IsBuffet = "0";
                        details.Cus_Name = item.Cus_Name;
                        details.Cus_Cd = item.Cus_Cd;
                        details.Disc_Per = item.Disc_Per;

                        details.Cm_Covers = item.Cm_Covers;
                        details.Tab_Cd = item.Tab_Cd;

                        selectedItemDetailses.add(details);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                billFragment.setData(selectedItemDetailses);
                billFragment.setIsTemporaryMode(false);
            }
            else {
                List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
                for (PendingKotItem item :ApplicationSingleton.getInstance().getTemporaryPendingKotItems()) {
                    try {
                        SelectedItemDetails details = new SelectedItemDetails();
                        details.itemCode = item.Itm_Cd;
                        details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                        details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                        details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                        details.Itm_UCost = item.KotD_UCost;
                        details.itemName = item.Itm_Name;
                        details.Unit_Fraction = item.Unit_Fraction;
                        details.Unit_cd = item.Kot_MuUnit;
                        details.barcode = item.Barcode;
                        details.Kot_No = item.Kot_No;
                        details.Kot_No = item.Kot_No;
                        details.Menu_SideDish = item.menu_SideDish;
                        details.Cmd_IsBuffet = "0";

                        details.Cus_Name = item.Cus_Name;
                        details.Cus_Cd = item.Cus_Cd;
                        details.Disc_Per = item.Disc_Per;

                        details.Cm_Type = item.Cm_Type;
                        details.Cm_Covers = item.Cm_Covers;
                        details.Sman_Cd = item.Sman_Cd;
                        details.Tab_Cd = item.Tab_Cd;
                        details.CmD_Amt = item.CmD_Amt;
                        selectedItemDetailses.add(details);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                billFragment.setData(selectedItemDetailses);
                billFragment.setIsTemporaryMode(true);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bill_details , menu);
        menu.findItem(R.id.menu_option_print).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                saveAndPlaceBill();

                return false;
            }
        });
        menu.findItem(R.id.menu_option_reload).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        return  true;
    }

    private void saveAndPlaceBill() {
        //TODO BIG O

        try {
            if(billFragment != null){
                List<SelectedItemDetails> allItems = billFragment.getAllSelectedItemDetailses();
                if(allItems != null){
                    if(allItems.size() > 0 ){
                        final BillContainer saveBillData = prepareData(allItems);
                        new BillFragmentDialog()
                                .setBillItems(saveBillData.details)
                                .setNewBillNumber(newBillNumber)
                                .setBillListner(new BillFragmentDialog.BillListner() {
                                    @Override
                                    public void onPositive(List<BillItem> billItems , FinalAmount finalAmount) {

                                        if(billItems != null){

                                            saveBillData.details = billItems;

                                            if(finalAmount != null) {
                                                if(finalAmount.focAmount != null)
                                                    saveBillData.Cm_FocAmt = finalAmount.focAmount;
                                                if(finalAmount.serviceAmount != null)
                                                    saveBillData.Service_Amt = finalAmount.serviceAmount;
                                                if(finalAmount.tax1Amount != null)
                                                    saveBillData.Cm_Tax1_Amt = finalAmount.tax1Amount;
                                                if(finalAmount.tax2Amount != null)
                                                    saveBillData.Cm_Tax2_Amt = finalAmount.tax2Amount;
                                                if(finalAmount.netAmount != null)
                                                    saveBillData.Cm_NetAmt = finalAmount.netAmount;
                                            }

                                            for(BillItem items : billItems){

                                                Log.e("items is FOC====>>" , String.valueOf(items.isFoc));
                                            }

                                        }

                                        if (!Utilities.isNetworkConnected(getApplicationContext()))
                                            Utilities.createNoNetworkDialog(getApplicationContext());
                                        else {
                                            final MaterialDialog dialog = new MaterialDialog.Builder(BillDetailsActivity.this)
                                                    .content("Saving Bill")
                                                    .progress(true, 1)
                                                    .titleColorRes(R.color.colorAccentDark)
                                                    .cancelable(false).build();

                                            dialog.show();
                                            try {
                                                Utilities.getRetrofitWebService(getApplicationContext()).saveBill(saveBillData).enqueue(new Callback<Map<String, Object>>() {
                                                    @Override
                                                    public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                                        dialog.cancel();
                                                        final Map<String, Object> stringObjectMap = response.body();
                                                        if ((Boolean) stringObjectMap.get("response_code")) {
                                                            new MaterialDialog.Builder(BillDetailsActivity.this)
                                                                    .titleColorRes(R.color.colorAccentDark)
                                                                    .title((Boolean)stringObjectMap.get("response_code") ? "Bill Saved" : "Can't save bill")
                                                                    .positiveText("Ok")
                                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                        @Override
                                                                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                            if ((Boolean)stringObjectMap.get("response_code")) {
                                                                                finish();
                                                                                finish();
                                                                            }
                                                                        }
                                                                    }).build().show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Throwable t) {
                                                        dialog.cancel();
                                                    }
                                                });
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    }

                                    @Override
                                    public void onNegative() {

                                    }
                                })
                                .show(getSupportFragmentManager(), "Billu");


                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BillContainer prepareData(List<SelectedItemDetails> selectedItemDetailses) {
        String Tab_Cd = "";
        String Cm_Covers = "";

        Context context = this;
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        SharedPreferences preferences = Utilities.getSharedPreferences(context);
        int index = 0;
        double Cm_TotAmt = 0;
        String Cus_Name = "";
        String Cus_Cd = "";
        String Disc_Per = "";
        BillContainer billContainer = new BillContainer();
        List<BillItem> billItems = new ArrayList<>();
        Log.e("BillContainer","prepareData");
        for(SelectedItemDetails details : selectedItemDetailses){

            Cus_Name = details.Cus_Name == null ? "" : details.Cus_Name;
            Cus_Cd = details.Cus_Cd == null ? "" : details.Cus_Cd;
            Disc_Per = details.Disc_Per == null ? "" : details.Disc_Per;

            Tab_Cd = details.Tab_Cd;
            Cm_Covers = details.Cm_Covers;
            BillItem billItem = new BillItem();
            billItem.Itm_cd = details.itemCode;
            //put modifiers
            billItem.Menu_SideDish = "";
            if(details.modify != null){
                for (int i= 0 ; i < details.modify.size() ; i++ ) {
                    ItemModifiers modifier = details.modify.get(i);
                    if(i + 1 == details.modify.size() )
                        billItem.Menu_SideDish += modifier.name;
                    else if(i == 0)
                        billItem.Menu_SideDish += modifier.name;
                    else
                        billItem.Menu_SideDish += "," + modifier.name;
                }
            }
            billItem.Cmd_ItmName = details.itemName;
            billItem.BarCode = details.barcode;
            billItem.Unit_Fraction = details.Unit_Fraction;
            billItem.Cmd_Qty = details.itemQuantity + "";
            billItem.Unit_Cd = details.Unit_cd;
            billItem.Cmd_MuUnit = "1";//TODO
            billItem.Cmd_MuQty = details.itemQuantity + "";
            billItem.Cmd_MuFraction = "1";
            billItem.Cmd_MuConvToBase = "1";
            billItem.Cmd_MuUcost = "1";
            billItem.Cmd_Rate = details.itemUnitCost + "";//// TODO: 10/21/2015
            double Cmd_Amta =(details.itemUnitCost)*(details.itemQuantity);
            String Cmd_DiscPers =billItem.Cmd_DiscPer;
            billItem.Cmd_DiscPer = details.Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
            if(Cmd_DiscPers!=null) {
                billItem.Cmd_DiscAmt = Utilities.getPercentage(Cmd_Amta , Double.parseDouble(billItem.Cmd_DiscPer)) + "";
            }
            else{
                billItem.Cmd_DiscAmt ="0";
            }
            billItem.Cmd_Amt = (Cmd_Amta   -  Double.parseDouble( billItem.Cmd_DiscAmt )) + "";
            billItem.Cmd_Ctr = index++ + "";
            billItem.Cmd_SubCtr = "0";
            billItem.Cmd_RowType = "A";
            billItem.Cmd_NotCosted = "0";
            billItem.Cmd_Cancelled = "0";
            billItem.Cmd_IsLzQty = "1";
            billItem.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO , 1) + "";
            billItem.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE , "");
            billItem.Cmd_Status = "1";
            billItem.Cm_ThruDo = "0";
            billItem.IsModified = "0";
            billItem.Cm_Ref = "1";
            billItem.Cmd_DiscReason = "No";
            billItem.Kot_No = details.Kot_No == null ? "0" : details.Kot_No;
            billItem.Rcp_Cd = "1";
            billItem.Cmd_Ucost = details.itemUnitCost + "";
            billItem.Cmd_IsBuffet = details.Cmd_IsBuffet;//// TODO: 10/21/2015
            billItems.add(billItem);
            Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);


            billItem.Cmd_DiscPer = "0";
        }
        billContainer.details = billItems;
        billContainer.Usr_id = preferences.getString(Constants.SHARED_PREF_KEY_ID, "");
        billContainer.Cm_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_No = singleton.getNewBillNumber();
        billContainer.Sman_Cd = singleton.getSalesman(this) == null ? "0" : singleton.getSalesman(this).Id + "";
        billContainer.Cus_Cd = Cus_Cd;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Id + "";
        billContainer.Cus_Name = Cus_Name;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Name + "";billContainer.Cm_TotAmt =
        billContainer.Cm_DiscPer = Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
        billContainer.Cm_DiscAmt = billContainer.Cm_DiscPer == null ? "0" : billContainer.Cm_DiscPer.length() == 0 ? "0" : Utilities.getPercentage(Cm_TotAmt , Double.parseDouble(billContainer.Cm_DiscPer)) + "";

        billContainer.Cm_TotAmt = Cm_TotAmt + "";

        Cm_TotAmt -= Double.parseDouble(billContainer.Cm_DiscAmt);

        billContainer.Cm_Tax1_Per = Utilities.getTaxFraction1(context) + "";
        billContainer.Cm_Tax1_Amt = Utilities.findTax1(context, Cm_TotAmt) + "";
        billContainer.Cm_Tax2_Per = Utilities.getTaxFraction2(context) + "";
        billContainer.Cm_Tax2_Amt = Utilities.findTax2(context, Cm_TotAmt) + "";
        billContainer.Cm_NetAmt = (Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) ) + "";
        billContainer.Cm_Tips_Amt = "0";
        billContainer.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO , 0) + "";
        billContainer.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
        billContainer.Cm_BC_Settle_Amt = "0.000";
        billContainer.Cm_Paid_Amt = "0.000";
        billContainer.Cm_Cancel_Reason = "";
        billContainer.Cm_Cancelled = "0";
        billContainer.Cm_Cus_Phone = singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().getId() + "";
        billContainer.Cm_Type = "1";//TODO get order type
        billContainer.Tab_Cd = Tab_Cd;//singleton.getTable() == null ? "" : singleton.getTable().Tab_Cd;//TODO
        billContainer.Cm_Covers = Cm_Covers;//singleton.getGuestCount();
        billContainer.Srvr_Cd = "1";
        billContainer.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME , "");
        billContainer.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME , "0");


        //(Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) - Double.parseDouble( billContainer.Cm_DiscPer)) + "";
        // billContainer.Collect_Sft_No = "2";//// TODO: 10/21/2015
        //billContainer.Collect_Sft_Dt = "10-10-2015";//// TODO: 10/21/2015
        int i = 0;
        i++;
        billContainer.Srvr_Cd = i + "";

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar1 = Calendar.getInstance();

        String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

        if(shiftDateString != null && !shiftDateString.matches("")){

            try {
                calendar1.setTime(dateFormat1.parse(shiftDateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        billContainer.date = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
        billContainer.time = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");


        return billContainer;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return true;
    }
}
