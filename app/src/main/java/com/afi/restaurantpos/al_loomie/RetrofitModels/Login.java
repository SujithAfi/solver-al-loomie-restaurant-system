package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.util.List;

/**
 * Created by AFI on 9/29/2015.
 */
public class Login {
    public int loginResponse;
    public String loginResponseString;
    public String sessionId;
    public String Usr_Name;
    public String Id;

    // Currency Decimal Format
    public String currencyDF;

    // Tax1 Description
    public String tax1_Caption;
    public String tax1_TaxRate;
    public String istax1_Enabled;

    // Tax2 Description
    public String istax2_Enabled;
    public String tax2_Caption;
    public String tax2_TaxRate;

    // Service Charge
    public String isService_Charge_Enabled;
    public String service_Charge_Rate;

    public int shift_id;
    public int shift_no;
    public String shift_date;
    public String shift_time;
    public String SlaesId;
    public String Shift;
    public String Counter;
    public String Remarks ;
    public String float_cash ;
    public boolean user_type;
    public List<User_Rights> UserRights;
    public String serverDate;

}
