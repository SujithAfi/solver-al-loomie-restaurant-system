package com.afi.restaurantpos.al_loomie.Fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.CustomerSelectionListAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.CustomerDetailsFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.SelectCustomerDialogFragment;
import com.afi.restaurantpos.al_loomie.Models.CreditAccount;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class CustomerFragment extends Fragment implements CustomerSelectionListAdapter.CustomerSelectionListner, View.OnClickListener{

    Customer customer;


    private static final String[] impDateTypes = new String[]{"BirthDay", "Marriage Aniversary"};
    private int addDate = 1;
    private int add_Date2 = 2;


    private TextInputEditText edtTxtCustomerCode;
    private TextInputEditText edtTxtCustomerName;
    private TextInputEditText edtTxtCustomerAddress;
    private TextInputEditText edtTxtDiscount;
    private TextInputEditText edtTxtAllergicInformation;
    private TextInputEditText edtTxtHabit;



    private CheckBox addMoreDretails;

    private AppCompatSpinner dateTypeSpinner1;
    private AppCompatSpinner dateTypeSpinner2;
    private AppCompatSpinner dateTypeSpinner3;

    private TextView impDate1;
    private TextView impDate2;
    private TextView impDate3;

    private TextInputEditText tvFav1;
    private TextInputEditText tvFav2;
    private TextInputEditText tvFav3;

    private RadioGroup radioPaymentMode;
    private RadioButton radioCash;
    private RadioButton radioCredit;

    private AppCompatSpinner creditAccountName;
    private EditText creditRoomNumber;
    private TextView creditCheckInDate;

    private Button create;
    private Button addImpDate1;
    private Button addImpDate2;


    private Handler uiHandler = new Handler();
    private List<CreditAccount> creaditAccounts;
    private String creditAccId;
    private ArrayAdapter<String> Aadapter;
    private String oldQuery = "";


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            try {
                int action = intent.getIntExtra("action", -1);

                if (action == 0) {//Query changed
                    String query = intent.getStringExtra("query");
                    onQueryChanged(query);
                } else if (action == 1) {//has focus
                    Log.e("has focus", "has focus");
                } else if (action == 2) {//Lost focus
                    Log.e("Lost focus", "Lost focus");
                } else if (action == 3) {//refresh list
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };
    private int itemIndex = -1;
    private Boolean isEditable = false;
    private boolean dataChanged = false;
    private String custId = null;
    private TextInputEditText etCustPhone1;
    private TextInputEditText etCustPhone2;
    private TextInputEditText etEamil;

    public CustomerFragment() {
        // Required empty public constructor
    }

    public static CustomerFragment newInstance(String param1, String param2) {
        CustomerFragment fragment = new CustomerFragment();

        return fragment;
    }

    public CustomerFragment setCustomer(Customer customer , Boolean isEditable) {
        this.customer = customer;
        this.isEditable = isEditable;
        return this;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoadCreditAccounts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.view_customer_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();

        edtTxtCustomerCode = (TextInputEditText) getView().findViewById(R.id.edtTxtCustomerCode);
        etCustPhone1 = (TextInputEditText) getView().findViewById(R.id.tiet_phone1);
        etCustPhone2 = (TextInputEditText) getView().findViewById(R.id.tiet_phone2);
        edtTxtCustomerName = (TextInputEditText) getView().findViewById(R.id.edtTxtCustomerName);
        edtTxtCustomerAddress = (TextInputEditText) getView().findViewById(R.id.edtTxtCustomerAddress);
        edtTxtAllergicInformation = (TextInputEditText) getView().findViewById(R.id.editTextAlergicInformation);
        edtTxtHabit = (TextInputEditText) getView().findViewById(R.id.editTextHabit);
        etEamil = (TextInputEditText) getView().findViewById(R.id.et_email);


        radioPaymentMode = (RadioGroup) getView().findViewById(R.id.rPaymentMode);
        radioCash = (RadioButton) getView().findViewById(R.id.rButtonCashPayment);
        radioCredit = (RadioButton) getView().findViewById(R.id.rButtonCredit);

        creditAccountName=(AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName);
        creditRoomNumber=(EditText) getView().findViewById(R.id.paymentRoomNumb);
        creditCheckInDate=(TextView)getView().findViewById(R.id.PaymentCheckIn);

        addMoreDretails = (CheckBox) getView().findViewById(R.id.checkboxSelectCreatedAccount);

        dateTypeSpinner1 = (AppCompatSpinner) getView().findViewById(R.id.dateTypSpinner1);
        dateTypeSpinner2 = (AppCompatSpinner) getView().findViewById(R.id.dateTypSpinner2);
        dateTypeSpinner3 = (AppCompatSpinner) getView().findViewById(R.id.dateTypSpinner3);

        impDate1 = (TextView) getView().findViewById(R.id.txtDate1);
        impDate2 = (TextView) getView().findViewById(R.id.txtDate2);
        impDate3 = (TextView) getView().findViewById(R.id.txtDate3);

        tvFav1 = (TextInputEditText) getView().findViewById(R.id.edtTxtFavItem1);
        tvFav2 = (TextInputEditText) getView().findViewById(R.id.edtTxtFavItem2);
        tvFav3 = (TextInputEditText) getView().findViewById(R.id.edtTxtFavItem3);

        create = (Button) getView().findViewById(R.id.btnCreateAccount);
        addImpDate1 = (Button) getView().findViewById(R.id.btnAddImpDate2);
        addImpDate2 = (Button) getView().findViewById(R.id.btnAddImpDate3);


        dateTypeSpinner1.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, impDateTypes));
        dateTypeSpinner2.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, impDateTypes));
        dateTypeSpinner3.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, impDateTypes));

        impDate1.setOnClickListener(this);
        impDate2.setOnClickListener(this);
        impDate3.setOnClickListener(this);
        creditCheckInDate.setOnClickListener(this);

        addImpDate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getView().findViewById(R.id.addImpDate2).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.btnAddImpDate2).setVisibility(View.GONE);

            }
        });

        addImpDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getView().findViewById(R.id.addImpDate3).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.btnAddImpDate3).setVisibility(View.GONE);


            }
        });

        radioCredit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getView().findViewById(R.id.ll_paymentmode).setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });


//        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PREF_ISADMIN, false)) {

            if (isEditable)
                getView().findViewById(R.id.btnCreateAccount).setVisibility(View.VISIBLE);
            else
                getView().findViewById(R.id.btnCreateAccount).setVisibility(View.GONE);
//        }

        getView().findViewById(R.id.btnCreateAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (((Button) getView().findViewById(R.id.btnCreateAccount)).getText().equals("Edit")) {

                    ((Button) getView().findViewById(R.id.btnCreateAccount)).setText("Update");
                    makeViewsEditable();


                }
                else{

                if (validate()) {

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
                    SimpleDateFormat inputDateFormat = new SimpleDateFormat(("dd-MM-yyyy"));
                    final Customer customer = new Customer();
                    if(custId != null)
                        customer.setId(custId);
                    customer.setCus_Phone1(etCustPhone1.getText().toString());
                    customer.setCus_Phone2(etCustPhone2.getText().toString());
                    customer.setCus_Email(etEamil.getText().toString());
                    customer.setName(edtTxtCustomerName.getText().toString());
                    customer.setAddress1(edtTxtCustomerAddress.getText().toString());
                    customer.setAllergicInfo(edtTxtAllergicInformation.getText().toString());
                    customer.setCusHabits(edtTxtHabit.getText().toString());

                    try {
                        customer.setImpDate1(addMoreDretails.isChecked() ? dateFormat.format(inputDateFormat.parse(impDate1.getText().toString())) : "");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        customer.setImpDate2(addMoreDretails.isChecked() ? dateFormat.format(inputDateFormat.parse(impDate2.getText().toString())) : "");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        customer.setImpDate3(addMoreDretails.isChecked() ? dateFormat.format(inputDateFormat.parse(impDate3.getText().toString())) : "");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    customer.setImpDate1Desc(addMoreDretails.isChecked() ? impDateTypes[dateTypeSpinner1.getSelectedItemPosition()] : "");
                    customer.setImpDate2Desc(addMoreDretails.isChecked() ? impDateTypes[dateTypeSpinner2.getSelectedItemPosition()] : "");
                    customer.setImpDate3Desc(addMoreDretails.isChecked() ? impDateTypes[dateTypeSpinner3.getSelectedItemPosition()] : "");

                    customer.setCusFavorite1(tvFav1.getText().toString().trim());
                    customer.setCusFavorite2(tvFav2.getText().toString().trim());
                    customer.setCusFavorite3(tvFav3.getText().toString().trim());

                    int radioButtonID = radioPaymentMode.getCheckedRadioButtonId();
                    View radioButton = radioPaymentMode.findViewById(radioButtonID);
                    int idx = radioPaymentMode.indexOfChild(radioButton);

                    if (idx == 1) {

                        customer.setRoomNo(creditRoomNumber.getText().toString());
                        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                        Calendar c = Calendar.getInstance();
                        try {
                            c.setTime(sdfDate.parse(creditCheckInDate.getText().toString().trim()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String checkinDate = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()) + " 00:00:00.000";
                        customer.setCheckinDate(checkinDate);

                        try {
                            if (creditAccId != null)
                                customer.setAccId(creditAccId);
                            else if (creaditAccounts.size() > 0)
                                customer.setAccId(creaditAccounts.get(0).getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        customer.setIsCredit(true);

                    }
                    if(!Utilities.isNetworkConnected(getActivity()))
                        Toast.makeText(CustomerFragment.this.getContext(), "No internet Connectivity", Toast.LENGTH_SHORT).show();
                    else {
                        if(dataChanged)
                            UpdateCustomer(customer);
                        else
                            Toast.makeText(CustomerFragment.this.getContext(), "No Changes to Update", Toast.LENGTH_SHORT).show();

                    }


                }
            }


            }
        });

        addMoreDretails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                getView().findViewById(R.id.importantDates).setVisibility(isChecked ? View.VISIBLE : View.GONE);
                getView().findViewById(R.id.favFoodItems).setVisibility(isChecked ? View.VISIBLE : View.GONE);



            }
        });

        if(customer != null){


            if(customer.getId() != null) {
                custId = customer.getId();
                edtTxtCustomerCode.setText(customer.getId());
            }
            if(customer.getCus_Phone1() != null)
                etCustPhone1.setText(customer.getCus_Phone1());
            if(customer.getCus_Phone2() != null)
                etCustPhone2.setText(customer.getCus_Phone2());
            if(customer.getName() != null)
                edtTxtCustomerName.setText(customer.getName());
            if(customer.getAddress1() != null)
                edtTxtCustomerAddress.setText(customer.getAddress1());
            if(customer.getAllergicInfo() != null)
                edtTxtAllergicInformation.setText(customer.getAllergicInfo());
            if(customer.getCusHabits() != null)
                edtTxtHabit.setText(customer.getCusHabits());
            if(customer.getCus_Email() != null)
                etEamil.setText(customer.getCus_Email());
            if(customer.getIsCredit()){
                radioCredit.setChecked(true);
                if(customer.getRoomNo() != null)
                    creditRoomNumber.setText(customer.getRoomNo());
                if(customer.getCheckinDate() != null) {

                    Calendar calDate = Calendar.getInstance();
                    SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                    List<String> items = Arrays.asList(customer.getCheckinDate().split("\\s+"));

                    if (items.size() > 0) {
                        try {

                            calDate.setTime(sdfDate.parse(items.get(0)));
                            creditCheckInDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }


                }

            }
            else
                radioCash.setChecked(true);

            if((customer.getCusFavorite1() != null && !customer.getCusFavorite1().matches("")) || ( customer.getImpDate1() != null && !customer.getImpDate1().matches(""))){

                addMoreDretails.setChecked(true);

                if(customer.getImpDate1() != null && !customer.getImpDate1().matches("")){


                        Calendar calDate = Calendar.getInstance();
                        SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                        List<String> items = Arrays.asList(customer.getImpDate1().split("\\s+"));

                        if (items.size() > 0) {
                            try {

                                calDate.setTime(sdfDate.parse(items.get(0)));
                                impDate1.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }



                    }
                }

                if(customer.getImpDate1Desc() != null && !customer.getImpDate1Desc().matches("")){

                    int dIndex = 0;
                    for(int i = 0 ; i < impDateTypes.length ; i++) {
                        if(impDateTypes[i].equalsIgnoreCase(customer.getImpDate1Desc()))
                            dIndex = i;
                    }

                    dateTypeSpinner1.setSelection(dIndex);
                }

                if(customer.getImpDate2() != null && !customer.getImpDate2().matches("")){

                    getView().findViewById(R.id.addImpDate2).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.btnAddImpDate2).setVisibility(View.GONE);

                    Calendar calDate = Calendar.getInstance();
                    SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                    List<String> items = Arrays.asList(customer.getImpDate2().split("\\s+"));

                    if (items.size() > 0) {
                        try {

                            calDate.setTime(sdfDate.parse(items.get(0)));
                            impDate2.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }



                    }

                    if(customer.getImpDate2Desc() != null && !customer.getImpDate2Desc().matches("")){

                        int dIndex = 0;
                        for(int i = 0 ; i < impDateTypes.length ; i++) {
                            if(impDateTypes[i].equalsIgnoreCase(customer.getImpDate2Desc()))
                                dIndex = i;
                        }

                        dateTypeSpinner2.setSelection(dIndex);
                    }


                }

                if(customer.getImpDate3() != null && !customer.getImpDate3().matches("")){

                    getView().findViewById(R.id.addImpDate3).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.btnAddImpDate3).setVisibility(View.GONE);

                    Calendar calDate = Calendar.getInstance();
                    SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                    List<String> items = Arrays.asList(customer.getImpDate3().split("\\s+"));

                    if (items.size() > 0) {
                        try {

                            calDate.setTime(sdfDate.parse(items.get(0)));
                            impDate3.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }



                    }

                    if(customer.getImpDate3Desc() != null && !customer.getImpDate3Desc().matches("")){

                        int dIndex = 0;
                        for(int i = 0 ; i < impDateTypes.length ; i++) {
                            if(impDateTypes[i].equalsIgnoreCase(customer.getImpDate3Desc()))
                                dIndex = i;
                        }

                        dateTypeSpinner3.setSelection(dIndex);
                    }


                }

            }

            if(customer.getCusFavorite1() != null && !customer.getCusFavorite1().matches("")) {
                getView().findViewById(R.id.favFoodItems).setVisibility(View.VISIBLE);
                tvFav1.setText(customer.getCusFavorite1());
                if (customer.getCusFavorite2() != null && !customer.getCusFavorite2().matches(""))
                    tvFav2.setText(customer.getCusFavorite2());
                if (customer.getCusFavorite3() != null && !customer.getCusFavorite3().matches(""))
                    tvFav3.setText(customer.getCusFavorite3());
            }
            else
                getView().findViewById(R.id.favFoodItems).setVisibility(View.GONE);
        }

        edtTxtCustomerCode.setVisibility(View.GONE);
        edtTxtCustomerName.setVisibility(View.GONE);
        etCustPhone1.setEnabled(false);
        etCustPhone2.setEnabled(false);
        edtTxtCustomerAddress.setEnabled(false);
        edtTxtAllergicInformation.setEnabled(false);
        edtTxtHabit.setEnabled(false);
        radioCredit.setEnabled(false);
        radioCash.setEnabled(false);
        creditRoomNumber.setEnabled(false);
        creditCheckInDate.setEnabled(false);
        impDate1.setEnabled(false);
        impDate2.setEnabled(false);
        impDate3.setEnabled(false);
        tvFav1.setEnabled(false);
        tvFav2.setEnabled(false);
        tvFav3.setEnabled(false);
        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setEnabled(false);
        dateTypeSpinner1.setEnabled(false);
        dateTypeSpinner2.setEnabled(false);
        dateTypeSpinner3.setEnabled(false);
        addMoreDretails.setEnabled(false);
        etEamil.setEnabled(false);
        addMoreDretails.setVisibility(View.GONE);


        etEamil.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        edtTxtCustomerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        edtTxtCustomerAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        edtTxtAllergicInformation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        edtTxtHabit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        creditCheckInDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        impDate1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        impDate2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        impDate3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        tvFav1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        tvFav2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        tvFav3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        edtTxtCustomerCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        etCustPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        etCustPhone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                dataChanged = true;

            }
        });

        radioCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataChanged = true;
            }
        });

        radioCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataChanged = true;
            }
        });

        dateTypeSpinner3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dataChanged = true;
                return false;
            }
        });

        dateTypeSpinner2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dataChanged = true;
                return false;
            }
        });

        dateTypeSpinner1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dataChanged = true;
                return false;
            }
        });

        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dataChanged = true;
                return false;
            }
        });

    }

    private void makeViewsEditable() {

        edtTxtCustomerCode.setVisibility(View.GONE);
        edtTxtCustomerCode.setEnabled(false);
        etCustPhone1.setEnabled(true);
        etCustPhone2.setEnabled(true);
        edtTxtCustomerName.setVisibility(View.VISIBLE);
        edtTxtCustomerAddress.setEnabled(true);
        edtTxtAllergicInformation.setEnabled(true);
        edtTxtHabit.setEnabled(true);
        radioCredit.setEnabled(true);
        radioCash.setEnabled(true);
        creditRoomNumber.setEnabled(true);
        creditCheckInDate.setEnabled(true);
        impDate1.setEnabled(true);
        impDate2.setEnabled(true);
        impDate3.setEnabled(true);
        tvFav1.setEnabled(true);
        tvFav2.setEnabled(true);
        tvFav3.setEnabled(true);
        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setEnabled(true);
        dateTypeSpinner1.setEnabled(true);
        dateTypeSpinner2.setEnabled(true);
        dateTypeSpinner3.setEnabled(true);
        addMoreDretails.setEnabled(true);
        etEamil.setEnabled(true);
        addMoreDretails.setVisibility(View.VISIBLE);
    }

    private boolean validate() {

        int radioButtonID = radioPaymentMode.getCheckedRadioButtonId();
        View radioButton = radioPaymentMode.findViewById(radioButtonID);
        int idx = radioPaymentMode.indexOfChild(radioButton);

        if (edtTxtCustomerCode.getText().toString().trim().length() == 0){

            edtTxtCustomerCode.setError("Enter phone");
            edtTxtCustomerCode.requestFocus();
            return false;
        }

        else if(edtTxtCustomerName.getText().toString().trim().length() == 0){
            edtTxtCustomerName.setError("Enter Name");
            edtTxtCustomerName.requestFocus();
            return false;

        }


       /* else if (edtTxtCustomerAddress.getText().toString().trim().length() == 0){

            edtTxtCustomerAddress.setError("Enter Address");
            edtTxtCustomerAddress.requestFocus();
            return false;
        }*/

        else if(idx == 1){

            if(creditRoomNumber.getText().toString().trim().length() == 0){

                creditRoomNumber.setError("RoomNumber");
                creditRoomNumber.requestFocus();
                return false;

            }
            else if(creditCheckInDate.getText().toString().trim().length() == 0){

                creditCheckInDate.setError("CheckInDate");
                creditCheckInDate.requestFocus();
                return false;
            }





        }




        return true;
    }

    private void UpdateCustomer(final Customer customer) {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Updating Customer");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final Response<Customer> response = Utilities.getRetrofitWebService(getContext()).updateCustomerN(customer).execute();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (response != null) {

                                if (response.body() != null) {

                                    Toast.makeText(CustomerFragment.this.getContext(), "Updated", Toast.LENGTH_SHORT).show();
                                        onCustomerSelected(response.body());

                                } else {
                                    Toast.makeText(CustomerFragment.this.getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                } catch (IOException e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(CustomerFragment.this.getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();


    }

    @Override
    public void onClick(final View v) {

        final Calendar calendar = Calendar.getInstance();

        String date = ((TextView) v).getText().toString();
        if (!date.isEmpty()) {
            try {
                String[] dateParts = date.split("-");
                calendar.set(Calendar.YEAR, Integer.parseInt(dateParts[2]));
                calendar.set(Calendar.MONTH, Integer.parseInt(dateParts[1]) - 1);
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateParts[0]));
            } catch (Exception e) {

            }
        }


        new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);
                ((TextView) v).setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));


            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show();



    }

    public void LoadCreditAccounts(){

        final MaterialDialog dialogSave = new MaterialDialog.Builder(getActivity())
                .content("Account Details")
                .progress(true, 0)
                .cancelable(false)
                .build();

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {

            dialogSave.show();
            Utilities.getRetrofitWebService(getActivity()).getCreditAccounts().enqueue(new Callback<List<CreditAccount>>() {
                @Override
                public void onResponse(Response<List<CreditAccount>> response, Retrofit retrofit) {
                    dialogSave.dismiss();
                    if (response.body() != null) {

                        creaditAccounts = response.body();
                        List<String> datas = new ArrayList<>();
                        if(creaditAccounts.size() > 0) {
                            for (CreditAccount creAccount : creaditAccounts) {
                                datas.add(creAccount.getName());

                            }


                            if(customer.getAccName() != null && datas.size() > 0){

                                itemIndex = datas.indexOf(customer.getAccName());
                            }
                        }


                        Aadapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);

                        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setAdapter(Aadapter);

                        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                try {
                                    creditAccId  = creaditAccounts.get(position).getId();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        if(itemIndex != -1)
                            ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setSelection(itemIndex);


                    } else
                        Utilities.showToast("creadit Account Failed" , getActivity());
//                        Utilities.showToast(getResources().getString(R.string.resv_items_not_saved), getActivity());
                }

                @Override
                public void onFailure(Throwable t) {
                    dialogSave.dismiss();
                }
            });

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter(SelectCustomerDialogFragment.CREATE_CUSTOMER_FRAGMENT_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onQueryChanged(String query) {

        try {
            if (query.equals(oldQuery)) {
                return;
            } else {
                oldQuery = query;
            }


            Log.e("Query changed", query);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onItemSelected(Customer customer) {
        try {
            Intent intent = new Intent("customerSelected");
            intent.putExtra("customer", customer);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onCustomerSelected(Customer customer) {


        try {
            Intent intent = new Intent("customerSelected");
            intent.putExtra("customer", customer);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        CustomerDetailsFragment.custFrag.dismiss();

    }
}
