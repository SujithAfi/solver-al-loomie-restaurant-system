package com.afi.restaurantpos.al_loomie.View;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.afi.restaurantpos.al_loomie.R;

/**
 * Created by AFI on 1/15/2016.
 */
public class CropedCircle extends View {

    //circle and text colors
    private int circleCol, labelCol;
    //label text
    private String circleText;

    private Paint circlePaint;

    int screenWidth = 10;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public CropedCircle(Context context) {
        super(context);
        circlePaint = new Paint();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screenWidth = size.y;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public CropedCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        circlePaint = new Paint();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screenWidth = size.y;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public CropedCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        circlePaint = new Paint();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screenWidth = size.y;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int viewWidthHalf = this.getMeasuredWidth() / 2;
        int viewHeightHalf = this.getMeasuredHeight() / 2;

        int radius = 0;
        if (viewWidthHalf > viewHeightHalf)
            radius = viewHeightHalf ;
        else
            radius = viewWidthHalf ;
        int yParam = radius;
        circlePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        circlePaint.setAntiAlias(true);
        if ( screenWidth != 10) {
            radius = radius * 3;
            int height = this.getMeasuredHeight();
            int offset  = radius - (height / 2);
            yParam -=  offset;
        }

        circlePaint.setColor(getContext().getResources().getColor(R.color.colorPrimary));

        canvas.drawCircle(viewWidthHalf, yParam, radius , circlePaint);

    }
}
