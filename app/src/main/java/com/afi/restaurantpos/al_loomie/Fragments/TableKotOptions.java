package com.afi.restaurantpos.al_loomie.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.TableKotOptionsListAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.BillFragmentDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.CancelBillAndKotDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.KotAlertDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.ReorderAlertDialog;
import com.afi.restaurantpos.al_loomie.Models.FinalAmount;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Fragment to display KOT options on Table
 */
public class TableKotOptions extends android.support.v4.app.Fragment {
    private static final String ARG_PARAM_KOTNUMBER = "param1";

    public List<PendingKotItem> kotumbers;
    private RecyclerView rvListKots;

    List<SelectedItemDetails> selectedItemDetailses;

    String newBillNumber;

    private TextView txtNoPendingBillsOrKot;
    private List<Table> tables;


    public static TableKotOptions newInstance(List<PendingKotItem> kotnUmber , List<Table> tables) {
        TableKotOptions fragment = new TableKotOptions();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.kotumbers = kotnUmber;
        fragment.tables = tables;
        return fragment;
    }

    public TableKotOptions() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_table_kot_options, container, false);
        rvListKots = (RecyclerView) v.findViewById(R.id.rvListKots);
        txtNoPendingBillsOrKot = (TextView) v.findViewById(R.id.txtNoPendingBillsOrKot);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rvListKots.setHasFixedSize(true);
//        rvListKots.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvListKots.setLayoutManager(new GridLayoutManager(getContext() , 2));


            String selectedDate = null;
            if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                selectedDate =  new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate());
            final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

            if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                if(ApplicationSingleton.getInstance().getCurrentDate().before(Calendar.getInstance().getTime())){
                    rvListKots.setAdapter(new TableKotOptionsListAdapter(kotumbers , "KOT", getActivity() ,new TableKotOptionsListAdapter.OnItemCLickListner() {
                        @Override
                        public void onItemClicked(int position) {

                            List<String> kot = new ArrayList<String>();
                            kot.add(kotumbers.get(position).Kot_No);
                            if (!Utilities.isNetworkConnected(getContext()))
                                Utilities.createNoNetworkDialog(getContext());
                            else
                                loadKotDetails(kot);

                        }

                        @Override
                        public void onDeleteItem(final int position) {

                            new AlertDialog.Builder(getContext())
                                    .setTitle("Do you want to cancel KOT ?")
//                                    .setMessage(String.format(String.format("Cancelled %s", cancelType == CancelType.CANCEL_KOT ? "Kot" : "Bill")))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            PendingKot pendingKot = new PendingKot();
                                            pendingKot.Kot_No = kotumbers.get(position).Kot_No;
                                            CancelBillAndKotDialog.newInstance(CancelType.CANCEL_KOT)
                                                    .setPendingKot(pendingKot)
                                                    .setDialogListner(new CancelBillAndKotDialog.CancelBillAndKotDialogListner() {
                                                        @Override
                                                        public void onFinishedAction(CancelBillAndKotDialog cancelBillAndKotDialog) {
                                                            //TODO refresh items
                                                            cancelBillAndKotDialog.dismiss();
                                                            getActivity().sendBroadcast(new Intent("reloadBillAndKot"));
                                                        }
                                                    })
                                                    .show(getChildFragmentManager(), "Pending KOT");
                                        }
                                    })
                                    .setNegativeButton("Cancel" , new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .create().show();




                        }

                        @Override
                        public void onAlertClicked(int position) {

                            PendingKot pendingKot = new PendingKot();
                            pendingKot.Kot_No = kotumbers.get(position).Kot_No;
                            pendingKot.Tab_cd = kotumbers.get(position).Tab_cd;
                            pendingKot.Cus_Name = kotumbers.get(position).Cus_Name;
                            KotAlertDialog.newInstance(CancelType.ALERT)
                                    .setPendingKot(pendingKot)
                                    .setDialogListner(new KotAlertDialog.CancelBillAndKotDialogListner() {
                                        @Override
                                        public void onFinishedAction(KotAlertDialog cancelAlertDialog) {
                                            //TODO refresh items
                                            cancelAlertDialog.dismiss();
                                            getActivity().sendBroadcast(new Intent("reloadBillAndKot"));
                                        }
                                    })
                                    .show(getChildFragmentManager(), "Alert KOT");

                        }

                        @Override
                        public void onReorderClicked(int position) {

                            PendingKot pendingKot = new PendingKot();
                            pendingKot.Kot_No = kotumbers.get(position).Kot_No;
                            pendingKot.Tab_cd = kotumbers.get(position).Tab_Cd;
                            pendingKot.Tab_Name = kotumbers.get(position).Tab_Name;
                            pendingKot.Cus_Name = kotumbers.get(position).Cus_Name;
                            pendingKot.Cus_Cd = kotumbers.get(position).Cus_Cd;
                            pendingKot.guestCount = kotumbers.get(position).Kot_Covers;
                            pendingKot.Cm_Counter = kotumbers.get(position).Cm_Counter;


                          /*  Log.e("Counter=====>>" , kotumbers.get(position).Cm_Counter);
                            Log.e("Customer Code 1111=====>>" , kotumbers.get(position).Cus_Cd);
                            Log.e("Counter Name 11111=====>>" , kotumbers.get(position).Cus_Name);*/

                            ReorderAlertDialog.newInstance(CancelType.REORDER_KOT)
                                    .setPendingKot(pendingKot)
                                    .setTables(tables)
                                    .setDialogListner(new ReorderAlertDialog.ReorderKotDialogListner() {
                                        @Override
                                        public void onFinishedAction(ReorderAlertDialog cancelAlertDialog) {
                                            //TODO refresh items
                                            cancelAlertDialog.dismiss();
                                            getActivity().sendBroadcast(new Intent("reloadBillAndKot"));
                                        }
                                    })
                                    .show(getChildFragmentManager(), "Reorder KOT");



                        }

                    }));

    }

    }


    private void loadKotDetails(final List<String> kotStringList) {

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(ApplicationSingleton.getInstance().getCurrentDate());

        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime());

       /* for(int i = 0 ; i < kotStringList.size() ; i ++){


            Log.e("Item======>>" + i , "    " + kotStringList.get(i));
        }*/

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Loading Selected Kot's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        dialog.show();
        Utilities.getRetrofitWebService(getContext()).getKotDetailsFromKotNumber(kotStringList , date).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
            @Override
            public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                dialog.cancel();
                if ( response.body() != null) {
                    Map<String, List<PendingKotItem>> listMap = response.body();

                    try {
                        ApplicationSingleton.getInstance().setSelectedKotMap(listMap);
                        ApplicationSingleton.getInstance().setTemporaryPendingKotItems(listMap.get(kotStringList.get(0)));
                    }catch (Exception e)
                    {
                    }

                    //Pocess bill data
                    selectedItemDetailses = new ArrayList<>();
                    try {
                        if(ApplicationSingleton.getInstance().getTemporaryPendingKotItems() != null)
                        for (PendingKotItem item : ApplicationSingleton.getInstance().getTemporaryPendingKotItems()) {
                            SelectedItemDetails details = new SelectedItemDetails();
                            if(item.Itm_Cd != null)
                                details.itemCode = item.Itm_Cd;
                            if(item.KotD_Qty != null)
                                details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                            if(item.KotD_Rate != null)
                                details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                            if(item.KotD_Amt != null)
                                details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                            if(item.KotD_UCost != null)
                                details.Itm_UCost = item.KotD_UCost;
                            if(item.Itm_Name != null)
                                details.itemName = item.Itm_Name;
                            if(item.Unit_Fraction != null)
                                details.Unit_Fraction = item.Unit_Fraction;
                            if(item.Kot_MuUnit != null)
                                details.Unit_cd = item.Kot_MuUnit;
                            if(item.Barcode != null)
                                details.barcode = item.Barcode;
                            if(item.Kot_No != null)
                                details.Kot_No = item.Kot_No;
                            if(item.menu_SideDish != null)
                                details.Menu_SideDish = item.menu_SideDish;

                            details.Cmd_IsBuffet = "0";

                            if(item.Cus_Name != null)
                                details.Cus_Name = item.Cus_Name;
                            if(item.Cus_Cd != null)
                                details.Cus_Cd = item.Cus_Cd;
                            if(item.Disc_Per != null)
                                details.Disc_Per = item.Disc_Per;

                            if(item.Cm_Type != null)
                                details.Cm_Type = item.Cm_Type;
                            if(item.Cm_Covers != null)
                                details.Cm_Covers = item.Cm_Covers;
                            if(item.Sman_Cd != null)
                                details.Sman_Cd = item.Sman_Cd;
                            if(item.Tab_Cd != null)
                                details.Tab_Cd = item.Tab_Cd;
                            if(item.CmD_Amt != null)
                                details.CmD_Amt = item.CmD_Amt;
                            selectedItemDetailses.add(details);
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }


                    final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                            .content("Creating new Bill")
                            .progress(true, 1)
                            .titleColorRes(R.color.colorAccentDark)
                            .cancelable(false).build();

                    if (!Utilities.isNetworkConnected(getContext()))
                        Utilities.createNoNetworkDialog(getContext());
                    else {
                        dialog.show();
                        Utilities.getRetrofitWebService(getContext()).getNewBillNumber(ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<Map<String, String>>() {
                            @Override
                            public void onResponse(Response<Map<String, String>> response, Retrofit retrofit) {
                                dialog.cancel();

                                Map<String, String> stringStringMap = response.body();
                                if (stringStringMap.get("bill_no") != null) {
                                    if (stringStringMap.get("bill_no").length() > 0) {
                                        ApplicationSingleton.getInstance().setNewBillNumber(Utilities.getKotFormat(stringStringMap.get("bill_no")));
                                    }
                                }
                                newBillNumber = String.format("Bill No : %s", Utilities.getKotFormat(response.body().get("bill_no")));

                                if (selectedItemDetailses != null) {
                                    if (selectedItemDetailses.size() > 0) {
                                        final BillContainer saveBillData = prepareData(selectedItemDetailses);
                                        new BillFragmentDialog()
                                                .setBillItems(saveBillData.details)
                                                .setNewBillNumber(newBillNumber)
                                                .setBillListner(new BillFragmentDialog.BillListner() {
                                                    @Override
                                                    public void onPositive(List<BillItem> billItems , FinalAmount finalAmount) {

                                                        if(billItems != null){

                                                            saveBillData.details = billItems;
                                                            if(finalAmount != null) {
                                                                if(finalAmount.focAmount != null)
                                                                    saveBillData.Cm_FocAmt = finalAmount.focAmount;
                                                                if(finalAmount.serviceAmount != null)
                                                                    saveBillData.Service_Amt = finalAmount.serviceAmount;
                                                                if(finalAmount.tax1Amount != null)
                                                                    saveBillData.Cm_Tax1_Amt = finalAmount.tax1Amount;
                                                                if(finalAmount.tax2Amount != null)
                                                                    saveBillData.Cm_Tax2_Amt = finalAmount.tax2Amount;
                                                                if(finalAmount.netAmount != null)
                                                                    saveBillData.Cm_NetAmt = finalAmount.netAmount;
                                                            }

                                                            for(BillItem items : billItems){

                                                                Log.e("items is FOC====>>" , String.valueOf(items.isFoc));
                                                            }

                                                        }

                                                        if (!Utilities.isNetworkConnected(getContext()))
                                                            Utilities.createNoNetworkDialog(getContext());
                                                        else {
                                                            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                                                    .content("Saving Bill")
                                                                    .progress(true, 1)
                                                                    .titleColorRes(R.color.colorAccentDark)
                                                                    .cancelable(false).build();
                                                            dialog.show();
                                                            Utilities.getRetrofitWebService(getContext()).saveBill(saveBillData).enqueue(new Callback<Map<String, Object>>() {
                                                                @Override
                                                                public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                                                    dialog.dismiss();
                                                                    Log.e("Respose" , new Gson().toJson(response.body()));
                                                                    final boolean response_code = (Boolean) response.body().get("response_code");

                                                                    if (response_code) {
                                                                        new MaterialDialog.Builder(getActivity())
                                                                                .titleColorRes(R.color.colorAccentDark)
                                                                                .title(response_code ? "Bill Saved" : "Can't save bill")
                                                                                .positiveText("Ok")
                                                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                                    @Override
                                                                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                                        if (response_code) {

                                                                                            getActivity().sendBroadcast(new Intent("reloadBillAndKot"));

                                                                                        }
                                                                                    }
                                                                                }).build().show();
                                                                    }

                                                                }

                                                                @Override
                                                                public void onFailure(Throwable t) {
                                                                    dialog.dismiss();

                                                                }
                                                            });


                                                        }
                                                    }

                                                    @Override
                                                    public void onNegative() {

                                                    }
                                                })
                                                .show(getChildFragmentManager(), "Billu");


                                    }
                                }

                            }

                            @Override
                            public void onFailure(Throwable t) {
                                dialog.cancel();
                            }
                        });


                    }
                }
                else {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.cancel();
            }
        });


    }




    private BillContainer prepareData(List<SelectedItemDetails> selectedItemDetailses) {
        String Tab_Cd = "";
        String Cm_Covers = "";

        Context context = getContext();
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        SharedPreferences preferences = Utilities.getSharedPreferences(context);
        int index = 0;
        double Cm_TotAmt = 0;
        String Cus_Name = "";
        String Cus_Cd = "";
        String Disc_Per = "";
        BillContainer billContainer = new BillContainer();
        List<BillItem> billItems = new ArrayList<>();
        for(SelectedItemDetails details : selectedItemDetailses){

            Cus_Name = details.Cus_Name;
            Cus_Cd = details.Cus_Cd;
            Disc_Per = details.Disc_Per;

            Tab_Cd = details.Tab_Cd;
            Cm_Covers = details.Cm_Covers;
            BillItem billItem = new BillItem();
            billItem.Itm_cd = details.itemCode;
            //put modifiers
            billItem.Menu_SideDish = "";
            if(details.modify != null){
                for (int i= 0 ; i < details.modify.size() ; i++ ) {
                    ItemModifiers modifier = details.modify.get(i);
                    if(i + 1 == details.modify.size() )
                        billItem.Menu_SideDish += modifier.name;
                    else if(i == 0)
                        billItem.Menu_SideDish += modifier.name;
                    else
                        billItem.Menu_SideDish += "," + modifier.name;
                }
            }
            else
                billItem.Menu_SideDish = details.Menu_SideDish;
            billItem.Cmd_ItmName = details.itemName;
            billItem.BarCode = details.barcode;
            billItem.Unit_Fraction = details.Unit_Fraction;
            billItem.Cmd_Qty = details.itemQuantity + "";
            billItem.Unit_Cd = details.Unit_cd;
            billItem.Cmd_MuUnit = "1";//TODO
            billItem.Cmd_MuQty = details.itemQuantity + "";
            billItem.Cmd_MuFraction = "1";
            billItem.Cmd_MuConvToBase = "1";
            billItem.Cmd_MuUcost = "1";
            billItem.Cmd_Rate = details.itemUnitCost + "";//// TODO: 10/21/2015
            double Cmd_Amta =(details.itemTotalCost);//*(details.itemQuantity);
            String Cmd_DiscPers =billItem.Cmd_DiscPer;
            billItem.Cmd_DiscPer = details.Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
            if(Cmd_DiscPers!=null) {

                billItem.Cmd_DiscAmt = Utilities.getPercentage(Cmd_Amta , Double.parseDouble(billItem.Cmd_DiscPer)) + "";
            }
            else{
                billItem.Cmd_DiscAmt ="0";
            }
            billItem.Cmd_Amt = (Cmd_Amta   -  Double.parseDouble( billItem.Cmd_DiscAmt )) + "";
            billItem.Cmd_Ctr = index++ + "";
            billItem.Cmd_SubCtr = "0";
            billItem.Cmd_RowType = "A";
            billItem.Cmd_NotCosted = "0";
            billItem.Cmd_Cancelled = "0";
            billItem.Cmd_IsLzQty = "1";
            billItem.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO , 1) + "";
            billItem.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE , "");
            billItem.Cmd_Status = "1";
            billItem.Cm_ThruDo = "0";
            billItem.IsModified = "0";
            billItem.Cm_Ref = "1";
            billItem.Cmd_DiscReason = "No";
            billItem.Kot_No = details.Kot_No == null ? "0" : details.Kot_No;
            billItem.Rcp_Cd = "1";
            billItem.Cmd_Ucost = details.itemUnitCost + "";
            billItem.Cmd_IsBuffet = details.Cmd_IsBuffet;//// TODO: 10/21/2015
            billItems.add(billItem);
            Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);
            if(billItem.isFoc)
                Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);
        }
        billContainer.details = billItems;
        billContainer.Usr_id = preferences.getString(Constants.SHARED_PREF_KEY_ID, "");
        billContainer.Cm_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_No = singleton.getNewBillNumber();
        billContainer.Sman_Cd = singleton.getSalesman(getContext()) == null ? "0" : singleton.getSalesman(getContext()).Id + "";
        billContainer.Cus_Cd = Cus_Cd;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Id + "";
        billContainer.Cus_Name = Cus_Name;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Name + "";billContainer.Cm_TotAmt =
        billContainer.Cm_DiscPer = Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
        billContainer.Cm_DiscAmt = Utilities.getPercentage(Cm_TotAmt , Double.parseDouble(billContainer.Cm_DiscPer)) + "";

        billContainer.Cm_TotAmt = Cm_TotAmt + "";

        Cm_TotAmt -= Double.parseDouble(billContainer.Cm_DiscAmt);

        billContainer.Cm_Tax1_Per = Utilities.getTaxFraction1(context) + "";
        billContainer.Cm_Tax1_Amt = Utilities.findTax1(context, Cm_TotAmt) + "";
        billContainer.Cm_Tax2_Per = Utilities.getTaxFraction2(context) + "";
        billContainer.Cm_Tax2_Amt = Utilities.findTax2(context, Cm_TotAmt) + "";
        billContainer.Service_Per = Utilities.getServiceChargeFraction(context) + "";
        billContainer.Service_Amt = Utilities.findServiceCharge(context , Cm_TotAmt) + "";
        billContainer.Cm_NetAmt = (Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) + Double.parseDouble(billContainer.Service_Amt)) + "";
        billContainer.Cm_Tips_Amt = "0";
        billContainer.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO , 0) + "";
        billContainer.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
        billContainer.Cm_BC_Settle_Amt = "0.000";
        billContainer.Cm_Paid_Amt = "0.000";
        billContainer.Cm_Cancel_Reason = "";
        billContainer.Cm_Cancelled = "0";
        billContainer.Cm_Cus_Phone = singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().getId() + "";
        billContainer.Cm_Type = "1";//TODO get order type
        billContainer.Tab_Cd = Tab_Cd;//singleton.getTable() == null ? "" : singleton.getTable().Tab_Cd;//TODO
        billContainer.Cm_Covers = Cm_Covers;//singleton.getGuestCount();
        billContainer.Srvr_Cd = "1";
        billContainer.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME , "");
        billContainer.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME , "0");
        //(Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) - Double.parseDouble( billContainer.Cm_DiscPer)) + "";
        // billContainer.Collect_Sft_No = "2";//// TODO: 10/21/2015
        //billContainer.Collect_Sft_Dt = "10-10-2015";//// TODO: 10/21/2015
        int i = 0;
        i++;
        billContainer.Srvr_Cd = i + "";

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar1 = Calendar.getInstance();

        String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

        if(shiftDateString != null && !shiftDateString.matches("")){

            try {
                calendar1.setTime(dateFormat1.parse(shiftDateString));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        billContainer.date = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
        billContainer.time = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");

        return billContainer;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            String selectedDate = null;
            if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                selectedDate =  new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate());
            final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

            if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                if(ApplicationSingleton.getInstance().getCurrentDate().before(Calendar.getInstance().getTime())) {
                    getActivity().registerReceiver(refreshActionReciever, new IntentFilter("refreshTableOptionsLists"));
                    txtNoPendingBillsOrKot.setVisibility(kotumbers.size() == 0 ? View.VISIBLE : View.GONE);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().unregisterReceiver(refreshActionReciever);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver refreshActionReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TableKotOptionsListAdapter tableKotOptionsListAdapter = (TableKotOptionsListAdapter) rvListKots.getAdapter();
            tableKotOptionsListAdapter.setKotNumbers(kotumbers);
            tableKotOptionsListAdapter.notifyDataSetChanged();
            txtNoPendingBillsOrKot.setVisibility(kotumbers.size() == 0 ? View.VISIBLE : View.GONE);
        }
    };
}
