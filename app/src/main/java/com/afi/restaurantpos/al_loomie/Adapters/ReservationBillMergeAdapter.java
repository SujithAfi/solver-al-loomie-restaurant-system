package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.ReservationMakeBillActivity;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.SplitAndMergeActivity;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AFI on 10/20/2015.
 */
public class ReservationBillMergeAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder>{

    List<ReservationMakeBillActivity.KotAdaterData> kotAdaterDatas;

    private onItemChange itemChange;

    public interface onItemChange{
        void onItemInvalidate(int pos , boolean ischecked);
    }

    public ReservationBillMergeAdapter(List<ReservationMakeBillActivity.KotAdaterData> kotAdaterDatas) {
        this.kotAdaterDatas = kotAdaterDatas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            return new SplitAndMegeAdapterItemVH(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.child_merge_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final SplitAndMegeAdapterItemVH vh = ((SplitAndMegeAdapterItemVH)holder);
            final PendingKotItem data = kotAdaterDatas.get(position).item;
            vh.txtItemName.setText(data.Itm_Name);
            vh.txtItemQuantity.setText(Utilities.getItemQuantityFormat(Double.parseDouble(data.KotD_Qty)));
            vh.txtItemRate.setText(Utilities.getDefaultCurrencyFormat(data.KotD_Rate + "", vh.txtItemAmount.getContext()));
            vh.txtItemAmount.setText(Utilities.getDefaultCurrencyFormat(data.KotD_Amt + "", vh.txtItemAmount.getContext()));

        vh.cbIsFOC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (itemChange != null) {
                    itemChange.onItemInvalidate(position , isChecked);
                    data.isFoc = isChecked;
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return kotAdaterDatas.size();
    }


    public static class SplitAndMegeAdapterItemVH extends RecyclerView.ViewHolder{

        private final CheckBox cbIsFOC;
        private TextView txtItemName;
        private TextView txtItemQuantity;
        private TextView txtItemRate;
        private TextView txtItemAmount;
        public SplitAndMegeAdapterItemVH(View itemView) {
            super(itemView);
            txtItemName = (TextView) itemView.findViewById(R.id.txtItemName);
            txtItemQuantity = (TextView) itemView.findViewById(R.id.txtItemQuantity);
            txtItemRate = (TextView) itemView.findViewById(R.id.txtItemRate);
            txtItemAmount = (TextView) itemView.findViewById(R.id.txtItemAmount);
            cbIsFOC = (CheckBox) itemView.findViewById(R.id.cb_foc);
        }
    }

    public void setItemChange(onItemChange itemChange) {
        this.itemChange = itemChange;
    }

    public List<PendingKotItem> getSelectedItemDetails(){
        List<PendingKotItem> kotItems = new ArrayList<>();
        for(ReservationMakeBillActivity.KotAdaterData kotAdaterData :kotAdaterDatas ){
            kotItems.add(kotAdaterData.item);
        }
        return kotItems;
    }


}
