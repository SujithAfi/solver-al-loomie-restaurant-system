package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.afi.restaurantpos.al_loomie.Dialogs.ItemDetailsFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.RecipeFragment;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;


/**
 * Created by afi-mac-001 on 16/06/16.
 */
public class ItemSelectionDialogFragmentAdapter extends FragmentPagerAdapter implements ItemDetailsFragment.ItemDdetailsCallBack {

    private String itemId;
    private String Barcode;
    private ItemDetailsFragment.ItemDdetailsCallBack callBack;

    public ItemSelectionDialogFragmentAdapter(FragmentManager fm, String itemId, String Barcode) {
        super(fm);
        this.itemId = itemId;
        this.Barcode = Barcode;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ItemDetailsFragment.newInstance(itemId, Barcode, null).setCallBack(this);
            case 1:
                return RecipeFragment.newInstance(itemId, Barcode);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public ItemSelectionDialogFragmentAdapter setCallBack(ItemDetailsFragment.ItemDdetailsCallBack callBack) {
        this.callBack = callBack;
        return this;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Item details";
            case 1:
                return "Recipes";
            default:
                return super.getPageTitle(position);
        }
    }

    @Override
    public void onItemSelected(SelectedItemDetails selecctedItemDetails, boolean isSuccess) {
        if (this.callBack != null) {
            callBack.onItemSelected(selecctedItemDetails, isSuccess);
        }
    }

    @Override
    public void onCanceled() {

    }
}
