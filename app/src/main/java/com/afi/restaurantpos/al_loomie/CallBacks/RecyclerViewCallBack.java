package com.afi.restaurantpos.al_loomie.CallBacks;

import android.view.View;

/**
 * Created by AFI on 10/5/2015.
 * interface for the RecyclerViewCallBack
 */
public abstract class RecyclerViewCallBack implements View.OnClickListener{
    private int positon;
    public abstract void onItemClick(int position );

    public RecyclerViewCallBack(int position) {
        this.positon = position;
    }

    @Override
    public void onClick(View v) {
        onItemClick(positon);
    }
}
