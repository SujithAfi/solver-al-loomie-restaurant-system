package com.afi.restaurantpos.al_loomie.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.ItemsAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afi.restaurantpos.al_loomie.View.DividerItemDecoration;
import com.afi.restaurantpos.al_loomie.View.VerticalSpaceItemDecoration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by afi on 10/29/2015.
 */
public class ItemFragment extends DialogFragment {
    private static final String ITEM_ID_KEY = "key_item_id";
    private static final String ITEM_NAME_KEY = "key_item_name";

    private String itemId;
    private String barCode;
    private String subCategoryName;

    private volatile boolean touchLock = false;

    private ViewGroup mContainer;
    private View mLayout;
    private Toolbar toolbar;
    private ProgressBar pBMaincategory;
    private RecyclerView rv_items;
    private FloatingActionButton fabCustomerDeatailsDown;
    private static final int VERTICAL_ITEM_SPACE = 48;
    private String kotDate;

    public static ItemFragment newInstance(String itemId, String subCategoryName , String kotDate) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_ID_KEY, itemId);
        args.putString(ITEM_NAME_KEY, subCategoryName);
        args.putString("kotDate", kotDate);
        fragment.setArguments(args);
        return fragment;
    }

    public ItemFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemId = getArguments().getString(ITEM_ID_KEY);
            subCategoryName = getArguments().getString(ITEM_NAME_KEY);
            kotDate = getArguments().getString("kotDate");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        mLayout = inflater.inflate(R.layout.fragment_food_details, container, false);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        rv_items = (RecyclerView) mLayout.findViewById(R.id.rv_items);
        pBMaincategory = (ProgressBar) mLayout.findViewById(R.id.pBItems);
        fabCustomerDeatailsDown = (FloatingActionButton) mLayout.findViewById(R.id.fabCustomerDeatailsDown);
        fabCustomerDeatailsDown.setVisibility(View.INVISIBLE);
        return mLayout;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String it = itemId;
        rv_items.setHasFixedSize(true);
//        rv_items.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        //add ItemDecoration
//        rv_items.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        rv_items.addItemDecoration(new DividerItemDecoration(getActivity()));
        rv_items.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_items.setItemAnimator(new DefaultItemAnimator());
        toolbar.setTitle(subCategoryName);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ItemFragment.this.dismiss();
            }
        });

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {

            /*Calendar c = Calendar.getInstance();
            c.setTime(Calendar.getInstance().getTime());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            Log.e("dayofWeek=====>>" , String.valueOf(dayOfWeek) );*/




            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(kotDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

//            Log.e("dayofWeek=====>>" , String.valueOf(dayOfWeek));

            pBMaincategory.setVisibility(View.VISIBLE);
            Utilities.getRetrofitWebService(getContext()).getItems(itemId , String.valueOf(dayOfWeek)).enqueue(new Callback<List<Item>>() {
                @Override
                public void onResponse(Response<List<Item>> response, Retrofit retrofit) {
                    try {
                        pBMaincategory.setVisibility(View.GONE);

                        if (response.body() != null && ItemFragment.this.isVisible()) {
                            List<Item> data = response.body();
                            rv_items.setAdapter(new ItemsAdapter(data, new ItemsAdapter.ItemsClickListner() {
                                @Override
                                public void onItemClick(Item item) {

                                    if (!Utilities.isNetworkConnected(getContext()))
                                        Utilities.createNoNetworkDialog(getContext());
                                    else {
                                        if (!ApplicationSingleton.getInstance().isLockFlag()) {
                                            Log.e("Item code===>>" , item.ItemCd );

                                            if (item.Is_MultiCourse) {
                                                if(getActivity() != null)
                                                    ((SelectItemsActivity) getActivity()).showMulticorseMenu(item);
                                            } else {
                                                if(getActivity() != null)
                                                    ((SelectItemsActivity) getActivity()).showItemDetailsFragment(item.ItemCd, item.BarCd, null);
                                                ApplicationSingleton.getInstance().setLockFlag(true);
                                            }
                                        }
                                    }


                                }
                            }, ItemFragment.this));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    pBMaincategory.setVisibility(View.GONE);
                }
            });


        }

        ViewCompat.animate(fabCustomerDeatailsDown)
                .scaleX(0)
                .scaleY(0)
                .setDuration(100)
                .start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabCustomerDeatailsDown.setVisibility(View.VISIBLE);
                fabCustomerDeatailsDown.clearAnimation();
                ViewCompat.animate(fabCustomerDeatailsDown)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator())
                        .start();

            }
        }, 1000);



        fabCustomerDeatailsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getActivity() != null) {
                    ((SelectItemsActivity) getActivity()).setUpperFabAnimation(true, 1);
                    ((SelectItemsActivity) getActivity()).animateBillFragment();
                }
            }
        });

    }

    public void close() {
        getDialog().dismiss();
    }

    @Override
    public void onStop() {
        ApplicationSingleton.getInstance().setSubCategoryClickLock(false);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        getContext().registerReceiver(errorReceiver, new IntentFilter("com.afi.infotech.error"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(errorReceiver);
    }

    private BroadcastReceiver errorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getActivity(), "Incomplete or corrupted data", Toast.LENGTH_LONG).show();
            ItemFragment.this.dismiss();
        }
    };

}
