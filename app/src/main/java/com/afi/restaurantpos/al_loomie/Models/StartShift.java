package com.afi.restaurantpos.al_loomie.Models;

/**
 * Created by AFI on 11/5/2015.
 */
public class StartShift {

    public String id;
    public String shift_No;
    public String cashier;
    public String emp_cd;
    public String emp_name;
    public String remarks;
    public String float_cash;
    public String date;
    public String time;


}
