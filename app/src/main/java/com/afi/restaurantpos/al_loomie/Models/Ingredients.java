package com.afi.restaurantpos.al_loomie.Models;

import java.io.Serializable;

/**
 * Created by afi-mac-001 on 18/09/16.
 */
public class Ingredients implements Serializable {

    private String Id;
    private String Name;
    private int Quantity;

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }


}
