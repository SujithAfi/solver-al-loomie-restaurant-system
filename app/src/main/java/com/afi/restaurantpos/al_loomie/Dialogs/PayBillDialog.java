package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Models.Payment;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PayBillDialog extends DialogFragment {

    private PendingKot pendingKot;
    private PendingBill pendingBill;
    private Toolbar toolbar;
    double total=0.000;
    double roundoff=0.000;
    private ViewGroup mContainer;
    private View mLayout;

    private EditText tip,tentered;
    private TextView counter,bill_no,totalamt,balance;

    private TextView txtBill;
    private TextView txtHotelName;

    private CancelType cancelType;

    private PayBillDialogListner dialogListner;
    private RadioGroup rgPaymentMode;
    private RadioButton rbCash;
    private RadioButton rbCreditCard;
    private RadioButton rbCreditAccount;
    private TableLayout tlDetails;
    private TableRow trCardNo;
    private EditText etCardNo;
    private TableRow trCreditAccountId;
    private TextView tvCreditAccountId;
    private TableRow trCreditAccountName;
    private TextView tvCreditAccountName;
    private TableRow trCreditRoomNo;
    private TextView tvCreditRoomNo;
    private TableRow trCreditCheckinDate;
    private TextView tvCreditCheckinDate;
    private String tax1Rate = "0";
    private String tax2Rate = "0";
    private String serviceChargeRate = "0";
    private double tax1Value = 0;
    private double tax2Value = 0;
    private double serviceChargeValue = 0;
    private TextView tvTax1;
    private TextView tvTax2;
    private TextView tvServiceCharge;
    private TextView tvTax1Head;
    private TextView tvTax2Head;
    private TextView tvServiceChargeHead;
    private TextView tvTotal;
    private EditText etRoundoff;
    private EditText etDiscountPercentage;
    private EditText etDiscountValue;
    private SharedPreferences preferences;
    private boolean discValueFlag;
    private TableRow trFoc;
    private TextView tvFocHead;
    private TextView tvFocAmount;
    //private boolean flag;

    public interface PayBillDialogListner {
        void onFinishedAction(PayBillDialog PayBillDialog);
    }

    public static PayBillDialog newInstance(CancelType cancelType) {
        PayBillDialog fragment = new PayBillDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.cancelType = cancelType;
        return fragment;
    }

    public PayBillDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        preferences = Utilities.getSharedPreferences(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_payment, container, false);
        tip = (EditText) mLayout.findViewById(R.id.txtTotalAmount);
        tentered = (EditText) mLayout.findViewById(R.id.txtDiscountAmount);
        etDiscountPercentage = (EditText) mLayout.findViewById(R.id.et_discount_percentage);
        etDiscountValue = (EditText) mLayout.findViewById(R.id.et_discount_value);
        counter = (TextView) mLayout.findViewById(R.id.txtCounter);
        bill_no = (TextView) mLayout.findViewById(R.id.txtbill);
        totalamt = (TextView) mLayout.findViewById(R.id.txtAmount);
        balance = (TextView) mLayout.findViewById(R.id.txtNetAmount);
        txtBill = (TextView) mLayout.findViewById(R.id.txtBill);
        txtHotelName = (TextView) mLayout.findViewById(R.id.txtHotelName);

        tvTotal = (TextView) mLayout.findViewById(R.id.tv_total_amt);
        tvTax1 = (TextView) mLayout.findViewById(R.id.tv_tax1);
        tvTax2 = (TextView) mLayout.findViewById(R.id.tv_tax2);
        tvServiceCharge = (TextView) mLayout.findViewById(R.id.tv_service_charge);

        tvTax1Head = (TextView) mLayout.findViewById(R.id.tv_tax1_head);
        tvTax2Head = (TextView) mLayout.findViewById(R.id.tax2_head);
        tvServiceChargeHead = (TextView) mLayout.findViewById(R.id.tv_service_charge_head);

        //payment type

        rgPaymentMode = (RadioGroup) mLayout.findViewById(R.id.rg_payment_mode);
        rbCash = (RadioButton) mLayout.findViewById(R.id.rb_cash_payment);
        rbCreditCard = (RadioButton) mLayout.findViewById(R.id.rb_credit_card);
        rbCreditAccount = (RadioButton) mLayout.findViewById(R.id.rb_credit_account);


        tlDetails = (TableLayout) mLayout.findViewById(R.id.tl_details);

        trCardNo = (TableRow) mLayout.findViewById(R.id.tr_cardno);
        etCardNo = (EditText) mLayout.findViewById(R.id.et_card_no);

        trCreditAccountId = (TableRow) mLayout.findViewById(R.id.tr_account_id);
        tvCreditAccountId = (TextView) mLayout.findViewById(R.id.tv_account_id);

        trCreditAccountName = (TableRow) mLayout.findViewById(R.id.tr_account_name);
        tvCreditAccountName = (TextView) mLayout.findViewById(R.id.tv_account_name);

        trCreditRoomNo = (TableRow) mLayout.findViewById(R.id.tr_room_no);
        tvCreditRoomNo = (TextView) mLayout.findViewById(R.id.tv_room_no);

        trCreditCheckinDate = (TableRow) mLayout.findViewById(R.id.tr_checkin_date);
        tvCreditCheckinDate = (TextView) mLayout.findViewById(R.id.tv_checkin_date);

        etRoundoff = (EditText) mLayout.findViewById(R.id.tv_round_off);

        trFoc = (TableRow) mLayout.findViewById(R.id.tr_foc);
        tvFocHead = (TextView) mLayout.findViewById(R.id.tv_foc_head);
        tvFocAmount = (TextView) mLayout.findViewById(R.id.tv_foc_amount);

        tentered.requestFocus();

        return mLayout;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_PAYMENT_ADDORSAVE, false))
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                    .setPositiveButton("Pay Bill", null)
                    .setNegativeButton("Cancel", null)
                    .create();

        else
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
//                    .setPositiveButton("Pay Bill", null)
                    .setNegativeButton("Cancel", null)
                    .create();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(true);
        tentered.addTextChangedListener(textWatcher);
        tip.addTextChangedListener(textWatcher);
        etRoundoff.addTextChangedListener(textWatcher);
        etDiscountValue.addTextChangedListener(textWatcher);
        etDiscountValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                    discValueFlag = hasFocus;
            }
        });

        rbCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                    tlDetails.setVisibility(View.GONE);


            }
        });

        rbCreditCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {

                    tlDetails.setVisibility(View.VISIBLE);
                    trCardNo.setVisibility(View.VISIBLE);
                    trCreditAccountId.setVisibility(View.GONE);
                    trCreditAccountName.setVisibility(View.GONE);
                    trCreditRoomNo.setVisibility(View.GONE);
                    trCreditCheckinDate.setVisibility(View.GONE);

                }


            }
        });

        rbCreditAccount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked) {

                    tlDetails.setVisibility(View.VISIBLE);
                    trCardNo.setVisibility(View.GONE);
                    trCreditAccountId.setVisibility(View.VISIBLE);
                    trCreditAccountName.setVisibility(View.VISIBLE);
                    trCreditRoomNo.setVisibility(View.VISIBLE);
                    trCreditCheckinDate.setVisibility(View.VISIBLE);

                }


            }
        });

        double focAmt = 0;
        switch (cancelType) {

            case CANCEL_BILL:

                try {
                    counter.setText(pendingBill.Cm_Counter);
                    bill_no.setText(pendingBill.Cm_No.replace("##" ,""));
//                totalamt.setText(pendingBill.Cm_NetAmt);
                    total=Double.parseDouble(pendingBill.Cm_NetAmt);

                    if(pendingBill.Cm_FocAmt != null)
                        focAmt = Double.parseDouble(pendingBill.Cm_FocAmt);

                    double cost = Double.parseDouble(pendingBill.Cm_TotAmt);
                    //double cost =Double.parseDouble(Utilities.getDefaultCurrencyFormat(singleton.getTotalOrderCostTemp(), getContext()));
                    double tax1 = Utilities.findTax1(getContext(), (cost - focAmt));
                    double tax2 = Utilities.findTax2(getContext(), (cost - focAmt));
                    double serviceCharge = Utilities.findServiceCharge(getContext(), (cost - focAmt));



                    if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null).matches(""))
                        tvTax1Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , null) + " (" + Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_1, "0") + "%)");
                    if(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null) != null && !Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null).matches(""))
                        tvTax2Head.setText(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , null) + " (" + Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_TAX_2, "0") + "%)");
                    tvServiceChargeHead.setText("Service Charge (" +  Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_RATE, "0") + "%)");

                    tvTotal.setText(Utilities.getDefaultCurrencyFormat(cost  , getContext()));
                    tvFocAmount.setText(Utilities.getDefaultCurrencyFormat(focAmt  , getContext()));
                    tvTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));
                    tvTax2.setText(Utilities.getDefaultCurrencyFormat(tax2 , getContext()));
                    tvServiceCharge.setText(Utilities.getDefaultCurrencyFormat(serviceCharge , getContext()));
//                    etDiscountValue.setText(Utilities.getDefaultCurrencyFormat(focAmt , getContext()));
                    totalamt.setText(Utilities.getDefaultCurrencyFormat(((Double.parseDouble(tvTotal.getText().toString()) + Double.parseDouble(tvTax1.getText().toString()) + Double.parseDouble(tvTax2.getText().toString()) + Double.parseDouble(tvServiceCharge.getText().toString())) - Double.parseDouble(tvFocAmount.getText().toString())) , getContext()));


                    if(pendingBill.isCredit){

                        tvCreditAccountId.setText(pendingBill.acc_Code);
                        tvCreditAccountName.setText(pendingBill.acc_Name);
                        tvCreditRoomNo.setText(pendingBill.room_No);

                        if(pendingBill.checkin_Date != null) {

                            Calendar calDate = Calendar.getInstance();
                            SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
                            List<String> items = Arrays.asList(pendingBill.checkin_Date.split("\\s+"));

                            if (items.size() > 0) {
                                try {

                                    calDate.setTime(sdfDate.parse(items.get(0)));
                                    tvCreditCheckinDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                    }
                    else
                        rbCreditAccount.setVisibility(View.GONE);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                break;
        }

        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "cac_champagne.ttf");
        txtBill.setTypeface(myTypeface);
        txtHotelName.setTypeface(myTypeface);

        /*rbCash.setOnCheckedChangeListener(this);
        rbCreditCard.setOnCheckedChangeListener(this);
        rbCreditAccount.setOnCheckedChangeListener(this);*/




        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED , false))
            tax1Rate = preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0");
        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED , false))
            tax2Rate = preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0");
        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_ENABLED , false))
            serviceChargeRate = preferences.getString(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_RATE, "0");

        try {
            tax1Value = Utilities.findTax1(getContext(), (Double.parseDouble(pendingBill.Cm_TotAmt) - focAmt));
            tax2Value = Utilities.findTax2(getContext(), (Double.parseDouble(pendingBill.Cm_TotAmt) - focAmt));
            serviceChargeValue = Utilities.findServiceCharge(getContext(), (Double.parseDouble(pendingBill.Cm_TotAmt) - focAmt));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        etDiscountPercentage.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.e("afterTextChanged", "afterTextChanged1");
//                if (flag) {
//                    flag = false;
//                }
//                else{
                try {

                    if(!discValueFlag) {

                        if (!etDiscountPercentage.getText().toString().equals(".") & !etDiscountPercentage.getText().toString().equals("") & etDiscountPercentage.getText().toString() != null & !etDiscountPercentage.getText().toString().matches("")) {
                            if (Double.parseDouble(etDiscountPercentage.getText().toString()) > 0) {
                                double discountValue = Utilities.getPercentage((Double.parseDouble(tvTotal.getText().toString()) - Double.parseDouble(tvFocAmount.getText().toString())), Double.parseDouble(etDiscountPercentage.getText().toString()));
                                etDiscountValue.setText(Utilities.getDefaultCurrencyFormat(discountValue, getContext()));
                            /*double cost = 0;
                            if(pendingBill.Cm_TotAmt != null)
                                cost = Double.parseDouble(pendingBill.Cm_TotAmt);

                            double serviceCharge = Utilities.findServiceCharge(getContext(), cost);
                            double discountValue = ((cost + serviceCharge) * Double.parseDouble(etDiscountPercentage.getText().toString())) / 100;*/
                                etDiscountValue.setText(Utilities.getDefaultCurrencyFormat(discountValue, getContext()));

                            }
                            if (Double.parseDouble(etDiscountPercentage.getText().toString()) > 100) {
                                etDiscountPercentage.setError("Percentage should not more than 100");
                            }
                            if (Double.parseDouble(etDiscountPercentage.getText().toString()) <= 0) {
                                etDiscountValue.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble("0"), getContext()));
                            }
                        } else {
                            etDiscountValue.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble("0"), getContext()));

                        /*double tax1 = Utilities.findTax1(getContext(), cost);
                        double tax2 = Utilities.findTax2(getContext(), cost);
                        tvTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));

                        tvTax2.setText(Utilities.getDefaultCurrencyFormat(tax2, getContext()));

                        totalamt.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble(pendingBill.Cm_NetAmt) , getContext()));
                        total=Double.parseDouble(pendingBill.Cm_NetAmt);*/
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }


            }
            //}
        });
    }

    public PayBillDialog setPendingKot(PendingKot pendingKot) {
        this.pendingKot = pendingKot;
        return this;
    }

    public PayBillDialog setPendingBill(PendingBill pendingBill) {
        this.pendingBill = pendingBill;
        return this;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        final AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    int radioButtonID = rgPaymentMode.getCheckedRadioButtonId();
                    View radioButton = rgPaymentMode.findViewById(radioButtonID);
                    int idx = rgPaymentMode.indexOfChild(radioButton);
//                    roundoff = Double.parseDouble(etRoundoff.getText().toString());
//                    || (roundoff != 0 && ((roundoff + tipValue) <= Double.parseDouble(tentered.getText().toString()))

                    try {
                        if (!Utilities.isNetworkConnected(getContext()))
                            Utilities.createNoNetworkDialog(getContext());
                        else {
                            if (!tentered.getText().toString().equals("")) {
                                     double tipValue = tip.getText().toString().isEmpty() ? 0 : Double.parseDouble(tip.getText().toString());
                                     double discPer = 0 ;
                                     double discValue = 0 ;
                                     discPer = !etDiscountPercentage.getText().toString().equals("") ? Double.parseDouble(etDiscountPercentage.getText().toString()) : 0;
                                     discValue = !etDiscountValue.getText().toString().equals("") ? Double.parseDouble(etDiscountValue.getText().toString()) : 0;

                                    if (!etDiscountPercentage.getText().toString().equals("") && discPer > 0) {
                                        if (Double.parseDouble(etDiscountPercentage.getText().toString()) != 0 & Double.parseDouble(etDiscountPercentage.getText().toString()) > 100) {
                                            Utilities.showToast("Discount percentage should not greater than 100", getContext());
                                            return;
                                        }
                                        else{
                                            if (!etDiscountValue.getText().toString().equals("") && discValue > 0) {
                                                if ((Double.parseDouble(etDiscountValue.getText().toString())) != 0 & (Double.parseDouble(etDiscountValue.getText().toString()) > Double.parseDouble(tvTotal.getText().toString())) ) {
                                                    Utilities.showToast("Discount amount should not greater than Total amount", getContext());
                                                    return;
                                                }
                                            }
                                        }

                                    }
                                    if (!etDiscountValue.getText().toString().equals("") && discValue > 0) {
                                        if ((Double.parseDouble(etDiscountValue.getText().toString())) != 0 & (Double.parseDouble(etDiscountValue.getText().toString()) > Double.parseDouble(tvTotal.getText().toString())) ) {
                                            Utilities.showToast("Discount amount should not greater than Total amount", getContext());
                                            return;
                                        }
                                        else{
                                            if (!etDiscountPercentage.getText().toString().equals("") && discPer > 0) {
                                                if (Double.parseDouble(etDiscountPercentage.getText().toString()) != 0 & Double.parseDouble(etDiscountPercentage.getText().toString()) > 100) {
                                                    Utilities.showToast("Discount percentage should not greater than 100", getContext());
                                                    return;
                                                }
                                            }
                                        }

                                    }
                                   if (((Double.parseDouble(totalamt.getText().toString())) <= Double.parseDouble(tentered.getText().toString()))) {
                                        SharedPreferences preferences = Utilities.getSharedPreferences(getContext());
                                        Payment payment = new Payment();
                                        payment.Cm_NetAmt = Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()), getContext());
                                        payment.Cm_Bc_Amt = Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()), getContext());
                                        payment.Cm_No = pendingBill.Cm_No;
                                        payment.Cm_Counter = pendingBill.Cm_Counter;
                                        payment.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME, "");
                                        payment.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "");
                                        payment.Collect_Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 0) + "";
                                        payment.Collect_Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
                                        payment.Cus_Cd = pendingBill.Cus_Cd;
                                        payment.Cm_Paid_Amt = Utilities.getDefaultCurrencyFormat(Double.parseDouble(tentered.getText().toString()), getContext());


                                        switch (idx) {

                                            case 0:
                                                payment.payment_Type = idx + "";
                                                payment.Cm_Bc_Settle_Amt = Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()), getContext());
                                                break;
                                            case 1:
                                                payment.payment_Type = idx + "";
                                                payment.Cm_Bc_Settle_Amt = "0";
                                                payment.credit_Card_No = (etCardNo.getText().toString().trim().length() > 0 ? etCardNo.getText().toString().trim() : "");
                                                break;
                                            case 2:
                                                payment.payment_Type = idx + "";
                                                payment.isCredit = pendingBill.isCredit;
                                                payment.acc_Code = pendingBill.acc_Code;
                                                payment.acc_Name = pendingBill.acc_Name;
                                                payment.room_No = pendingBill.room_No;
                                                payment.checkin_Date = pendingBill.checkin_Date;
                                                payment.Cm_Bc_Settle_Amt = "0";
                                                break;
                                            default:
                                                break;
                                        }

    //                                Log.e("Cust Code===>>" , idx + "");
    //                                Log.e("Cust Code===>>" , pendingBill.Cus_Cd);


                                        payment.tax1_Rate = tax1Rate;
                                        payment.tax2_Rate = tax2Rate;
                                        payment.service_charge_Rate = serviceChargeRate;

                                        if (tvTax1.getText().toString() != null && !tvTax1.getText().toString().matches(""))
                                            payment.tax1_value = tvTax1.getText().toString();
                                        else
                                            payment.tax1_value = "0";

                                        if (tvTax2.getText().toString() != null && !tvTax2.getText().toString().matches(""))
                                            payment.tax2_value = tvTax2.getText().toString();
                                        else
                                            payment.tax2_value = "0";

                                        if (tvServiceCharge.getText().toString() != null && !tvServiceCharge.getText().toString().matches(""))
                                            payment.service_value = tvServiceCharge.getText().toString();
                                        else
                                            payment.service_value = "0";

                                        if (etDiscountPercentage.getText().toString() != null && !etDiscountPercentage.getText().toString().equals(".") && !etDiscountPercentage.getText().toString().matches(""))
                                            payment.Cm_DiscPer = etDiscountPercentage.getText().toString();
                                        else
                                            payment.Cm_DiscPer = "0";

                                        if (etDiscountValue.getText().toString() != null && !etDiscountValue.getText().toString().equals(".") && !etDiscountValue.getText().toString().matches(""))
                                            payment.Cm_DiscAmt = etDiscountValue.getText().toString();
                                        else
                                            payment.Cm_DiscAmt = "0";

                                        if (tip.getText().toString() != null && !tip.getText().toString().equals(".") && !tip.getText().toString().matches(""))
                                            payment.Cm_Tips_Amt = tip.getText().toString();
                                        else
                                            payment.Cm_Tips_Amt = "0";

                                        if (etRoundoff.getText().toString() != null && !etRoundoff.getText().toString().equals(".") && !etRoundoff.getText().toString().matches(""))
                                            payment.Cm_RoundAmt = etRoundoff.getText().toString();
                                        else
                                            payment.Cm_RoundAmt = "0";

                                        if (balance.getText().toString() != null && !balance.getText().toString().equals(".") && !balance.getText().toString().matches(""))
                                            payment.Cm_Bc_Amt = balance.getText().toString();
                                        else
                                            payment.Cm_Bc_Amt = "0";

                                   /* payment.tax2_value = String.valueOf(tax2Value);
                                    payment.service_value = String.valueOf(serviceChargeValue);*/

                                    /*payment.Cm_DiscPer = etDiscountPercentage.getText().toString() != null ? etDiscountPercentage.getText().toString() : "0";
                                    payment.Cm_DiscAmt = etDiscountValue.getText().toString() != null ? etDiscountValue.getText().toString() : "0";
                                    payment.Cm_Tips_Amt = tip.getText().toString() != null ? tip.getText().toString() : "0";
                                    payment.Cm_RoundAmt = etRoundoff.getText().toString() != null ? etRoundoff.getText().toString() : "0";*/


                                        if (idx == 1) {

                                            if (etCardNo.getText().toString() == null || etCardNo.getText().toString().matches("") || etCardNo.getText().toString().length() < 0) {

                                                Utilities.showToast("Card No can not be empty.", getContext());
                                                etCardNo.setError("Enter Card No.");
                                                return;
                                            }

                                        }

                                       SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");
                                       Calendar calendar1 = Calendar.getInstance();

                                       String shiftDateString = ApplicationSingleton.getInstance().getLogin().shift_date;

                                       if(shiftDateString != null && !shiftDateString.matches("")){

                                           try {
                                               calendar1.setTime(dateFormat1.parse(shiftDateString));
                                           } catch (ParseException e) {
                                               e.printStackTrace();
                                           }
                                       }

                                       payment.date = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) + " 00:00:00.000");
                                       payment.time = (new SimpleDateFormat("yyyy-MM-dd").format(calendar1.getTime()) +" " +  new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + ".000");

                                        final MaterialDialog payBillDialogs = new MaterialDialog.Builder(getContext())
                                                .content("Paying Bill")
                                                .progress(true, 1)
                                                .titleColorRes(R.color.colorAccentDark)
                                                .cancelable(false).build();
                                        payBillDialogs.show();
                                        Utilities.getRetrofitWebService(getContext()).payBill(payment).enqueue(new Callback<Map<String, Object>>() {
                                            @Override
                                            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                                payBillDialogs.cancel();
                                                Map<String, Object> map = response.body();
                                                if (((Boolean) map.get("response_code"))) {
                                                    new MaterialDialog.Builder(getContext())
                                                            .content("Bill Payed successfully")
                                                            .titleColorRes(R.color.colorAccentDark)
                                                            .positiveText("Ok")
                                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                @Override
                                                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                    if (dialogListner != null)
                                                                        dialogListner.onFinishedAction(PayBillDialog.this);
                                                                }
                                                            })
                                                            .cancelable(false).build().show();
                                                } else {
                                                    Log.e("Response====>>", map.get("insertResponse").toString());
                                                    new MaterialDialog.Builder(getContext())
                                                            .content("Error")
                                                            .titleColorRes(R.color.colorAccentDark)
                                                            .positiveText("Ok")
                                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                @Override
                                                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                    // loadPendingBills();
                                                                }
                                                            })
                                                            .cancelable(false).build().show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Throwable t) {
                                                payBillDialogs.cancel();
                                            }
                                        });


                                    } else {
                                        Utilities.showToast(getResources().getString(R.string.bill_not_saved), getContext());
                                    }
                                } else {

                                    Utilities.showToast(getResources().getString(R.string.bill_not_saved), getContext());
                                }
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } catch (Resources.NotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public PayBillDialog setDialogListner(PayBillDialogListner dialogListner) {
        this.dialogListner = dialogListner;
        return this;
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
//            Log.e("afterTextChanged","afterTextChanged2");
            try {
                double cost = 0;
                if(pendingBill.Cm_TotAmt != null)
                    cost = Double.parseDouble(pendingBill.Cm_TotAmt);

        /*    if(!etRoundoff.getText().toString().equals(".")&!etRoundoff.getText().toString().equals(""))
                totalamt.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()) - Double.parseDouble(etRoundoff.getText().toString()) , getContext()));
            else
                totalamt.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()) , getContext()));

            if(!tip.getText().toString().equals(".")&!tip.getText().toString().equals(""))
                totalamt.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()) + Double.parseDouble(tip.getText().toString()) , getContext()));
            else
                totalamt.setText(Utilities.getDefaultCurrencyFormat(Double.parseDouble(totalamt.getText().toString()) , getContext()));

            double tax1 = ((cost - Double.parseDouble(etDiscountValue.getText().toString())) * Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0"))) / 100;
            tvTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));

            double tax2 = ((cost - Double.parseDouble(etDiscountValue.getText().toString())) * Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0"))) / 100;
            tvTax2.setText(Utilities.getDefaultCurrencyFormat(tax2, getContext()));*/

                       /* Tax1 Amount = ((Total Amount – Disc. Amount) * Tax1 Per) / 100
                        Tax2 Amount = ((Total Amount – Disc. Amount) * Tax2 Per) / 100*/


                // Service Charge
                double mServiceCharge = 0;
                if(tvServiceCharge.getText().toString() != null && !tvServiceCharge.getText().toString().matches("") && !tvServiceCharge.getText().toString().equals("."))
                    mServiceCharge = Double.parseDouble(tvServiceCharge.getText().toString());
                else
                    mServiceCharge = 0;

                // Total Amount
                double mTotalAmount = 0;
                if(tvTotal.getText().toString() != null && !tvTotal.getText().toString().matches(""))
                    mTotalAmount = Double.parseDouble(tvTotal.getText().toString());
                else
                    mTotalAmount = 0;

            /*if(!etDiscountValue.getText().toString().equals("") & etDiscountValue.getText().toString()!= null){
                //flag = true;
                //etDiscountPercentage.setTextIsSelectable(false);
                //double v = Double.parseDouble(etDiscountValue.getText().toString()) *  (Double.parseDouble(etDiscountValue.getText().toString()))/100;
                //etDiscountPercentage.setText(String.valueOf(v));
                if(Double.parseDouble(etDiscountValue.getText().toString()) > mTotalAmount ) {
                    etDiscountValue.setError("Discount amount should not greater than total amount");
                }else{
                    etDiscountValue.setError(null);
                }
            }*/

                // Tips
                double mTips = 0;
                if(tip.getText().toString() != null && !tip.getText().toString().matches("") && !tip.getText().toString().equals("."))
                    mTips = Double.parseDouble(tip.getText().toString());
                else
                    mTips = 0;

                // RoundOff
                double mRoundOff = 0;
                if(etRoundoff.getText().toString() != null && !etRoundoff.getText().toString().matches("") && !etRoundoff.getText().toString().equals("."))
                    mRoundOff = Double.parseDouble(etRoundoff.getText().toString());
                else
                    mRoundOff = 0;

                // Discount Tax 1 Tax 2
                double mDiscount = 0;
                double mtax1 = 0;
                double mtax2 = 0;
                if(etDiscountValue.getText().toString() != null && !etDiscountValue.getText().toString().matches("") && !etDiscountValue.getText().toString().equals(".")) {

                    if(Double.parseDouble(etDiscountValue.getText().toString()) > mTotalAmount ) {
                        etDiscountValue.setError("Discount amount should not greater than total amount");
                    }else{
                        etDiscountValue.setError(null);

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                double discountAmount = (100/(Double.parseDouble(tvTotal.getText().toString()) - Double.parseDouble(tvFocAmount.getText().toString()))) * Double.parseDouble(etDiscountValue.getText().toString());
                                if(discValueFlag)
                                    etDiscountPercentage.setText(Utilities.getDefaultCurrencyFormat(discountAmount , getContext()));
                            }
                        }, 100);


                        mDiscount = Double.parseDouble(etDiscountValue.getText().toString());

                        double tax1 = (((cost - Double.parseDouble(tvFocAmount.getText().toString())) - (Double.parseDouble(etDiscountValue.getText().toString()))) * Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0"))) / 100;
                        tvTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));

                        double tax2 = (((cost - Double.parseDouble(tvFocAmount.getText().toString())) - (Double.parseDouble(etDiscountValue.getText().toString()))) * Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0"))) / 100;
                        tvTax2.setText(Utilities.getDefaultCurrencyFormat(tax2, getContext()));

                        if(tvTax1.getText().toString() != null && !tvTax1.getText().toString().matches(""))
                            mtax1 = Double.parseDouble(tvTax1.getText().toString());
                        else
                            mtax1 = 0;

                        if(tvTax2.getText().toString() != null && !tvTax2.getText().toString().matches(""))
                            mtax2 = Double.parseDouble(tvTax2.getText().toString());
                        else
                            mtax2 = 0;
                    }


                }
                else {
                    mDiscount = 0;

                    double tax1 = Utilities.findTax1(getContext(), (cost - Double.parseDouble(tvFocAmount.getText().toString())));
                    double tax2 = Utilities.findTax2(getContext(), (cost - Double.parseDouble(tvFocAmount.getText().toString())));

                    mtax1 = tax1;
                    mtax2 = tax2;
                    tvTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));
                    tvTax2.setText(Utilities.getDefaultCurrencyFormat(tax2, getContext()));

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms

                            if(discValueFlag)
                                etDiscountPercentage.setText(Utilities.getDefaultCurrencyFormat(0.0 , getContext()));
                        }
                    }, 100);

                }


                total = ( (mTotalAmount - Double.parseDouble(tvFocAmount.getText().toString())) + mtax1 + mtax2 + mServiceCharge + mTips) - (mDiscount + mRoundOff );
                totalamt.setText(Utilities.getDefaultCurrencyFormat(total , getContext()));


                try {
                    if(!tentered.getText().toString().equals(".") &!tentered.getText().toString().equals("")){
                        double paid=Double.parseDouble(tentered.getText().toString());
        //                double tips=Double.parseDouble(tip.getText().toString());
        //                double roundoff=Double.parseDouble(etRoundoff.getText().toString());
                        if(paid>Double.parseDouble(totalamt.getText().toString())){
                            Double balance_amt = paid-Double.parseDouble(totalamt.getText().toString());
                            balance.setText(Utilities.getDefaultCurrencyFormat(balance_amt.toString() , getContext()));
                        }
                        else {
                            balance.setText("");
                        }
                    }
                    else{
                        balance.setText("0.000");
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }
    };


}
