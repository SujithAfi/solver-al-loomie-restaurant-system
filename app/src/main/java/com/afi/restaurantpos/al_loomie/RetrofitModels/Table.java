package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.io.Serializable;

/**
 * Created by AFI on 10/2/2015.
 */
public class Table implements Serializable {

    public String Name;
    public String Tab_Cd;
    public int Type_Index;
    public String Caption;
    public int Guests;
    public int book;
    public int bill;
    public String chairs;

    public String getChairs() {
        return chairs;
    }

    public void setChairs(String chairs) {
        this.chairs = chairs;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTab_Cd() {
        return Tab_Cd;
    }

    public void setTab_Cd(String tab_Cd) {
        Tab_Cd = tab_Cd;
    }

    public int getType_Index() {
        return Type_Index;
    }

    public void setType_Index(int type_Index) {
        Type_Index = type_Index;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String caption) {
        Caption = caption;
    }

    public int getGuests() {
        return Guests;
    }

    public void setGuests(int guests) {
        Guests = guests;
    }

    public int getBook() {
        return book;
    }

    public void setBook(int book) {
        this.book = book;
    }

    public int getBill() {
        return bill;
    }

    public void setBill(int bill) {
        this.bill = bill;
    }
}
