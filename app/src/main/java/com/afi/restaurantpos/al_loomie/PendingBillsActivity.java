package com.afi.restaurantpos.al_loomie;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.PendingBillAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.CancelBillAndKotDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.PayBillDialog;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.MaterialDialog;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by afi on 10/21/2015.
 * Activity to display the pending bills
 */
public class PendingBillsActivity extends AppCompatActivity {

    private RecyclerView pendingBillView;
    private Toolbar toolbar;
    private TextView txtNoPendingBills;

    private boolean clickLock = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.initKitkatStatusbarTransparancy(this);
        setContentView(R.layout.activity_pending_bill);
        Utilities.initURL(this);
        pendingBillView = (RecyclerView) findViewById(R.id.rvpendingBill);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtNoPendingBills = (TextView) findViewById(R.id.txtNoPendingBills);
        toolbar.setTitle("Pending BILL's");
        pendingBillView.setHasFixedSize(true);
        pendingBillView.setLayoutManager(new GridLayoutManager(this, 2));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else
            loadPendingBills();
    }

    /**
     * Load the pending bills
     */
    private void loadPendingBills() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ApplicationSingleton.getInstance().getCurrentDate());

        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Loading Pending Bill's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        dialog.show();

        Utilities.getRetrofitWebService(getApplicationContext()).getAllPendingBill(date).enqueue(new Callback<List<PendingBill>>() {
            @Override
            public void onResponse(Response<List<PendingBill>> response, Retrofit retrofit) {
                List<PendingBill> pendingBills = response.body();
                dialog.cancel();
                pendingBillView.setAdapter(new PendingBillAdapter(pendingBills == null ? new ArrayList<PendingBill>() : pendingBills, PendingBillsActivity.this , new PendingBillAdapter.PendingBillSelectListner() {
                    @Override
                    public void onBillselected(String BillNumber, final PendingBill pendingBill) {
                        if (!clickLock) {
                            clickLock = true;
                            clickHandler.postDelayed(clickLockRunnable, 1000);
                            PayBillDialog.newInstance(CancelType.CANCEL_BILL)
                                    .setPendingBill(pendingBill)
                                    .setDialogListner(new PayBillDialog.PayBillDialogListner() {
                                        @Override
                                        public void onFinishedAction(PayBillDialog PayBillDialog) {
                                            //TODO refresh items
                                            PayBillDialog.dismiss();
                                            if (!Utilities.isNetworkConnected(getApplicationContext()))
                                                Utilities.createNoNetworkDialog(getApplicationContext());
                                            else
                                                loadPendingBills();
                                        }
                                    })
                                    .show(getSupportFragmentManager(), "Pay bill KOT");
                        }
                    }

                    @Override
                    public void onBillCheckChange() {


                    }

                    @Override
                    public void onBilllongselected(String BillNumber, final PendingBill pendingBill) {
                        CancelBillAndKotDialog.newInstance(CancelType.CANCEL_BILL)
                                .setPendingBill(pendingBill)
                                .setDialogListner(new CancelBillAndKotDialog.CancelBillAndKotDialogListner() {
                                    @Override
                                    public void onFinishedAction(CancelBillAndKotDialog cancelBillAndKotDialog) {
                                        //TODO refresh items
                                        cancelBillAndKotDialog.dismiss();
                                        if (!Utilities.isNetworkConnected(getApplicationContext()))
                                            Utilities.createNoNetworkDialog(getApplicationContext());
                                        else
                                            loadPendingBills();
                                    }
                                })
                                .show(getSupportFragmentManager(), "Pending KOT");

                    }


                }));
                if ( pendingBills != null)
                    showResult(pendingBills.isEmpty());
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.cancel();
                showResult(true);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private void showResult( boolean isListEmpty ) {
        if(isListEmpty) {
            pendingBillView.setVisibility(View.INVISIBLE);
            txtNoPendingBills.setVisibility(View.VISIBLE);

        }
        else {
            pendingBillView.setVisibility(View.VISIBLE);
            txtNoPendingBills.setVisibility(View.INVISIBLE);
            pendingBillView.setBackgroundColor(getResources().getColor(R.color.cardview_outer));
        }
    }

    private Handler clickHandler = new Handler();

    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };
}
