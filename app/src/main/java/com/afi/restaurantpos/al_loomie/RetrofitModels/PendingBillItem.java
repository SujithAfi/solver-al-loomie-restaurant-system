package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by afi on 10/21/2015.
 */
public class PendingBillItem {
    public String Itm_cd;
    public String Cmd_ItmName;
    public String Cmd_Qty;
    public String Cmd_Amt;
    public String Kot_No;
}
