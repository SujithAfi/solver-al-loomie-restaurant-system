package com.afi.restaurantpos.al_loomie;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Adapters.OrderDetailsFragmentPagerAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.CustomerDetailsFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.SelectCustomerDialogFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.SelectSalesmanDialogFragment;
import com.afi.restaurantpos.al_loomie.Dialogs.ShiftManagementDialog;
import com.afi.restaurantpos.al_loomie.Fragments.CustomersNewFragment;
import com.afi.restaurantpos.al_loomie.Fragments.GuestDetailsFragment;
import com.afi.restaurantpos.al_loomie.Fragments.OrderDetailsFragment;
import com.afi.restaurantpos.al_loomie.Fragments.ReservationDetailsFragment;
import com.afi.restaurantpos.al_loomie.Fragments.ReservationFragment;
import com.afi.restaurantpos.al_loomie.Fragments.TableDetailsFragment;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Models.StartShift;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Floor;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Login;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Salesman;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * OrderDetails Activity
 */
public class OrderDetails extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OrderDetailsFragment.OrderDetailsFragmentListner, CustomersNewFragment.SelectCustomerListner, ReservationDetailsFragment.OnHeadlineSelectedListener {

    private static final int MENU_OPTION_ID_SHIFT_MANAGEMENT = 4000;
    private static final int MENU_OPTION_ID_LANGUAGE = 4001;
    private static final int MENU_OPTION_ID_SETTINGS = 4002;
    private static final String[] LANGUAGES = {"English", "Arabic(عربي)"};
    public boolean isCreateCustomerFragmentActive = false;
    int floorMenuOrder = 0;
    GuestDetailsFragment guestDetailsFragment;
    private Toolbar mToolbar;
    private int LAST_SELECTED_FLOOR_ID = 0;
    private int MENU_ID_FLOR_GROUP = 2000;
    private int MENU_ID_OPTIONS_GROUP = 3000;
    private Menu navMenu;
    private TextView txtWaiterName;
    private TextView txtCounterNumber;
    private TextView txtWaiterAvatar;
    private TextView txtLoginUser;
    private List<Salesman> salesmans = new ArrayList<>();
    private boolean lock = false;
    private int fragmentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details_main);
        Utilities.initURL(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Solver Restaurant POS");
        mToolbar.inflateMenu(R.menu.menu_table_fragment);

        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.action_pending_kot) {

                    Intent intent = new Intent(OrderDetails.this, PendingKotActivity.class);
                    startActivity(intent);

                } else if (item.getItemId() == R.id.action_pending_bills) {

                    Intent intent = new Intent(OrderDetails.this, PendingBillsActivity.class);
                    startActivity(intent);

                } else if (item.getItemId() == R.id.action_pending_reservations) {
                    //

                    Intent i = new Intent(OrderDetails.this, ReservationsActivity.class);
                    startActivity(i);


                }
                /*else if (item.getItemId() == R.id.action_day_end) {
                    //

                   *//* new AlertDialog.Builder(OrderDetails.this)
                            .setTitle(R.string.success)
                            .setMessage(R.string.shift_ended)
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {*//*
                    if (!Utilities.isNetworkConnected(OrderDetails.this))
                        Utilities.createNoNetworkDialog(OrderDetails.this);
                    else
                        closeShift();


                             *//*   }
                            }).create().show();
*//*


                }*/

                return false;
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else {
            loadSalesManNames();
        }
        init_tax_details();


        View headerView = navigationView.getHeaderView(0);
        if (headerView != null) {
            txtWaiterName = (TextView) headerView.findViewById(R.id.txtWaiterName);
            txtCounterNumber = (TextView) headerView.findViewById(R.id.txtCounterNumber);
            txtWaiterAvatar = (TextView) headerView.findViewById(R.id.txtWaiterAvatar);
            txtLoginUser = (TextView) headerView.findViewById(R.id.txtLoginUser);
            try {
                SharedPreferences preferences = Utilities.getSharedPreferences(this);
                txtWaiterName.setText(preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, ""));
                txtCounterNumber.setText(String.format("Counter %s", preferences.getString(Constants.SHARED_PREF_COUNTER, "")));
                txtWaiterAvatar.setText(preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "").substring(0, 1).toUpperCase());
                txtLoginUser.setText(String.format("%s", preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "")));
            } catch (Exception e) {

            }
            Typeface myTypeface = Typeface.createFromAsset(getAssets(), "CaviarDreams_BoldItalic.ttf");

            txtWaiterAvatar.setTypeface(myTypeface);
            txtWaiterAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (getSalesmans().size() > 0) {
                        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                        SelectSalesmanDialogFragment fragment = new SelectSalesmanDialogFragment();
                        try {
                            fragment.setSalesmans(getSalesmans());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        fragment.setDialogCallback(new SelectSalesmanDialogFragment.DialogCallback() {
                            @Override
                            public void onPossitiveButtonClick(int indexOfData) {
                                try {
                                    Salesman salesman = getSalesmans().get(indexOfData);
                                    txtWaiterName.setText(salesman.Name);
                                    txtWaiterAvatar.setText(salesman.Name.substring(0, 1).toUpperCase());
                                    ApplicationSingleton.getInstance().setSalesman(salesman);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNegativeButtonClick() {

                            }
                        });
                        fragment.show(fragmentManager, "sfsdfdsdsfds");
                    }

                }
            });
        }
        navMenu = navigationView.getMenu();
        prepareNavigationMenu(navMenu);

        //Check user Permission to access Table details
        if (Utilities.getSharedPreferences(OrderDetails.this).getBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_VIEW, false))
            replaceFragment(new TableDetailsFragment(), "Table details");
        else
            Toast.makeText(OrderDetails.this, "You don't have permission to access", Toast.LENGTH_LONG).show();

    /*if(getUsername() != null)
        Log.e("Nameeeee====>>" , getUsername());*/

    }

    private void closeShift() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ApplicationSingleton.getInstance().getCurrentDate());

            /*StartShift startShift = new StartShift();
            startShift.id = ApplicationSingleton.getInstance().getCounterNumber().trim();
            startShift.float_cash = ApplicationSingleton.getInstance().getLogin().float_cash;*/
        String date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + " 00:00:00.000";
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(calendar.getTime());


        /*Log.e("Date=======>>" , date);
        Log.e("Time=======>>" , time);*/


        try {
            final ProgressDialog progressDialog = new ProgressDialog(OrderDetails.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setTitle(R.string.closing_shift);
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);

            progressDialog.show();
//            ApplicationSingleton.getInstance().getCounterNumber().trim(),ApplicationSingleton.getInstance().getLogin().float_cash
            Utilities.getRetrofitWebService(OrderDetails.this).stopShift(ApplicationSingleton.getInstance().getCounterNumber().trim(), ApplicationSingleton.getInstance().getLogin().float_cash, date, time).enqueue(new Callback<Map<String, Object>>() {
                @Override
                public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                    progressDialog.dismiss();

                    if ((Boolean) response.body().get("response_code")) {
                        try {
                            new AlertDialog.Builder(OrderDetails.this)
                                    .setTitle(R.string.dayend_success)
                                    .setMessage(R.string.login_again)
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Utilities.getSharedPreferences(OrderDetails.this)
                                                    .edit()
                                                    .putBoolean(Constants.SHARED_PREF_KEY_AUTHENTICATED, false)
                                                    .apply();
//                                            dialogCallback.onShiftEnded(ShiftManagementDialog.this);
                                            startActivity(new Intent(OrderDetails.this, LoginActivity.class));
                                            finish();


                                        }
                                    }).create().show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            new AlertDialog.Builder(OrderDetails.this)
                                    .setTitle(R.string.info)
                                    .setMessage((String) response.body().get("insertResponse"))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, null).create().show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();

                    new AlertDialog.Builder(OrderDetails.this)
                            .setTitle(R.string.info)
                            .setMessage("Network error...!")
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, null).create().show();
                }
            });
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }

    private void prepareNavigationMenu(final Menu navMenu) {
        List<Floor> floors = ApplicationSingleton.getInstance().getFloors();
        if (floors == null) {
            if (!Utilities.isNetworkConnected(this))
                Utilities.createNoNetworkDialog(this);
            else {
                try {
                    Utilities.getRetrofitWebService(getApplicationContext()).getFloors().enqueue(new Callback<List<Floor>>() {
                        @Override
                        public void onResponse(Response<List<Floor>> response, Retrofit retrofit) {
                            if (response.body() != null) {
                                initMenuWithFloors(navMenu, response.body());
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else
            initMenuWithFloors(navMenu, floors);
    }

    /**
     * Add floor numbers tom the menu
     *
     * @param navMenu
     * @param floors
     */
    private void initMenuWithFloors(Menu navMenu, List<Floor> floors) {
        if (floors != null) {
            //if (floors.size() > 0) {
            ApplicationSingleton.getInstance().setFloor(floors.get(0).Id);
            ApplicationSingleton.getInstance().setFloors(floors);
            Intent intent = new Intent("floorChanged");
            intent.putExtra("floor", floors.get(0).Id);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            //}
            if (navMenu != null) {

                //Sub menu for floors
                SubMenu floorsSubMenu = navMenu.addSubMenu(Menu.NONE, MENU_ID_FLOR_GROUP, 1, "Floors");
                for (Floor floorVar : floors) {
                    floorsSubMenu.add(MENU_ID_FLOR_GROUP, floors.indexOf(floorVar), floorMenuOrder++, floorVar.floor)
                            .setIcon(getIcon(floorMenuOrder - 1)).setChecked(floors.indexOf(floorVar) == 0);
                    // .setChecked(floorMenuOrder - 1 == 0);
                    if (floors.indexOf(floorVar) == 0)
                        LAST_SELECTED_FLOOR_ID = floors.indexOf(floorVar);
                }
                floorsSubMenu.setGroupCheckable(MENU_ID_FLOR_GROUP, true, true);

                //SubMenu Options
                SubMenu optionsSubMenu = navMenu.addSubMenu(Menu.NONE, MENU_ID_OPTIONS_GROUP, 2, "Options");
//                optionsSubMenu.add(MENU_ID_OPTIONS_GROUP, MENU_OPTION_ID_SHIFT_MANAGEMENT, 0, "Shift management").setIcon(R.drawable.ic_access_time_black_18dp);
                optionsSubMenu.add(MENU_ID_OPTIONS_GROUP, MENU_OPTION_ID_LANGUAGE, 0, "Language").setIcon(R.drawable.ic_language_black_18dp);
                optionsSubMenu.add(MENU_ID_OPTIONS_GROUP, MENU_OPTION_ID_SETTINGS, 1, "Settings").setIcon(R.drawable.ic_settings_black_18dp);
            }
        }
    }

    /**
     * Get icon for the floors
     *
     * @param floorNumber
     * @return
     */
    private int getIcon(int floorNumber) {
        switch (floorNumber) {
            case 0:
                return R.drawable.ic_looks_one_black_18dp;
            case 1:
                return R.drawable.ic_looks_two_black_18dp;
            case 2:
                return R.drawable.ic_looks_3_black_18dp;
            case 3:
                return R.drawable.ic_looks_4_black_18dp;
            case 4:
                return R.drawable.ic_looks_5_black_18dp;
            case 5:
                return R.drawable.ic_looks_6_black_18dp;
            default:
                return R.drawable.ic_looks_one_black_18dp;
        }
    }

    /**
     * Pre calculate tax percentage
     */
    private void init_tax_details() {

        SharedPreferences preferences = Utilities.getSharedPreferences(this);
        if (preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED, false)) {

            Float tax1 = Float.parseFloat(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0"));
            ApplicationSingleton.getInstance().setTaxFraction1(tax1);
        } else
            ApplicationSingleton.getInstance().setTaxFraction1(0);

        if (preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED, false)) {
            Float tax2 = Float.parseFloat(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0"));

            ApplicationSingleton.getInstance().setTaxFraction2(tax2);
        } else
            ApplicationSingleton.getInstance().setTaxFraction1(0);

    }


    @Override
    public void setTitle(CharSequence title) {
        mToolbar.setTitle(title);
    }

    /**
     * Load the Salesmans
     */
    private void loadSalesManNames() {

        try {
            Utilities.getRetrofitWebService(getApplicationContext()).getSalesManList().enqueue(new Callback<List<Salesman>>() {
                @Override
                public void onResponse(Response<List<Salesman>> response, Retrofit retrofit) {
                    if (response.body() != null) {
                        setSalesmans(response.body());
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_details, menu);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (isCreateCustomerFragmentActive) {
                Intent intent = new Intent("backActionBroadcastReceiver");

                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            } else {

                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 1) {
                    super.onBackPressed();
                } else {
                    try {
                        new AlertDialog.Builder(OrderDetails.this)
                                .setCancelable(false)
                                .setTitle("Exit Application ?")
                                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .create().show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }


    public List<Salesman> getSalesmans() {
        return salesmans;
    }

    public void setSalesmans(List<Salesman> salesmans) {
        this.salesmans = salesmans;
    }

    /**
     * Save the table details on the singleton class
     *
     * @param table
     */
    public void onTableSelected(final Table table) {
        synchronized (this) {
            ApplicationSingleton.getInstance().setTable(table);
            ApplicationSingleton.getInstance().setReservation(true);
            SelectCustomerDialogFragment selectCustomer = new SelectCustomerDialogFragment();
            selectCustomer.show(getSupportFragmentManager(), "SelectCustomerDialogFragment");
            selectCustomer.setSelectCustomerListner(new SelectCustomerDialogFragment.SelectCustomerListner() {
                @Override
                public void onCustomerSelected(Customer customer) {
                    Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
                    intent.putExtra("customer", customer);
                    intent.putExtra("table", table);
                    intent.putExtra("isReservationKot", false);
                    intent.putExtra("isReservationItemEdit", false);
                    intent.putExtra("hasCustomer", true);
                    intent.putExtra("kotDate", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
                    startActivityForResult(intent, 67);
                }

                @Override
                public void onSkipAction() {
                    Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
                    intent.putExtra("hasCustomer", false);
                    intent.putExtra("isReservationKot", false);
                    intent.putExtra("isReservationItemEdit", false);
                    intent.putExtra("table", table);
                    intent.putExtra("kotDate", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
                    startActivityForResult(intent, 67);
                }
            });

        }
    }

    public void onCustomerDetails(Customer customer) {

        boolean editCustFlag = false;

        // Check permission for edit customer
        if (Utilities.getSharedPreferences(OrderDetails.this).getBoolean(Constants.UR_CUSTOMER_EDIT, false))
            editCustFlag = true;

        CustomerDetailsFragment.newInstance(customer, editCustFlag).show(getSupportFragmentManager(), "Thsjkd");

    }

    public void onReservationSelected(final Table table) {
        synchronized (this) {
            ApplicationSingleton.getInstance().setTable(table);
            ApplicationSingleton.getInstance().setReservation(false);
            SelectCustomerDialogFragment selectCustomer = new SelectCustomerDialogFragment();
            selectCustomer.show(getSupportFragmentManager(), "SelectCustomerDialogFragment");
            selectCustomer.setSelectCustomerListner(new SelectCustomerDialogFragment.SelectCustomerListner() {
                @Override
                public void onCustomerSelected(Customer customer) {

                    ReservationFragment reservationFragment = new ReservationFragment();
                    Bundle arguments = new Bundle();
                    arguments.putSerializable("customer", customer);
                    arguments.putSerializable("table", ApplicationSingleton.getInstance().getTable());
                    reservationFragment.setArguments(arguments);
                    Utilities.replaceFragmentWithAnimation1(reservationFragment, "Reservation", getSupportFragmentManager(), R.id.fl_container);

                }

                @Override
                public void onSkipAction() {

                }
            });

        }
    }

    public void onReservationDetails(Reservation reservation) {

        ReservationDetailsFragment reservationFragment = new ReservationDetailsFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable("reservation", reservation);
        reservationFragment.setArguments(arguments);
        Utilities.replaceFragmentWithAnimation1(reservationFragment, "Reservation Details", getSupportFragmentManager(), R.id.fl_container);

    }

    public void onReorderNewItems(Table table, Customer customer) {

        boolean hasCustomer = false;

        if (customer.getId() != null && !customer.getId().matches(""))
            hasCustomer = true;


        ApplicationSingleton.getInstance().setTable(table);
        ApplicationSingleton.getInstance().setReservation(true);
        Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
        intent.putExtra("customer", customer);
        intent.putExtra("table", table);
        intent.putExtra("isReservationKot", false);
        intent.putExtra("isReservationItemEdit", false);
        intent.putExtra("hasCustomer", hasCustomer);
        intent.putExtra("kotDate", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
        startActivityForResult(intent, 67);

    }


    public void onAddItemForReservation(String reservationId, Customer customer, String resvDate) {

        Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
        intent.putExtra("customer", customer);
        intent.putExtra("isReservationKot", true);
        intent.putExtra("isReservationItemEdit", false);
        intent.putExtra("resvId", reservationId);
        intent.putExtra("table", ApplicationSingleton.getInstance().getTable());
        intent.putExtra("hasCustomer", true);
        intent.putExtra("resvDate", resvDate);
        startActivityForResult(intent, 67);

    }

    public void onViewItemForReservation(Reservation reservation, Customer customer, Boolean makeKOT, String resvDate) {

        Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
        intent.putExtra("customer", customer);
        intent.putExtra("isReservationKot", true);
        intent.putExtra("isReservationItemEdit", true);
        intent.putExtra("makeKOT", makeKOT);
        intent.putExtra("reservation", reservation);
        intent.putExtra("table", ApplicationSingleton.getInstance().getTable());
        intent.putExtra("hasCustomer", true);
        intent.putExtra("resvDate", resvDate);
        startActivityForResult(intent, 67);

    }

    /**
     * Change the orer type
     *
     * @param orderType
     */
    public void changeOrderType(OrderDetailsFragmentPagerAdapter.OrderType orderType) {

        switch (orderType) {
            case DINE_IN:
//                txtOrderType.setText("Dine in");

              /*  txtOrderType.setAlpha(0f);
                txtOrderType.setVisibility(View.VISIBLE);
                txtOrderType.animate().alpha(1f)
                        .setDuration(500);*/


//                imgNavigation.setAlpha(0f);
//                imgNavigation.setVisibility(View.VISIBLE);
//                imgNavigation.animate().alpha(1f)
//                        .setDuration(500);

                replaceFragment(new TableDetailsFragment(), "Table details");
                break;
            case TAKE_AWAY:
//                txtOrderType.setText("Take away");
/*
                txtOrderType.setAlpha(0f);
                txtOrderType.setVisibility(View.VISIBLE);
                txtOrderType.animate().alpha(1f)
                        .setDuration(500);*/


//                imgNavigation.setAlpha(0f);
//                imgNavigation.setVisibility(View.VISIBLE);
//                imgNavigation.animate().alpha(1f)
//                        .setDuration(500);
                ApplicationSingleton.getInstance().setTable(null);
                replaceFragment(new CustomersNewFragment().setSelectCustomerListner(this), "Take away");
                break;
            case HOME_DELIVERY:
//                txtOrderType.setText("Home delivery");

            /*    txtOrderType.setAlpha(0f);
                txtOrderType.setVisibility(View.VISIBLE);
                txtOrderType.animate().alpha(1f)
                        .setDuration(500);*/


//                imgNavigation.setAlpha(0f);
//                imgNavigation.setVisibility(View.VISIBLE);
//                imgNavigation.animate().alpha(1f)
//                        .setDuration(500);
                ApplicationSingleton.getInstance().setTable(null);
                replaceFragment(new CustomersNewFragment().setSelectCustomerListner(this), "Home delivery");
                break;
            case MAIN_PAGE:
                // txtOrderType.setText("Home delivery");
           /*     txtOrderType.setVisibility(View.VISIBLE);
                txtOrderType.setAlpha(1f);
                txtOrderType.animate().alpha(0f)
                        .setDuration(500);*/

//                imgNavigation.setVisibility(View.VISIBLE);
//                imgNavigation.setAlpha(1f);
//                imgNavigation.animate().alpha(0f)
//                        .setDuration(500);
                break;

        }
        try {
            SubMenu floorGroup = navMenu.findItem(MENU_ID_FLOR_GROUP).getSubMenu();
            floorGroup.getItem().setVisible(orderType == OrderDetailsFragmentPagerAdapter.OrderType.DINE_IN);
        } catch (Exception e) {
            Log.e("EXe", e.toString());
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (item.getGroupId() == MENU_ID_OPTIONS_GROUP) {
            int id = item.getItemId();
            if (id == MENU_OPTION_ID_SETTINGS) {
                startActivity(new Intent(this, Settings.class));
            } /*else if (id == MENU_OPTION_ID_SHIFT_MANAGEMENT) {

                ShiftManagementDialog.newInstance(new ShiftManagementDialog.ShiftManagementDialogCallback() {
                    @Override
                    public void onShiftStarted() {
                    }

                    @Override
                    public void onCanceled() {

                    }

                    @Override
                    public void onShiftEnded(ShiftManagementDialog shiftManagementDialog) {
                        shiftManagementDialog.dismiss();
                        Intent intent = new Intent(OrderDetails.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, ApplicationSingleton.getInstance().getLogin(), ApplicationSingleton.getInstance().getLogin().Counter).show(getSupportFragmentManager(), "ShiftManagementDialog");

            }*/ else if (id == MENU_OPTION_ID_LANGUAGE) {
                showLanguageSelectionDialog();
            }
        } else if (item.getGroupId() == MENU_ID_FLOR_GROUP) {


            try {
                SubMenu floorGroup = navMenu.findItem(item.getGroupId()).getSubMenu();
                for (int i = 0; i < floorGroup.size(); i++) {
                    MenuItem menuItem = floorGroup.getItem(i);
                    menuItem.setChecked(menuItem == item);
                }
            } catch (Exception e) {

            }
            if (!Utilities.isNetworkConnected(this)) {
                Utilities.createNoNetworkDialog(this);
            } else {
                LAST_SELECTED_FLOOR_ID = item.getItemId();


                int order = item.getOrder();
                String floorId = ApplicationSingleton.getInstance().getFloors().get(order).Id;
                ApplicationSingleton.getInstance().setFloor(floorId);

                try {
                    Intent intent = new Intent("floorChanged");
                    intent.putExtra("floor", floorId);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }

    private void showLanguageSelectionDialog() {
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(OrderDetails.this,
                android.R.layout.simple_spinner_item, LANGUAGES);
        final Spinner sp = new Spinner(OrderDetails.this);
        sp.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        sp.setAdapter(adp);
        try {
            new AlertDialog.Builder(this)
                    .setTitle("Select your language")
                    .setPositiveButton("Ok", null)
                    .setView(sp)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void replaceFragment(Fragment fragment, String tag) {

        Utilities.replaceFragmentWithAnimation(fragment, tag, getSupportFragmentManager());
    }

    @Override
    public void onFloorChanged(String floorId) {

        //TODO
        /*if( tableDetailsFragment != null && orderType == OrderType.DINE_IN)
            tableDetailsFragment.getKotItems(false , floorId);*/

    }

    @Override
    public void onCustomerSelected(Customer customer) {
        Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
        intent.putExtra("customer", customer);
        intent.putExtra("hasCustomer", true);
        startActivityForResult(intent, 67);
    }

    @Override
    public void onSkipAction() {
        Intent intent = new Intent(OrderDetails.this, SelectItemsActivity.class);
        intent.putExtra("hasCustomer", false);
        startActivityForResult(intent, 67);
    }

    @Override
    public void onArticleSelected() {

    }

   /* public String getUsername() {
        AccountManager manager = AccountManager.get(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            Account[] accounts = manager.getAccountsByType("com.google");
            List<String> possibleEmails = new LinkedList<String>();

            for (Account account : accounts) {
                // TODO: Check possibleEmail against an email regex or treat
                // account.name as an email address only for certain account.type values.
                possibleEmails.add(account.name);
            }

            if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
                String email = possibleEmails.get(0);
                String[] parts = email.split("@");

                if (parts.length > 1)
                    return parts[0];
            }
            return null;
        }

        return null;
    }*/
}
