package com.afi.restaurantpos.al_loomie.Fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.McItemModifiersAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.View.DividerItemDecorationWithMargin;

import java.util.ArrayList;


public class MuticorseItemChangeFragment extends DialogFragment {

    private static final String NAME_KEY = "item_name";
    private static final String ITEM_KEY = "mappeditem";
    private static final String ITEM_QTY = "item_qty";
    private static final String MAIN_ITEM_KEY = "main_item";
    private static final String SELECTED_POS = "selected_pos";
    private static final String REMARKS = "item_remark";
    private static final String MAIN_ITEM_COUNT = "main_item_count";
    public boolean clickLock = false;
    private View mLayout;
    private Toolbar toolbar;
    private ItemChangeCallBack callBack;
    private RecyclerView recyclerViewMuticorseItems;
    private String itemName;
    private ArrayList<Item> mappedItems;
    private TextView tvItemName;
    private EditText etQuantity;
    private ImageView ivSub;
    private ImageView ivAdd;
    private String itemQty;
    private Item changedItem;
    private ArrayList<Item> mainItems;
    private int mainItemSelectedPos;
    private ImageView ivSave;
    private TextView tvAllergicInfo;
    private LinearLayout llAllergicInfo;
    private String allergies;
    private int tempItemCount = 0;
    private int mainItemCount = 0;


    public MuticorseItemChangeFragment() {
    }

    public static MuticorseItemChangeFragment newInstance(ArrayList<Item> mappedItems ,  String itemName , String itemQty , ArrayList<Item> mainItems , String remark , int selectedPos , String mainCount) {
        MuticorseItemChangeFragment fragment = new MuticorseItemChangeFragment();
        Bundle args = new Bundle();
        args.putSerializable(ITEM_KEY, mappedItems);
        args.putString(NAME_KEY, itemName);
        args.putString(ITEM_QTY, itemQty);
        args.putSerializable(MAIN_ITEM_KEY, mainItems);
        args.putString(REMARKS, remark);
        args.putInt(SELECTED_POS, selectedPos);
        args.putInt(MAIN_ITEM_COUNT, Integer.parseInt(mainCount));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {

            mappedItems = (ArrayList<Item>)getArguments().getSerializable(ITEM_KEY);
            itemName = getArguments().getString(NAME_KEY);
            itemQty = getArguments().getString(ITEM_QTY);
            mainItems = (ArrayList<Item>)getArguments().getSerializable(MAIN_ITEM_KEY);
            mainItemSelectedPos = getArguments().getInt(SELECTED_POS);
            allergies = getArguments().getString(REMARKS);
            mainItemCount = getArguments().getInt(MAIN_ITEM_COUNT);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.fragment_multicorse_change_items, container, false);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        tvItemName = (TextView) mLayout.findViewById(R.id.tv_item);
        recyclerViewMuticorseItems = (RecyclerView) mLayout.findViewById(R.id.recyclerViewMuticorseItems);
        ivSub = (ImageView) mLayout.findViewById(R.id.iv_sub);
        ivAdd = (ImageView) mLayout.findViewById(R.id.iv_add);
        etQuantity = (EditText) mLayout.findViewById(R.id.et_quantity);
        ivSave = (ImageView) mLayout.findViewById(R.id.iv_save);
        tvAllergicInfo = (TextView) mLayout.findViewById(R.id.tv_allergic_info);
        llAllergicInfo = (LinearLayout) mLayout.findViewById(R.id.ll_allergicinfo);

        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);

        recyclerViewMuticorseItems.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewMuticorseItems.addItemDecoration(new DividerItemDecorationWithMargin(getActivity()));

        toolbar.setNavigationIcon(R.drawable.back_butt);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MuticorseItemChangeFragment.this.dismiss();
            }
        });

        if(mainItems != null ){

            tempItemCount = mainItems.get(mainItemSelectedPos).tempItemCount;
            if(tempItemCount > 0) {
                etQuantity.setText(tempItemCount + "");
            }
            else {
                if (Long.parseLong(itemQty) > 0)
                    etQuantity.setText(itemQty);
            }
        }


        if(itemName != null)
            tvItemName.setText(itemName);

        if(allergies != null && !allergies.matches("")){

            llAllergicInfo.setVisibility(View.VISIBLE);
            tvAllergicInfo.setText(allergies);
        }

        recyclerViewMuticorseItems.setAdapter(new McItemModifiersAdapter(new McItemModifiersAdapter.onItemClickCallBack() {
            @Override
            public void onItemSelected(int position) {

                if(mappedItems != null){
                if (mappedItems.size() > 0) {

                    tvItemName.setText(mappedItems.get(position).Item_Name);
                    changedItem = mappedItems.get(position);
                    mappedItems.remove(position);
                    allergies = changedItem.getRemarks();
                    if (allergies != null && !allergies.matches("")) {

                        llAllergicInfo.setVisibility(View.VISIBLE);
                        tvAllergicInfo.setText(allergies);
                    }

                }
            }


            }
        }, mappedItems));

        initButtons();


        ivSave.setOnClickListener(new View.OnClickListener() {
            public boolean changedFalg;

            @Override
            public void onClick(View v) {

                changedFalg = true;

                int qty = tempItemCount == 0 ? Integer.parseInt(itemQty) : tempItemCount;

                if (!etQuantity.getText().toString().matches("") && etQuantity.getText().toString() != null) {
                if (Integer.parseInt(etQuantity.getText().toString()) <= qty) {
                    if (Integer.parseInt(etQuantity.getText().toString()) == 0) {

                        mainItems.get(mainItemSelectedPos).itemCount = 0;
                        if (tempItemCount == 0)
                            mainItems.get(mainItemSelectedPos).tempItemCount = Integer.parseInt(itemQty);
                        else
                            mainItems.get(mainItemSelectedPos).tempItemCount = tempItemCount;
                    } else {

                        if (changedItem != null) {

                            if (Integer.parseInt(etQuantity.getText().toString()) == Integer.parseInt(itemQty)) {

                                mainItems.remove(mainItemSelectedPos);
                                if (Integer.parseInt(etQuantity.getText().toString()) != mainItemCount) {
                                    changedItem.itemCount = Integer.parseInt(etQuantity.getText().toString());
                                }
                                mainItems.add(mainItemSelectedPos, changedItem);


                            } else {

                                mainItems.get(mainItemSelectedPos).itemCount = Integer.parseInt(itemQty) - Integer.parseInt(etQuantity.getText().toString());
                                changedItem.itemCount = Integer.parseInt(etQuantity.getText().toString());
                                changedItem.Is_TempItem = true;
                                changedItem.modificationRemarks = "";
                                if (mainItems.get(mainItemSelectedPos).getModificationRemarks() != null && !mainItems.get(mainItemSelectedPos).getModificationRemarks().matches(""))
                                    mainItems.get(mainItemSelectedPos).setModificationRemarks(mainItems.get(mainItemSelectedPos).getModificationRemarks() + "," + changedItem.itemCount + " quantity replaced with " + changedItem.Item_Name);
                                else
                                    mainItems.get(mainItemSelectedPos).setModificationRemarks(changedItem.itemCount + " quantity replaced with " + changedItem.Item_Name);

                                mainItems.add((mainItemSelectedPos + 1) , changedItem);

                            }
                        } else {

                            if (Integer.parseInt(etQuantity.getText().toString()) == Integer.parseInt(itemQty)) {

                                if (tempItemCount > 0 && Integer.parseInt(itemQty) == 0) {
                                    if (tempItemCount == 0)
                                        mainItems.get(mainItemSelectedPos).itemCount = -1;
                                    else
                                        mainItems.get(mainItemSelectedPos).itemCount = tempItemCount;

                                } else
                                    mainItems.get(mainItemSelectedPos).itemCount = -1;

                            } else {
                                changedFalg = false;
                                if (tempItemCount > 0 && Integer.parseInt(itemQty) == 0) {
                                    if (tempItemCount == 0)
                                        mainItems.get(mainItemSelectedPos).itemCount = -1;
                                    else
                                        mainItems.get(mainItemSelectedPos).itemCount = tempItemCount;

                                } else if (Integer.parseInt(etQuantity.getText().toString()) != 0 && tempItemCount <= Integer.parseInt(itemQty)) {
                                    mainItems.get(mainItemSelectedPos).itemCount = Integer.parseInt(etQuantity.getText().toString());

                                } else
                                    mainItems.get(mainItemSelectedPos).itemCount = -1;

                            }
                        }

                        mainItems.get(mainItemSelectedPos).tempItemCount = 0;
                    }

                    if(callBack != null)
                        callBack.onItemSelected(mainItems, changedFalg);
                    dismiss();
                } else {

                    Toast.makeText(getActivity(), "Quantity should not greater than item quantity.", Toast.LENGTH_LONG).show();

                }
            }
                else {

                    Toast.makeText(getActivity(), "Please a enter valid Quantity.", Toast.LENGTH_LONG).show();

                }

            }
        });
    }


    private void initButtons() {

        try {
            ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.parseInt(etQuantity.getText().toString()) < Integer.parseInt(itemQty)) {
                        etQuantity.setText(
                                etQuantity.getText().toString().length() > 0 ? String.valueOf(Integer.parseInt(etQuantity.getText().toString()) + 1) : ""

                        );
                    }
                }
            });

            ivSub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etQuantity.setText(
                            etQuantity.getText().toString().length() > 0 ?
                                    (Integer.parseInt(etQuantity.getText().toString()) > 0 ? String.valueOf(Integer.parseInt(etQuantity.getText().toString()) - 1)
                                            : etQuantity.getText().toString()) :
                                    etQuantity.getText().toString()
                    );
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void setCallBack(ItemChangeCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onStop() {
        super.onStop();
        ApplicationSingleton.getInstance().setLockFlag(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface ItemChangeCallBack {
        void onItemSelected(ArrayList<Item> changeItems, boolean isSuccess);

        void onCanceled();
    }

}
