package com.afi.restaurantpos.al_loomie;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.PreviousBillAdapter;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PreviousBill;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Activity to dislay the previous BIlls
 */
public class PreviousBillActivity extends AppCompatActivity {

    private RecyclerView rVPreviousBills;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_bill);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Utilities.initURL(this);
        rVPreviousBills = (RecyclerView) findViewById(R.id.rVPreviousBills);
        rVPreviousBills.setHasFixedSize(true);
        rVPreviousBills.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else
            loadPreviousBlls();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

         if(id == android.R.id.home)
            this.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    /**
     * Load the pending bills
     */
    private void loadPreviousBlls() {

        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading previous bills");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
            Utilities.getRetrofitWebService(getApplicationContext()).getPreiousBills().enqueue(new Callback<List<PreviousBill>>() {
                @Override
                public void onResponse(Response<List<PreviousBill>> response, Retrofit retrofit) {
                    progressDialog.cancel();
                    List<PreviousBill> previousBills = response.body();

                    if (previousBills != null)

                    rVPreviousBills.setAdapter(new PreviousBillAdapter(previousBills, new PreviousBillAdapter.itemCLickListner() {
                        @Override
                        public void onItemClick(int position, PreviousBill previousBill) {


                            Utilities.showToast("Printing" , PreviousBillActivity.this);


                        }
                    }));
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

}
