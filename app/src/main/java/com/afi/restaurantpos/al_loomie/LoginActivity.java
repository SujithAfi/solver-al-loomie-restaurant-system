package com.afi.restaurantpos.al_loomie;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Dialogs.CounterIdSetupDialog;
import com.afi.restaurantpos.al_loomie.Models.Shift_Date;
import com.afi.restaurantpos.al_loomie.Models.StartShift;
import com.afi.restaurantpos.al_loomie.Models.ParamData;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Login;
import com.afi.restaurantpos.al_loomie.RetrofitModels.User_Rights;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afi.restaurantpos.al_loomie.Utills.ValidateExeption;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Activity for login
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, TextView.OnEditorActionListener {


    private static final int FORM_VALIDATION_INDEPENDANT = 1;
    private static final int FORM_VALIDATION_DEPENDANT = 2;
    private static final String[] LANGUAGES = {"English", "Arabic(عربي)"};
    private EditText edtTxtUserName , edtTxtPassword;
    private Button btnLogin;
    private String userName , password;

    //User Rights Table
    private boolean tableAddorSave = false;
    private boolean tableEdit = false;
    private boolean tableDelete = false;
    private boolean tableView = false;

    //User Rights Reservation
    private boolean resvAddorSave = false;
    private boolean resvEdit = false;
    private boolean resvDelete = false;
    private boolean resvView = false;

    //User Rights Billing
    private boolean billAddorSave = false;
    private boolean billEdit = false;
    private boolean billDelete = false;
    private boolean billView = false;


    //User Rights Payment
    private boolean paymentAddorSave = false;
    private boolean paymentEdit = false;
    private boolean paymentDelete = false;
    private boolean paymentView = false;

    //User Rights Customer
    private boolean custAddorSave = false;
    private boolean custEdit = false;
    private boolean custDelete = false;
    private boolean custView = false;

    private Login login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_new);
        Utilities.initURL(this);
        Utilities.initKitkatStatusbarTransparancy(this);
        edtTxtUserName = (EditText)findViewById(R.id.EditTextUserName);
        edtTxtPassword = (EditText)findViewById(R.id.EditTextPassword);
        btnLogin = (Button)findViewById(R.id.ButtonLogin);
        btnLogin.setOnClickListener(this);
        edtTxtPassword.setOnEditorActionListener(this);
        edtTxtUserName.setOnEditorActionListener(this);

    }

    @Override
    public void onClick(View v) {
        Utilities.hideKeybord(this);
        switch (v.getId()){
            case R.id.ButtonLogin:
                if (!Utilities.isNetworkConnected(this))
                    Utilities.createNoNetworkDialog(this);
                else
                    try {

                        if(validateLoginForm(FORM_VALIDATION_DEPENDANT)){


                            if ( checkIsSoftEng()) {
                                Intent intent = new Intent( LoginActivity.this , ServerSettingsActivity.class);
                                startActivityForResult(intent, 32);
                                return;
                            }

                            String id  = Utilities.getDeviceId(LoginActivity.this);

                            Map<String , String> credentials = new HashMap<>();
                            credentials.put("username", userName);
                            credentials.put("passsword", password);
                            credentials.put("device_id", id);

                            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                            progressDialog.setTitle("Loging in...");
                            progressDialog.setMessage("Please wait");
                            progressDialog.show();

                            Call<Login> loginCall = Utilities.getRetrofitWebService(getApplicationContext()).getLoginData(credentials);
                            loginCall.enqueue(new Callback<Login>() {
                                @Override
                                public void onResponse(Response<Login> response, Retrofit retrofit) {
                                    progressDialog.cancel();
                                    if ( response.body() !=  null) {
                                        final Login loginData = response.body();


                                        if( loginData.loginResponse == 1) {
                                            edtTxtPassword.setText("");
                                            edtTxtUserName.setText("");
                                            if(loginData.Counter.equals("0")) {
                                                new CounterIdSetupDialog()
                                                        .setCounterIdSetupDialogCallBack(new CounterIdSetupDialog.CounterIdSetupDialogCallBack() {
                                                            @Override
                                                            public void onPositive(CounterIdSetupDialog counterIdSetupDialog , String counterId) {

                                                                if(counterId != null && !counterId.matches(""))
                                                                    loginData.Counter = counterId;
                                                                    counterIdSetupDialog.dismiss();

                                                                if ( loginData.Shift.equals("CLOSED") ||  loginData.Shift.equals("CLOSE")) {
                                                                    //Need to open the shift

                                                                    if (!Utilities.isNetworkConnected(LoginActivity.this))
                                                                        Utilities.createNoNetworkDialog(LoginActivity.this);
                                                                    else {


                                                                        try {

                                                                            Utilities.getRetrofitWebService(LoginActivity.this).getParamDate(loginData.Counter).enqueue(new Callback<String>() {
                                                                                @Override
                                                                                public void onResponse(Response<String> response, Retrofit retrofit) {


                                                                                    if (response.body() != null){

                                                                                        final Calendar calendar = Calendar.getInstance();
                                                                                        final Calendar calendar1 = Calendar.getInstance();
                                                                                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                                                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");


                                                                                        if(response.body() != null && !response.body().matches("")){

                                                                                            try {
                                                                                                calendar.setTime(dateFormat.parse(response.body()));
                                                                                            } catch (ParseException e) {
                                                                                                e.printStackTrace();
                                                                                            }
                                                                                        }


                                                                                        if(loginData.serverDate != null && !loginData.serverDate.matches("")){

                                                                                            try {
                                                                                                calendar1.setTime(dateFormat1.parse(loginData.serverDate.substring(0,10)));
                                                                                            } catch (ParseException e) {
                                                                                                e.printStackTrace();
                                                                                            }
                                                                                        }


                                                                                        if(calendar.getTime().before(calendar1.getTime())){

                                                                                            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
                                                                                                    .setTitle("Shift Date " + dateFormat.format(calendar.getTime()) + " is less than the Server Date " + dateFormat.format(calendar1.getTime()))
                                                                                                    .setCancelable(false)
                                                                                                    .setMessage("Do you want to update the Shift Date with Server Date..?")
                                                                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                                                        @Override
                                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                                            updateParamsDate(calendar1 , loginData);

                                                                                                        }
                                                                                                    })
                                                                                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                                                                        @Override
                                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                                            startShift(loginData , calendar);
                                                                                                        }
                                                                                                    })
                                                                                                    .create();
                                                                                            alertDialog.show();


                                                                                        }
                                                                                        else if(calendar.getTime().after(calendar1.getTime())){


                                                                                            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
                                                                                                    .setTitle("Shift Date " + dateFormat.format(calendar.getTime()) + " is greater than the Server Date " + dateFormat.format(calendar1.getTime()))
                                                                                                    .setCancelable(false)
                                                                                                    .setMessage("Do you want to update the Shift Date with Server Date..?")
                                                                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                                                        @Override
                                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                                            updateParamsDate(calendar1 , loginData);

                                                                                                        }
                                                                                                    })
                                                                                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                                                                        @Override
                                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                                            startShift(loginData , calendar);
                                                                                                        }
                                                                                                    })
                                                                                                    .create();
                                                                                            alertDialog.show();

                                                                                        }
                                                                                        else{

                                                                                            updateParamsDate(calendar , loginData);
                                                                                        }


                                                                                    }

                                                                                }

                                                                                @Override
                                                                                public void onFailure(Throwable t) {

                                                                                }
                                                                            });
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }


                                                                    }

                                                                }
                                                                else if ( loginData.Shift.equals("OPEN") ){
                                                                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                                                                    Calendar calendar = Calendar.getInstance();

                                                                    if(loginData.shift_date != null && !loginData.shift_date.matches("")){

                                                                        try {
                                                                            calendar.setTime(dateFormat.parse(loginData.shift_date));
                                                                        } catch (ParseException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }


                                                                    startOrderDetailsPage(loginData , calendar);
                                                                }
                                                                else{


                                                                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                                                                    Calendar calendar = Calendar.getInstance();

                                                                    if(loginData.shift_date != null && !loginData.shift_date.matches("")){

                                                                        try {
                                                                            calendar.setTime(dateFormat.parse(loginData.shift_date));
                                                                        } catch (ParseException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }

                                                                    updateParamsDate(calendar , loginData);

                                                                }
                                                            }


                                                            @Override
                                                            public void onNegative(CounterIdSetupDialog counterIdSetupDialog) {

                                                                counterIdSetupDialog.dismiss();

                                                            }
                                                        } )
                                                        .show(getSupportFragmentManager(), "countwe");
                                            }

                                            else
                                            {
                                                if ( loginData.Shift.equals("CLOSED") ||  loginData.Shift.equals("CLOSE") || loginData.Shift.length() == 0) {

                                                    if (!Utilities.isNetworkConnected(LoginActivity.this))
                                                        Utilities.createNoNetworkDialog(LoginActivity.this);
                                                    else {


                                                        try {

                                                            Utilities.getRetrofitWebService(LoginActivity.this).getParamDate(loginData.Counter).enqueue(new Callback<String>() {
                                                                @Override
                                                                public void onResponse(Response<String> response, Retrofit retrofit) {


                                                                    if (response.body() != null){


                                                                        final Calendar calendar = Calendar.getInstance();
                                                                        final Calendar calendar1 = Calendar.getInstance();
                                                                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");


                                                                        if(response.body() != null && !response.body().matches("")){

                                                                            try {
                                                                                calendar.setTime(dateFormat.parse(response.body()));
                                                                            } catch (ParseException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }


                                                                        if(loginData.serverDate != null && !loginData.serverDate.matches("")){

                                                                            try {
                                                                                calendar1.setTime(dateFormat1.parse(loginData.serverDate.substring(0,10)));
                                                                            } catch (ParseException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }


                                                                        if(calendar.getTime().before(calendar1.getTime())){

                                                                            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
                                                                                    .setTitle("Shift Date " + dateFormat.format(calendar.getTime()) + " is less than the Server Date " + dateFormat.format(calendar1.getTime()))
                                                                                    .setCancelable(false)
                                                                                    .setMessage("Do you want to update the Shift Date with Server Date..?")
                                                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                            updateParamsDate(calendar1 , loginData);

                                                                                        }
                                                                                    })
                                                                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                            startShift(loginData , calendar);
                                                                                        }
                                                                                    })
                                                                                    .create();
                                                                            alertDialog.show();


                                                                        }
                                                                        else if(calendar.getTime().after(calendar1.getTime())){


                                                                            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
                                                                                    .setTitle("Shift Date " + dateFormat.format(calendar.getTime()) + " is greater than the Server Date " + dateFormat.format(calendar1.getTime()))
                                                                                    .setCancelable(false)
                                                                                    .setMessage("Do you want to update the Shift Date with Server Date..?")
                                                                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                            updateParamsDate(calendar1 , loginData);

                                                                                        }
                                                                                    })
                                                                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                            startShift(loginData , calendar);
                                                                                        }
                                                                                    })
                                                                                    .create();
                                                                            alertDialog.show();

                                                                        }
                                                                        else{

                                                                            updateParamsDate(calendar , loginData);
                                                                        }


                                                                    }

                                                                }

                                                                @Override
                                                                public void onFailure(Throwable t) {

                                                                }
                                                            });
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }


                                                    }



                                                }
                                                else if( loginData.Shift.equals("OPEN")) {

                                                    ApplicationSingleton.getInstance().setLogin(loginData);


                                                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                                                    Calendar calendar = Calendar.getInstance();


                                                    if(loginData.shift_date != null && !loginData.shift_date.matches("")){

                                                        try {
                                                            calendar.setTime(dateFormat.parse(loginData.shift_date));
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }


                                                    startOrderDetailsPage(loginData , calendar);
                                                }


                                                Utilities.getSharedPreferences(LoginActivity.this)
                                                        .edit()
                                                        .putString(Constants.COUNTER_NAME, loginData.Counter)
                                                        .apply();
                                            }
                                        }
                                        else if(loginData.loginResponse == 0)
                                            Utilities.showSnackBar("Invalid credentials" , LoginActivity.this);
                                    }
                                    else {
                                        Utilities.showSnackBar("Network unavaliable" , LoginActivity.this);
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    progressDialog.cancel();
                                    Utilities.showSnackBar("Network unavaliable" , LoginActivity.this);
                                }
                            });


                        }

                    } catch (ValidateExeption validateExeption) {
                        Utilities.showSnackBar(validateExeption.getMessage() , LoginActivity.this);
                    }
        }
    }


    private void updateParamsDate(final Calendar cal , final Login login) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        ParamData param = new ParamData();

        param.counter = login.Counter;
        param.date = dateFormat.format(cal.getTime());

        if (!Utilities.isNetworkConnected(LoginActivity.this))
            Utilities.createNoNetworkDialog(LoginActivity.this);
        else {


            try {

                Utilities.getRetrofitWebService(LoginActivity.this).updateParamsDate(param).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Response<String> response, Retrofit retrofit) {

                        if (response.body().equals("Successfully Updated")) {

                            startShift(login , cal);

                        } else {
                            try {

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startShift(final Login loginData , Calendar calendar){

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setTitle("Loging in...");
        progressDialog.setMessage("Please wait");
        progressDialog.show();

        login = loginData;

        if (!Utilities.isNetworkConnected(LoginActivity.this))
            Utilities.createNoNetworkDialog(LoginActivity.this);
        else {


            try {
                login.Shift = "OPEN";
                login.Remarks = "";
                ApplicationSingleton.getInstance().setLogin(login);

                StartShift startShift = new StartShift();
                startShift.id = login.Counter + "";
                startShift.shift_No = login.shift_no == 0 ? "1" : login.shift_no + "";
                startShift.cashier = login.SlaesId + "";
                startShift.emp_cd = login.SlaesId + "";
                startShift.emp_name = login.Usr_Name + "";
                startShift.remarks = "";
                startShift.float_cash = login.float_cash;
                startShift.date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + " 00:00:00.000";
                startShift.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(calendar.getTime());

                Utilities.getRetrofitWebService(LoginActivity.this).startShift(startShift).enqueue(new Callback<Shift_Date>() {
                    @Override
                    public void onResponse(Response<Shift_Date> response, Retrofit retrofit) {
                        progressDialog.dismiss();
                        if (response.body() != null) {

                            Shift_Date shift_date = response.body();

                            if(shift_date != null) {

                                if(shift_date.Message != null){

                                if (shift_date.Message.equals("Successfully Inserted")) {

                                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                                    Calendar calendar = Calendar.getInstance();

                                    if(shift_date.ShiftDate != null){
                                        try {
                                            calendar.setTime(dateFormat1.parse(shift_date.ShiftDate));
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    else if (login.shift_date != null && !login.shift_date.matches("")) {

                                        try {
                                            calendar.setTime(dateFormat.parse(login.shift_date));
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    try {
                                        loginData.shift_date = dateFormat.format(calendar.getTime());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                    startOrderDetailsPage(loginData, calendar);


                                }
                            }
                        }
                    }

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        progressDialog.dismiss();
                        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
                                .setTitle(R.string.error)
                                .setCancelable(false)
                                .setMessage("Network error...!")
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .create();
                        alertDialog.show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }

    /**
     * check of the credentials provided matched with SOFTENG username password combo
     * @return
     */
    private boolean checkIsSoftEng() {

        if ( edtTxtUserName.getText().toString().equalsIgnoreCase(Constants.SOFTENG_USERNAME)) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMHHmm");
            String formattrDate = simpleDateFormat.format(calendar.getTime());
            if( edtTxtPassword.getText().toString() .equalsIgnoreCase(formattrDate + "THANKGOD")){
                return true;
            }
        }
        return false;
    }

    /**
     * Show the error dialog
     * @param cause
     */
    private void ShowLoginErrorDialog(String cause){
        try {
            new AlertDialog.Builder(LoginActivity.this)
                    .setTitle(R.string.error)
                    .setMessage(cause)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ath this point the autentication is complated and the second activity will be started
     * @param login login response
     */
    private void startOrderDetailsPage(Login login , Calendar calendar) {


        ApplicationSingleton.getInstance().setCurrentDate(calendar.getTime());

        Utilities.showSnackBar("Success", LoginActivity.this);


        ArrayList<Boolean> urTableLayout = new ArrayList<>();
        ArrayList<Boolean> urReservation = new ArrayList<>();
        ArrayList<Boolean> urBilling = new ArrayList<>();
        ArrayList<Boolean> urPayment = new ArrayList<>();
        ArrayList<Boolean> urCustomer = new ArrayList<>();


        //Populate User Rights
        if(login != null){

            if(login.UserRights != null){

                for(User_Rights user_right : login.UserRights){

                    switch (user_right.getRights_Caption()){

                        case Constants.USER_RIGHTS_TABLELAYOUT :

                            if(user_right.Rights_Text != null)
                                for(int i = 0 ; i < user_right.Rights_Text.length() ; i ++){

                                    String value = String.valueOf(user_right.Rights_Text.charAt(i));

                                    if(value.equalsIgnoreCase("1"))
                                        urTableLayout.add(true);
                                    else
                                        urTableLayout.add(false);

                                }
                            break;
                        case Constants.USER_RIGHTS_RESERVATION :

                            if(user_right.Rights_Text != null)
                                for(int i = 0 ; i < user_right.Rights_Text.length() ; i ++){

                                    String value = String.valueOf(user_right.Rights_Text.charAt(i));

                                    if(value.equalsIgnoreCase("1"))
                                        urReservation.add(true);
                                    else
                                        urReservation.add(false);

                                }
                            break;
                        case Constants.USER_RIGHTS_BILLING :

                            if(user_right.Rights_Text != null)
                                for(int i = 0 ; i < user_right.Rights_Text.length() ; i ++){

                                    String value = String.valueOf(user_right.Rights_Text.charAt(i));

                                    if(value.equalsIgnoreCase("1"))
                                        urBilling.add(true);
                                    else
                                        urBilling.add(false);

                                }
                            break;
                        case Constants.USER_RIGHTS_PAYMENT :

                            if(user_right.Rights_Text != null)
                                for(int i = 0 ; i < user_right.Rights_Text.length() ; i ++){

                                    String value = String.valueOf(user_right.Rights_Text.charAt(i));

                                    if(value.equalsIgnoreCase("1"))
                                        urPayment.add(true);
                                    else
                                        urPayment.add(false);

                                }
                            break;
                        case Constants.USER_RIGHTS_CUSTOMER :

                            if(user_right.Rights_Text != null)
                                for(int i = 0 ; i < user_right.Rights_Text.length() ; i ++){

                                    String value = String.valueOf(user_right.Rights_Text.charAt(i));

                                    if(value.equalsIgnoreCase("1"))
                                        urCustomer.add(true);
                                    else
                                        urCustomer.add(false);

                                }
                            break;

                        default:
                            break;

                    }

                }

            }


        }


        try {
            if(urTableLayout.size() > 0){

                tableAddorSave = urTableLayout.get(0);
                tableEdit = urTableLayout.get(1);
                tableDelete = urTableLayout.get(2);
                tableView = urTableLayout.get(3);
            }

            if(urReservation.size() > 0){

                resvAddorSave = urReservation.get(0);
                resvEdit = urReservation.get(1);
                resvDelete = urReservation.get(2);
                resvView = urReservation.get(3);
            }

            if(urBilling.size() > 0){

                billAddorSave = urBilling.get(0);
                billEdit = urBilling.get(1);
                billDelete = urBilling.get(2);
                billView = urBilling.get(3);
            }

            if(urPayment.size() > 0){

                paymentAddorSave = urPayment.get(0);
                paymentEdit = urPayment.get(1);
                paymentDelete = urPayment.get(2);
                paymentView = urPayment.get(3);
            }

            if(urCustomer.size() > 0){

                custAddorSave = urCustomer.get(0);
                custEdit = urCustomer.get(1);
                custDelete = urCustomer.get(2);
                custView = urCustomer.get(3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        SharedPreferences preferences = Utilities.getSharedPreferences(LoginActivity.this);
        preferences.edit()

                .putString(Constants.SHARED_PREF_KEY_USERNAME, login.Usr_Name)
                .putString(Constants.SHARED_PREF_KEY_PASSWORD, edtTxtPassword.getText().toString())
                .putString(Constants.SHARED_PREF_KEY_ID , login.SlaesId)
                .putBoolean(Constants.SHARED_PREF_KEY_AUTHENTICATED, true)

                .putString(Constants.SHARED_PREF_KEY_CURRENCY_FORMAT, login.currencyDF)

                .putBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED, login.istax1_Enabled.equalsIgnoreCase("True"))
                .putString(Constants.SHARED_PREF_KEY_TAX_1_CAPTION , login.tax1_Caption)
                .putString(Constants.SHARED_PREF_KEY_TAX_1 , login.tax1_TaxRate)

                .putBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED, login.istax2_Enabled.equalsIgnoreCase("True"))
                .putString(Constants.SHARED_PREF_KEY_TAX_2_CAPTION , login.tax2_Caption)
                .putString(Constants.SHARED_PREF_KEY_TAX_2, login.tax2_TaxRate)

                .putBoolean(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_ENABLED , login.isService_Charge_Enabled.equalsIgnoreCase("True"))
                .putString(Constants.SHARED_PREF_KEY_SERVICE_CHARGE_RATE, login.service_Charge_Rate)

                .putInt(Constants.SHARED_PREF_KEY_SHIFT_ID, login.shift_id)
                .putInt(Constants.SHARED_PREF_KEY_SHIFT_NO , login.shift_no)
                .putString(Constants.SHARED_PREF_KEY_SHIFT_DATE, login.shift_date.toString())
                .putString(Constants.SHARED_PREF_KEY_SHIFT_TIME , login.shift_time.toString())
                .putString(Constants.SHARED_PREF_COUNTER , login.Counter)
                .putBoolean(Constants.SHARED_PREF_ISADMIN, login.user_type)

                .putBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_ADDORSAVE, tableAddorSave)
                .putBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_EDIT, tableEdit)
                .putBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_DELETE, tableDelete)
                .putBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_VIEW, tableView)

                .putBoolean(Constants.SHARED_PRE_UR_RESERVATION_ADDORSAVE, resvAddorSave)
                .putBoolean(Constants.SHARED_PRE_UR_RESERVATION_EDIT, resvEdit)
                .putBoolean(Constants.SHARED_PRE_UR_RESERVATION_DELETE, resvDelete)
                .putBoolean(Constants.SHARED_PRE_UR_RESERVATION_VIEW, resvView)

                .putBoolean(Constants.SHARED_PRE_UR_BILLING_ADDORSAVE, billAddorSave)
                .putBoolean(Constants.SHARED_PRE_UR_BILLING_EDIT, billEdit)
                .putBoolean(Constants.SHARED_PRE_UR_BILLING_DELETE, billDelete)
                .putBoolean(Constants.SHARED_PRE_UR_BILLING_VIEW, billView)

                .putBoolean(Constants.SHARED_PRE_UR_PAYMENT_ADDORSAVE, paymentAddorSave)
                .putBoolean(Constants.SHARED_PRE_UR_PAYMENT_EDIT, paymentEdit)
                .putBoolean(Constants.SHARED_PRE_UR_PAYMENT_DELETE, paymentDelete)
                .putBoolean(Constants.SHARED_PRE_UR_PAYMENT_VIEW, paymentView)

                .putBoolean(Constants.UR_CUSTOMER_ADDORSAVE, custAddorSave)
                .putBoolean(Constants.UR_CUSTOMER_EDIT, custEdit)
                .putBoolean(Constants.UR_CUSTOMER_DELETE, custDelete)
                .putBoolean(Constants.UR_CUSTOMER_VIEW, custView)

                .apply();

        ApplicationSingleton.getInstance().setCounterNumber(preferences.getString(Constants.COUNTER_NAME , ""));
        Intent intent = new Intent(this , OrderDetails.class);
        startActivity(intent);
        finish();
    }

    private void showLanguageSelectionDialog(){
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(LoginActivity.this,
                android.R.layout.simple_spinner_item, LANGUAGES);
        final Spinner sp = new Spinner(LoginActivity.this);
        sp.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        sp.setAdapter(adp);
        new AlertDialog.Builder(this)
                .setTitle("Select your language")
                .setPositiveButton("Ok" , null)
                .setView(sp)
                .show();
    }

    /**
     * Validate the login form
     * @param mode
     * @return
     * @throws ValidateExeption
     */
    private boolean validateLoginForm(int mode) throws ValidateExeption{


        userName = edtTxtUserName.getText().toString();
        password = edtTxtPassword.getText().toString();

        if(mode == FORM_VALIDATION_DEPENDANT) {

            //case 1
            if (userName.length() == 0 && password.length() == 0) {
                edtTxtUserName.setError("You need to enter a Username");
                edtTxtPassword.setError("You need to enter a Password");
                throw new ValidateExeption("Input username and password");
            }
            //case 2
            else if (userName.length() == 0) {
                edtTxtPassword.setError(null);
                edtTxtUserName.setError("You need to enter a Username");
                edtTxtUserName.requestFocus();
                throw new ValidateExeption("Input Username");
            }
            //case 3
            else if (password.length() == 0) {
                edtTxtUserName.setError(null);
                edtTxtPassword.setError("You need to enter a Password");
                edtTxtPassword.requestFocus();
                throw new ValidateExeption("Input Password");
            }
            //case 4
            else {
                edtTxtPassword.setError(null);
                edtTxtUserName.setError(null);
                return true;
            }
        }
        else if(mode == FORM_VALIDATION_INDEPENDANT){

            if (userName.length() == 0) {
                edtTxtPassword.setError(null);
                edtTxtUserName.setError("You need to enter a Username");
            }
            else {
                edtTxtUserName.setError(null);
            }

            return true;
        }

        return false;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        switch (v.getId()){
            case R.id.EditTextUserName:
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    try {
                        validateLoginForm(FORM_VALIDATION_INDEPENDANT);
                    } catch (ValidateExeption validateExeption) {
                        validateExeption.printStackTrace();
                    }
                }
                break;
            case R.id.EditTextPassword:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                    return false;
                }
                break;

        }
        return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == 32 && resultCode == RESULT_OK) {
            edtTxtUserName.setText("");
            edtTxtPassword.setText("");

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        edtTxtUserName.setText("");
        edtTxtPassword.setText("");
    }
}
