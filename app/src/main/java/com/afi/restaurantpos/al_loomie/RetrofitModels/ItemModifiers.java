package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by AFI on 10/5/2015.
 * <p>
 *     POJO class for ItemModifiers
 * </p>
 */
public class ItemModifiers {

    public String code;
    public String name;
    public String rate;

}
