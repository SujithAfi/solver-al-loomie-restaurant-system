package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.util.List;


public class BillContainer {
    public String Usr_id;
    public String Cm_Counter;
    public String Cm_No;
    public String Sman_Cd;
    public String Cus_Cd;
    public String Cus_Name;
    public String Cm_TotAmt;
    public String Cm_FocAmt;
    public String Cm_DiscPer;
    public String Cm_DiscAmt;
    public String Cm_Tax1_Per;
    public String Cm_Tax1_Amt;
    public String Cm_Tax2_Per;
    public String Cm_Tax2_Amt;
    public String Service_Per;
    public String Service_Amt;
    public String Cm_NetAmt;
    public String Cm_Tips_Amt;
    public String Sft_No;
    public String Sft_Dt;
    public String Cm_BC_Settle_Amt;
    public String Cm_Paid_Amt;
    public String Cm_Cancel_Reason;
    public String Cm_Cancelled;
    public String Cm_Cus_Phone;
    public String Cm_Type;
    public String Tab_Cd;
    public String Cm_Covers;
    public String Srvr_Cd;
    public String Cm_Collected_Counter;
    public String Cm_Collected_User;
    public String date;
    public String time;
    public List<BillItem> details;
}
