package com.afi.restaurantpos.al_loomie.Fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.McItemsAdater;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;


public class ItemDetailsMuticorseFragment extends DialogFragment {

    private static final String ITEM_KEY = "item";
    public boolean clickLock = false;
    List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
    private String itemId;
    private String barCode;
    private View mLayout;
    private Toolbar toolbar;
    private TextView txtRate;
    private EditText edtTxtQuantity;
    private Button btnPlus;
    private Button btnMinus;
    private ItemDdetailsListCallBack callBack;
    private RecyclerView recyclerViewMuticorseItems;
    private Handler uiHandler = new Handler();
    private int selectedMainItemPos = 0;
    private ArrayList<Item> mainItemList;
    private ArrayList<Item> subitems;
    private String rate = "0";
    private TextView TxtGtotal;
    private String itemName;
    private TextView tvAllergicInfo;
    private LinearLayout llAllergicInfo;
    private FloatingActionButton fabCustomerDeatailsDown;
    private TextView tvToolbarTitle;
    private ImageView ivAddMore;
    private McItemsAdater adapter;
    private int selectedIngredientsPos = 0;
    private ImageView ivSave;
    private Item item;
    private boolean savingItems = false;
    private ProgressBar pbItemDetails;
    private boolean itemChangeFlag = false;

    private TextWatcher textWatcher = new TextWatcher() {
        private CharSequence prevSequence;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            this.prevSequence = s;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {

                /*if(edtTxtQuantity.getText().toString().length() == 0 || Integer.parseInt(edtTxtQuantity.getText().toString()) == 0 )
                    edtTxtQuantity.setText("1");*/
            if (edtTxtQuantity.getText().toString() != null && !edtTxtQuantity.getText().toString().matches(""))
                if (edtTxtQuantity.getText().toString().length() != 0 && Integer.parseInt(edtTxtQuantity.getText().toString()) != 0)
                    calculateCostForItem(edtTxtQuantity.getText().toString().length() > 0 ? Double.parseDouble(edtTxtQuantity.getText().toString()) : 1);

            quantityChanged();

        }
    };



    public ItemDetailsMuticorseFragment() {
    }

    public static ItemDetailsMuticorseFragment newInstance(Item item) {
        ItemDetailsMuticorseFragment fragment = new ItemDetailsMuticorseFragment();
        Bundle args = new Bundle();
        args.putSerializable(ITEM_KEY , item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {

            item = (Item) getArguments().getSerializable(ITEM_KEY);

            itemId = item.ItemCd;
            barCode = item.BarCd;
            rate = item.Rate;
            itemName = item.Item_Name;

        }

        savingItems = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.fragment_multicorse_item_details, container, false);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        tvToolbarTitle = (TextView) mLayout.findViewById(R.id.toolbar_title);
        fabCustomerDeatailsDown = (FloatingActionButton) mLayout.findViewById(R.id.fabCustomerDeatailsDown);
        fabCustomerDeatailsDown.setVisibility(View.INVISIBLE);
        recyclerViewMuticorseItems = (RecyclerView) mLayout.findViewById(R.id.recyclerViewMuticorseItems);
        fabCustomerDeatailsDown = (FloatingActionButton) mLayout.findViewById(R.id.fabCustomerDeatailsDown);
        txtRate = (TextView) mLayout.findViewById(R.id.tv_rate);
        edtTxtQuantity = (EditText) mLayout.findViewById(R.id.edtTxtQuantity);
        TxtGtotal = (TextView) mLayout.findViewById(R.id.tv_total);
        btnPlus = (Button) mLayout.findViewById(R.id.btnPlus);
        btnMinus = (Button) mLayout.findViewById(R.id.btnMinus);
        ivAddMore = (ImageView) mLayout.findViewById(R.id.iv_addMore);
        ivSave = (ImageView) mLayout.findViewById(R.id.iv_save);
        tvAllergicInfo = (TextView) mLayout.findViewById(R.id.tv_allergic_info);
        llAllergicInfo = (LinearLayout) mLayout.findViewById(R.id.ll_allergicinfo);
        pbItemDetails = (ProgressBar) mLayout.findViewById(R.id.pb_items_details);
        return mLayout;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);
        recyclerViewMuticorseItems.setLayoutManager(new LinearLayoutManager(getContext()));
        if(itemName != null)
            tvToolbarTitle.setText(itemName);
        toolbar.setNavigationIcon(R.drawable.back_butt);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ItemDetailsMuticorseFragment.this.dismiss();
            }
        });

        edtTxtQuantity.addTextChangedListener(textWatcher);

        if(item.getRemarks() != null && !item.getRemarks().matches("")) {
            llAllergicInfo.setVisibility(View.VISIBLE);
            tvAllergicInfo.setText(item.getRemarks());
        }
        initButtons();
        loadMuticorseMenu();
        if(Double.parseDouble(rate) > 0) {
            txtRate.setText(Utilities.getDefaultCurrencyFormat(rate  , getActivity()));
            TxtGtotal.setText(Utilities.getDefaultCurrencyFormat(rate  , getActivity()));
        }

        ViewCompat.animate(fabCustomerDeatailsDown)
                .scaleX(0)
                .scaleY(0)
                .setDuration(100)
                .start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabCustomerDeatailsDown.setVisibility(View.VISIBLE);
                fabCustomerDeatailsDown.clearAnimation();
                ViewCompat.animate(fabCustomerDeatailsDown)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator())
                        .start();

            }
        }, 1000);

        fabCustomerDeatailsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(getActivity() != null) {
                        ((SelectItemsActivity) getActivity()).setUpperFabAnimation(true, 1);
                        ((SelectItemsActivity) getActivity()).animateBillFragment();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ivAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (edtTxtQuantity.getText().toString() == null || edtTxtQuantity.getText().toString().matches("")) {
                        Utilities.showToast("Quantity can not be empty.", getContext());
                        edtTxtQuantity.setError("Enter quantity.");
                    }
                    else if(Integer.parseInt(edtTxtQuantity.getText().toString()) == 0){

                        Utilities.showToast("Item quantity not valid.", getContext());
                        edtTxtQuantity.setError("Enter quantity.");
                    }
                    else {
                        if(mainItemList != null){
                        if (callBack != null) {


                            if (mainItemList.size() > 0) {


                                for (Item item1 : mainItemList) {

                                    if (item1.itemCount != 0) {

                                        SelectedItemDetails details = new SelectedItemDetails();
                                        details.itemName = item1.Item_Name;
                                        details.barcode = item1.ItemCd;
                                        details.itemCode = item1.ItemCd;
                                        details.Unit_Fraction = "1";
                                        details.Itm_UCost = item1.Rate;
                                        details.itemUnitCost = Double.parseDouble(item1.Rate);
                                        details.itemExtraRate = 0;
                                        details.itemTotalCost = 0;

                                        try {
                                            if (item1.itemCount == -1)
                                                details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                            else
                                                details.itemQuantity = Double.parseDouble(String.valueOf(item1.itemCount));
                                        } catch (Exception e) {
                                            details.itemQuantity = 1;
                                        }
                                        details.modify = new ArrayList<>();
                                        details.Cm_Covers = "1";
                                        details.isSubitem = true;
                                        details.isMultiC = true;
                                        details.modificationRemarks = item1.modificationRemarks;
                                        details.Is_TempItem = item1.Is_TempItem;
                                        selectedItemDetailses.add(details);
                                    }
                                }

                                SelectedItemDetails details = new SelectedItemDetails();
                                details.itemName = itemName;
                                details.barcode = barCode;
                                details.itemCode = itemId;
                                details.Unit_Fraction = "1";
                                details.Itm_UCost = rate;
                                details.itemUnitCost = Double.parseDouble(rate);
                                details.itemExtraRate = 0;
                                details.itemTotalCost = Double.parseDouble(TxtGtotal.getText().toString());

                                try {
                                    details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                } catch (Exception e) {
                                    details.itemQuantity = 1;
                                }
                                details.modify = new ArrayList<>();
                                details.Cm_Covers = "1";
                                details.isSubitem = false;
                                details.isMultiC = true;
                                details.modificationRemarks = "";

                                selectedItemDetailses.add(0, details);


                            }

                            callBack.onItemSelected(selectedItemDetailses, false, false);
                            Snackbar
                                    .make(mLayout, getResources().getString(R.string.item_saved), Snackbar.LENGTH_LONG)
                                    .show();

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    dismiss();
                                }
                            }, 500);


                        }
                    }
                        else{
                            Utilities.showToast("Item should not be empty.", getContext());
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }


            }
        });


        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(!savingItems){

                        if (edtTxtQuantity.getText().toString() == null || edtTxtQuantity.getText().toString().matches("")) {
                            Utilities.showToast("Quantity can not be empty.", getContext());
                            edtTxtQuantity.setError("Enter quantity.");
                        }
                        else if(Integer.parseInt(edtTxtQuantity.getText().toString()) == 0){

                            Utilities.showToast("Item quantity not valid.", getContext());
                            edtTxtQuantity.setError("Enter quantity.");
                        }
                        else {
                            if(mainItemList != null) {
                                if (callBack != null) {


                                    if (mainItemList.size() > 0) {

                                        savingItems = true;
                                        for (Item item1 : mainItemList) {

                                            if (item1.itemCount != 0) {

                                                SelectedItemDetails details = new SelectedItemDetails();
                                                details.itemName = item1.Item_Name;
                                                details.barcode = item1.ItemCd;
                                                details.itemCode = item1.ItemCd;
                                                details.Unit_Fraction = "1";
                                                details.Itm_UCost = item1.Rate;
                                                details.itemUnitCost = Double.parseDouble(item1.Rate);
                                                details.itemExtraRate = 0;
                                                details.itemTotalCost = 0;

                                                try {
                                                    if (item1.itemCount == -1)
                                                        details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                                    else
                                                        details.itemQuantity = Double.parseDouble(String.valueOf(item1.itemCount));
                                                } catch (Exception e) {
                                                    details.itemQuantity = 1;
                                                }
                                                details.modify = new ArrayList<>();
                                                details.Cm_Covers = "1";
                                                details.isSubitem = true;
                                                details.isMultiC = true;
                                                details.modificationRemarks = item1.modificationRemarks;
                                                details.Is_TempItem = item1.Is_TempItem;
                                                selectedItemDetailses.add(details);
                                            }


                                        }

                                        SelectedItemDetails details = new SelectedItemDetails();
                                        details.itemName = itemName;
                                        details.barcode = barCode;
                                        details.itemCode = itemId;
                                        details.Unit_Fraction = "1";
                                        details.Itm_UCost = rate;
                                        details.itemUnitCost = Double.parseDouble(rate);
                                        details.itemExtraRate = 0;
                                        details.itemTotalCost = Double.parseDouble(TxtGtotal.getText().toString());

                                        try {
                                            details.itemQuantity = Double.parseDouble(edtTxtQuantity.getText().toString());
                                        } catch (Exception e) {
                                            details.itemQuantity = 1;
                                        }
                                        details.modify = new ArrayList<>();
                                        details.Cm_Covers = "1";
                                        details.isSubitem = false;
                                        details.isMultiC = true;
                                        details.modificationRemarks = "";

                                        selectedItemDetailses.add(0, details);


                                    }


                                    callBack.onItemSelected(selectedItemDetailses, false, true);
                                    savingItems = false;
                                    dismiss();


                                }
                            }
                            else{
                                Utilities.showToast("Item should not be empty.", getContext());
                            }
                    }

                }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadMuticorseMenu() {

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {
            pbItemDetails.setVisibility(View.VISIBLE);
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {

                        final Response<List<Item>> response = Utilities.getRetrofitWebService(getContext()).getMuticourseItems(itemId).execute();
                        if (response != null && response.body() != null) {
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {

                                    pbItemDetails.setVisibility(View.GONE);
                                    mainItemList = (ArrayList<Item>) response.body();
                                    if (mainItemList.size() > 0) {
                                        selectedMainItemPos = 0;
                                        onMulticorseMenuloaded(mainItemList);
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                pbItemDetails.setVisibility(View.GONE);
                            }
                        });

                    }

                }
            }).start();
        }
    }

    private void quantityChanged(){

                  if (itemChangeFlag){

                      loadMuticorseMenu();

                itemChangeFlag = false;

        }


    }

    private void onMulticorseMenuloaded(final List<Item> itemList) {


         adapter = new McItemsAdater(itemList, new McItemsAdater.ItemsClickListner() {
            @Override
            public void onItemClick(String id, String barcode, boolean isMutiCorse) {

            }
        }, null);

        adapter.setOnItemPositionClickListner(new McItemsAdater.ItemPositionClickListner() {
            @Override
            public void onItemClick(int position) {

                selectedMainItemPos = position;
                onMulticorseItemSelected(itemList.get(position).ItemCd , itemList.get(position).Item_Name , itemList.get(position).Remarks , position);

            }
        });

        adapter.setOnItemPositionClickListner1(new McItemsAdater.ItemPositionClickListner1() {
            @Override
            public void onItemClick(int position) {
                selectedIngredientsPos = position;
                onMulticorseItemIngredients(itemList.get(position).ItemCd , itemList.get(position));

            }
        });


        recyclerViewMuticorseItems.setAdapter(adapter);



    }

    private void onMulticorseItemSelected(final String itemCode , final String itemName , final String remarks , final int position) {

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {
        pbItemDetails.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    final Response<List<Item>> response = Utilities.getRetrofitWebService(getContext()).getMcMappedItems(itemCode).execute();
                    if (response != null && response.body() != null) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                pbItemDetails.setVisibility(View.GONE);

                                if (response != null)
                                    subitems = (ArrayList<Item>) response.body();
                                else {
                                    subitems = null;
                                    Toast.makeText(getActivity(), "Empty items", Toast.LENGTH_SHORT).show();
                                }

                                gotoItemsChangePage(subitems, itemName, position, remarks);


                            }
                        });
                    } else {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                pbItemDetails.setVisibility(View.GONE);
                                subitems = null;
                                gotoItemsChangePage(subitems, itemName, position, remarks);
                            }
                        });
                    }


                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                    pbItemDetails.setVisibility(View.GONE);
                        }
                    });
                    e.printStackTrace();
                }

            }
        }).start();
    }

    }

    private void gotoItemsChangePage(ArrayList<Item> subitems , String itemName ,  int position ,  String remarks) {

        try {
            android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            MuticorseItemChangeFragment itemDetailsMuticorseFragment = MuticorseItemChangeFragment.newInstance(subitems , itemName ,mainItemList.get(position).itemCount == -1 ? edtTxtQuantity.getText().toString () : String.valueOf(mainItemList.get(position).itemCount), mainItemList , remarks , selectedMainItemPos , edtTxtQuantity.getText().toString ());
            itemDetailsMuticorseFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyMaterialTheme_AppBarOverlay);
            itemDetailsMuticorseFragment.setCallBack(new MuticorseItemChangeFragment.ItemChangeCallBack() {

                @Override
                public void onItemSelected(ArrayList<Item> changeItems, boolean isSuccess) {

                        mainItemList = changeItems;
                        onMulticorseMenuloaded(mainItemList);
                        itemChangeFlag = true;

                }

                @Override
                public void onCanceled() {

                }
            });
            itemDetailsMuticorseFragment.show(fragmentManager, "ChangeItem");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onMulticorseItemIngredients(final String itemCode , final Item  item) {


        if(item.getIngredients() != null || item.getIngredients().size() > 0) {
            android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            MuticorseItemIngredientsFragment itemDetailsMuticorseFragment = MuticorseItemIngredientsFragment.newInstance(itemCode, item, item.itemCount == -1 ? Integer.parseInt(edtTxtQuantity.getText().toString()) : item.itemCount);
            itemDetailsMuticorseFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyMaterialTheme_AppBarOverlay);
            itemDetailsMuticorseFragment.setCallBack(new com.afi.restaurantpos.al_loomie.Fragments.MuticorseItemIngredientsFragment.ItemDdetailsListCallBack() {
                @Override
                public void onItemSelected(Item selectedItemDetailses, boolean isSuccess) {

                    if (selectedIngredientsPos != 0) {

                        mainItemList.get(selectedIngredientsPos).setModificationRemarks(selectedItemDetailses.modificationRemarks);
                        itemChangeFlag = true;

                    }

                }

                @Override
                public void onCanceled() {

                }
            });
            itemDetailsMuticorseFragment.show(fragmentManager, "ingredients");
        }
        else
            Toast.makeText(getActivity() , "No ingredients" , Toast.LENGTH_LONG).show();

    }



    private void initButtons() {

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTxtQuantity.setText(
                        edtTxtQuantity.getText().toString().length() > 0 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) + 1) : ""
                );
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtTxtQuantity.setText(
                        edtTxtQuantity.getText().toString().length() > 0 ?
                                (Integer.parseInt(edtTxtQuantity.getText().toString()) > 1 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) - 1)
                                        : edtTxtQuantity.getText().toString()) :
                                edtTxtQuantity.getText().toString()
                );
            }
        });

    }


    private void calculateCostForItem(double qty) {


        try {
            double total_cost = 0;
            if (rate != null) {
                total_cost = qty * Double.parseDouble(rate);
            }


            if (total_cost > 0) {
                TxtGtotal.setText(Utilities.getDefaultCurrencyFormat(String.valueOf(total_cost)  , getActivity()));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    public void setCallBack(ItemDdetailsListCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onStop() {
        super.onStop();
        ApplicationSingleton.getInstance().setLockFlag(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface ItemDdetailsListCallBack {
        void onItemSelected(List<SelectedItemDetails> selectedItemDetailses, boolean isSuccess ,boolean makeKot);

        void onCanceled();
    }

}
