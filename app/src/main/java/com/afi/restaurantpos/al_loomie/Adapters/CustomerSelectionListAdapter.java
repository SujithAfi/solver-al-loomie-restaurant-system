package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.lb.recyclerview_fast_scroller.RecyclerViewFastScroller;

/**
 * Created by afi-mac-001 on 13/06/16.
 */
public class CustomerSelectionListAdapter extends RecyclerView.Adapter<CustomerSelectionListAdapter.CustomerSelectionListAdapterVh> implements RecyclerViewFastScroller.BubbleTextGetter {

    private final Context context;
    private List<Customer> customers;

    private List<Customer> allCustomers = new ArrayList<>();

    private CustomerSelectionListner customerSelectionListner;

    public CustomerSelectionListAdapter(List<Customer> customers, CustomerSelectionListner listner , Context context) {
        this.customers = customers;
        this.customerSelectionListner = listner;
        this.allCustomers.addAll(customers);
        this.context = context;
    }


    @Override
    public CustomerSelectionListAdapterVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomerSelectionListAdapterVh(LayoutInflater.from(parent.getContext()).inflate(R.layout.child_customer_selection_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final CustomerSelectionListAdapterVh holder, final int position) {

        // Check permission to view Customers
        if(Utilities.getSharedPreferences(context).getBoolean(Constants.UR_CUSTOMER_VIEW, false))
            holder.tvView.setVisibility(View.VISIBLE);
        else
            holder.tvView.setVisibility(View.GONE);

        holder.txtCustomerName.setText(customers.get(position).getName());
        holder.txtCustomerCodeData.setText(customers.get(position).getId());
        /*if(customers.get(position).getCus_Phone() != null && !customers.get(position).getCus_Phone().matches("")) {
            holder.tvCustPhone.setVisibility(View.VISIBLE);
            holder.txtCustomerCodeData.setText(customers.get(position).getCus_Phone());
        }
        else {
            holder.tvCustPhone.setVisibility(View.GONE);
            holder.txtCustomerCodeData.setText("");
        }*/
        holder.rlRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (customerSelectionListner != null) {
                    customerSelectionListner.onItemSelected(customers.get(position));
                }
            }
        });

        holder.tvView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((OrderDetails) context).onCustomerDetails(customers.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    public void filerResults(String query) {

        if (query.isEmpty()) {
            customers.clear();
            customers.addAll(allCustomers);
        } else {
            customers.clear();
            for (Customer customer : this.allCustomers) {
                if (customer.getName().toLowerCase().contains(query.toLowerCase()) || customer.getId().toLowerCase().contains(query.toLowerCase())) {
                    customers.add(customer);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        return Character.toString(customers.get(pos).getName().charAt(0));
    }

    public interface CustomerSelectionListner {
        void onItemSelected(Customer customer);
    }

    public static class CustomerSelectionListAdapterVh extends RecyclerView.ViewHolder {

        private final TextView tvView;
        private final RelativeLayout rlRoot;
        private final TextView tvCustPhone;
        private ImageView imgCustomerImage;
        private TextView txtCustomerName;
        private TextView txtCustomerCodeData;

        public CustomerSelectionListAdapterVh(View itemView) {
            super(itemView);

            imgCustomerImage = (ImageView) itemView.findViewById(R.id.imgCustomerImage);
            txtCustomerName = (TextView) itemView.findViewById(R.id.txtCustomerName);
            txtCustomerCodeData = (TextView) itemView.findViewById(R.id.txtCustomerCodeData);
            tvView = (TextView) itemView.findViewById(R.id.tv_viewcust);
            rlRoot = (RelativeLayout) itemView.findViewById(R.id.rl_root);
            tvCustPhone = (TextView)itemView.findViewById(R.id.txtCustomerCode);
        }
    }

}
