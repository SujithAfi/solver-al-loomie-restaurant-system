package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by afi on 10/21/2015.
 */
public class PendingBill {
    public String Cm_No;
    public String Cm_TotAmt;
    public String Cm_Counter;
    public String Cus_name;
    public String Cus_Cd;
    public String Cm_DiscAmt;
    public String Cm_NetAmt;
    public String Tab_Cd;
    public String Tab_Name;
    public String Sman_Cd;
    public String Sman_Name;

    public Boolean isCredit;
    public String acc_Code;
    public String acc_Name;
    public String room_No;
    public String checkin_Date;

    public String focAmt;
    public String Cm_FocAmt;

}
