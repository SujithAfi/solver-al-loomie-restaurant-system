package com.afi.restaurantpos.al_loomie.Models;

/**
 * Created by afi-mac-001 on 09/11/16.
 */

public class FinalAmount {

    public String focAmount;
    public String serviceAmount;
    public String tax1Amount;
    public String tax2Amount;
    public String netAmount;
}
