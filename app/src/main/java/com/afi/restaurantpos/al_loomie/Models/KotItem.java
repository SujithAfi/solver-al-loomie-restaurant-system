package com.afi.restaurantpos.al_loomie.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AFI on 10/14/2015.
 */
public class KotItem implements Serializable{
    private String Itm_Cd;
    private String KotD_Qty;
    private String KotD_Rate;
    private String KotD_Amt;
    private String KotD_UCost;
    private String KotD_Ctr;
    private String KotD_Counter;
    private String Itm_Name;
    private String Unit_Fraction;
    private String Kot_MuUnit;
    private String KotD_Remarks;
    private String Barcode;
    private String Menu_SideDish;
    private String Cmd_IsBuffet;
    private Boolean isMultic;
    private Boolean isSubitem;


    public Boolean getIsSubitem() {
        return isSubitem;
    }

    public void setIsSubitem(Boolean isSubitem) {
        this.isSubitem = isSubitem;
    }

    public Boolean getMultic() {
        return isMultic;
    }

    public void setMultic(Boolean multic) {
        isMultic = multic;
    }


    public String getItm_Cd() {
        return Itm_Cd;
    }

    public void setItm_Cd(String itm_Cd) {
        Itm_Cd = itm_Cd;
    }

    public String getKotD_Qty() {
        return KotD_Qty;
    }

    public void setKotD_Qty(String kotD_Qty) {
        KotD_Qty = kotD_Qty;
    }

    public String getKotD_Rate() {
        return KotD_Rate;
    }

    public void setKotD_Rate(String kotD_Rate) {
        KotD_Rate = kotD_Rate;
    }

    public String getKotD_Amt() {
        return KotD_Amt;
    }

    public void setKotD_Amt(String kotD_Amt) {
        KotD_Amt = kotD_Amt;
    }

    public String getKotD_UCost() {
        return KotD_UCost;
    }

    public void setKotD_UCost(String kotD_UCost) {
        KotD_UCost = kotD_UCost;
    }

    public String getKotD_Ctr() {
        return KotD_Ctr;
    }

    public void setKotD_Ctr(String kotD_Ctr) {
        KotD_Ctr = kotD_Ctr;
    }

    public String getKotD_Counter() {
        return KotD_Counter;
    }

    public void setKotD_Counter(String kotD_Counter) {
        KotD_Counter = kotD_Counter;
    }

    public String getItm_Name() {
        return Itm_Name;
    }

    public void setItm_Name(String itm_Name) {
        Itm_Name = itm_Name;
    }

    public String getUnit_Fraction() {
        return Unit_Fraction;
    }

    public void setUnit_Fraction(String unit_Fraction) {
        Unit_Fraction = unit_Fraction;
    }

    public String getKot_MuUnit() {
        return Kot_MuUnit;
    }

    public void setKot_MuUnit(String kot_MuUnit) {
        Kot_MuUnit = kot_MuUnit;
    }

    public String getKotD_Remarks() {
        return KotD_Remarks;
    }

    public void setKotD_Remarks(String kotD_Remarks) {
        KotD_Remarks = kotD_Remarks;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    public String getMenu_SideDish() {
        return Menu_SideDish;
    }

    public void setMenu_SideDish(String menu_SideDish) {
        Menu_SideDish = menu_SideDish;
    }

    public String getCmd_IsBuffet() {
        return Cmd_IsBuffet;
    }

    public void setCmd_IsBuffet(String cmd_IsBuffet) {
        Cmd_IsBuffet = cmd_IsBuffet;
    }
}