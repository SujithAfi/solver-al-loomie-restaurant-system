package com.afi.restaurantpos.al_loomie.Models;

/**
 * Created by AFI on 10/7/2015.
 */
public class Bill {

    public String itemName;
    public String itemQty;
    public String itemRate;
    public String itemAmount;
    public  boolean isMock;
}
