package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.util.List;

/**
 * Created by AFI on 10/12/2015.
 * <p>
 *     POJO class for ItemRecipies
 * </p>
 */
public class ItemRecipeDetails {

    public String Name;
    public String Quantity;
    public String Cost;
    public List<Recipe> Recipe;
    public String Details;

}
