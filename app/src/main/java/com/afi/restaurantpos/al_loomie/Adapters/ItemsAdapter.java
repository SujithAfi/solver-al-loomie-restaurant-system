package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.ShowPdfActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;


/**
 * Created by AFI on 10/2/2015.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsAdapterViewHolder> {
    SharedPreferences preferences;
    private List<Item> items;
    private ItemsClickListner itemsClickListner;
    private Fragment fragment;
    private ItemPositionClickListner itemPositionClickListner;
    public ItemsAdapter(List<Item> items , ItemsClickListner itemsClickListner , Fragment fragment) {
        this.items = items;
        this.itemsClickListner = itemsClickListner;
        this.fragment = fragment;
    }

    @Override
    public ItemsAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        preferences = Utilities.getDefaultSharedPref(viewGroup.getContext());
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.multicaurse_item, viewGroup, false);
        ItemsAdapterViewHolder itemsAdapterViewHolder = new ItemsAdapterViewHolder(v);
        return itemsAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemsAdapterViewHolder holder, final int position) {
        try {


            holder.itemName.setText(items.get(position).Item_Name);
            holder.itemView.setOnClickListener(new ItemCLickListner(position));
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    Bundle b = new Bundle();
                    b.putString("itemName" , items.get(position).Item_Name);
                    b.putString("itemCode" , items.get(position).ItemCd);
                    Intent i = new Intent(holder.itemView.getContext() , ShowPdfActivity.class);
                    i.putExtra("bundle" , b);
                    holder.itemView.getContext().startActivity(i);
                    return false;
                }
            });


           /* holder.txtItemCost.setText(Utilities.getDefaultCurrencyFormat(this.items.get(position).Rate, holder.itemImage.getContext()));
            holder.textView.setSelected(true);
            if (preferences.getBoolean("key_show_item_image", true)) {

                Picasso.with(holder.itemImage.getContext())
                        .load(
                                Utilities.getImageUrl(items.get(position).ItemCd, items.get(position).BarCd)
                        )
                        .placeholder(R.drawable.logo)
                        .fit()
                        .centerCrop()
                        .into(holder.itemImage);
            } else {
                holder.itemImage.setImageResource(R.drawable.logo);
            }*/
        } catch (Exception e) {
            fragment.getContext().sendBroadcast(new Intent("com.afi.infotech.error"));
        }

    }

    @Override
    public int getItemCount() {
        if(items == null)
            return 0;
        else
            return items.size();
    }

    public interface ItemsClickListner {
        void onItemClick(Item item);
    }

    public static class ItemsAdapterViewHolder extends RecyclerView.ViewHolder {
        private final TextView itemName;
        private final TextView itemSDisc;
//        private final RelativeLayout itemContainer;

       /* private TextView textView;
        private ImageView itemImage;
        private TextView txtItemCost;
        private CardView cardView;*/

        public ItemsAdapterViewHolder(View itemView) {
            super(itemView);
            /*textView = (TextView) itemView.findViewById(R.id.txtName);
            itemImage = (ImageView) itemView.findViewById(R.id.imgItem);
            txtItemCost = (TextView) itemView.findViewById(R.id.txtItemCost);
            cardView = (CardView) itemView.findViewById(R.id.card_view);*/

//            itemContainer = (RelativeLayout) itemView.findViewById(R.id.rl_item);
            itemName = (TextView) itemView.findViewById(R.id.tv_item_name);
            itemSDisc = (TextView) itemView.findViewById(R.id.tv_item_sdesc);
        }
    }

    public void setOnItemPositionClickListner(ItemPositionClickListner onItemPositionClickListner) {
        this.itemPositionClickListner = onItemPositionClickListner;
    }
    public interface ItemPositionClickListner {

        void onItemClick(int position);

    }

    private class ItemCLickListner extends RecyclerViewCallBack{

        public ItemCLickListner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {
            if(itemsClickListner != null)
                itemsClickListner.onItemClick(items.get(position));

            if (itemPositionClickListner != null) {
                itemPositionClickListner.onItemClick(position);
            }
        }
    }

}
